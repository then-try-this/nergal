// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import { start, greet, Game } from "nergal";

var loadinglist = [
    // remember to add text change in nergal_generate_graphs
    // remember to add to update_averages
    ["network", "/networks/badger_matrix.txt"],
    ["network", "/networks/dolphin_matrix.txt"],
    ["network", "/networks/elephant_matrix.txt"],
    
    ["conversations", "/networks/conversation-clean.graphml"],
    
    ["sound", "/sounds/chatter-1.mp3"],
    ["sound", "/sounds/chatter-2.mp3"],
    ["sound", "/sounds/chatter-3.mp3"],
    ["sound", "/sounds/lake-1.mp3"],
    ["sound", "/sounds/lake-2.mp3"],
    ["sound", "/sounds/lake-3.mp3"],
    ["sound", "/sounds/lake-4.mp3"],
    ["sound", "/sounds/steps-normal-fast-player.mp3"],
    ["sound", "/sounds/steps-normal-fast.mp3"],
    ["sound", "/sounds/test.mp3"],
    ["sound", "/sounds/cloud.mp3"],
    ["sound", "/sounds/cloud-2.mp3"],
    ["sound", "/sounds/piano-low.mp3"],
    ["sound", "/sounds/piano.mp3"],
    ["sound", "/sounds/piano-1.mp3"],
    ["sound", "/sounds/piano-2.mp3"],
    ["sound", "/sounds/piano-3.mp3"],
    ["sound", "/sounds/piano-4.mp3"],
    ["sound", "/sounds/piano-5.mp3"],
    ["sound", "/sounds/piano-6.mp3"],
    ["sound", "/sounds/piano-7.mp3"],
    ["sound", "/sounds/rainbow-1.mp3"],
    ["sound", "/sounds/rainbow-2.mp3"],
    ["sound", "/sounds/hit-plant-1.mp3"],
    ["sound", "/sounds/hit-plant-2.mp3"],
    ["sound", "/sounds/hit-plant-3.mp3"],
    ["sound", "/sounds/hit-plant-4.mp3"],
    ["sound", "/sounds/hit-rock-1.mp3"],
    ["sound", "/sounds/hit-rock-2.mp3"],
    ["sound", "/sounds/hit-rock-3.mp3"],
    ["sound", "/sounds/hit-rock-4.mp3"],
    ["sound", "/sounds/notify.mp3"],
    ["sound", "/sounds/notify-2.mp3"],
    ["sound", "/sounds/notify-3.mp3"],
    ["sound", "/sounds/notify-4.mp3"],
    ["sound", "/sounds/notify-5.mp3"],
    ["sound", "/sounds/notify-6.mp3"],
    ["sound", "/sounds/notify-damage.mp3"],
    ["sound", "/sounds/sneeze-1.mp3"],
    ["sound", "/sounds/sneeze-2.mp3"],
    ["sound", "/sounds/sneeze-3.mp3"],
    ["sound", "/sounds/sneeze-4.mp3"],
    ["sound", "/sounds/sick-1.mp3"],
    ["sound", "/sounds/sick-2.mp3"],
    ["sound", "/sounds/sick-3.mp3"],
    ["sound", "/sounds/sick-4.mp3"],
    ["sound", "/sounds/bump-1.mp3"],
    ["sound", "/sounds/bump-2.mp3"],
    ["sound", "/sounds/bump-3.mp3"],
    ["sound", "/sounds/bump-4.mp3"],
    ["sound", "/sounds/bump-5.mp3"],
    ["sound", "/sounds/bump-6.mp3"],
    ["sound", "/sounds/nomnom-1.mp3"],
    ["sound", "/sounds/nomnom-2.mp3"],
//    ["sound", "/sounds/snore-1.mp3"],
//    ["sound", "/sounds/snore-2.mp3"],
    ["sound", "/sounds/cloud-2.mp3"],
	["sound", "/sounds/leaves-large.mp3"],
    ["sound", "/sounds/leaves-medium.mp3"],
	["sound", "/sounds/leaves-small.mp3"],
	["sound", "/sounds/leaves-vlarge.mp3"],
    ["sound", "/sounds/steps-splashy-3.mp3"],
    ["sound", "/sounds/tree-1.mp3"],
	["sound", "/sounds/tree-2.mp3"],
    ["sound", "/sounds/tree-3.mp3"],

    
	["image", "sprites/plants/plant_apple.png", "sprites/plants/plant_apple_night.png", ""],
	["image", "sprites/plants/plant_banyan.png", "sprites/plants/plant_banyan_night.png", ""],
	["image", "sprites/plants/plant_baobab.png", "sprites/plants/plant_baobab_night.png", ""],
	["image", "sprites/plants/plant_begonia.png", "sprites/plants/plant_begonia_night.png", ""],
	["image", "sprites/plants/plant_bulbinetortaa.png", "sprites/plants/plant_bulbinetortaa_night.png", ""],
	["image", "sprites/plants/plant_bulbinetortab.png", "sprites/plants/plant_bulbinetortab_night.png", ""],
	["image", "sprites/plants/plant_bulbinetortac.png", "sprites/plants/plant_bulbinetortac_night.png", ""],
	["image", "sprites/plants/plant_bulbinetortad.png", "sprites/plants/plant_bulbinetortad_night.png", ""],
	["image", "sprites/plants/plant_calathea.png", "sprites/plants/plant_calathea_night.png", ""],
	["image", "sprites/plants/plant_dollseye.png", "sprites/plants/plant_dollseye_night.png", ""],
	["image", "sprites/plants/plant_dragonblood.png", "sprites/plants/plant_dragonblood_night.png", ""],
	["image", "sprites/plants/plant_drosera.png", "sprites/plants/plant_drosera_night.png", ""],
	["image", "sprites/plants/plant_fishbone.png", "sprites/plants/plant_fishbone_night.png", ""],
	["image", "sprites/plants/plant_jasmine.png", "sprites/plants/plant_jasmine_night.png", ""],
	["image", "sprites/plants/plant_lecanopteris.png", "sprites/plants/plant_lecanopteris_night.png", ""],
	["image", "sprites/plants/plant_monkeymask.png", "sprites/plants/plant_monkeymask_night.png", ""],
	["image", "sprites/plants/plant_ponytailpalm.png", "sprites/plants/plant_ponytailpalm_night.png", ""],
	["image", "sprites/plants/plant_stefonia.png", "sprites/plants/plant_stefonia_night.png", ""],
	["image", "sprites/plants/plant_strelizia.png", "sprites/plants/plant_strelizia_night.png",  ""],
	["image", "sprites/plants/plant_tillandsia.png", "sprites/plants/plant_tillandsia_night.png", ""],

	["image", "sprites/rocks/rock1.png", "", "A small green rock"],
	["image", "sprites/rocks/rock2.png", "", "A small purple rock"],
	["image", "sprites/rocks/rock3.png", "", "A small orange rock"],
	["image", "sprites/rocks/rock4.png", "", "A big yellow rock"],
	["image", "sprites/rocks/rock5.png", "", "A big purple rock"],
	["image", "sprites/rocks/rock6.png", "", "A large orange rock"],

	["image", "sprites/rainbows/rainbow1trans.png", "", "A yellow, purple and green rainbow"],
	["image", "sprites/rainbows/rainbow2trans.png", "", "A green, red and orange rainbow"],
	["image", "sprites/rainbows/rainbow3trans.png", "", "A red, orange and green rainbow"],
	["image", "sprites/rainbows/rainbow4trans.png", "", "A pink, green and purple rainbow"],

    ["image", "sprites/clouds/cloud-1.png", "", "A small, fluffy cloud"],
    ["image", "sprites/clouds/cloud-2.png", "", "A large cloud"],
    ["image", "sprites/clouds/cloud-3.png", "", "A small cloud"],
    ["image", "sprites/clouds/cloud-4.png", "", "A medium sized cloud"],
    ["image", "sprites/clouds/cloud-5.png", "", "A medium sized cloud"],
    ["image", "sprites/clouds/cloud-6.png", "", "A medium sized cloud"],

    ["image", "sprites/gifts/banana.png", "", ""],
    ["image", "sprites/gifts/beetroot.png", "", ""],
    ["image", "sprites/gifts/blueberries.png", "", ""],
    ["image", "sprites/gifts/carrot.png", "", ""],
    ["image", "sprites/gifts/cupcake.png", "", ""],
    ["image", "sprites/gifts/hazelnuts.png", "", ""],
    ["image", "sprites/gifts/mango.png", "", ""],
    ["image", "sprites/gifts/popcorn.png", "", ""],
    ["image", "sprites/gifts/ramen.png", "", ""],    

    ["image", "sprites/astronomical/moon.png", "", ""],    
    ["image", "sprites/astronomical/star-1.png", "", "A star"],    
    ["image", "sprites/astronomical/star-2.png", "", "A star"],    
    ["image", "sprites/astronomical/star-3.png", "", "A star"],    
    ["image", "sprites/astronomical/star-4.png", "", "A star"],    
    ["image", "sprites/astronomical/star-5.png", "", "A star"],    

    ["image", "sprites/characters/stick-sheet.png", "", ""],
    ["image", "sprites/characters/cloud-sheet.png", "", ""],
    ["image", "sprites/characters/egg-sheet.png", "", ""],
    ["image", "sprites/characters/stick-stetson.png", "", ""],
    ["image", "sprites/characters/stick-party.png", "", ""],
    ["image", "sprites/characters/stick-acorn.png", "", ""],
    ["image", "sprites/characters/cloud-pussy.png", "", ""],
    ["image", "sprites/characters/cloud-stetson.png", "", ""],
    ["image", "sprites/characters/cloud-bobble.png", "", ""],
    ["image", "sprites/characters/egg-bobble.png", "", ""],
    ["image", "sprites/characters/egg-acorn.png", "", ""],
    ["image", "sprites/characters/egg-pussy.png", "", ""],
    ["image", "sprites/characters/ghost-sheet.png", "", ""],
    ["image", "sprites/characters/ghost-shroom.png", "", ""],
    ["image", "sprites/characters/ghost-party.png", "", ""],
    
    ["image", "sprites/footprint.png", "sprites/footprint-night.png", ""],

    ["image", "scenery/lake_mediumgreen.png", "", "A medium sized, green lake"],
    ["image", "scenery/lake_bigyellow.png", "", "A big yellow lake"],
    ["image", "scenery/lake_mediumpurple.png", "", "A medium, purple lake"],
    ["image", "scenery/lake_smallpink.png", "", "A small pink lake"],
    ["image", "scenery/lake_smallblue.png", "", "A small blue lake"],
    ["image", "scenery/lake_mediumpalepink.png", "", "A medium pale pink lake"],
    ["image", "scenery/lake_mediumdarkpink.png", "", "A medium dark pink lake"],
    ["image", "scenery/lake_bigdarkgreen.png", "", "A big dark green lake"],
    
    ["image", "scenery/stripes_bigyellow.png", "", "Big yellow piano stripes"],
    ["image", "scenery/stripes_bigyellow_notes.png", "", ""],
    ["image", "scenery/stripes_smallpink.png", "", "Small pink piano stripes"],
    ["image", "scenery/stripes_smallpink_notes.png", "", ""],
    ["image", "scenery/stripes_bigorange.png", "", "Big orange piano stripes"],
    ["image", "scenery/stripes_bigorange_notes.png", "", ""],
    ["image", "scenery/stripes_mediumblue.png", "", "Medium blue piano stripes"],
    ["image", "scenery/stripes_mediumblue_notes.png", "", ""],
    ["image", "scenery/stripes_mediumpink.png", "", "Medium pink piano stripes"],
    ["image", "scenery/stripes_mediumpink_notes.png", "", ""],
    ["image", "scenery/stripes_mediumpurple.png", "", "Medium purple piano stripes"],
    ["image", "scenery/stripes_mediumpurple_notes.png", "", ""],

    ["bitmap", "scenery/lake_mediumgreen.png"],
    ["bitmap", "scenery/lake_bigyellow.png"],
    ["bitmap", "scenery/lake_mediumpurple.png"],
    ["bitmap", "scenery/lake_smallpink.png"],
    ["bitmap", "scenery/lake_smallblue.png"],
    ["bitmap", "scenery/lake_mediumpalepink.png"],
    ["bitmap", "scenery/lake_mediumdarkpink.png"],
    ["bitmap", "scenery/lake_bigdarkgreen.png"],
    ["bitmap", "scenery/stripes_bigyellow_notes.png"],
    ["bitmap", "scenery/stripes_smallpink_notes.png"],
    ["bitmap", "scenery/stripes_bigorange_notes.png"],
    ["bitmap", "scenery/stripes_mediumblue_notes.png"],
    ["bitmap", "scenery/stripes_mediumpink_notes.png"],
    ["bitmap", "scenery/stripes_mediumpurple_notes.png"],
    
    ["image", "gui/photos.png", "", ""],
    ["image", "gui/logos.png", "gui/logos-night.png", ""],
    ["image", "gui/edin.png", "gui/edin-night.png", ""],
    ["image", "gui/ttt.png", "gui/ttt-night.png", ""],
    ["image", "gui/isight.png", "gui/isight-night.png", ""],
    ["image", "gui/plus.png", "gui/plus-night.png", ""],
    ["image", "gui/minus.png", "", ""],
    ["image", "gui/bubble.png", "", ""],
    ["image", "gui/bubble-left.png", "", ""],
    ["image", "gui/bubble-right.png", "", ""],
    ["image", "gui/bubble-pointless.png", "", ""],
    ["image", "gui/menu-point.png", "", ""],
    ["image", "gui/menu-point-hover.png", "", ""],
    ["image", "gui/stick-button.png", "", "Stick shaped character"],
    ["image", "gui/egg-button.png", "", "Egg shaped character"],
    ["image", "gui/cloud-button.png", "", "Cloud shaped character"],
    ["image", "gui/stick-stetson.png", "", "Stetson"],
    ["image", "gui/stick-party.png", "", "Party hat"],
    ["image", "gui/stick-acorn.png", "", "Acorn hat"],
    ["image", "gui/cloud-pussy.png", "", "Pink hat with ears"],
    ["image", "gui/cloud-stetson.png", "", "Stetson"],
    ["image", "gui/cloud-bobble.png", "", "Bobble hat"],
    ["image", "gui/egg-bobble.png", "", "Bobble hat"],
    ["image", "gui/egg-acorn.png", "", "Acorn hat"],
    ["image", "gui/egg-pussy.png", "", "Pink hat with ears"],
    ["image", "gui/ghost-shroom.png", "", ""],
    ["image", "gui/ghost-party.png", "", ""],
    ["image", "sprites/heart.png", "", ""],
    ["image", "gui/biscuits/cloud_clouds.png", "", ""],
    ["image", "gui/biscuits/cloud_plants.png", "", ""],
    ["image", "gui/biscuits/cloud_sick.png", "", ""],
    ["image", "gui/biscuits/cloud_snacks.png", "", ""],
    ["image", "gui/biscuits/cloud_social.png", "", ""],
    ["image", "gui/biscuits/egg_clouds.png", "", ""],
    ["image", "gui/biscuits/egg_plants.png", "", ""],
    ["image", "gui/biscuits/egg_sick.png", "", ""],
    ["image", "gui/biscuits/egg_snacks.png", "", ""],
    ["image", "gui/biscuits/egg_social.png", "", ""],
    ["image", "gui/biscuits/ghost_clouds.png", "", ""],
    ["image", "gui/biscuits/ghost_plants.png", "", ""],
    ["image", "gui/biscuits/ghost_sick.png", "", ""],
    ["image", "gui/biscuits/ghost_snacks.png", "", ""],
    ["image", "gui/biscuits/ghost_social.png", "", ""],
    ["image", "gui/biscuits/stick_clouds.png", "", ""],
    ["image", "gui/biscuits/stick_plants.png", "", ""],
    ["image", "gui/biscuits/stick_sick.png", "", ""],
    ["image", "gui/biscuits/stick_snacks.png", "", ""],
    ["image", "gui/biscuits/stick_social.png", "", ""],
    ["image", "gui/down-arrow.png", "", ""],

];

/*
const fps = new class {
  constructor() {
    this.fps = document.getElementById("fps");
    this.frames = [];
    this.lastFrameTimeStamp = performance.now();
  }

  render() {
    // Convert the delta time since the last frame render into a measure
    // of frames per second.
    const now = performance.now();
    const delta = now - this.lastFrameTimeStamp;
    this.lastFrameTimeStamp = now;
    const fps = 1 / delta * 1000;

    // Save only the latest 100 timings.
    this.frames.push(fps);
    if (this.frames.length > 100) {
      this.frames.shift();
    }

    // Find the max, min, and mean of our 100 latest timings.
    let min = Infinity;
    let max = -Infinity;
    let sum = 0;
    for (let i = 0; i < this.frames.length; i++) {
      sum += this.frames[i];
      min = Math.min(this.frames[i], min);
      max = Math.max(this.frames[i], max);
    }
    let mean = sum / this.frames.length;

    // Render the statistics.
    this.fps.textContent = `fps: ${Math.round(mean)}`.trim();

  }
};


const stats = new class {
    constructor() {
        this.stats = document.getElementById("stats");
    }
    render() {
        this.stats.innerHTML = "";
        var items = game.stats();
        for (var i=0; i<items.length; i++) {
            var entry = document.createElement('li');
            entry.appendChild(document.createTextNode(items[i]));
            this.stats.appendChild(entry);
        };
    }
};

const scores = new class {
    constructor() {
        this.scores = document.getElementById("scores");
    }
    render() {
        this.scores.innerHTML = "";
        var items = game.scores();
        for (var i=0; i<items.length; i++) {
            var entry = document.createElement('li');
            entry.appendChild(document.createTextNode(items[i]));
            this.scores.appendChild(entry);
        };
    }
};
*/
/////////////////////////////////////////////////////////////////////

const url = [location.protocol, '//', location.host, location.pathname].join('')
var loading_cursor = 0;
var finished = 0;

function load_next_resource(game) {
    if (loading_cursor<loadinglist.length) {
        var loading = document.getElementById("loading-pc");

        loading.textContent = Math.round((loading_cursor/loadinglist.length)*100);
        
        var r = loadinglist[loading_cursor];
        loading_cursor += 1;
        
        if (r[0]=="image") {
            load_image("/images/"+r[1],"/images/"+r[2],r[3],game);
        } 

        if (r[0]=="bitmap") {
            load_bitmap("/images/"+r[1], game);
        } 
        
        if (r[0]=="network") {
            load_graph(r[1],game);        
        }

        if (r[0]=="conversations") {
            load_conversations(r[1],game);        
        }

        if (r[0]=="sound") {
            load_sound(r[1],game);        
        }
    } else {
        if (!finished) { // whyyyy
            var loading = document.getElementById("loading");
            loading.style.display = "none";
            game.init();
            requestAnimationFrame(renderLoop);
            finished = 1;
        }
    }
}
    
function load_graph(path,game) {
    var client = new XMLHttpRequest();
    client.open('GET', url+path);
    client.onreadystatechange = function() {
        if (client.readyState === XMLHttpRequest.DONE) {
            game.add_adjacency_graph(path, client.responseText);
            load_next_resource(game);
        }
    }
    client.send();
}

function load_sound(path,game) {
    var audioCtx = new AudioContext();
    var client = new XMLHttpRequest();
    client.responseType = 'arraybuffer';
    client.open('GET', url+path, true);

    client.onreadystatechange = function() {
        if (client.readyState === XMLHttpRequest.DONE) {
            audioCtx.decodeAudioData(client.response, function (buffer) {
                game.add_audio_buffer(path, buffer);
                load_next_resource(game);
            });
        }
    }
    client.send();
}

function load_conversations(path,game) {
    var client = new XMLHttpRequest();
    client.open('GET', url+path);
    client.onreadystatechange = function() {
        if (client.readyState === XMLHttpRequest.DONE) {
            game.add_conversations(client.responseText);
            load_next_resource(game);
        }
    }
    client.send();
}

// load two version for theming
function load_image(path,path_night,desc,game) { 
    var img = new Image();
    img.src = url+path;
    img.onload = function () {
        if (path_night != "/images/") {
            var img_night = new Image();
            img_night.src = url+path_night;
            img_night.onload = function () {        
                game.add_themed_sprite_resource(path,img,img_night,desc);
                load_next_resource(game);
            }
        } else {            
            game.add_sprite_resource(path,img,desc);
            load_next_resource(game);
        }
    }
} 

// load two version for theming
function load_bitmap(path,game) { 
    var img = new Image();
    img.src = url+path;
    img.onload = function () {            
        game.add_bitmap_resource(path,img);
        load_next_resource(game);
    }
} 
 
///////////////////////////////////////////////////////////////////////

var game = new Game()
var access_id = document.getElementById('access');

// mask retriggers
let keys = {};

function keycode_to_string(code) {
    switch (event.keyCode) {
    case 27: return "escape"; 
    case 37: return "a"; 
    case 38: return "w"; 
    case 39: return "d"; 
    case 40: return "s"; 
    case 16: return "shift"; 
    default: return false;
    }
}

document.addEventListener("keydown", event => {
    access_id.innerHTML="";
    if (!keys[event.code]) {
        keys[event.code] = true;
        let k = keycode_to_string(event.code);
        if (k) {
            game.handle(k);
        } else {
            game.handle(event.key.toLowerCase());
        }
    }
});

document.addEventListener("keyup", event => {
    keys[event.code] = false;
    let k = keycode_to_string(event.code);
    if (k) {
        game.handle_release(k);
    } else {
        game.handle_release(event.key.toLowerCase());
    }
});
                          
// touch?
document.getElementById('canvas').addEventListener("mousedown", event => { 
    const rect = document.getElementById('canvas').getBoundingClientRect();    
    const x = (event.clientX - rect.left)/rect.width;
    const y = (event.clientY - rect.top)/rect.height;
    game.mouse(x*1700,y*750,"mousedown");
}); 

document.getElementById('canvas').addEventListener("mousemove", event => {
    const rect = document.getElementById('canvas').getBoundingClientRect();    
    const x = (event.clientX - rect.left)/rect.width;
    const y = (event.clientY - rect.top)/rect.height;
    game.mouse(x*1700,y*750,"mousemove");
}); 

// Add event listeners for visibility change 
document.addEventListener("visibilitychange", () => {
    game.visible(document.hidden)
});

if (navigator.userAgent.indexOf("Safari") > -1) {
    console.log("running on safari");
    game.safari_hack();
}

const renderLoop = () => {
    //stats.render();
    //fps.render();
    game.render();
    requestAnimationFrame(renderLoop);
};

load_next_resource(game);

