/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 	};
/******/
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/
/******/
/******/ 	// script path function
/******/ 	function jsonpScriptSrc(chunkId) {
/******/ 		return __webpack_require__.p + "" + chunkId + ".bootstrap.js"
/******/ 	}
/******/
/******/ 	// object to store loaded and loading wasm modules
/******/ 	var installedWasmModules = {};
/******/
/******/ 	function promiseResolve() { return Promise.resolve(); }
/******/
/******/ 	var wasmImportObjects = {
/******/ 		"../pkg/nergal_bg.wasm": function() {
/******/ 			return {
/******/ 				"./nergal_bg.js": {
/******/ 					"__wbindgen_object_drop_ref": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_object_drop_ref"](p0i32);
/******/ 					},
/******/ 					"__wbindgen_string_new": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_string_new"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbindgen_object_clone_ref": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_object_clone_ref"](p0i32);
/******/ 					},
/******/ 					"__wbindgen_cb_drop": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_cb_drop"](p0i32);
/******/ 					},
/******/ 					"__wbindgen_jsval_eq": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_jsval_eq"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbindgen_is_object": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_is_object"](p0i32);
/******/ 					},
/******/ 					"__wbindgen_is_undefined": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_is_undefined"](p0i32);
/******/ 					},
/******/ 					"__wbindgen_in": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_in"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbindgen_number_get": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_number_get"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbindgen_error_new": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_error_new"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbindgen_jsval_loose_eq": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_jsval_loose_eq"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbindgen_boolean_get": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_boolean_get"](p0i32);
/******/ 					},
/******/ 					"__wbindgen_string_get": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_string_get"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_getwithrefkey_edc2c8960f0f1191": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_getwithrefkey_edc2c8960f0f1191"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_queueMicrotask_3cbae2ec6b6cd3d6": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_queueMicrotask_3cbae2ec6b6cd3d6"](p0i32);
/******/ 					},
/******/ 					"__wbindgen_is_function": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_is_function"](p0i32);
/******/ 					},
/******/ 					"__wbg_queueMicrotask_481971b0d87f3dd4": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_queueMicrotask_481971b0d87f3dd4"](p0i32);
/******/ 					},
/******/ 					"__wbg_crypto_58f13aa23ffcb166": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_crypto_58f13aa23ffcb166"](p0i32);
/******/ 					},
/******/ 					"__wbg_process_5b786e71d465a513": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_process_5b786e71d465a513"](p0i32);
/******/ 					},
/******/ 					"__wbg_versions_c2ab80650590b6a2": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_versions_c2ab80650590b6a2"](p0i32);
/******/ 					},
/******/ 					"__wbg_node_523d7bd03ef69fba": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_node_523d7bd03ef69fba"](p0i32);
/******/ 					},
/******/ 					"__wbindgen_is_string": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_is_string"](p0i32);
/******/ 					},
/******/ 					"__wbg_require_2784e593a4674877": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_require_2784e593a4674877"]();
/******/ 					},
/******/ 					"__wbg_msCrypto_abcb1295e768d1f2": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_msCrypto_abcb1295e768d1f2"](p0i32);
/******/ 					},
/******/ 					"__wbg_randomFillSync_a0d98aa11c81fe89": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_randomFillSync_a0d98aa11c81fe89"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_getRandomValues_504510b5564925af": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_getRandomValues_504510b5564925af"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_instanceof_Window_3e5cd1f48c152d01": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_instanceof_Window_3e5cd1f48c152d01"](p0i32);
/******/ 					},
/******/ 					"__wbg_document_d609202d16c38224": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_document_d609202d16c38224"](p0i32);
/******/ 					},
/******/ 					"__wbg_speechSynthesis_c40cfe67970932eb": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_speechSynthesis_c40cfe67970932eb"](p0i32);
/******/ 					},
/******/ 					"__wbg_open_1526872b77d837c5": function(p0i32,p1i32,p2i32,p3i32,p4i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_open_1526872b77d837c5"](p0i32,p1i32,p2i32,p3i32,p4i32);
/******/ 					},
/******/ 					"__wbg_fetch_6c415b3a07763878": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_fetch_6c415b3a07763878"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_location_6fd8f140f024ba10": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_location_6fd8f140f024ba10"](p0i32);
/******/ 					},
/******/ 					"__wbg_createElement_fdd5c113cb84539e": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_createElement_fdd5c113cb84539e"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_getElementById_65b9547a428b5eb4": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_getElementById_65b9547a428b5eb4"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_instanceof_CanvasRenderingContext2d_b43c8f92c4744b7b": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_instanceof_CanvasRenderingContext2d_b43c8f92c4744b7b"](p0i32);
/******/ 					},
/******/ 					"__wbg_setglobalAlpha_e0ca444d935ec28d": function(p0i32,p1f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setglobalAlpha_e0ca444d935ec28d"](p0i32,p1f64);
/******/ 					},
/******/ 					"__wbg_setglobalCompositeOperation_fe372fcb9787ddef": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setglobalCompositeOperation_fe372fcb9787ddef"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_setstrokeStyle_1b4551387606453b": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setstrokeStyle_1b4551387606453b"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_setfillStyle_1ebd7d8f502888fa": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setfillStyle_1ebd7d8f502888fa"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_setfilter_d28bcf74aa2e424c": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setfilter_d28bcf74aa2e424c"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_setimageSmoothingEnabled_09ef00d9928b0eb4": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setimageSmoothingEnabled_09ef00d9928b0eb4"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_setlineWidth_d101118d79143f42": function(p0i32,p1f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setlineWidth_d101118d79143f42"](p0i32,p1f64);
/******/ 					},
/******/ 					"__wbg_setfont_a622e8f71fcc67bd": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setfont_a622e8f71fcc67bd"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_drawImage_32fe67cd2dd4350b": function(p0i32,p1i32,p2f64,p3f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_drawImage_32fe67cd2dd4350b"](p0i32,p1i32,p2f64,p3f64);
/******/ 					},
/******/ 					"__wbg_drawImage_e193863962ffad3c": function(p0i32,p1i32,p2f64,p3f64,p4f64,p5f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_drawImage_e193863962ffad3c"](p0i32,p1i32,p2f64,p3f64,p4f64,p5f64);
/******/ 					},
/******/ 					"__wbg_drawImage_e264496193b08c0e": function(p0i32,p1i32,p2f64,p3f64,p4f64,p5f64,p6f64,p7f64,p8f64,p9f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_drawImage_e264496193b08c0e"](p0i32,p1i32,p2f64,p3f64,p4f64,p5f64,p6f64,p7f64,p8f64,p9f64);
/******/ 					},
/******/ 					"__wbg_beginPath_5c4c10cc904edc64": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_beginPath_5c4c10cc904edc64"](p0i32);
/******/ 					},
/******/ 					"__wbg_fill_854920f07573dffd": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_fill_854920f07573dffd"](p0i32);
/******/ 					},
/******/ 					"__wbg_stroke_d6cd6e7c98952ff8": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_stroke_d6cd6e7c98952ff8"](p0i32);
/******/ 					},
/******/ 					"__wbg_getImageData_343953482ee5ec46": function(p0i32,p1f64,p2f64,p3f64,p4f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_getImageData_343953482ee5ec46"](p0i32,p1f64,p2f64,p3f64,p4f64);
/******/ 					},
/******/ 					"__wbg_arc_1e8c0f1f2b86edf7": function(p0i32,p1f64,p2f64,p3f64,p4f64,p5f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_arc_1e8c0f1f2b86edf7"](p0i32,p1f64,p2f64,p3f64,p4f64,p5f64);
/******/ 					},
/******/ 					"__wbg_closePath_957f75b5de9f7391": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_closePath_957f75b5de9f7391"](p0i32);
/******/ 					},
/******/ 					"__wbg_moveTo_23c53eed282c0730": function(p0i32,p1f64,p2f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_moveTo_23c53eed282c0730"](p0i32,p1f64,p2f64);
/******/ 					},
/******/ 					"__wbg_rect_49dd4a0421db314a": function(p0i32,p1f64,p2f64,p3f64,p4f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_rect_49dd4a0421db314a"](p0i32,p1f64,p2f64,p3f64,p4f64);
/******/ 					},
/******/ 					"__wbg_roundRect_5bc558db0fe10790": function(p0i32,p1f64,p2f64,p3f64,p4f64,p5f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_roundRect_5bc558db0fe10790"](p0i32,p1f64,p2f64,p3f64,p4f64,p5f64);
/******/ 					},
/******/ 					"__wbg_restore_7c05e5338cb2f7dc": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_restore_7c05e5338cb2f7dc"](p0i32);
/******/ 					},
/******/ 					"__wbg_save_7222a99e05e4831b": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_save_7222a99e05e4831b"](p0i32);
/******/ 					},
/******/ 					"__wbg_fillText_2dc739bc08581cf8": function(p0i32,p1i32,p2i32,p3f64,p4f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_fillText_2dc739bc08581cf8"](p0i32,p1i32,p2i32,p3f64,p4f64);
/******/ 					},
/******/ 					"__wbg_measureText_a0445eaa09a34fe6": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_measureText_a0445eaa09a34fe6"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_scale_202bd3c19df51811": function(p0i32,p1f64,p2f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_scale_202bd3c19df51811"](p0i32,p1f64,p2f64);
/******/ 					},
/******/ 					"__wbg_transform_f1cd583f652d907c": function(p0i32,p1f64,p2f64,p3f64,p4f64,p5f64,p6f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_transform_f1cd583f652d907c"](p0i32,p1f64,p2f64,p3f64,p4f64,p5f64,p6f64);
/******/ 					},
/******/ 					"__wbg_translate_52a4e2eb4836f205": function(p0i32,p1f64,p2f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_translate_52a4e2eb4836f205"](p0i32,p1f64,p2f64);
/******/ 					},
/******/ 					"__wbg_log_a4530b4fe289336f": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_log_a4530b4fe289336f"](p0i32);
/******/ 					},
/******/ 					"__wbg_width_75698c1c4d3f873d": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_width_75698c1c4d3f873d"](p0i32);
/******/ 					},
/******/ 					"__wbg_newwithstr_e710277767f3bba6": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_newwithstr_e710277767f3bba6"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_get_02fcadd6792fe86a": function(p0i32,p1i32,p2i32,p3i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_get_02fcadd6792fe86a"](p0i32,p1i32,p2i32,p3i32);
/******/ 					},
/******/ 					"__wbg_settype_15cd8054aebe6cbd": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_settype_15cd8054aebe6cbd"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_frequency_5cdcbcca8379b143": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_frequency_5cdcbcca8379b143"](p0i32);
/******/ 					},
/******/ 					"__wbg_detune_dae3467daea5d732": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_detune_dae3467daea5d732"](p0i32);
/******/ 					},
/******/ 					"__wbg_Q_5af0964a74f13129": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_Q_5af0964a74f13129"](p0i32);
/******/ 					},
/******/ 					"__wbg_gain_e67c8a691f532bc8": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_gain_e67c8a691f532bc8"](p0i32);
/******/ 					},
/******/ 					"__wbg_set_27f236f6d7a28c29": function(p0i32,p1i32,p2i32,p3i32,p4i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_set_27f236f6d7a28c29"](p0i32,p1i32,p2i32,p3i32,p4i32);
/******/ 					},
/******/ 					"__wbg_instanceof_Response_4c3b1446206114d1": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_instanceof_Response_4c3b1446206114d1"](p0i32);
/******/ 					},
/******/ 					"__wbg_json_34535d9848f043eb": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_json_34535d9848f043eb"](p0i32);
/******/ 					},
/******/ 					"__wbg_connect_7374783233c39eda": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_connect_7374783233c39eda"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_connect_31a298f1463fdfc8": function(p0i32,p1i32,p2i32,p3i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_connect_31a298f1463fdfc8"](p0i32,p1i32,p2i32,p3i32);
/******/ 					},
/******/ 					"__wbg_connect_d90f0ce77918ce41": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_connect_d90f0ce77918ce41"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_instanceof_HtmlCanvasElement_fba0ac991170cc00": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_instanceof_HtmlCanvasElement_fba0ac991170cc00"](p0i32);
/******/ 					},
/******/ 					"__wbg_setwidth_7591ce24118fd14a": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setwidth_7591ce24118fd14a"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_setheight_f7ae862183d88bd5": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setheight_f7ae862183d88bd5"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_getContext_164dc98953ddbc68": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_getContext_164dc98953ddbc68"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_pending_10c04a0106c6860b": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_pending_10c04a0106c6860b"](p0i32);
/******/ 					},
/******/ 					"__wbg_cancel_7ba443264cf81b9d": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_cancel_7ba443264cf81b9d"](p0i32);
/******/ 					},
/******/ 					"__wbg_getVoices_72436f5e0f702fe0": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_getVoices_72436f5e0f702fe0"](p0i32);
/******/ 					},
/******/ 					"__wbg_speak_6aab4c5ba2b522ce": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_speak_6aab4c5ba2b522ce"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_setvolume_56b22c13159d318b": function(p0i32,p1f32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setvolume_56b22c13159d318b"](p0i32,p1f32);
/******/ 					},
/******/ 					"__wbg_setrate_7f82baa9c2074123": function(p0i32,p1f32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setrate_7f82baa9c2074123"](p0i32,p1f32);
/******/ 					},
/******/ 					"__wbg_setpitch_c8eab72c95b03c56": function(p0i32,p1f32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setpitch_c8eab72c95b03c56"](p0i32,p1f32);
/******/ 					},
/******/ 					"__wbg_newwithtext_31186c2f05d1ef0c": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_newwithtext_31186c2f05d1ef0c"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_data_6e0048ff907c2ca3": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_data_6e0048ff907c2ca3"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_settype_523bfbd496f822de": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_settype_523bfbd496f822de"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_frequency_2dccb3ae0f823e46": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_frequency_2dccb3ae0f823e46"](p0i32);
/******/ 					},
/******/ 					"__wbg_start_58f106eeea44565c": function(p0i32,p1f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_start_58f106eeea44565c"](p0i32,p1f64);
/******/ 					},
/******/ 					"__wbg_stop_68e8483f50db87f9": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_stop_68e8483f50db87f9"](p0i32);
/******/ 					},
/******/ 					"__wbg_gain_ea4961e006680142": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_gain_ea4961e006680142"](p0i32);
/******/ 					},
/******/ 					"__wbg_width_c277bfb56e064434": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_width_c277bfb56e064434"](p0i32);
/******/ 					},
/******/ 					"__wbg_height_8843b7d865dabc0e": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_height_8843b7d865dabc0e"](p0i32);
/******/ 					},
/******/ 					"__wbg_destination_4d44007f7d08d71b": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_destination_4d44007f7d08d71b"](p0i32);
/******/ 					},
/******/ 					"__wbg_currentTime_d2be936586614ff4": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_currentTime_d2be936586614ff4"](p0i32);
/******/ 					},
/******/ 					"__wbg_new_db252a952beaf4fa": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_new_db252a952beaf4fa"]();
/******/ 					},
/******/ 					"__wbg_close_b87bff143de234d5": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_close_b87bff143de234d5"](p0i32);
/******/ 					},
/******/ 					"__wbg_createBiquadFilter_963b8ec8f335a87a": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_createBiquadFilter_963b8ec8f335a87a"](p0i32);
/******/ 					},
/******/ 					"__wbg_createBufferSource_2900236a9a0ed333": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_createBufferSource_2900236a9a0ed333"](p0i32);
/******/ 					},
/******/ 					"__wbg_createChannelMerger_28ecd1a71e706f85": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_createChannelMerger_28ecd1a71e706f85"](p0i32);
/******/ 					},
/******/ 					"__wbg_createGain_c702b552853e1a86": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_createGain_c702b552853e1a86"](p0i32);
/******/ 					},
/******/ 					"__wbg_createOscillator_33e2fb4cedf63575": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_createOscillator_33e2fb4cedf63575"](p0i32);
/******/ 					},
/******/ 					"__wbg_createPanner_d3d3e9a7d4d89ac8": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_createPanner_d3d3e9a7d4d89ac8"](p0i32);
/******/ 					},
/******/ 					"__wbg_setvalue_6d68fd7da7d604f9": function(p0i32,p1f32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setvalue_6d68fd7da7d604f9"](p0i32,p1f32);
/******/ 					},
/******/ 					"__wbg_cancelScheduledValues_d4684dd3b37d4028": function(p0i32,p1f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_cancelScheduledValues_d4684dd3b37d4028"](p0i32,p1f64);
/******/ 					},
/******/ 					"__wbg_linearRampToValueAtTime_d7663013272d6c74": function(p0i32,p1f32,p2f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_linearRampToValueAtTime_d7663013272d6c74"](p0i32,p1f32,p2f64);
/******/ 					},
/******/ 					"__wbg_setValueAtTime_6460f895778e5046": function(p0i32,p1f32,p2f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setValueAtTime_6460f895778e5046"](p0i32,p1f32,p2f64);
/******/ 					},
/******/ 					"__wbg_search_9f7ca8896c2d0804": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_search_9f7ca8896c2d0804"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_setbuffer_fcb9ea4265b72e7f": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setbuffer_fcb9ea4265b72e7f"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_playbackRate_61049b10033b11e5": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_playbackRate_61049b10033b11e5"](p0i32);
/******/ 					},
/******/ 					"__wbg_detune_9dd31618b553bbe0": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_detune_9dd31618b553bbe0"](p0i32);
/******/ 					},
/******/ 					"__wbg_setloop_fcdea367e07806a1": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setloop_fcdea367e07806a1"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_setloopStart_87df7a13e4d761bd": function(p0i32,p1f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setloopStart_87df7a13e4d761bd"](p0i32,p1f64);
/******/ 					},
/******/ 					"__wbg_setloopEnd_31b576012611598e": function(p0i32,p1f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setloopEnd_31b576012611598e"](p0i32,p1f64);
/******/ 					},
/******/ 					"__wbg_start_88b66d1c6c29149b": function(p0i32,p1f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_start_88b66d1c6c29149b"](p0i32,p1f64);
/******/ 					},
/******/ 					"__wbg_stop_08a686dbb8fd0855": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_stop_08a686dbb8fd0855"](p0i32);
/******/ 					},
/******/ 					"__wbg_positionX_67169550cf810a5f": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_positionX_67169550cf810a5f"](p0i32);
/******/ 					},
/******/ 					"__wbg_positionY_1be0d38dbfe52bd5": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_positionY_1be0d38dbfe52bd5"](p0i32);
/******/ 					},
/******/ 					"__wbg_positionZ_30b56124dcaf472d": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_positionZ_30b56124dcaf472d"](p0i32);
/******/ 					},
/******/ 					"__wbg_orientationX_7560cd90a2908551": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_orientationX_7560cd90a2908551"](p0i32);
/******/ 					},
/******/ 					"__wbg_orientationY_2e768ea227a63386": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_orientationY_2e768ea227a63386"](p0i32);
/******/ 					},
/******/ 					"__wbg_orientationZ_f44f7e97cfb71e62": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_orientationZ_f44f7e97cfb71e62"](p0i32);
/******/ 					},
/******/ 					"__wbg_setdistanceModel_d1ab8c35576a1c28": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setdistanceModel_d1ab8c35576a1c28"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_setrefDistance_4f3aff9fa6e6fe90": function(p0i32,p1f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setrefDistance_4f3aff9fa6e6fe90"](p0i32,p1f64);
/******/ 					},
/******/ 					"__wbg_setmaxDistance_4e73f903aff68627": function(p0i32,p1f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setmaxDistance_4e73f903aff68627"](p0i32,p1f64);
/******/ 					},
/******/ 					"__wbg_setrolloffFactor_4cd9a8ca16988d9b": function(p0i32,p1f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setrolloffFactor_4cd9a8ca16988d9b"](p0i32,p1f64);
/******/ 					},
/******/ 					"__wbg_headers_d135d2bb8cc60413": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_headers_d135d2bb8cc60413"](p0i32);
/******/ 					},
/******/ 					"__wbg_newwithstrandinit_f581dff0d19a8b03": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_newwithstrandinit_f581dff0d19a8b03"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_get_bd8e338fbd5f5cc8": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_get_bd8e338fbd5f5cc8"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_length_cd7af8117672b8b8": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_length_cd7af8117672b8b8"](p0i32);
/******/ 					},
/******/ 					"__wbg_new_16b304a2cfa7ff4a": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_new_16b304a2cfa7ff4a"]();
/******/ 					},
/******/ 					"__wbg_newnoargs_e258087cd0daa0ea": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_newnoargs_e258087cd0daa0ea"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_get_e3c254076557e348": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_get_e3c254076557e348"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_call_27c0f87801dedf93": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_call_27c0f87801dedf93"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_new_72fb9a18b5ae2624": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_new_72fb9a18b5ae2624"]();
/******/ 					},
/******/ 					"__wbg_self_ce0dbfc45cf2f5be": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_self_ce0dbfc45cf2f5be"]();
/******/ 					},
/******/ 					"__wbg_window_c6fb939a7f436783": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_window_c6fb939a7f436783"]();
/******/ 					},
/******/ 					"__wbg_globalThis_d1e6af4856ba331b": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_globalThis_d1e6af4856ba331b"]();
/******/ 					},
/******/ 					"__wbg_global_207b558942527489": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_global_207b558942527489"]();
/******/ 					},
/******/ 					"__wbg_push_a5b05aedc7234f9f": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_push_a5b05aedc7234f9f"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_instanceof_ArrayBuffer_836825be07d4c9d2": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_instanceof_ArrayBuffer_836825be07d4c9d2"](p0i32);
/******/ 					},
/******/ 					"__wbg_call_b3ca7c6051f9bec1": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_call_b3ca7c6051f9bec1"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_now_3014639a94423537": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_now_3014639a94423537"]();
/******/ 					},
/******/ 					"__wbg_resolve_b0083a7967828ec8": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_resolve_b0083a7967828ec8"](p0i32);
/******/ 					},
/******/ 					"__wbg_then_0c86a60e8fcfe9f6": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_then_0c86a60e8fcfe9f6"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_then_a73caa9a87991566": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_then_a73caa9a87991566"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_buffer_12d079cc21e14bdb": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_buffer_12d079cc21e14bdb"](p0i32);
/******/ 					},
/******/ 					"__wbg_newwithbyteoffsetandlength_aa4a17c33a06e5cb": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_newwithbyteoffsetandlength_aa4a17c33a06e5cb"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_new_63b92bc8671ed464": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_new_63b92bc8671ed464"](p0i32);
/******/ 					},
/******/ 					"__wbg_set_a47bac70306a19a7": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_set_a47bac70306a19a7"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_length_c20a40f15020d68a": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_length_c20a40f15020d68a"](p0i32);
/******/ 					},
/******/ 					"__wbg_instanceof_Uint8Array_2b3bbecd033d19f6": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_instanceof_Uint8Array_2b3bbecd033d19f6"](p0i32);
/******/ 					},
/******/ 					"__wbg_newwithlength_e9b4878cebadb3d3": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_newwithlength_e9b4878cebadb3d3"](p0i32);
/******/ 					},
/******/ 					"__wbg_subarray_a1f73cd4b5b42fe1": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_subarray_a1f73cd4b5b42fe1"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_set_1f9b04f170055d33": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_set_1f9b04f170055d33"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbindgen_debug_string": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_debug_string"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbindgen_throw": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_throw"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbindgen_memory": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_memory"]();
/******/ 					},
/******/ 					"__wbindgen_closure_wrapper1685": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_closure_wrapper1685"](p0i32,p1i32,p2i32);
/******/ 					}
/******/ 				}
/******/ 			};
/******/ 		},
/******/ 	};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var promises = [];
/******/
/******/
/******/ 		// JSONP chunk loading for javascript
/******/
/******/ 		var installedChunkData = installedChunks[chunkId];
/******/ 		if(installedChunkData !== 0) { // 0 means "already installed".
/******/
/******/ 			// a Promise means "currently loading".
/******/ 			if(installedChunkData) {
/******/ 				promises.push(installedChunkData[2]);
/******/ 			} else {
/******/ 				// setup Promise in chunk cache
/******/ 				var promise = new Promise(function(resolve, reject) {
/******/ 					installedChunkData = installedChunks[chunkId] = [resolve, reject];
/******/ 				});
/******/ 				promises.push(installedChunkData[2] = promise);
/******/
/******/ 				// start chunk loading
/******/ 				var script = document.createElement('script');
/******/ 				var onScriptComplete;
/******/
/******/ 				script.charset = 'utf-8';
/******/ 				script.timeout = 120;
/******/ 				if (__webpack_require__.nc) {
/******/ 					script.setAttribute("nonce", __webpack_require__.nc);
/******/ 				}
/******/ 				script.src = jsonpScriptSrc(chunkId);
/******/
/******/ 				// create error before stack unwound to get useful stacktrace later
/******/ 				var error = new Error();
/******/ 				onScriptComplete = function (event) {
/******/ 					// avoid mem leaks in IE.
/******/ 					script.onerror = script.onload = null;
/******/ 					clearTimeout(timeout);
/******/ 					var chunk = installedChunks[chunkId];
/******/ 					if(chunk !== 0) {
/******/ 						if(chunk) {
/******/ 							var errorType = event && (event.type === 'load' ? 'missing' : event.type);
/******/ 							var realSrc = event && event.target && event.target.src;
/******/ 							error.message = 'Loading chunk ' + chunkId + ' failed.\n(' + errorType + ': ' + realSrc + ')';
/******/ 							error.name = 'ChunkLoadError';
/******/ 							error.type = errorType;
/******/ 							error.request = realSrc;
/******/ 							chunk[1](error);
/******/ 						}
/******/ 						installedChunks[chunkId] = undefined;
/******/ 					}
/******/ 				};
/******/ 				var timeout = setTimeout(function(){
/******/ 					onScriptComplete({ type: 'timeout', target: script });
/******/ 				}, 120000);
/******/ 				script.onerror = script.onload = onScriptComplete;
/******/ 				document.head.appendChild(script);
/******/ 			}
/******/ 		}
/******/
/******/ 		// Fetch + compile chunk loading for webassembly
/******/
/******/ 		var wasmModules = {"0":["../pkg/nergal_bg.wasm"]}[chunkId] || [];
/******/
/******/ 		wasmModules.forEach(function(wasmModuleId) {
/******/ 			var installedWasmModuleData = installedWasmModules[wasmModuleId];
/******/
/******/ 			// a Promise means "currently loading" or "already loaded".
/******/ 			if(installedWasmModuleData)
/******/ 				promises.push(installedWasmModuleData);
/******/ 			else {
/******/ 				var importObject = wasmImportObjects[wasmModuleId]();
/******/ 				var req = fetch(__webpack_require__.p + "" + {"../pkg/nergal_bg.wasm":"752873cc8e02edd5d4b4"}[wasmModuleId] + ".module.wasm");
/******/ 				var promise;
/******/ 				if(importObject instanceof Promise && typeof WebAssembly.compileStreaming === 'function') {
/******/ 					promise = Promise.all([WebAssembly.compileStreaming(req), importObject]).then(function(items) {
/******/ 						return WebAssembly.instantiate(items[0], items[1]);
/******/ 					});
/******/ 				} else if(typeof WebAssembly.instantiateStreaming === 'function') {
/******/ 					promise = WebAssembly.instantiateStreaming(req, importObject);
/******/ 				} else {
/******/ 					var bytesPromise = req.then(function(x) { return x.arrayBuffer(); });
/******/ 					promise = bytesPromise.then(function(bytes) {
/******/ 						return WebAssembly.instantiate(bytes, importObject);
/******/ 					});
/******/ 				}
/******/ 				promises.push(installedWasmModules[wasmModuleId] = promise.then(function(res) {
/******/ 					return __webpack_require__.w[wasmModuleId] = (res.instance || res).exports;
/******/ 				}));
/******/ 			}
/******/ 		});
/******/ 		return Promise.all(promises);
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// on error function for async loading
/******/ 	__webpack_require__.oe = function(err) { console.error(err); throw err; };
/******/
/******/ 	// object with all WebAssembly.instance exports
/******/ 	__webpack_require__.w = {};
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./bootstrap.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./bootstrap.js":
/*!**********************!*\
  !*** ./bootstrap.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// A dependency graph that contains any wasm must all be imported\n// asynchronously. This `bootstrap.js` file does the single async import, so\n// that no one else needs to worry about it again.\n__webpack_require__.e(/*! import() */ 0).then(__webpack_require__.bind(null, /*! ./index.js */ \"./index.js\"))\n  .catch(e => console.error(\"Error importing `index.js`:\", e));\n\n\n//# sourceURL=webpack:///./bootstrap.js?");

/***/ })

/******/ });