[package]   
name = "nergal"
version = "0.1.0"
authors = ["Dave Griffiths <dave@thentrythis.org>"]
edition = "2018"
description = "A citizen science game"
repository = "https://gitlab.com/then-try-this/nergal"
license = "GPL affero v3"
        
[lib]
crate-type = ["cdylib", "rlib"]

[features]
default = ["console_error_panic_hook"]

[dependencies]
wasm-bindgen = "0.2.84"
wasm-bindgen-futures = "0.4.42"
js-sys = "0.3.66"
vector2math = "0.14.0"
rand = "0.8.5"
getrandom = { version = "0.2", features = ["js"] }
quick-xml = "0.31.0"
lazy_static = { version = "1.4.0" }
instant = { version = "0.1", features = [ "wasm-bindgen", "inaccurate" ] }
serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0"
maplit = "1.0.2"
serde-wasm-bindgen = "0.6.5"

# The `console_error_panic_hook` crate provides better debugging of panics by
# logging them with `console.error`. This is great for development, but requires
# all the `std::fmt` and `std::panicking` infrastructure, so isn't great for
# code size when deploying.
console_error_panic_hook = { version = "0.1.7", optional = true }

[dependencies.web-sys]
version = "0.3.4"
features = [
  'console',   
  'CanvasRenderingContext2d',
  'Document',
  'Location',
  'UrlSearchParams',  
  'Element',
  'HtmlCanvasElement',
  'Window',
  'HtmlImageElement',
  'TextMetrics', 
  'Headers',      
  'Request',    
  'RequestInit',
  'RequestMode',
  'Response',
  'ImageData',
  'AudioContext',
  'AudioDestinationNode',
  'AudioNode',
  'AudioParam',
  'GainNode',
  'OscillatorNode',
  'OscillatorType',
  'BiquadFilterNode',  
  'BiquadFilterType',
  'ChannelMergerNode',
  'ChannelMergerOptions',
  'AudioBufferSourceNode',
  'AudioBuffer',
  'PannerNode', 
  'SpeechSynthesis',
  'SpeechSynthesisUtterance',
  'DistanceModelType',
  'SpeechSynthesisVoice',
]

[dev-dependencies]
wasm-bindgen-test = "0.3.34"

[dependencies.uuid]
version = "1.10.0"
features = [
    "v4",                # Lets you generate random UUIDs
    "fast-rng",          # Use a faster (but still sufficiently random) RNG
    "macro-diagnostics", # Enable better diagnostics for compile-time UUIDs
]
    
[profile.release]
# Tell `rustc` to optimize for small code size.
opt-level = 3

    