//! Test suite for the Web and headless browsers.

#![cfg(target_arch = "wasm32")]

extern crate wasm_bindgen_test;
use wasm_bindgen_test::*;

use nergal::sound::synth::*;

wasm_bindgen_test_configure!(run_in_browser);
use web_sys::console;

#[wasm_bindgen_test]
fn pass() {
    assert_eq!(1 + 1, 2);


      let data = r#"
        {
            "name": "osc",
            "parameters": {
                "shape": "sine",
                "freq": 440
            },
            "children": {
                "freq": {
                    "name": "osc",
                    "parameters": {
                        "freq": 440
                    },
                    "children": {}
                }
            }
        }"#;
        
        let p: SynthNodeDesc = serde_json::from_str(data).unwrap();

    console::log_1(&format!("{:?}", p).into());

    let ctx = web_sys::AudioContext::new().unwrap();

    let node = p.create_synth_node(&ctx).unwrap();
    
    
}
