use std::rc::Rc;
use crate::items::info_item::{ InfoItem };

#[derive(Clone)]
pub struct Poem {
    pub lines: Rc<Vec<String>>
}


impl Poem {
    pub fn new() -> Self {
        Poem {
            lines: Rc::new(vec![])
        }
    }
    
    pub fn create(lines: Vec<String>) -> Self {
        Poem {
            lines: Rc::new(lines)
        }
    }

    pub fn is_complete(&self, items: Vec<&InfoItem>) -> bool {

        for line in &*self.lines {
            let mut found = false;
            for item in &items {
                if item.text == *line {
                    found = true;
                }
            }
            if !found { return false; }
        }        
        true
    }
}
