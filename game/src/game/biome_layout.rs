// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


use rand::Rng;


use crate::maths::vec2::*;

use vector2math::*;

use crate::game::entity::{ Entity };

use crate::entities::plant_entity::{ PlantEntity };
use crate::entities::rock_entity::{ RockEntity };

use crate::entities::scenery_entity::{ SceneryEntity };
use crate::entities::rainbow_entity::{ RainbowEntity };
use crate::entities::piano_entity::{ PianoEntity };

use web_sys::console;

fn set_pos(entity: &mut Box<dyn Entity>, pos: Vec2) {

    //console::log_1(&format!("{:?}", pos).into());
    
    if let Some(e) = entity.as_any_mut().downcast_mut::<PlantEntity>() {
        e.pos = pos;
    }

    if let Some(e) = entity.as_any_mut().downcast_mut::<RockEntity>() {
        e.pos = pos;
    }

    if let Some(e) = entity.as_any_mut().downcast_mut::<SceneryEntity>() {
        e.pos = pos;
    }

    if let Some(e) = entity.as_any_mut().downcast_mut::<RainbowEntity>() {
        e.pos = pos;
    }

    if let Some(e) = entity.as_any_mut().downcast_mut::<PianoEntity>() {
        e.pos = pos;
    }
}

const PLAYER_CLEARING_SIZE: f64 = 500.;

/////////////////////////////////////////////////////////////////////

pub fn create_layout_hexgrid(area_box: (Vec2, Vec2), space_between: f64, player_clearing: bool) -> Vec<Vec2> {   
    
    let _rng = &mut rand::thread_rng();
    let mut grid = vec![];

    // make sure there are enough grid positions for all the entities!
    let grid_x = ((area_box.1.x - area_box.0.x) / space_between) as u32;
    let grid_y = ((area_box.1.y - area_box.0.y) / space_between) as u32;
    let centre = area_box.0.add(area_box.1.sub(area_box.0).div(2.));

    //console::log_1(&format!("{},{}", grid_x, grid_y).into());
    
    for x in 0..grid_x {
        for y in 0..grid_y {
            let pos =
                if y % 2 == 0 {
                    area_box.0.add(Vec2::new(x as f64 * space_between,
                                             y as f64 * space_between))
                } else {
                    area_box.0.add(Vec2::new(x as f64 * space_between + space_between / 2.,
                                             y as f64 * space_between))
                };
            
            if !player_clearing || pos.dist(centre) > PLAYER_CLEARING_SIZE {            
                grid.push(pos);
            }
        }
    }

    grid
}

pub fn layout_into_grid<T: 'static>(entities: &mut [Box<dyn Entity>], grid: &mut Vec<Vec2>) {   
    
    let rng = &mut rand::thread_rng();
    
    for e in &mut entities.iter_mut() {
        if let Some(_ee) = e.as_any().downcast_ref::<T>() {
            let num = rng.gen_range(0.0..grid.len() as f64) as usize;

            // add slight jitter
            set_pos(e, grid[num].add(Vec2::crnd().mul(50.)));
            
            if grid.len()>5 {
                grid.remove(num);
            } else {
                console::log_1(&"ran out of hexgrid positions in layout".into());                
            }
        }
    }
}


/////////////////////////////////////////////////////////////////////

#[cfg(test)]
mod tests {
    use super::*;
    use crate::entities::plant_entity::{ PlantEntity };
    use crate::entities::rock_entity::{ RockEntity };
    use crate::entities::social_entity::{ SocialEntity };
    
    #[test]
    fn basics() {


    }
}
