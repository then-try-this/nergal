// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;
use std::any::Any;
use instant::Duration;
use crate::game::entity::{ Entity };
use crate::game::inventory::{ InventoryVec };
use crate::game::message::{ MessageVec };
use crate::game::resources::{ Resources };

/// Broad categories of inventory items - used for filtering.
#[derive(Debug,Clone,Copy,PartialEq,Eq,Hash)]
pub enum InventoryItemType {
    Info,
    Disease,
    Item
}

#[macro_export]
macro_rules! item_declare_any {
    () => {
        fn as_any(&self) -> &dyn Any { self }
        fn as_any_mut(&mut self) -> &mut dyn Any { self }
    };
}

/// Inventory items are general purpose things that could represent
/// collected items, information or diseases. 
pub trait InventoryItem {
    fn item_type(&self) -> InventoryItemType;
    fn delete_me(&self) -> bool { false }
    fn dups_allowed(&self) -> bool { true }

    /// This allows us to prevent duplicates of things which don't
    /// make sense to keep - e.g. information.
    fn is_dup_of(&self, _other: &Rc<dyn InventoryItem>) -> bool { false }

    /// In the update we can see the other items in our inventory, and
    /// the entity that owns us. Return an updated item and a list of
    /// messages. The messages get sent out to the world, but most of
    /// the time we probably want to send them to our owner entity.
    fn update(&self, inventory: &InventoryVec, owner: &dyn Entity, resources: &Resources, delta: &Duration) -> (Rc<dyn InventoryItem>, MessageVec);

    fn as_any(&self) -> &dyn Any;
    fn as_any_mut(&mut self) -> &mut dyn Any;

}

/// Helper to convert an item to it's type
pub fn item_as<T: 'static>(item: &Rc<dyn InventoryItem>) -> Option<&T> {
    item.as_any().downcast_ref::<T>()
}


