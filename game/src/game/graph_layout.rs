// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use crate::maths::vec2::*;
use vector2math::*;

use crate::game::id::{ EntityId, PLAYER_ID };
use crate::game::social_graph::{ SocialGraph };



/// A node temporarily used to layout entity positions based on their
/// social network connections
#[derive(Debug, Clone)]
pub struct LayoutNode {
    pub id: EntityId,
    pub pos: Vec2,
    pub vel: Vec2,
    pub connections: Vec<EntityId>
}

impl LayoutNode {
    
    pub fn relax(
        &self,
        centre: Vec2,
        nodes: &HashMap<EntityId,LayoutNode>,
        friction: f64,
        edge_length: f64,
        centre_attraction: f64,
        other_repel: f64,
        edge_attraction: f64,
        max_speed: f64) -> Self {
        let mut delta = self.vel.mul(friction);

        delta = delta.add(centre.sub(self.pos).mul(centre_attraction));

        delta = delta.add(nodes.iter()
                          .fold(Vec2::zero(), |acc, other| -> Vec2 {
                              if other.1.id!=self.id {
                                  // repel from each other  
                                  let r = self.pos.sub(other.1.pos);
                                  if r.mag()>0.0 {
                                      return acc.add(r.norm().mul(other_repel))
                                  }
                              }
                              acc
                          }));
        
        delta = delta.add(self.connections.iter()
                          .fold(Vec2::zero(), |acc, other_id| -> Vec2 {
                              if *other_id != self.id {
                                  let other = &nodes[other_id];
                                  // try 
                                  let b = other.pos.sub(self.pos);
                                  let distance = b.mag()-edge_length;
                                  return acc.add(b.norm().mul(distance*edge_attraction))
                              }
                              acc
                          }));

        if delta.mag()>max_speed { delta = delta.norm().mul(max_speed); }

        LayoutNode {
            pos: self.pos.add(delta),
            vel: delta,
            connections: self.connections.clone(),
            ..*self                
        }
    }

/*    pub fn strain(&self, centre: Vec2, nodes: &HashMap<EntityId,LayoutNode>) -> f64 {        
        if self.connections.is_empty() { return 0.; }

        self.connections.iter()
            .fold(0., |acc, other_id| -> f64 {
                if *other_id != self.id {
                    let other = &nodes[other_id];
                    // try 
                    let b = other.pos.sub(self.pos);
                    let distance = b.mag()-DEFAULT_EDGE_LENGTH;
                    return acc + distance;
                }
                acc
            }) / self.connections.len() as f64
    }
*/
}

pub fn init_layout_nodes(graph: &SocialGraph, area_box: (Vec2, Vec2)) -> HashMap<EntityId, LayoutNode> {
    let mut layout_nodes = HashMap::new();

    // fill with random positions
    for n in &graph.nodes {
        let mut connections = vec![];
        
        for r in graph.find_relationships(n.id) {
            if r.to != n.id {
                connections.push(r.to);
            } else {
                connections.push(r.from);
            }
        }

        let rndvec = Vec2::rnd();
        let area = area_box.1.sub(area_box.0);
        
        let pos = area_box.0.add(Vec2::new(rndvec.x*area.x, rndvec.y*area.y));
        
        layout_nodes.insert(
            n.id,
            LayoutNode {
                id: n.id,
                pos,
                vel: Vec2::zero(),
                connections                    
            }
        );
    }
    layout_nodes
}

pub fn deterministic_layout_nodes(graph: &SocialGraph, area_box: (Vec2, Vec2)) -> HashMap<EntityId, LayoutNode> {
    let mut layout_nodes = HashMap::new();
    
    // fill with non-random positions
    for n in &graph.nodes {
        let mut connections = vec![];
        
        for r in graph.find_relationships(n.id) {
            if r.to != n.id {
                connections.push(r.to);
            } else {
                connections.push(r.from);
            }
        }

        let area = area_box.1.sub(area_box.0);
        let mangled_id: usize = n.id.0 % 10000; 
        let pos = area_box.0.add(Vec2::new((mangled_id % area.x as usize) as f64,
                                           (mangled_id * 10 % area.y as usize) as f64));
        layout_nodes.insert(
            n.id,
            LayoutNode {
                id: n.id,
                pos,
                vel: Vec2::zero(),
                connections                    
            }
        );
    }

    layout_nodes
}

pub fn iterate_layout_nodes(
    layout_nodes: &HashMap<EntityId, LayoutNode>,
    centre: Vec2,
    friction: f64,
    edge_length: f64,
    centre_attraction: f64,
    other_repel: f64,
    edge_attraction: f64,
    max_speed: f64) -> (HashMap<EntityId, LayoutNode>, Vec2)  {

    let mut new_centre = Vec2::zero();
    
    let mut new_nodes = HashMap::new();
    for (id, n) in layout_nodes {
        let new_node = n.relax(
            centre,
            layout_nodes,
            friction,
            edge_length,
            centre_attraction,
            other_repel,
            edge_attraction,
            max_speed
        );

        if *id == PLAYER_ID {
            new_centre = new_node.pos;
        }
        
        new_nodes.insert(*id, LayoutNode {
            connections: n.connections.clone(),
            ..new_node.clone()
        });
    }        

    (new_nodes, new_centre)
}

/// Takes the socal graph and the allowed area, and 'relaxes' the
/// network to layout entities 'close' to their connected friends and
/// 'far' from unconnected entities
pub fn layout_graph(graph: &SocialGraph, area_box: (Vec2, Vec2)) -> HashMap<EntityId, Vec2> {   

    let mut layout_nodes = init_layout_nodes(graph, area_box);
    
    let centre = area_box.0.add(area_box.1.sub(area_box.0).mul(0.5));
    let mut player_centre = Vec2::zero();
    let mut iterations = 150;

    while iterations>0 {
        let (new_nodes, new_centre) = iterate_layout_nodes(
            &layout_nodes,
            centre,
            0.99,                // friction
            30.,                 // edge length
            0.01,                // centre attraction
            0.1,                 // other_repel
            0.1,              // edge_attraction
            200.0                // max speed
        );
        layout_nodes = new_nodes;
        player_centre = new_centre;
        iterations-=1;
    }

    let mut node_to_pos = HashMap::new();

    for (id, n) in &layout_nodes {
        node_to_pos.insert(
            *id,
            centre
                .add(player_centre.sub(n.pos).mul(10.))
                .clamp(area_box)
        );
    }
    
    node_to_pos
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basics() {
        let mut g = SocialGraph::new();

        g=g.load_from_block(&r#"0 0 1 0
                                   1 0 0 0
                                   0 1 0 0
                                   0 0 0 0"#);
 
        println!("{:?}", g);
        
        let positions = layout_graph(&g, (Vec2::new(0.,0.),
                                          Vec2::new(1000.,1000.)));
        
        for p in positions {
            println!("{:?}: {:?}",p.0, p.1);
        }

        

    }
}
