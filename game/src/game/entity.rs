// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use vector2math::*;
use instant::{ Duration };
use std::any::Any;
use rand::prelude::SliceRandom;
use crate::maths::vec2::{ Vec2 };
use crate::game::id::{ EntityId, PLAYER_ID, WORLD_ID };
use crate::game::message::*;
use crate::game::resources::{ Resources };
use crate::rendering::rendering::{ RenderPass };
use crate::rendering::animation::{ ClipId };
use crate::sound::synth_node_desc::{ SynthNodeDesc };

/// Describes the current state of an entity. These are globally defined to
/// allow us to share states across different entities.
#[derive(Debug,Copy,Clone,PartialEq)]
#[allow(dead_code)]
pub enum EntityState {
    Idle,
    StartApproach,
    Approach,
    StartWalkTo,
    WalkTo,
    StartAvoid,
    Avoid,
    Activity,
    Listening,
    Talking,
    Question,
    Answer,
    OpenMenu(bool),
    WaitMenu,
    Symptomatic,
    Dead,
    Describe
}

/// An arbitrary entity category to allow for fast searching/filtering
/// without the need to downcast. 
#[allow(non_snake_case)]
pub mod EntityCatFlags {
    pub const ANIMAL: u16          = 0x0001;
    pub const VEGETABLE: u16       = 0x0002;
    pub const MINERAL: u16         = 0x0004;
    pub const IMAGINARY: u16       = 0x0008;
    pub const MENU: u16            = 0x0010;
    pub const SENTIENT: u16        = 0x0020;
    pub const SOLID: u16           = 0x0040;
    pub const SOLID_TO_PLAYER: u16 = 0x0080;
    pub const MOVABLE: u16         = 0x0100;
    pub const FOCUSABLE: u16       = 0x0200;
    pub const QUIET: u16           = 0x0400;
}

pub type EntityCat = u16;
pub type EntityVec = Vec<Box<dyn Entity>>;
pub type EntityRefVec<'a> = Vec<&'a Box<dyn Entity>>;

/// Util for making downcasting possible
#[macro_export]
macro_rules! declare_any {
    () => {
        fn as_any(&self) -> &dyn Any { self }
        fn as_any_mut(&mut self) -> &mut dyn Any { self }
    };
}

/// Util to provide a dispatch function that sends messages one by one
/// to apply_message. This makes it easier to write for the entity.
#[macro_export]
macro_rules! declare_message_reader {
    () => {
        fn dispatch(&self, messages: &MessageVec, resources: &Resources) -> Box<dyn Entity> {
            messages.into_iter().fold(
                Box::new(self.clone()),
                |acc: Box<dyn Entity>, msg| {
                    acc.apply_message(msg, resources)
                }
            )          
        }
    };
}

/// An entity is a game character, lifeform or scenery object that
/// inhabits a location, and can be interacted with by other entities.
pub trait Entity {
    ////////////////////////////////////////////////////////////////////////
    // entity interface

    fn id(&self) -> EntityId;
    fn pos(&self) -> Vec2;
    fn bb(&self) -> (Vec2, Vec2) { (Vec2::zero(), Vec2::zero()) }
    fn state(&self) -> EntityState { EntityState::Idle }
    fn set_state(&mut self, _state: EntityState) {}
    fn cat(&self) -> EntityCat { EntityCatFlags::IMAGINARY }
    fn describe(&self, _resources: &Resources) -> String { "".into() }
    
    /// Returns an updated entity and a bunch of outgoing messages.
    /// Updates are called only on entities around the player.
    fn update(&self, entities: &EntityRefVec, resources: &Resources, delta: &Duration) -> (Box<dyn Entity>, MessageVec);

    /// This function returns a new version of this entity that has
    /// been updated by a single message. We get access to all the
    /// messages sent per frame, so could contain systemwide ones etc
    /// it's up to us to e.g. filter them to only listen to messages
    /// sent to us. This is called on all entities every tick, so all
    /// entities get messages sent to them even if they haven't
    /// updated recently.
    fn apply_message(&self, msg: &Message, resources: &Resources) -> Box<dyn Entity>;

    /// Render to the provided HTML5 canvas. The render pass indicates
    /// what to render and allows the engine to order rendering, for
    /// e.g. shadow passes.
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, delta: &Duration);

    fn dispatch(&self, messages: &MessageVec, resources: &Resources) -> Box<dyn Entity>;

    /////////////////////////////////////////////////////////////////////////
    // helper functions

    fn state_is_moving(&self, state: EntityState) -> bool {
        [EntityState::StartApproach,
         EntityState::StartWalkTo,
         EntityState::StartAvoid,
         EntityState::Approach,
         EntityState::WalkTo,
         EntityState::Avoid].contains(&state)
    }

    fn state_do_not_disturb(&self, state: EntityState) -> bool {
        [EntityState::Listening,
         EntityState::Talking,
         EntityState::Question,
         EntityState::Answer,
         EntityState::WaitMenu].contains(&state)
    }

    fn is_moving(&self) -> bool {
        self.state_is_moving(self.state())
    }

    fn do_not_disturb(&self) -> bool {
        self.state_do_not_disturb(self.state())
    }

    // make this simpler as we are putting them everywhere
    fn play_sound_msg(&self, resources: &Resources, sound_name: &str, speed: f64) -> Message {
        // default short duration
        self.play_sound_msg_with_duration(resources, sound_name, speed, 2.0)
    }

    fn play_sound_msg_with_duration(&self, resources: &Resources, sound_name: &str, speed: f64, duration: f64) -> Message {
        Message::new(
            MessageType::PlaySoundAt(
                self.pos().sub(resources.world_state.player_pos),
                duration, 
                SynthNodeDesc::build(
                    "sample",
                    &[
                        ("file_name", sound_name.into()),
                        ("playback_rate", speed.into())
                    ], &[])
            ), self.id(), WORLD_ID
        )            
    }

    // make this simpler as we are putting them everywhere
    fn play_looped_sound_msg(&self, id: &str, resources: &Resources, sound_name: &str, speed: f64) -> Message {
        Message::new(
            MessageType::PlaySoundAtId(
                id.to_string(),
                self.pos().sub(resources.world_state.player_pos),
                10.0, // default 'infinite' duration
                SynthNodeDesc::build(
                    "sample",
                    &[
                        ("file_name", sound_name.into()),
                        ("playback_rate", speed.into()),
                        ("loop", 1.0.into()),
//                        ("loop_start", 0.0.into()),
//                        ("loop_end", 1.0.into()),
                    ], &[])
            ), self.id(), WORLD_ID
        )            
    }

    /// Returns the nearest entity in the list of entities provided
    fn nearest<'a>(&self, entities: &'a EntityRefVec) -> Option<&'a Box<dyn Entity>> {
        self.nearest_to_pos(self.pos(), entities)
    }

    /// Returns the nearest entity to a position in the list of entities provided
    fn nearest_to_pos<'a>(&self, pos: Vec2, entities: &'a EntityRefVec) -> Option<&'a Box<dyn Entity>> {
        let mut distance = f64::MAX;
        let mut index: usize = 0;
        let mut found: bool = false;
        
        for (i, e) in entities.iter().enumerate() {
            if self.id() != e.id() {
                let d = pos.dist(e.pos());
                if d<distance {
                    distance=d;
                    index=i;
                    found=true;
                }
            }
        }

        if found {
            return Some(entities[index]);
        }

        None
    }

    /// Returns the nearest entity of the category provided from the list of entities.
    fn nearest_of<'a>(&self, cat: EntityCat, entities: &'a EntityRefVec) -> Option<&'a Box<dyn Entity>> {
        let mut distance = f64::MAX;
        let mut index: usize = 0;
        let mut found: bool = false;
        
        for (i, e) in entities.iter().enumerate() {
            if self.id() != e.id() && e.cat() == cat {
                let d = self.pos().dist(e.pos());
                if d<distance {
                    distance=d;
                    index=i;
                    found=true;
                }
            }
        }

        if found {
            return Some(entities[index]);
        }

        None
    }

    /// Returns all entities from the list provided that are within
    /// the bounding box
    fn within_box<'a>(&'a self, entities: &EntityRefVec<'a>, min: Vec2, max: Vec2) -> EntityRefVec {
        entities.iter().filter(|e| {
            e.id()!=self.id() && e.pos().inside(min,max)
        }).cloned().collect()        
    }

    /// Returns all entities from the list within the radius from our
    /// position.
    fn within_radius<'a>(&'a self, entities: &EntityRefVec<'a>, radius: f64) -> EntityRefVec {
        // optimise by filtering by bounding box before doing sqrts etc
        self.within_box(entities,
                        Vec2::new(self.pos().x-radius,self.pos().y-radius),
                        Vec2::new(self.pos().x+radius,self.pos().y+radius))
            .iter()
            .filter(|e| {
                self.pos().dist(e.pos())<radius
            })
            .cloned()
            .collect()   
    }

    /// Returns all entities that match the supplied flags within the
    /// radius from our position.
    fn within_radius_of<'a>(&'a self, entities: &EntityRefVec<'a>, radius: f64, flags: EntityCat) -> EntityRefVec {
        self.within_radius(&entities
                           .iter()
                           .filter(|e| { e.cat() & flags != 0 })
                           .cloned()
                           .collect(),
                           radius)
    }

    /// Choose a random connected entity (based on the social network)
    /// from the list provided.
    fn choose_connected<'a>(&self, entities: &EntityRefVec<'a>, connected: &[EntityId]) -> Result<&'a Box<dyn Entity>, &str> {
        Ok(*entities.iter().filter(|e| {
            connected.contains(&e.id())
        })
           .collect::<Vec<_>>()
           .choose(&mut rand::thread_rng())
           .ok_or("no connections availible")?)
    }


    /// Choose a random connected entity (based on the social network)
    /// from the list provided. If the player is connected to us, pick
    /// them first.
    fn choose_connected_prioritise_player<'a>(&self, entities: &'a EntityRefVec<'a>, connected: &[EntityId]) -> Result<&'a Box<dyn Entity>, &str> {
        if connected.contains(&PLAYER_ID) {
            if let Some(player) = self.find_entity(PLAYER_ID, entities) {
                return Ok(player);
            }
        }
        return self.choose_connected(entities, connected);
    }
        

    /// Returns all the connected entities (based on the social network) within the list provided.
    fn connected_entities<'a>(&self, entities: &EntityRefVec<'a>, connected: &[EntityId]) -> EntityRefVec<'a> {
        entities.iter().filter(|e| {
            connected.contains(&e.id())
        })
            .cloned()
            .collect::<Vec<_>>()
    }

    /// 'Internal' use for vectors of references, built by iterators
    fn nearest_refvec<'a>(&self, entities: EntityRefVec<'a>) -> Option<&'a Box<dyn Entity>> {
        if !entities.is_empty() {
            Some(entities.iter().fold((&entities[0],f64::MAX),|acc,e| {
                // check self id
                let d = self.pos().dist(e.pos());
                if d<acc.1 { (e,d) } else { acc }
            }).0)
        } else { None }
    }

    /// Returns the nearest connected entity (based on the social network).
    fn nearest_connected<'a>(&self, entities: &'a EntityRefVec, connected: &[EntityId]) -> Option<&'a Box<dyn Entity>> {
        self.nearest_refvec(entities.iter().filter(|e| {
            connected.contains(&e.id())
        }).cloned().collect())
    }

    /// Returns the nearest entity which is not connected (based on the social network) to us.
    fn nearest_unconnected<'a>(&self, entities: &'a EntityVec, connected: &[EntityId]) -> Option<&'a Box<dyn Entity>> {
        self.nearest_refvec(entities.iter().filter(|e| {
            !connected.contains(&e.id())
        }).collect::<Vec<_>>())
    }

    /// Filter the messages so only ones from connected entities remain.
    fn messages_from_connected<'a>(&self, messages: &'a MessageVec, connected: &[EntityId]) -> MessageRefVec<'a> {
        messages.iter().filter(|msg| {
            for connection in connected {
                if connection == &msg.sender { return true }
            }
            false
        }).collect()
    }

    /// Search for an entity in the provided list by ID (slow).
    fn find_entity<'a>(&self, id: EntityId, entities: &'a EntityRefVec) -> Option<&'a Box<dyn Entity>> {
       entities.iter().find(|&e| e.id()==id).copied()
    }

    /// Returns the anim clip id for up/down/left/right depending on
    /// comparing the positions between our entity and a target location
    fn anim_for_target(&self, target: Vec2) -> ClipId {
        let d = target.sub(self.pos());

        if d.y.abs()<d.x.abs() {        
            if target.x<self.pos().x {
                ClipId::WalkLeft 
            } else {
                ClipId::WalkRight
            }
        } else if target.y<self.pos().y {
            ClipId::WalkUp
        } else {
            ClipId::WalkDown
        }
    }
    
    /// Returns the anim clip id for left/right depending on comparing
    /// the positions between our entity and a target location
    fn anim_for_target_left_right(&self, target: Vec2) -> ClipId {
        if target.x<self.pos().x {
            ClipId::WalkLeft 
        } else {
            ClipId::WalkRight
        }

    }

    /// This allows us to cast to Any so we can then convert to any entity type
    fn as_any(&self) -> &dyn Any;

    /// This allows us to cast to Any so we can then convert to any entity type
    fn as_any_mut(&mut self) -> &mut dyn Any;
}

