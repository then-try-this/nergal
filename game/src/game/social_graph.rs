// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
use std::collections::HashMap;
use std::collections::HashSet;
use rand::prelude::SliceRandom;


use vector2math::*;
use crate::game::relationship::{ Relationship };
use crate::game::id::{ EntityId, PLAYER_ID };



use web_sys::console;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Node {
    pub id: EntityId,
    pub social_id: u32
}

#[derive(Debug)]
pub struct SocialGraph {
    pub nodes: Vec<Node>,
    pub relationships: Vec<Relationship>,
    pub conn_cache: HashMap<EntityId,Vec<EntityId>>,
    pub player_friend_cache: HashSet<EntityId>,
    pub social_id_cache: HashMap<EntityId,u32>
}

// can be as slow as we like...
impl Clone for SocialGraph {
    fn clone(&self) -> Self {
        SocialGraph {
            nodes: self.nodes.clone(),
            relationships: self.relationships.clone(),
            conn_cache: self.conn_cache.clone(),
            player_friend_cache: self.player_friend_cache.clone(),
            social_id_cache: self.social_id_cache.clone()
        }
    }
    
}

#[allow(dead_code)]
impl SocialGraph {
    pub fn new() -> Self {
        SocialGraph {
            nodes: vec![],
            relationships: vec![],
            conn_cache: HashMap::new(),
            player_friend_cache: HashSet::new(),
            social_id_cache: HashMap::new()
        }
    }

    pub fn build(nodes: Vec<Node>, relationships: Vec<Relationship>) -> Self {
        SocialGraph {
            nodes,
            relationships,
            conn_cache: HashMap::new(),
            player_friend_cache: HashSet::new(),
            social_id_cache: HashMap::new()
        }
    }

    pub fn pretty_print(&self) {
        for node in &self.nodes {
            let mut text = format!("node {:?}", node.id);
            let mut count: u32 = 0;
            for _r in self.find_relationships(node.id) {
                count+=1;
            }
            text = format!("{} : {}", text, count);
            console::log_1(&text.into());
        }
    }
    
    pub fn find_node(&self, id: EntityId) -> Option<&Node> {
        self.nodes.iter().find(|&n| n.id == id)
    }

    pub fn find_relationship(&self, rel: &Relationship) -> Option<&Relationship> {
        self.relationships.iter().find(|&r| r.from == rel.from && r.to == rel.to)
    }
    
    pub fn find_node_by_social_id(&self, social_id: u32) -> Option<&Node> {
        self.nodes.iter().find(|&n| n.social_id == social_id)
    }

    pub fn find_relationships(&self, entity_id: EntityId) -> Vec<&Relationship> {
        self.relationships.iter().filter(|r| {
            r.from == entity_id || r.to == entity_id             
        }).collect()
    }

    pub fn find_relationship_mut(&mut self, from: EntityId, to: EntityId) -> Option<&mut Relationship> {
        self.relationships.iter_mut().find(|n| (n.from == from && n.to == to) || (n.from == to && n.to == from))
    }

    pub fn clear(&mut self) {
        self.relationships = vec![];
        self.conn_cache = HashMap::new();
        self.player_friend_cache = HashSet::new();
    }

    pub fn add(&mut self, from: EntityId, to: EntityId) {
        self.relationships.push(Relationship::new(from, to));       
    }

    pub fn remove(&mut self, from: EntityId, to: EntityId) {
        let nr = self.relationships.clone().into_iter().filter(|rel| {
            rel.from != from || rel.to != to           
        }).collect();
        self.relationships = nr;
    }

    pub fn merge(&mut self, b: SocialGraph) {
        for n in b.nodes {
            if self.find_node(n.id).is_none() {
                self.nodes.push(n);
            }
        }

        for r in b.relationships {
            if self.find_relationship(&r).is_none() {
                self.relationships.push(r);
            }
        }
    }
    
    pub fn ego_graph(&self, entity_id: EntityId, distance: u32) -> Self {
        if let Some(node) = self.find_node(entity_id) {
            let mut g = SocialGraph::build(vec![*node],vec![]);
            if distance > 0 {
                for rel in self.find_relationships(entity_id) {
                    g.relationships.push(rel.clone());
                    g.merge(self.ego_graph(
                        rel.to,
                        distance-1
                    ));
                }
            }
            g
        } else {
            SocialGraph::new()
        }
    }
    
    /// Replace a random node with the player id
    pub fn insert_player_id(&mut self) {
        let mut rng = rand::thread_rng();
        let replace_node: &mut Node = self.nodes.choose_mut(&mut rng).unwrap();

        for i in 0..self.relationships.len() {
            if self.relationships[i].from == replace_node.id {
                self.relationships[i].from = PLAYER_ID;
            }
            if self.relationships[i].to == replace_node.id {
                self.relationships[i].to = PLAYER_ID;
            }
        }

        replace_node.id = PLAYER_ID;
    }

    
/*    pub fn disconnect_all_from(&mut self, id: EntityId) {
        self.relationships = self.relationships.into_iter().filter(|rel| {
            rel.from != id && rel.to != id           
        }).collect();
    }*/

    pub fn generate_cache(&mut self) {
        self.conn_cache = HashMap::new();
        self.player_friend_cache = HashSet::new();
        
        for i in 0..self.relationships.len() {
            self.cache_entity_id(self.relationships[i].from);
            self.cache_entity_id(self.relationships[i].to);

            if self.relationships[i].from == PLAYER_ID {
                self.player_friend_cache.insert(self.relationships[i].to);
            }
            
            if self.relationships[i].to == PLAYER_ID {
                self.player_friend_cache.insert(self.relationships[i].from);
            }
        }

        self.social_id_cache = HashMap::new();
        for n in &self.nodes {
            self.social_id_cache.insert(n.id, n.social_id);
        }
    }
    
    pub fn cache_entity_id(&mut self, entity_id: EntityId) {
        let connections = self.relationships.iter()
            .fold(vec![], |mut acc, edge: &Relationship| {           
                if edge.from==entity_id {
                    acc.push(edge.to);
                }
                //                        if edge.to==entity_id {
                //                            acc.push(edge.from);
                //                        }
                acc
            });
        self.conn_cache.insert(entity_id,connections);
    }

    pub fn all_connected_entity_ids(&self, entity_id: EntityId) -> Vec<EntityId> {
        if let Some(connections) = self.conn_cache.get(&entity_id) {
            connections.to_vec()                
        } else {
            vec![]
        }
    }

    pub fn is_player_connected(&self, entity_id: EntityId) -> bool {
        self.player_friend_cache.get(&entity_id).is_some()
    }
/*
    pub fn load_from_graphml(&self, xml: &String) -> Self {

        
        let mut g = SocialGraph::new();
        
        let mut reader = Reader::from_str(xml);
        reader.trim_text(true);
        reader.expand_empty_elements(true);

        let mut txt = Vec::new();
        let mut buf = Vec::new();

        // The `Reader` does not implement `Iterator` because it outputs borrowed data (`Cow`s)
        loop {
            // NOTE: this is the generic case when we don't know about the input BufRead.
            // when the input is a &str or a &[u8], we don't actually need to use another
            // buffer, we could directly call `reader.read_event()`
            match reader.read_event_into(&mut buf) {
                Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
                // exits the loop when reaching end of file
                Ok(Event::Eof) => break,

                Ok(Event::Start(e)) => {
                    match e.name().as_ref() {
                        b"node" => {
                            let mut id: String="".to_string();
                            
                            for attr in e.attributes() {
                                let a = attr.unwrap(); 
                                let name = std::str::from_utf8(a.key.as_ref()).unwrap();
                                let value = std::str::from_utf8(&a.value).unwrap();

                                if name=="id" { id = value.to_string(); }                                
                            }          

                            g.nodes.push( Node{
                                id: EntityId::new(),
                                social_id: id
                            });
                        },
                        b"edge" => {
                            let mut target: String="".into();
                            let mut source: String="".into();

                            for attr in e.attributes() {
                                let a = attr.unwrap(); 
                                let name = std::str::from_utf8(a.key.as_ref()).unwrap();
                                let value = std::str::from_utf8(&a.value).unwrap();
                                
                                if name=="source" { source = value.to_string(); }                                
                                if name=="target" { target = value.to_string(); }                                                                
                            }
                            
                            g.relationships.push(Relationship{
                                from: g.find_node_by_name(source).unwrap().id,
                                to: g.find_node_by_name(target).unwrap().id,
                                strength: 1
                            });
                            
                        },
                        _ => (),
                    }
                }
                
                Ok(Event::Text(e)) => {
                    //println!("{}",e.unescape().unwrap().into_owned());
                    txt.push(e.unescape().unwrap().into_owned())
                },

                // There are several other `Event`s we do not consider here
                _ => (),
            }
            // if we don't keep a borrow elsewhere, we can clear the buffer to keep memory usage low
            buf.clear();
        }
        
        g
    }
     */
    
    pub fn load_from_block(&self, text: &str) -> Self {        
        let mut g = SocialGraph::new();

        // first read the lines adding all the nodes
        for (id,txt) in text.split('\n').enumerate() {
            if !txt.is_empty() {
                g.nodes.push( Node{
                    id: EntityId::new(),
                    social_id: id as u32
                });                
            }
        }

        // now start again adding the relationships
        for (parent_id, line) in text.split('\n').enumerate() {
            for (child_id, char) in line.split_whitespace().enumerate() {
                if char=="1" {
                    //println!("{}->{}",parent_id,child_id);
                    if let Some(parent_node) = g.find_node_by_social_id(parent_id as u32) {
                        if let Some(child_node) = g.find_node_by_social_id(child_id as u32) {
                                                     
                            g.relationships.push(Relationship{                        
                                from: parent_node.id,
                                to: child_node.id
                            });
                        } else {
                            console::log_1(&format!("could not find child node: {}", child_id).into());
                        }
                    } else {
                        console::log_1(&format!("could not find parent node: {}", parent_id).into());
                    }
                }
            }
        }
        
        g
    }
    
    
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::relationship::{Relationship};
    
    #[test]
    fn basics() {
        let mut g = SocialGraph::new();
        g.relationships.push(Relationship::new(1.into(),2.into()));
        g.generate_cache();
        
        let conn = g.all_connected_entity_ids(1.into());
        assert_eq!(conn.len(), 1);
        assert_eq!(conn[0], 2.into());

        g.clear();

        assert_eq!(g.relationships.is_empty(),true);

//         g=g.load_from_graphml(&r#"<?xml version='1.0' encoding='utf-8'?>
// <graphml>
//   <key attr.name="weight" attr.type="double" for="edge" id="d0" />
//   <graph edgedefault="undirected">
//     <node id="spike" />
//     <node id="quickeaze" />
//     <edge source="spike" target="quickeaze">
//       <data key="d0">0.0010245902</data>
//     </edge>
//   </graph>
// </graphml>"#.to_string());
        
//         assert_eq!(g.relationships.len(),1);
//         assert_eq!(g.nodes.len(),2);

        g.clear();

        g=g.load_from_block(&r#" 0 1 1 1 1
1 0 1 1 1
0 1 0   1 1
  1 1 1 0 1
1 1 1 1 0"#.to_string());
        
        assert_eq!(g.relationships.len(),19);
        assert_eq!(g.nodes.len(),5);

        let e = g.ego_graph(g.nodes[0].id, 1);
        println!("hello");
        println!("{:?}", e);

        
        g.clear();

        g=g.load_from_block(&r#"0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1
1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0"#.to_string());
        
        assert_eq!(g.relationships.len(),32*32-32);
        assert_eq!(g.nodes.len(),32);

        

        
    }
}
