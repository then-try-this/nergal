// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use core::cmp::Ordering;
use std::collections::HashMap;
use instant::{ Duration };
use crate::game::bucket::Bucket;
use crate::game::id::{ EntityId };
use crate::game::message::{ MessageVec };
use crate::game::entity::{ Entity, EntityRefVec };
use crate::game::resources::{ Resources };
use crate::rendering::rendering::{ RenderPass };
use crate::maths::vec2::Vec2;
use crate::sound::sound::{ Sound };

use vector2math::*;

use web_sys::console;

/// A spatial container for all the entities. Designed to optimise
/// entity updates (by only supplying local surrounding entities to
/// each entity update) as well as speeding up searching/lookup.
pub struct BucketGrid {
    pub width: usize,
    pub height: usize,
    pub bucket_size: Vec2,
    pub grid: Vec<Bucket>,
    pub num_crossings: u32,
    pub entity_to_address: HashMap<EntityId,usize>
}

#[allow(dead_code)]
impl BucketGrid {
    pub fn new(width: usize, height: usize, bucket_size: Vec2) -> Self {
        let mut grid = vec![];
        let mut index = 0;
        for y in 0..height {
            for x in 0..width {
                grid.push(Bucket::new(
                    index,
                    Vec2::new(
                        bucket_size.x * x as f64,
                        bucket_size.y * y as f64
                    ),
                    Vec2::new(
                        bucket_size.x * (x + 1) as f64,
                        bucket_size.y * (y + 1) as f64,
                    ),                    
                ));
                index += 1;
            }
        }
        BucketGrid {
            width,
            height,
            bucket_size,
            grid,
            num_crossings: 0,
            entity_to_address: HashMap::new()                
        }
    }

    /// Create a bucket address from and x,y coordinate.
    pub fn address_xy(&self, x: usize, y: usize) -> usize {
        y * self.width + x
    }

    /// Create a bucket x,y coordinate from a position.
    pub fn address_tuple(&self, pos: Vec2) -> (usize,usize) {
        ((pos.x / self.bucket_size.x) as usize,
         (pos.y / self.bucket_size.y) as usize)
    }

    /// Get the bucket address for a given position.
    pub fn address(&self, pos: Vec2) -> usize {
        let (x, y) = self.address_tuple(pos);
        self.address_xy(x, y)
    }

    /// Insert the supplied entity into the correct bucket.
    pub fn insert(&mut self, e: Box<dyn Entity>) {
        let (x, y) = self.address_tuple(e.pos());
        if x < self.width && y < self.height {
            let addr = self.address_xy(x, y);
            // add to/update the lookup hashmap
            self.entity_to_address.insert(e.id(),addr);
            self.grid[addr].entities.push(e);
        } else {
            console::log_1(&"entity out of bounds!".into());
        }
    }

    /// Remove the entity from it's containing bucket.
    pub fn remove_entity(&mut self, id: EntityId) {
        if let Ok(entity) = self.get_entity(id) {
            let address = self.address(entity.pos());
            self.grid[address].remove_entity(id);
        }
    }

    /// Returns a vector of addresses of the 9 buckets containing and
    /// surrounding this location. Todo: does not work for edges
    /// currently.
    pub fn get_surrounding(&self, pos: Vec2) -> Vec<usize> {
        let (x, y) = self.address_tuple(pos);
        if x>0 && y>0 && x<self.width-1 && y<self.height-1 {        
            // return 9 surrounding cell indices to the centre one
            [self.address_xy(x-1,y-1), self.address_xy(x,y-1), self.address_xy(x+1,y-1),
             self.address_xy(x-1,y), self.address_xy(x,y), self.address_xy(x+1,y),
             self.address_xy(x-1,y+1), self.address_xy(x,y+1), self.address_xy(x+1,y+1),
             ].into()
        } else {
            // 
            vec![]
        }        
    }

    /// Returns a list of all the entities in the 9 buckets containing
    /// and surrounding the given position.
    pub fn get_surrounding_entities(&self, pos: Vec2) -> EntityRefVec {
        self.get_surrounding(pos).iter().fold(vec![],|mut acc,b| {
            // need to convert to a mut refvec temporary explicitly:
            // a) so we can so we consume it in the append, which moves
            //    from one vec to the other
            // b) as we need a refvec anyway so we don't need to copy data
            // also - iter() does the job for us:
            let mut temp = self.grid[*b].entities.iter().collect();
            acc.append(&mut temp); acc
        })
    }

    /// A slow lookup by id of an entity.
    pub fn get_entity(&self, id: EntityId) -> Result<&Box<dyn Entity>, &str> {
        // use the entitiy_to_address lookup to quickly find the bucket
        // containing the entitiy, then search within that
        self.grid[*self.entity_to_address.get(&id)
                  .ok_or("entity not in lookup")?].get_entity(id)
    }

    /// A slow lookup by id of an entity.
    pub fn get_entity_mut(&mut self, id: EntityId) -> Result<&mut Box<dyn Entity>, &str> {        
        self.grid[*self.entity_to_address.get(&id)
                  .ok_or("entity not in lookup")?].get_entity_mut(id)
    }

    /// A slow lookup by id of an entity and return it as the given type.
    pub fn get_entity_as<T: 'static>(&self, id: EntityId) -> Result<&T, &str> {
        let a = self.get_entity(id)?.as_any();
        let p = a.downcast_ref::<T>();
        match p {
            Some(pe) => Ok(pe),
            None => Err("not found"),
        }
    }

    /// A slow lookup by id of an entity and return it as the given type.
    pub fn get_entity_mut_as<T: 'static>(&mut self, id: EntityId) -> Result<&mut T, &str> {
        let a = self.get_entity_mut(id)?.as_any_mut();
        let p = a.downcast_mut::<T>();
        match p {
            Some(pe) => Ok(pe),
            None => Err("not found"),
        }
    }

    // /// Updates all the entities surrounding the player and all the movable entities elsewhere.
    // pub fn update_clever(&mut self, resources: &Resources, sound: &mut Sound, delta: &Duration) -> MessageVec {
    //     let mut messages = vec![];
    //     // only update characters we can see
    //     // todo: update out of sight entities at a slower rate?
    //     let surrounding = self.get_surrounding(resources.world_state.player_pos);

    //     for index in &surrounding {
    //         let b = &self.grid[*index];
    //         let centre = b.min.add(b.max).div(2.0);
    //         let (new_b,new_messages) = b.update(self.get_surrounding_entities(centre), resources, sound, delta);
    //         messages.extend(new_messages);
    //         self.grid[*index] = new_b;
    //     }

    //     for index in 0..self.width * self.height {
    //         if !surrounding.contains(&index) {
    //             let b = &self.grid[index];
    //             let centre = b.min.add(b.max).div(2.0);
    //             let (new_b, new_messages = b.update_movable(self.get_surrounding_entities(centre), resources, sound, delta);
    //             messages.extend(new_messages);
    //             self.grid[index] = new_b;
    //         }
    //     }

    //     messages         
    // }

    /// Updates all the buckets surrounding and containing the player.
    pub fn update_partial(&mut self, resources: &Resources, sound: &mut Sound, delta: &Duration) -> MessageVec {
        let mut messages = vec![];
        // only update characters we can see
        // todo: update out of sight entities at a slower rate?
        let surrounding = self.get_surrounding(resources.world_state.player_pos);

        for index in surrounding {
            let b = &self.grid[index];
            let centre = b.min.add(b.max).div(2.0);
            let (new_b,new_messages) = b.update(self.get_surrounding_entities(centre), resources, sound, delta);
            messages.extend(new_messages);
            self.grid[index] = new_b;
        }
        
        messages         
    }

    /// Dispatches messages to all the entities near the player
    pub fn dispatch_partial(&mut self, messages: &MessageVec, resources: &Resources) {
        // all players get updated here, in sight or not
        let surrounding = self.get_surrounding(resources.world_state.player_pos);

        for index in surrounding {
            let b = &self.grid[index];
            self.grid[index] = b.dispatch(messages, resources);
        }            
    }    
    
    /// Updates all the buckets surrounding and containing the player.
    pub fn update_all(&mut self, resources: &Resources, sound: &mut Sound, delta: &Duration) -> MessageVec {
        let mut messages = vec![];

        for index in 0..self.width * self.height {
            let b = &self.grid[index];
            let centre = b.min.add(b.max).div(2.0);
            let (new_b,new_messages) = b.update(self.get_surrounding_entities(centre), resources, sound, delta);
            messages.extend(new_messages);
            self.grid[index] = new_b;
        }
        
        messages         
    }

    /// Dispatches messages to all the entities in the grid,
    /// surrounding the player or far away.
    pub fn dispatch_all(&mut self, messages: &MessageVec, resources: &Resources) {
        // all players get updated here, in sight or not
        for index in 0..self.width * self.height {
            let b = &self.grid[index];
            self.grid[index] = b.dispatch(messages, resources);
        }            
    }    

    /// Update the grid based on who has moved outside of their current buckets.
    pub fn update_grid(&mut self) {
        let mut i = 0;
        self.num_crossings = 0;
        // iterating by index seems simpler than using iterators with borrow check
        while i < self.grid.len() {
            for e in self.grid[i].scan_and_remove() {
                self.insert(e);
                // for stats
                self.num_crossings+=1;
            }
            i += 1;
        }
    }

    /// Render all the entities surrounding the player in 'depth' order
    pub fn render(&self, context: &web_sys::CanvasRenderingContext2d, player_pos: Vec2, pass: RenderPass, resources: &Resources, delta: &Duration) {
        let mut to_render = self.get_surrounding_entities(player_pos);

        // sort from top to bottom for rendering order
        to_render.sort_by(|a,b| match a.pos().y.partial_cmp(&b.pos().y) {
            Some(ord) => ord,
            None => Ordering::Less
        });
        
        for entity in to_render {
            entity.render(context,pass,resources,delta);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::entities::social_entity::SocialEntity;
    use crate::rendering::animation::AnimId;

    #[test]
    fn bucket_grid() {
        let mut bg = BucketGrid::new(5, 5, Vec2::new(50.0, 50.0));
        bg.insert(Box::new(SocialEntity::at_pos(1.into(), Vec2::new(55.0, 55.0), AnimId::StickCharacter)));

        assert_eq!(bg.grid[bg.address(Vec2::new(55.0, 55.0))].entities.len(), 1);
        assert_eq!(bg.grid[bg.address(Vec2::new(15.0, 55.0))].entities.len(), 0);

        bg.insert(Box::new(SocialEntity::at_pos(2.into(), Vec2::new(55.0, 55.0), AnimId::StickCharacter)));
        assert_eq!(bg.grid[bg.address(Vec2::new(55.0, 55.0))].entities.len(), 2);

        bg.update_grid();
        assert_eq!(bg.num_crossings,0);
        bg.update_grid();
        assert_eq!(bg.num_crossings,0);
        
        // move the entity
        bg.get_entity_mut_as::<SocialEntity>(1.into()).unwrap().pos.x = 15.0;

        bg.update_grid();
        assert_eq!(bg.num_crossings,1);
        assert_eq!(bg.grid[bg.address(Vec2::new(55.0, 55.0))].entities.len(), 1);
        assert_eq!(bg.grid[bg.address(Vec2::new(15.0, 55.0))].entities.len(), 1);
        bg.update_grid();
        assert_eq!(bg.grid[bg.address(Vec2::new(55.0, 55.0))].entities.len(), 1);
        assert_eq!(bg.grid[bg.address(Vec2::new(15.0, 55.0))].entities.len(), 1);
        assert_eq!(bg.num_crossings,0);

        bg.insert(Box::new(SocialEntity::at_pos(3.into(), Vec2::new(105.0, 55.0), AnimId::StickCharacter)));
        bg.insert(Box::new(SocialEntity::at_pos(3.into(), Vec2::new(102.0, 55.0), AnimId::StickCharacter)));

        assert_eq!(bg.get_surrounding_entities(Vec2::new(55.0, 55.0)).len(),4); 
    }
}
