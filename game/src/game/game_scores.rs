// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashSet;
use crate::game::id::*;
use crate::game::message::*;
use crate::game::message::MessageType::*;
use serde::Deserialize;
/// An entity that renders lines of text.
use crate::gui::highlight_text_entity::{ TextBlock };

#[derive(Debug,Copy,PartialEq,Clone,Deserialize)]
pub struct ScoreValues {
    pub times_ill: f64,
    pub nergals_infected: f64,
    pub nergals_talked_to: f64,
    pub num_friends_made: f64,
    pub num_friends_at_end: f64,
    pub lifetime: f64,
    pub snacks_eaten: f64,
    pub plants_talked_to: f64,
    pub clouds_talked_to: f64,
    pub music_made: f64
}

impl ScoreValues {
    pub fn new() -> Self {
        ScoreValues {
            times_ill: 0.,
            nergals_infected: 0.,
            nergals_talked_to: 0.,
            num_friends_made: 0.,
            num_friends_at_end: 0.,
            lifetime: 0.,
            snacks_eaten: 0.,
            plants_talked_to: 0.,
            clouds_talked_to: 0.,
            music_made: 0.
        }        
    }

    pub fn from_scores(scores: GameScores) -> Self {
        ScoreValues {
            times_ill: scores.times_ill.into(),
            nergals_infected: scores.nergals_infected.into(),
            nergals_talked_to: scores.nergals_talked_to.len() as f64,
            num_friends_made: scores.num_friends_made.into(),
            num_friends_at_end: scores.num_friends_at_end.into(),
            lifetime: scores.lifetime as f64 / 1000.0,
            snacks_eaten: scores.snacks_eaten.into(),
            plants_talked_to: scores.plants_talked_to.into(),
            clouds_talked_to: scores.clouds_talked_to.into(),
            music_made: scores.music_made.into()            
        }        
    }

}

#[derive(Debug, Clone)]
pub struct GameScores {
    pub times_ill: u32,
    pub nergals_infected: u32,
    pub nergals_talked_to: HashSet<EntityId>,
    pub num_friends_made: u32,
    pub num_friends_at_end: u32,
    pub lifetime: u128,
    pub snacks_eaten: u32,
    pub plants_talked_to: u32,
    pub clouds_talked_to: u32,
    pub music_made: u32
}

fn pluralify(count: usize, name: &str) -> String {
    if count == 1 {
        format!("{} {}", count, name)
    } else {
        format!("{} {}s", count, name)
    }
}    

impl GameScores {
    pub fn new() -> Self {
        GameScores {
            times_ill: 0,
            nergals_infected: 0,
            nergals_talked_to: HashSet::new(),
            num_friends_made: 0,
            num_friends_at_end: 0,
            lifetime: 0,
            snacks_eaten: 0,
            plants_talked_to: 0,
            clouds_talked_to: 0,
            music_made: 0 // todo
        }
    }

    pub fn update(&mut self, msg: &Message) {
        match &msg.data {
            InfectionSuccess(source) => {
                if msg.sender == PLAYER_ID {
                    self.times_ill += 1
                } else if *source == PLAYER_ID {
                    self.nergals_infected += 1
                }
            },            
            // Question(cursor) => {
            //     if msg.recipient != PLAYER_ID {
            //         self.nergals_talked_to.insert(msg.recipient);
            //     }
            // }
            AddRelationship(_id) => {
                self.num_friends_made += 1;
            }
            TalkTo(id, entity, _pos) => {
                match entity.as_str() {
                    "cloud" => { self.clouds_talked_to += 1; }
                    "plant" => { self.plants_talked_to += 1; }
                    "nergal" => {
                        self.nergals_talked_to.insert(*id);
                    }
                    _ => {}                        
                }
            }
            SendGift(_snack, recipient) => {
                if *recipient == PLAYER_ID {
                    self.snacks_eaten += 1;
                }
            }
            _ => {}
        }
    }

    pub fn diagnostic(&self) -> &str {
        if self.times_ill < 2 {
            if self.snacks_eaten == 0  {
                "You didn't eat any snacks, and died - you can talk to friends to get snacks!"
            } else {
                "You died - you might need to talk to more friends to get more snacks!"
            }
        } else {
            if self.snacks_eaten == 0  {
                "You died - remember to talk to friends to get snacks, and be careful of catching that nasty disease!"
            } else {
                "You died - be careful of catching that nasty disease!"
            }           
        }
    }
    
    pub fn interpret_sociability_1(&self, averages: &ScoreValues) -> Vec<TextBlock> {
        vec![
            TextBlock::new("You're ".into(), false),
            TextBlock::new(if self.nergals_talked_to.len() as f64 > averages.nergals_talked_to {
                "more sociable".into()
            } else {
                "less sociable".into()
            }, true),
            TextBlock::new(" than most Nergals.".into(), false)
        ]                    
    }
    
    pub fn interpret_sociability_2(&self, _averages: &ScoreValues) -> String {
        format!("You talked to {}, and made {}.",
                pluralify(self.nergals_talked_to.len(), "Nergal"),
                pluralify(self.num_friends_made as usize, "friend"))
    }
    
    pub fn interpret_health_1(&self, averages: &ScoreValues) -> Vec<TextBlock> {
        vec![
            TextBlock::new("You're ".into(), false),
            TextBlock::new(if self.times_ill as f64 > averages.times_ill {
                "more sickly".into()
            } else {
                "less sickly".into()
            }, true),
            TextBlock::new(" than most Nergals.".into(), false)
        ]
    }

    pub fn interpret_health_2(&self, _averages: &ScoreValues) -> String {
        format!("You lived for {} seconds, got sick {} and infected {}.",
                (self.lifetime as f64 / 1000.) as u32,
                pluralify(self.times_ill as usize, "time"),
                pluralify(self.nergals_infected as usize, "other Nergal")
        )
    }

    pub fn interpret_other(&self, averages: &ScoreValues, other_type: &str) -> Vec<TextBlock> {
        match other_type {
            "snacks" => vec![
                TextBlock::new("You were ".into(), false),
                TextBlock::new(if self.snacks_eaten as f64 > averages.snacks_eaten {
                    "more hungry".into()
                } else {
                    "less hungry".into()
                }, true),
                TextBlock::new(format!(" than most Nergals, eating {} snacks.",
                                       self.snacks_eaten),
                               false)
            ],
            
            "clouds" => vec![
                TextBlock::new("You talked to ".into(), false),
                TextBlock::new(if self.clouds_talked_to as f64 > averages.clouds_talked_to {
                    "more clouds".into()
                } else {
                    "fewer clouds".into()
                }, true),
                TextBlock::new(" than most Nergals.".into(), false)
            ],

            "plants" => vec![
                TextBlock::new("You talked to ".into(), false),
                TextBlock::new(if self.plants_talked_to as f64 > averages.plants_talked_to {
                    "more plants".into()
                } else {
                    "fewer plants".into()
                }, true),
                TextBlock::new(" than most Nergals.".into(), false)
            ],

            _ => vec![TextBlock::new("".into(), false)]
        }

    }

}
