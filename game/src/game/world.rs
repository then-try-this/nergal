// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
use std::rc::Rc;
use rand::Rng;
use instant::{ Duration };
use vector2math::*;
use rand::prelude::SliceRandom;
use std::collections::HashMap;
use crate::game::id::*;
use crate::game::message::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::graph_layout::*;
use crate::game::entity::*;
use crate::game::inventory::*;
use crate::game::inventory_item::*;
use crate::game::bucket_grid::{ BucketGrid };
use crate::game::resources::{ Resources };
use crate::rendering::rendering::*;
use crate::rendering::animation::{ AnimId };
use crate::game::constants::*;
use crate::game::world_state::{ MenuHolder, WorldPhase };

use crate::entities::social_entity::{ SocialEntity };
use crate::entities::player_entity::{ PlayerEntity };
use crate::entities::particle_entity::{ ParticleEntity, Movement };
use crate::entities::gift_entity::{ GiftEntity };

use crate::gui::bubble_entity::{ BubbleEntity };
use crate::gui::menu_entity::{ MenuEntity };
use crate::gui::menu_item::{ MenuItem };

use crate::sound::sound::{ Sound };
use crate::sound::speech::{ Voice };
use crate::nergal::*;

use crate::items::disease_item::*;

#[allow(unused)]
use web_sys::console;

/// The main game world, containing all the characters and background
/// scenery.
pub struct World {
    pub entity_grid: BucketGrid,
    pub num_messages: u32,
    pub world_offset: Vec2,
    pub world_centre: Vec2,
    pub time_since_infection: u128,
    pub time_since_player_health: u128,
    pub player_has_been_infected: bool,
    pub have_infected_friend: bool
}

#[allow(dead_code)]
impl World {

    pub fn new() -> Self {
        World {
            entity_grid: BucketGrid::new(1,1,Vec2::zero()),
            num_messages: 0,
            world_centre: Vec2::zero(),
            world_offset: Vec2::zero(),
            time_since_infection: 0,
            time_since_player_health: 0,
            player_has_been_infected: false,
            have_infected_friend: false
        }        
    }

    /// Build the bucket grids and initially populate them with
    /// (plant) entities and scenery.
    pub fn create(resources: &mut Resources, sound: &Sound) -> Self {
        // todo: move game specific stuff to lib.rs
        let num_buckets_x = 15;
        let num_buckets_y = 15;
        let bucket_size = Vec2::new(resources.world_state.screen_size.x/1.5, 800.);
        
        let world_size_x = num_buckets_x as f64 * bucket_size.x;
        let world_size_y = num_buckets_y as f64 * bucket_size.y;

        resources.world_state.world_size.x = world_size_x;
        resources.world_state.world_size.y = world_size_y;

        resources.world_state.accessible_area = (
            bucket_size, 
            resources.world_state.world_size
                .sub(Vec2::new(1.,1.))
                .sub(bucket_size)
        );
        
        
        let centre =  Vec2::new(world_size_x/2.,world_size_y/2.);
        
        let _bg = BucketGrid::new(num_buckets_x,num_buckets_y,bucket_size);        

        // create the entities into a flat vec
        let entity_vec = nergal_entities(resources, sound, &centre);

        // insert them into the bucket grid
        let mut bg = BucketGrid::new(num_buckets_x,num_buckets_y,bucket_size);                
        for e in entity_vec {
            //console::log_1(&format!("{:?}",e.pos()).into());
            bg.insert(e);                              
        }
        
        World {
            entity_grid: bg,
            num_messages: 0,
            world_centre: centre,
            world_offset: Vec2::zero(),
            time_since_infection: 0,
            time_since_player_health: 0,
            player_has_been_infected: false,
            have_infected_friend: false
        }        
    }

    pub fn check_and_infect(&mut self, resources: &Resources) -> MessageVec {
        match resources.world_state.phase() {
            WorldPhase::Quiet => { vec![] },
            WorldPhase::Normal | WorldPhase::Manic => {
                // check all entities around the player's location
                let mut ids = vec![];
                let mut rng = rand::thread_rng();
                
                for e in &self.entity_grid.get_surrounding_entities(resources.world_state.player_pos) {
                    if e.cat() & EntityCatFlags::SENTIENT != 0 {
                        if let Some(a) = e.as_any().downcast_ref::<SocialEntity>() {
                            if !a.filter_items(InventoryItemType::Disease).is_empty() {
                                self.time_since_infection = 0;
                                return vec![];
                            }
                            
                            if rng.gen_range(0..100) < INFECT_PROB_PER_NERGAL {
                                ids.push(a.id());
                            }
                        }
                        else if let Some(a) = e.as_any().downcast_ref::<PlayerEntity>() {
                            if !a.filter_items(InventoryItemType::Disease).is_empty() {
                                self.time_since_infection = 0;
                                return vec![];
                            }
                            //     ids.push(a.id());
                        }                
                    }
                }

                // if none are infected, choose a random individual from the
                // whole population to infect
                if ids.is_empty() {
                    // possible there are none near the player
                    vec![]
                } else {
                    vec![
                        Message::new(
                            MessageType::Infect(DiseaseStrain::DiseaseA, WORLD_ID),
                            WORLD_ID,
                            *ids.choose(&mut rand::thread_rng()).unwrap()
                        )
                    ]
                }
            }
        }
    }
    
    /// Is there an entity in this location (roughly)
    pub fn entity_here(&self, pos: Vec2) -> bool {
        for e in self.entity_grid.get_surrounding_entities(pos) {
            if e.cat() & EntityCatFlags::SENTIENT != 0 && pos.inside_box(e.bb()) {
                return true;
            }
        }
        false
    }

    /// Populate the world with a load of connected (or not) entities
    /// built from a social network.
    pub fn update_entities_from_graph(&mut self, resources: &mut Resources, player_char: AnimId) {
        let mut entity_ids = vec![];

        let area_box = resources.world_state.accessible_area;

        let positions = layout_graph(&resources.social_graph, area_box);

        let character = match player_char {
            AnimId::StickStetsonCharacter => AnimId::StickCharacter,
            AnimId::StickPartyCharacter => AnimId::StickCharacter,
            AnimId::StickAcornCharacter => AnimId::StickCharacter,
            AnimId::CloudPussyCharacter => AnimId::CloudCharacter,
            AnimId::CloudStetsonCharacter => AnimId::CloudCharacter,
            AnimId::CloudBobbleCharacter => AnimId::CloudCharacter,
            AnimId::EggBobbleCharacter => AnimId::EggCharacter,
            AnimId::EggAcornCharacter => AnimId::EggCharacter,
            AnimId::EggPussyCharacter => AnimId::EggCharacter,
            AnimId::GhostShroomCharacter => AnimId::GhostCharacter,
            AnimId::GhostPartyCharacter => AnimId::GhostCharacter,
            _ => AnimId::EggCharacter
        };
        
        resources.world_state.player_character = player_char;
        
        // add new entities
        for node in &resources.social_graph.nodes {            
            // make sure we haven't made this entity already
            match self.entity_grid.get_entity(node.id) {
                Ok(_) => (),
                Err(_) => {
                    if node.id == PLAYER_ID {
                        self.entity_grid.insert(
                            Box::new(PlayerEntity::new(
                                node.id,
                                positions[&node.id],
                                player_char
                            ))
                        );
                    }  else {
                        let e = SocialEntity::at_pos(
                            node.id,
                            positions[&node.id],
                            character
                        );
                        self.entity_grid.insert(Box::new(e));
                        entity_ids.push(node.id);
                    }                    
                }
            }
        }        
    }

    /// Update the world, taking a social network (which may have
    /// changed) and a list of outside messages that may contain user
    /// input. Resources are needed to proved e.g. conversation
    /// text.
    pub fn update(&mut self, outside_messages: &mut MessageVec, resources: &mut Resources, sound: &mut Sound, delta: &Duration) -> MessageVec {        
        let mut messages = vec![];
        let mut menu_gone = false;
        
        if let Ok(player) = self.entity_grid.get_entity(PLAYER_ID) {
            let player_pos=player.pos();

            self.world_offset = Vec2::new(resources.world_state.screen_size.x / 2. - player_pos.x,
                                          (resources.world_state.screen_size.y / 2. - player_pos.y) + VIEW_Y_OFFSET);

            resources.world_state.player_pos = player_pos;
            resources.world_state.player_state = player.state();
            
            // the main entity update
            messages = self.entity_grid.update_partial(resources, sound, delta);

            // append input messages from the outside
            messages.append(outside_messages);

            if self.time_since_infection > MAX_TIME_SINCE_INFECTION_MILLIS {
                messages.append(&mut self.check_and_infect(resources));
            }

            if self.time_since_player_health > MAX_TIME_SINCE_PLAYER_HEALTH_MILLIS {
                messages.push(Message::new(MessageType::HealthChange(PLAYER_HEALTH_CHANGE, HealthReason::Tired), WORLD_ID, PLAYER_ID));
                self.time_since_player_health = 0;
            }
            
            // a place to collect messages from the world to entities
            let mut world_messages = vec![];

            let mut menu_requests_from_mouse = HashMap::new();
            let mut menu_requests_from_keypress = HashMap::new();
            
            for msg in &messages {
                //console::log_1(&format!("msg {:?} from: {} to: {}", msg.data, msg.sender.0, msg.recipient.0).into());

                if matches!(msg.data, MessageType::Infect(..)) {
                    self.time_since_infection = 0;
                }

                // message snooping
                match &msg.data {
                    // catch health change to spawn particles
                    MessageType::HealthChange(change, reason) => {
                        if msg.recipient == PLAYER_ID {
                            let pos = resources.world_state.player_pos;
                            
                            if *change > 0. {
                                self.entity_grid.insert(
                                    Box::new(ParticleEntity::at_pos(
                                        EntityId::new(),
                                        pos.add(Vec2::new(0.,-100.)),
                                        resources.get_handle_from_path("/images/gui/plus.png"),
                                        Movement::Rise,
                                        change.round() as usize,
                                        5000
                                    ))
                                );
                            } else if *reason != HealthReason::Tired {
                                self.entity_grid.insert(
                                    Box::new(ParticleEntity::at_pos(
                                        EntityId::new(),
                                        pos.add(Vec2::new(0.,-100.)),
                                        resources.get_handle_from_path("/images/gui/minus.png"),
                                        Movement::Rise,
                                        (-change).round() as usize,
                                        5000
                                    ))
                                );
                            }
                        }
                    }

                    MessageType::KeyPress(key) => {
                        // launch menu of current selected entity
                        if key == " " && resources.world_state.current_entity_selection != NO_ID {
                            
                            let menu_holder = &resources.world_state.current_menu;
                            resources.world_state.menu_open = true;
                            resources.world_state.menu_id = menu_holder.id;
                            resources.world_state.current_entity_selection = NO_ID;
                            
                            let menu = Box::new(MenuEntity::new(
                                menu_holder.id,
                                menu_holder.desc.to_string(),
                                menu_holder.pos, 
                                menu_holder.y_offset,
                                resources.world_state.current_entity_selection,
                                WORLD_ID,
                                menu_holder.menu_items.clone()
                            ));
                            
                            resources.speech.say(Voice::Narrator, &menu.describe(resources));
                                
                            // only add the closest menu to the front of the screen
                            self.entity_grid.insert(menu);                                
                        }
                    }

                    MessageType::TalkTo(_, _, _) => {
                        resources.world_state.conversation_happening = true;
                    }

                    MessageType::ConversationOver => {
                        resources.world_state.conversation_happening = false;
                    }
                    
                    _ => {}
                }
                
                
                // Act on messages that need to modify, or request
                // information about the world
                // todo: move operations that do not add new messages to dispatch
                // so other things can send them
                if msg.recipient == WORLD_ID {
                    match &msg.data {
                        MessageType::RemoveMe => {
                            self.entity_grid.remove_entity(msg.sender);
                        },

                        MessageType::SpeechBubble(text, voice, recipient) => {
                            // Does the sender still exist?
                            if let Ok(sender) = self.entity_grid.get_entity(msg.sender) {
                                // hide empty speech bubbles (used to override the snack one)
                                if !text.is_empty() {
                                    let bubble = Box::new(BubbleEntity::new(
                                        EntityId::new(),
                                        // Note: this gets overriden on
                                        // the next frame when bubble
                                        // updates it's own position -
                                        // needs to be close for bucket
                                        // calculation
                                        sender.pos(),                                    
                                        sender.id(),
                                        *recipient,
                                        text
                                    ));
                                    
                                    resources.speech.say(*voice, &bubble.describe(resources));
                                    
                                    self.entity_grid.insert(bubble);
                                }
                            }
                        },

                        MessageType::Spread(strain) => {
                            // Does the sender still exist?
                            if let Ok(sender) = self.entity_grid.get_entity(msg.sender) {
                                for entity in self.entity_grid.get_surrounding_entities(sender.pos()) {

                                    let infection_likelihood = match resources.world_state.phase() {
                                        WorldPhase::Quiet => 0,
                                        WorldPhase::Normal => INFECTION_LIKELIHOOD,
                                        WorldPhase::Manic => INFECTION_LIKELIHOOD_MANIC
                                    };
                                    
                                    if rand::thread_rng().gen_range(0..100) < infection_likelihood && entity.pos().dist(sender.pos()) < INFECTION_RADIUS {
                                        world_messages.push(Message::new(
                                            MessageType::Infect(*strain, msg.sender), WORLD_ID, entity.id()
                                        ));
                                    }
                                }
                            }
                        }
                        
                        MessageType::OpenMenu(desc, menu_items, y_offset, from_key) => {
                            // todo: using y_offset is pointless as we have to get
                            // the entity in the menu update anyway
                            if let Ok(sender) = self.entity_grid.get_entity(msg.sender) {
                                if *from_key {
                                    menu_requests_from_keypress.insert(
                                        sender.id(),
                                        (sender.pos(), menu_items.clone(), y_offset, desc)
                                    );
                                } else {                                        
                                    menu_requests_from_mouse.insert(
                                        sender.id(),
                                        (sender.pos(), menu_items.clone(), y_offset, desc)
                                    );
                                }
                            }
                        },

                        MessageType::SendGift(gift_type, recipient) => {
                            // Does the sender still exist?
                            if let Ok(sender) = self.entity_grid.get_entity(msg.sender) {                                                            
                                self.entity_grid.insert(Box::new(GiftEntity::at_pos(
                                    EntityId::new(),
                                    Vec2::centre(sender.bb()),
                                    *recipient,
                                    *gift_type,
                                    resources
                                )));                            
                            }
                        },

                        MessageType::WhereIs(entity_id) => {
                            if let Ok(entity) = self.entity_grid.get_entity(*entity_id) {
                                world_messages.push(Message::new(
                                    MessageType::Response(Box::new(MessageResponse::new(
                                        MessageType::WhereIs(*entity_id),
                                        entity.pos()
                                    ))),
                                    WORLD_ID,
                                    msg.sender
                                ));
                            }
                        },

                        MessageType::AddRelationship(to) => {
                            resources.social_graph.add(msg.sender, *to);
                            resources.social_graph.add(*to, msg.sender);
                            resources.social_graph.generate_cache();
                        },

                        MessageType::RemoveRelationship(to) => {
                            resources.social_graph.remove(msg.sender, *to);
                            resources.social_graph.remove(*to, msg.sender);
                            resources.social_graph.generate_cache();
                        },

                        // todo: I don't think this is used outside of the gui now
                        MessageType::StateChange(state) => {
                            if let Ok(entity) = self.entity_grid.get_entity_mut(msg.sender) {
                                entity.set_state(*state);
                            }
                        }                        
                        
                        MessageType::MenuGone => {
                            // defer setting world_state until after
                            // spawning new menus to prevent click through
                            // as selecting the menu + menu_gone need to
                            // be seperated
                            menu_gone = true;
                        }

                        MessageType::InfectionSuccess(_source) => {
                            if msg.sender == PLAYER_ID {
                                self.player_has_been_infected = true;
                            }
                        }

                        _ => {}
                    }
                }                
            }


            // find the topmost entity requesting a menu open
            // for mouse clicks find the entity closest to the front
            // (for overlapping entities)
            if !menu_requests_from_mouse.is_empty() && !menu_gone {
                // !menu_gone as we don't want to launch a new entity
                // menu if the player has pressed select (space) in a
                // chat menu, and ended it 
                
                let mut closest_id = NO_ID;
                let mut closest_y = 0.;
                for (entity_id, value) in &menu_requests_from_mouse {
                    let (sender_pos, _menu_items, _y_offset, _desc) = value;
                    
                    if sender_pos.y > closest_y {
                        closest_y = sender_pos.y;
                        closest_id = *entity_id;
                    }
                }
                
                if closest_id != NO_ID {
                    let req = menu_requests_from_mouse.get(&closest_id).unwrap();
                    let (sender_pos, menu_items, y_offset, desc) = req;
                    
                    let menu_id = EntityId::new();
                    resources.world_state.menu_open = true;
                    resources.world_state.menu_id = menu_id;
                        
                    let menu = Box::new(MenuEntity::new(
                        menu_id,
                        desc.to_string(),
                        *sender_pos, 
                        **y_offset,
                        closest_id,
                        WORLD_ID,
                        menu_items.clone()
                    ));

                    resources.speech.say(Voice::Narrator, &menu.describe(resources));
                    
                    // only add the closest menu to the front of the screen
                    self.entity_grid.insert(menu);
                }
                
            }

            // while on key press we want to cycle through 'close
            // enough' entities - using recent_menu_parents
            if !menu_requests_from_keypress.is_empty() && !resources.world_state.menu_open && !menu_gone {
                // cycle through nearby entities (with q/tab)
                // !menu_gone as we don't want to launch a new entity
                // menu if the player has pressed select (space) in a
                // chat menu, and ended it 

                resources.world_state.current_entity_selection = NO_ID;
                for (entity_id, value) in &menu_requests_from_keypress {
                    
                    if resources.world_state.current_entity_selection == NO_ID && !resources.world_state.recent_selections.contains(entity_id) {
                        // set the entity selection so it can render an arrow
                        resources.world_state.current_entity_selection = *entity_id;
                        let (pos, menu_items, y_offset, desc) = value;
                        
                        let mut new_menu_items = menu_items.to_vec();
                        let menu_id = EntityId::new();

                        new_menu_items.push(MenuItem::new(
                            "Close menu".into(),
                            Rc::new(vec![
                                Message::new(MessageType::RemoveMe, menu_id, WORLD_ID)
                            ])
                        ));
                        
                        // store the menu but don't show it yet
                        resources.world_state.current_menu = MenuHolder {
                            id: menu_id,
                            pos: *pos,
                            menu_items: Rc::new(new_menu_items),
                            y_offset: **y_offset,
                            desc: desc.to_string()
                        };
                        // add to the recent selections
                        resources.world_state.recent_selections.push(*entity_id);

                        if let Ok(entity) = self.entity_grid.get_entity(*entity_id) {
                            let dir = entity.pos().sub(resources.world_state.player_pos);
                            let paces = (dir.mag() / PIXELS_PER_PACE) as u32;
                            let clock = vec_to_clock(dir);
                            
                            resources.speech.say(Voice::Narrator, &format!(
                                "Selected at {} paces at {} oclock, {}",
                                paces,
                                if clock == 0 { 12 } else { clock },
                                entity.describe(resources)
                            ));                            
                        }
                    }
                }

                if resources.world_state.current_entity_selection == NO_ID && !resources.world_state.recent_selections.is_empty() {
                    // when we've cycled through them all, clear the list                    
                    resources.world_state.recent_selections.clear();                    
                    resources.speech.say(Voice::Narrator, "Nothing selected");                            
                }                
            }
            
            if menu_gone {
                resources.world_state.menu_open = false;
            }
            
            self.entity_grid.update_grid();
            self.time_since_infection+=delta.as_millis();
            self.time_since_player_health+=delta.as_millis();
            
            messages.append(&mut world_messages);
        }
        
        messages
    }

    /// Update all the entities with the messages provided.
    pub fn dispatch(&mut self, messages: &MessageVec, resources: &Resources) {                
        self.entity_grid.dispatch_partial(messages, resources);
        self.num_messages = messages.len() as u32;
    }

    /// Render the game.
    #[allow(unused_must_use)]
    pub fn render(&self, context: &web_sys::CanvasRenderingContext2d, resources: &Resources, delta: &Duration) {
        let player_pos = resources.world_state.player_pos;

        context.save();
        
        // move the world around the player
        context.translate(self.world_offset.x, self.world_offset.y);

        self.entity_grid.render(context, player_pos, RenderPass::Bottom, resources, delta);
        
        // set up for shadow pass
        context.save();
        context.set_global_alpha(0.10);
        if resources.safari_hack {
            context.set_filter("brightness(0%)");
        } else {
            context.set_filter("brightness(0)");
        }
        // render shadows
        self.entity_grid.render(context, player_pos, RenderPass::Shadow, resources, delta);
        context.restore();
        
        // render the main pass
        self.entity_grid.render(context, player_pos, RenderPass::Beauty, resources, delta);

        self.entity_grid.render(context, player_pos, RenderPass::Top, resources, delta);

        // GUI pass
        self.entity_grid.render(context, player_pos, RenderPass::GUI, resources, delta);

        // finally debug
        //self.entity_grid.render(context, player_pos, RenderPass::Debug, resources, delta);
        
        context.restore();
    }
}

impl Default for World {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::relationship::{Relationship};
    
    
    #[test]
    fn basics() {
        // can't do sound without webasm
        /*
        let mut r = Resources::new();
        
        let mut n = World::create(2000,1000,&mut r, &Sound::new().unwrap());
        n.entity_grid.insert(Box::new(SocialEntity::at_pos(1.into(), Vec2::new(10.0,0.0), AnimId::StickCharacter)));
        n.entity_grid.insert(Box::new(SocialEntity::at_pos(2.into(), Vec2::new(20.0,0.0), AnimId::StickCharacter)));                                           
                              
        assert_eq!(n.entity_grid.get_entity(1.into()).unwrap().id(), 1.into());
        assert_eq!(n.entity_grid.get_entity(2.into()).unwrap().id(), 2.into());
        
        let mut g = SocialGraph::new();
        g.relationships.push(Relationship::new(1.into(),2.into()));
        g.generate_cache();
        
        let empty = vec![];
        let conn =
            match n.entity_grid.get_entity(1.into()) {
                Ok(entity) => g.all_connected_entity_ids(entity.id()),
                Err(_) => empty
            };
        assert_eq!(conn.len(), 1);
        assert_eq!(conn[0], 2.into());        
         */
    }
}
