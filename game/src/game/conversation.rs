// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use quick_xml::events::Event;
use quick_xml::reader::Reader;
use std::collections::HashMap;
use std::collections::hash_map::DefaultHasher;
use std::hash::Hasher;
use rand::prelude::SliceRandom;

pub struct ConversationNode {
    pub text: String,
    pub id: u64,
    pub action: String
}

impl Clone for ConversationNode {
    fn clone(&self) -> Self {
        ConversationNode {
            text: self.text.clone(),
            action: self.action.clone(),
            ..*self
        }
    }
}

pub struct ConversationEdge {
    pub from: u64,
    pub to: u64,
}

pub struct ConversationGraph {
    pub nodes: Vec<ConversationNode>,
    pub edges: Vec<ConversationEdge>,
    pub roots: HashMap<String,Vec<u64>>
}

enum LoadingMode {
    Init,
    Node,
    Action,
    Text,
    Starting
}

impl ConversationGraph {
    pub fn new() -> Self {
        ConversationGraph {
            nodes: vec![],
            edges: vec![],
            roots: HashMap::new()
        }
    }    

    pub fn find_node(&self, id: u64) -> Option<&ConversationNode> {
        for n in &self.nodes {
            if n.id == id { return Some(n) }
        }
        None
    }
    
    pub fn node_children(&self, node: &ConversationNode) -> Vec<&ConversationNode> {
        let mut ret = vec![];
        for e in &self.edges {
            if e.from == node.id {
                if let Some(node) = self.find_node(e.to) {
                    ret.push(node);
                }
            }
        }
        ret
    }

    pub fn check_node(&self, node: &ConversationNode, marked: &mut Vec<u64>) {
        marked.push(node.id);
        for child in self.node_children(node) {
            self.check_node(child, marked)
        }
    }
    
    pub fn check(&self) {
        let mut v = vec![];

        for nodes in self.roots.values() {
            for node_id in nodes {
                if let Some(node) = self.find_node(*node_id) {
                    self.check_node(node,&mut v);
                }
            }           
        }
        
        // if cfg!(target_family="wasm") {
        //     for node in &self.nodes {
        //         if !v.contains(&node.id) {
        //             console::log_1(&format!("isolated: {}", node.text).into());
        //         }
        //     }
        // }

    }
    
    // pub fn node_height(&self, node: &ConversationNode) -> u32 {
    //     let children = self.node_children(node);
    //     if children.is_empty() {
    //         return 1;
    //     } else {
    //         children.iter.fold(0,|acc, child| {
    //             acc + self.node_height(child)
    //         });           
    //     }
        
    // }
    
    // pub fn node_to_csv(&self, x: u32, y: u32, node: ConversationNode) -> (Vec<([u32;2], String)>, u32) {
    //     let mut ret = vec![([x, y], node.text)];
    //     let mut child_y = y;
        
    //     for child in self.node_children(node) {
    //         let (v, height) = self.node_to_csv(x+1, child_y, child);
    //         ret.push(v);
    //         child_y += height;
    //     }
        
    //     y-child_y
    // }
    
    // pub fn to_csv(&self) -> Vec<([u32;2], String)> {
    //     let mut l = vec![];
    //     let mut y = 0;
        
    //     for root in self.roots() {            
    //         let (c, height) = self.node_to_csv(0, y, root);            
    //         l.append(c);
    //         y+=height;
    //     }

    //     l
    // }

    
    /// todo: make hashmap
    pub fn text_from_cursor(&self, cursor: u64) -> &str {
        for n in &self.nodes {
            if n.id == cursor {
                return &n.text;
            }
        }

        "no text found..."
    }

    /// todo: make hashmap
    pub fn action_from_cursor(&self, cursor: u64) -> &str {
        for n in &self.nodes {
            if n.id == cursor {
                return &n.action;
            }
        }

        ""
    }

    pub fn next_ids_from_cursor(&self, cursor: u64) -> Vec<u64> {
        let mut ret: Vec<u64> = vec![];
        
        for e in &self.edges {
            if e.from == cursor {
                ret.push(e.to);
            }
        }

        ret
    }

    fn hash_id_string(&self, id_str: &str) -> u64 {
        let mut s = DefaultHasher::new();
        s.write(id_str.as_bytes());
        s.finish()
    }

    fn add_root(&mut self, id: String, node_id: u64) {
        if let std::collections::hash_map::Entry::Vacant(e) = self.roots.entry(id.clone()) {
            e.insert(vec![node_id]);
        } else {
            self.roots.get_mut(&id).unwrap().push(node_id);
        }
    }

    pub fn get_rnd_root(&self, starting_id: &str) -> u64 {
        if let Some(root_vec) = self.roots.get(starting_id) {
            *root_vec.choose(&mut rand::thread_rng()).unwrap()
        } else {
            if cfg!(target_family="wasm") {
                //console::log_1(&format!("no conversation starting with {} found", starting_id).into());
            }
            0
        }
    }
    
    pub fn load_from_graphml(&self, xml: &str) -> Self {

        let mut g = ConversationGraph::new();
        
        let mut reader = Reader::from_str(xml);
        reader.trim_text(true);
        reader.expand_empty_elements(true);

        let mut buf = Vec::new();

        let mut mode = LoadingMode::Init;
        let _text: String = "".into();
        
        let mut current_node = ConversationNode {
            id: 0,
            text: "".into(),
            action: "".into()
        };
        
        let mut starting: String = "".into();        

        //console::log_1(&format!("{}", xml).into());
        
        loop {

            match reader.read_event_into(&mut buf) {
                Err(e) => panic!("Error at position {}: {:?}", reader.buffer_position(), e),
                // exits the loop when reaching end of file
                Ok(Event::Eof) => break,
                
                Ok(Event::Start(e)) => {
                    //console::log_1(&format!("{:?}", e.name()).into());

                    match e.name().as_ref() {
                        b"node" => {
                            mode = LoadingMode::Node;
                            for attr in e.attributes() {
                                let a = attr.unwrap(); 
                                let name = std::str::from_utf8(a.key.as_ref()).unwrap();
                                let value = std::str::from_utf8(&a.value).unwrap();
                                if name == "id" {
                                    current_node.id = self.hash_id_string(value);
                                    current_node.text = "".into();
                                    current_node.action = "".into();
                                    starting = "".into();                                                                            
                                }
                            }
                        },

                        b"data" => {
                            //console::log_1(&format!("{:?}", e).into());
                            for attr in e.attributes() {
                                let a = attr.unwrap(); 
                                let _name = std::str::from_utf8(a.key.as_ref()).unwrap();
                                let value = std::str::from_utf8(&a.value).unwrap();
                                match value {
                                    "label" => mode = LoadingMode::Text,
                                    "starting" => mode = LoadingMode::Starting,                                
                                    "action" => mode = LoadingMode::Action,
                                    _ => {}
                                }
                            }          
                        }

                        b"edge" => {
                            let mut target: u64 = 0;
                            let mut source: u64 = 0;

                            for attr in e.attributes() {
                                let a = attr.unwrap(); 
                                let name = std::str::from_utf8(a.key.as_ref()).unwrap();
                                let value = std::str::from_utf8(&a.value).unwrap();
                                
                                if name=="source" { source =  self.hash_id_string(value); }                                
                                if name=="target" { target =  self.hash_id_string(value); }                                                                
                            }
                            
                            g.edges.push(ConversationEdge {
                                from: source,
                                to: target
                            });
                            
                        },
                        _ => (),
                    }
                }
                
                Ok(Event::Text(e)) => {
                    match mode {
                        LoadingMode::Text => current_node.text = e.unescape().unwrap().into_owned(),
                        LoadingMode::Starting => starting = e.unescape().unwrap().into_owned(),
                        LoadingMode::Action => current_node.action = e.unescape().unwrap().into_owned(),
                        _ => {}
                    }
                }

                Ok(Event::End(e)) => {
                    mode = LoadingMode::Init;
                    match e.name().as_ref() {
                        b"node" => {
                            if !starting.is_empty() {
                                g.add_root(starting.clone(), current_node.id);
                            }
                            g.nodes.push( current_node.clone() );
                        },
                        _ => {}
                    }
                }

                // There are several other `Event`s we do not consider here
                _ => (),
            }
            // if we don't keep a borrow elsewhere, we can clear the buffer to keep memory usage low
            buf.clear();
        }

        g.check();
        
        g
    }
}
