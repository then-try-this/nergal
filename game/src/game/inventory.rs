// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;
use instant::Duration;
use crate::game::entity::{ Entity };
use crate::game::resources::{ Resources };
use crate::game::message::{ MessageVec };

use crate::game::inventory_item::*;



/// We store inventory items slightly differently to entities, this is
/// because we need to update inventory functionally rather than via
/// mutation as they are owned by entities
pub type InventoryVec = Vec<Rc<dyn InventoryItem>>;
pub type InventoryRefVec<'a> = Vec<&'a Rc<dyn InventoryItem>>;

/// Inventories store anything collected by an entity, this can be
/// information, items from the world, or transmittable diseases, we
/// keep them all here - they can communicate with the outside world
/// via messages sent from their updates.
pub trait Inventory {

    fn items(&self) -> &InventoryVec;

    /// Update all the items in the inventory
    fn inventory_update(&self, owner: &dyn Entity, resources: &Resources, delta: &Duration) -> (InventoryVec, MessageVec) {
        let mut messages: MessageVec = vec![]; 
        let new_items = self.items()
            .iter()
            .map(|item| {
                let (new_item, msgs) = item.update(self.items(), owner, resources, delta);
                messages.extend(msgs);
                new_item
            })
            .filter(|item| {
                !item.delete_me()
            })            
            .collect();
              
        (new_items, messages)
    }

    /// Add a new item to the inventory
    fn inventory_add<T: InventoryItem + 'static>(&self, item: T) -> InventoryVec {        
        let mut new_items = self.items().clone();

        for inv_item in self.items() {
            if item.is_dup_of(inv_item) {                
                return new_items
            }
        }
       
        new_items.push(Rc::new(item));
        new_items
    }

    /// Returns a vec of references to items filtered by type
    fn filter_items(&self, t: InventoryItemType) -> InventoryRefVec {
        self.items().iter().filter(|item| {
            item.item_type() == t
        }).collect()
    }

    fn inventory_contains(&self, t: InventoryItemType) -> bool {
        !self.filter_items(t).is_empty()
    }

}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::items::info_item::*;
    use crate::items::disease_item::*;
    use crate::entities::social_entity::*;
    use instant::Instant;
    
    #[test]
    fn basics() {

/*
        let i = Inventory::empty();
        assert_eq!(i.items.len(),0);
        let j = Inventory::new(vec![
            Rc::new(InfoItem::new("One".into())),
            Rc::new(InfoItem::new("Two".into())),
            Rc::new(DiseaseItem::new(DiseaseStrain::DiseaseA))
        ]);

        assert_eq!(j.items.len(),3);
        assert_eq!(j.filter_items(InventoryItemType::Info).len(),2);
        assert_eq!(j.filter_items(InventoryItemType::Disease).len(),1);

        let delta = Duration::from_millis(300);
        let entity = SocialEntity::new(1.into());

        let (new_j,messages) = j.update(&entity, &delta);
        
        // test the timer updates on the disease item
        assert_eq!(Inventory::item_as::<DiseaseItem>(&new_j.items[2]).unwrap().timer, 300);
  */      
    }
}
