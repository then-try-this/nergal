// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#[derive(Debug, Clone, Copy)]
pub struct SpriteSheet {
    pub tiles_x: u32,
    pub tiles_y: u32,
    pub tile_w: u32,
    pub tile_h: u32
}

impl SpriteSheet {
    pub fn new(tiles_x: u32, tiles_y: u32, image_w: u32, image_h: u32) -> Self {
        SpriteSheet{
            tiles_x,
            tiles_y,
            tile_w: image_w/tiles_x,
            tile_h: image_h/tiles_y,
        }        
    }

    pub fn frame_to_subimage(&self, frame: u32) -> [u32; 2] {
        let x=(frame%self.tiles_x)*self.tile_w;
        let y=(frame/self.tiles_x)*self.tile_h;
        [x,y]
    }
    
}

#[cfg(test)]
mod tests {
    use super::*;
     
    #[test]
    fn basics() {
        let s = SpriteSheet::new(10,10,100,100);

        assert_eq!(s.frame_to_subimage(0)[0],0);
        assert_eq!(s.frame_to_subimage(0)[1],0);
        assert_eq!(s.frame_to_subimage(1),[10,0]);
        assert_eq!(s.frame_to_subimage(10),[0,10]);
        
    }
}
