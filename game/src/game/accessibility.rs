use std::collections::HashMap;
use web_sys::HtmlElement;
use wasm_bindgen::JsCast;

pub struct Accessibility {
    pub root: web_sys::Element,
    pub image_desc: HashMap<String, String>     
}

impl Accessibility {
    pub fn new() -> Self {
        let document = web_sys::window().unwrap().document().unwrap();

        Accessibility {
            root: document.get_element_by_id("access").unwrap(),
            image_desc: HashMap::new()
        }
    }

    pub fn say(&self, text: &str) {
        if let Some(desc) = self.image_desc.get(text) {
            web_sys::console::log_1(&desc.into());
            self.root.set_inner_html(desc);
        } else {
            web_sys::console::log_1(&text.into());
            self.root.set_inner_html(text);
        }

        let html_element: HtmlElement = self.root.clone().dyn_into().unwrap();
        html_element.click();
    }

    pub fn add_image_desc(&mut self, path: &str, desc: &str) {
        self.image_desc.insert(path.into(),desc.into());
    }
    
}
