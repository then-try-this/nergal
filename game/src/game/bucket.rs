// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use instant::{ Duration };
use vector2math::*;
use crate::game::entity::*;
use crate::game::id::{ EntityId };
use crate::maths::vec2::{ Vec2 };
use crate::game::resources::{ Resources };
use crate::game::message::{ MessageVec };
use crate::sound::sound::{ Sound };
use crate::sound::speech::{ Voice };

use web_sys::console;

/// A spatial container for entities, consists of a bounding box and
/// the entities contained within it. After updating they may not
/// still be inside the boundaries.
pub struct Bucket {
    pub addr: usize,
    pub min: Vec2,
    pub max: Vec2,
    pub entities: EntityVec
}

impl Bucket {
    pub fn new(addr: usize, min:Vec2, max:Vec2) -> Self {
        Bucket {
            addr,
            min,
            max,
            entities: vec![]
        }
    }

    pub fn build(entities: EntityVec) -> Self {
        Bucket {
            addr: 0,
            min: Vec2::zero(),
            max: Vec2::zero(),
            entities
        }
    }

    /// Slow but (should be) hardly used - e.g. only to retrieve the player by id once per frame.
    pub fn get_entity(&self, id: EntityId) -> Result<&Box<dyn Entity>,&str> {
        for entity in &self.entities {
            if entity.id() == id {
                return Ok(entity)
            }
        }
        Err("not found")
    }

    /// Slow but (should be) hardly used - e.g. only to retrieve the player by id once per frame.
    pub fn get_entity_mut(&mut self, id: EntityId) -> Result<&mut Box<dyn Entity>, &str> {
        for entity in &mut self.entities {
            if entity.id() == id {
                return Ok(entity)
            }
        }
        Err("not found")
    }

    /// A slow lookup by id of an entity and return it as the given type.
    pub fn get_entity_as<T: 'static>(&self, id: EntityId) -> Result<&T, &str> {
        let a = self.get_entity(id)?.as_any();
        let p = a.downcast_ref::<T>();
        match p {
            Some(pe) => Ok(pe),
            None => Err("not found"),
        }
    }

    /// A slow lookup by id of an entity and return it as the given type.
    pub fn get_entity_mut_as<T: 'static>(&mut self, id: EntityId) -> Result<&mut T, &str> {
        let a = self.get_entity_mut(id)?.as_any_mut();
        let p = a.downcast_mut::<T>();
        match p {
            Some(pe) => Ok(pe),
            None => Err("not found"),
        }
    }
    
    /// Delete the entity from this bucket
    pub fn remove_entity(&mut self, id: EntityId) {
        if self.get_entity(id).is_ok() {                                        
            if let Some(index) = self.entities.iter().position(|e| e.id() == id) {
                self.entities.remove(index);                                
            } else {                                
                console::log_1(&"Bucket::remove_entity() - trying to delete entity which doesn't exist".into());
            }
        } else {                                
            console::log_1(&"Bucket::remove_entity() - trying to delete entity which doesn't exist".into());
        }
    }
    
    /// Update all the entities in this bucket, returns a new bucket
    /// and a list of messages generated.
    pub fn update(&self, surrounding_entities: EntityRefVec, resources: &Resources, sound: &mut Sound, delta: &Duration) -> (Self, MessageVec) {
        let mut messages: MessageVec = vec![];
        
        let new_entities = self.entities.iter().map(|entity| {
            let (entity,msg) = entity.update(&surrounding_entities, resources, delta);
            // relative to the player
            sound.update_position(
                entity.id(),
                entity.pos().sub(resources.world_state.player_pos)
            );
            messages.extend(msg);            
            entity
        }).collect::<EntityVec>();

        (Bucket { entities: new_entities, ..*self }, messages)
    }

    // /// Update all the entities in this bucket, returns a new bucket
    // /// and a list of messages generated.
    // pub fn update_movable(&self, surrounding_entities: EntityRefVec, resources: &Resources, sound: &mut Sound, delta: &Duration) -> (Self, MessageVec) {
    //     let mut messages: MessageVec = vec![];
        
    //     // let mut new_entities = self.entities.iter().map(|entity| {
    //     //     if entity.cat() & EntityCatFlags::MOVABLE != 0 {
    //     //         let (entity, msg) = entity.update(&surrounding_entities, resources, delta);
    //     //         // don't update sounds as this is for far away entities only
    //     //         messages.extend(msg);
    //     //         entity
    //     //     } else {
    //     //         entity
    //     //     }
    //     // }).collect::<EntityVec>();

    //     //let new_entities = self.entities.iter().collect();

        
    //     (Bucket { entities: new_entities, ..*self }, messages)
    // }
    
    /// Dispatch all the messages provided to our entities and return
    /// an bucket containing updated entities.
    pub fn dispatch(&self, messages: &MessageVec, resources: &Resources) -> Self {        
        Bucket {
            entities: self.entities.iter().map(|entity| {
                entity.dispatch(messages, resources)
            }).collect(),            
            ..*self
        }              
    }

    /// Removes all entities now outside of this bucket and returns
    /// them as a list to be sorted into their new buckets
    pub fn scan_and_remove(&mut self) -> EntityVec {
        // this would be nice
        //self.entities.extract_if(|e| {
        //    !e.pos().inside(self.min, self.max)        
        //}).collect()

        if self.entities.is_empty() {
            return vec![];
        }

        // but, this is also fine...
        let mut ret = vec![];

        // well, remove in reverse order so indices remain correct 
        let mut i = self.entities.len()-1;
        loop {
            if !&self.entities[i].pos().inside(self.min, self.max) {
                ret.push(self.entities.remove(i));
            }
            if i>0 { i-=1; } else { break; }
        }
        ret
    }

    pub fn describe(&self, resources: &Resources) {
        let mut s = String::new();        
        for entity in &self.entities {
            if entity.cat() & EntityCatFlags::QUIET == 0 && entity.cat() & EntityCatFlags::SENTIENT == 0 {
                let t = entity.describe(resources);
                s = format!("{} {}.", s, t)
            }
        }
        resources.speech.say(Voice::Narrator, &s);        
    }
}


#[cfg(test)]
mod tests {
    use super::*; 
    use crate::entities::social_entity::{ SocialEntity };
    use crate::rendering::animation::{ AnimId };
   
    #[test]
    fn basics() {
        let es:EntityVec = vec![Box::new(SocialEntity::at_pos(0.into(),Vec2::new(5.0,5.0), AnimId::StickCharacter))];
        
        let mut b=Bucket{
            addr: 0,
            min: Vec2::new(0.0,0.0),
            max: Vec2::new(10.0,10.0),
            entities: es
        };

        {
            let r = b.scan_and_remove();
            assert_eq!(r.len(),0);
        }
        // add an entity out of the box and see if it gets removed...
        b.entities.push(Box::new(SocialEntity::at_pos(99.into(),Vec2::new(50.0,50.0), AnimId::StickCharacter)));
        {
            let r = b.scan_and_remove();
            assert_eq!(r.len(),1);
            assert_eq!(r[0].id(),99.into());            
            assert_eq!(b.entities.len(),1);
        }
        b.entities.push(Box::new(SocialEntity::at_pos(100.into(),Vec2::new(50.0,50.0), AnimId::StickCharacter)));
        b.entities.push(Box::new(SocialEntity::at_pos(101.into(),Vec2::new(5.0,5.0), AnimId::StickCharacter)));
        b.entities.push(Box::new(SocialEntity::at_pos(102.into(),Vec2::new(50.0,50.0), AnimId::StickCharacter)));
        {
            let r = b.scan_and_remove();
            assert_eq!(r.len(),2);
            assert_eq!(r[0].id(),102.into());            
            assert_eq!(r[1].id(),100.into());            
            assert_eq!(b.entities.len(),2);
            assert_eq!(b.entities[0].id(),0.into());            
            assert_eq!(b.entities[1].id(),101.into());            
            let r = b.scan_and_remove();
            assert_eq!(r.len(),0);            
        }

    }
}
