// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;
use std::any::Any;
use std::collections::HashMap;
use serde_json::Value;
use crate::maths::vec2::{ Vec2 };
use crate::game::id::{ EntityId };
use crate::game::entity::{ EntityState };
use crate::rendering::animation::{ AnimId };
use crate::items::disease_item::{ DiseaseStrain };
use crate::gui::menu_item::{ MenuItem };
use crate::gui::screen_manager::{ ScreenState };
use crate::sound::synth_node_desc::{ SynthNodeDesc };
use crate::entities::gift_entity::{ GiftType };

use crate::sound::speech::{ Voice };

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum HealthReason {
    Tired,
    Snack,
    Disease,
    Social
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Theme {
    Daytime,
    Nighttime
}

#[derive(Debug, Copy, Eq, Hash, Clone, PartialEq)]
pub enum GameOverReason {
    OutOfTime,
    NoHealth,
    QuitButton
}


#[derive(Debug, Clone)]
/// All possible message types defined here
pub enum MessageType {
    
    // entity to entity messages
    Question(u64),
    Answer(u64),
    TalkTo(EntityId, String, Vec2),
    ConversationOver,
    Infect(DiseaseStrain, EntityId), // originator
    Spread(DiseaseStrain),
    InfectionSuccess(EntityId),
    Recovered,
    MenuGone,

    // world instructions
    SpeechBubble(String, Voice, EntityId),
    OpenMenu(String, Rc<Vec<MenuItem>>, f64, bool),
    SendGift(GiftType, EntityId),
    RemoveMe,
    AddRelationship(EntityId),
    RemoveRelationship(EntityId),
    WhereIs(EntityId),
    StateChange(EntityState),
    InitGame(AnimId, String), // player char type, network filename
    SetTheme(Theme),
    PlayerOutsideWorld,
    
    // world response to questions
    Response(Box<MessageResponse>),

    // world to entity broadcasts
    NewGame,
    Highlight, // on tab, for speaking
    Unhighlight, // on tab away, for speaking
    TimeWarning,
    PlayerHealthWarning,
    PlayerDied,
    GameOver(GameOverReason),
    
    // input messages
    MouseClick(Vec2, Vec2, bool),
    MouseMove(Vec2, Vec2, bool),
    KeyPress(String),
    KeyRelease(String),

    // item to entity messages
    HealthChange(f64, HealthReason),

    // screen manager messages
    ScreenChange(ScreenState),

    // sounds
    PlaySoundId(String, f64, SynthNodeDesc),
    PlaySoundAt(Vec2, f64, SynthNodeDesc),
    PlaySoundAtId(String, Vec2, f64, SynthNodeDesc),
    ModifySound(String, HashMap<String, Value>),
    StopSound(String),
    Say(Voice, String),
    AddSpeechToQueue(Voice, String),
    SpeechParam(String, f32),
    
    // recording citizen science data
    PlayerInfo(String),
    DeactivateRecording
}

/// Messages are untyped free form objects that are passed between
/// entities for communication or to and from the world to create new
/// entities, delete themselves, query things, etc...
#[derive(Debug,Clone)]
pub struct Message {
    pub sender: EntityId,
    pub recipient: EntityId,
    pub data: MessageType
}

impl Message {
    pub fn new(data: MessageType, sender: EntityId, recipient: EntityId) -> Self {
        Message {
            data,
            sender,
            recipient
        }
    }
    
/*    pub fn value_as<T: 'static>(&self) -> &T {
        self.value.downcast_ref::<T>().expect("wrong type for message value")
    }
*/
}

pub type MessageVec = Vec<Message>;
pub type MessageRefVec<'a> = Vec<&'a Message>;

pub fn msg_concat(v: &MessageVec, m: &MessageVec) -> MessageVec {
    [v.as_slice(), m.as_slice()].concat()
}

pub fn msg_cons(v: &MessageVec, m: Message) -> MessageVec {
    [v.as_slice(), vec![m].as_slice()].concat()
}

/// Message responses are sent from the world in reply to specific messages
#[derive(Debug, Clone)]
pub struct MessageResponse {
    pub request: MessageType,
    pub answer: Rc<dyn Any>
}

impl MessageResponse {
    pub fn new<T: 'static>(request: MessageType, answer: T) -> Self {
        MessageResponse {
            request,
            answer: Rc::new(answer)
        }
    }

    pub fn answer_as<T: 'static>(&self) -> &T {
        self.answer.downcast_ref::<T>().expect("wrong type for answer value")
    }
}
