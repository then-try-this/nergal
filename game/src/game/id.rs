// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::sync::atomic::{AtomicUsize, Ordering};

// An id for all things in the game
#[derive(Debug,Copy,Clone,PartialEq,Hash,Eq)]
pub struct EntityId(pub usize);

pub const NO_ID: EntityId = EntityId(0);
pub const WORLD_ID: EntityId = EntityId(usize::MAX);
pub const SCREEN_MANAGER_ID: EntityId = EntityId(usize::MAX-1);
pub const PLAYER_ID: EntityId = EntityId(usize::MAX-2);

impl EntityId {
    pub fn new() -> Self {
        static COUNTER:AtomicUsize = AtomicUsize::new(1);
        Self(COUNTER.fetch_add(1, Ordering::Relaxed))
    }
}

impl From<u32> for EntityId {
	fn from(id: u32) -> Self {
		Self(id as usize)
	}
}

impl From<i32> for EntityId {
	fn from(id: i32) -> Self {
		Self(id as usize)
	}
}

impl Default for EntityId {
    fn default() -> Self {
        Self::new()
    }
}

