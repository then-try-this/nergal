// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

pub const GAME_TIME_LENGTH: u128 = 5*60*1000;
pub const PHASE_QUIET: u128 = 1*60*1000;
pub const PHASE_NORMAL: u128 = 2*60*1000;
pub const PHASE_MANIC: u128 = 2*60*1000;
pub const MAX_TIME_SINCE_INFECTION_MILLIS: u128 = 10_000;
pub const MAX_TIME_SINCE_PLAYER_HEALTH_MILLIS: u128 = 5_000;
pub const PLAYER_HEALTH_CHANGE: f64 = -3.3;
pub const OFFSCREEN_POS_X: f64 = -10000.;
pub const OFFSCREEN_POS_Y: f64 =-10000.;
pub const INFECTION_LIKELIHOOD: u32 = 25;
pub const INFECTION_LIKELIHOOD_MANIC: u32 = 75;
pub const HEALTH_WARNING_PERCENT: f64 = 20.;
pub const WALK_SPEED: f64 = 0.1; // pixels per millisecond
pub const MENU_TRIGGER_DISTANCE: f64 = 300.;
pub const MENU_WAIT_TIME_MILLIS: u128 = 7000;
pub const SOUND_TRIGGER_DIST: f64 = 500.;
pub const SOUND_TRIGGER_PERIOD: u128 = 5000; 
pub const DESCRIPTION_DISTANCE: f64 = 500.;
pub const VIEW_Y_OFFSET: f64 = 100.;
pub const SPEAK_DISTANCE: f64 = 100.0;
pub const SNACK_SIZE: f64 = 20.;
pub const FALLBACK_INFECT_FRIEND_DIST: f64 = 500.;
pub const BUBBLE_LIFETIME_MILLIS: u128 = 2500;
pub const BUBBLE_LIFETIME_MILLIS_LONG: u128 = 5000;
pub const BUBBLE_LIFETIME_MILLIS_VLONG: u128 = 7500;
pub const PIXELS_PER_PACE: f64 = 20.;
pub const INFECT_PROB_PER_NERGAL: u32 = 5;
pub const SOCIAL_AWARD_HEALTH: f64 = 2.5;

pub const NIGHTTIME_TALK_MULTIPLIER: u128 = 2;


pub fn is_next_key(key: &String) -> bool {
    key == "f"
}

pub fn is_trigger_menu_key(key: &String) -> bool {
    key == "f"
}

pub fn is_select_key(key: &String) -> bool {
    key == " "
}

pub fn is_describe_key(key: &String) -> bool {
    key == "j"
}
