// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;

use crate::maths::vec2::{ Vec2 };
use vector2math::*;
use crate::rendering::animation::{ AnimId };
use crate::game::game_scores::{ GameScores };
use crate::game::entity::{ EntityState };
use crate::game::id::*;
use crate::game::constants::*;
use crate::gui::menu_item::MenuItem;

pub struct MenuHolder {
    pub id: EntityId,
    pub pos: Vec2,
    pub desc: String,
    pub menu_items: Rc<Vec<MenuItem>>,
    pub y_offset: f64,
}

impl MenuHolder {
    pub fn new() -> Self {
        MenuHolder {
            id: NO_ID,
            pos: Vec2::zero(),
            desc: "".into(),
            menu_items: Rc::new(vec![]),
            y_offset: 0.
        }
    }
}

pub enum WorldPhase {
    Quiet,
    Normal,
    Manic
}

pub struct WorldState {
    pub world_size: Vec2,
    pub screen_size: Vec2,
    pub player_pos: Vec2,
    pub player_state: EntityState,
    pub player_character: AnimId,
    pub accessible_area: (Vec2, Vec2),
    pub network_filename: String,
    pub scores: GameScores,
    pub menu_open: bool,
    pub menu_id: EntityId,
    pub recent_selections: Vec<EntityId>,
    pub current_entity_selection: EntityId,
    pub current_menu: MenuHolder,
    pub game_time: u128,
    pub consent_given: bool,
    pub conversation_happening: bool
}

impl WorldState {
    pub fn new() -> Self {
        WorldState {
            // these are not reset every game - need to be
            // initialised in InitGame message!
            world_size: Vec2::zero(),
            screen_size: Vec2::new(1700., 750.),
            player_pos: Vec2::new(1700. / 2., 750. / 2.),
            player_state: EntityState::Idle,
            player_character: AnimId::StickCharacter,
            accessible_area: (Vec2::zero(), Vec2::zero()),
            network_filename: "".into(),
            scores: GameScores::new(),
            menu_open: false,
            menu_id: NO_ID,
            recent_selections: vec![],
            current_entity_selection: NO_ID,
            current_menu: MenuHolder::new(),
            game_time: 0,
            consent_given: false,
            conversation_happening: false
        }
    }

    pub fn screen_box(&self) -> (Vec2, Vec2) {
        (self.player_pos.sub(Vec2::new(self.screen_size.x / 2.,
                                       self.screen_size.y / 2. + VIEW_Y_OFFSET)),
         self.screen_size)
    }
   
    pub fn phase(&self) -> WorldPhase {
        if self.game_time < PHASE_QUIET {
            WorldPhase::Quiet
        } else if self.game_time > PHASE_QUIET && self.game_time < PHASE_QUIET + PHASE_NORMAL {
            WorldPhase::Normal
        } else {
            WorldPhase::Manic
        }
    }
}

