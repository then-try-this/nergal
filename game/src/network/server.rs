// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::{ JsFuture };
use web_sys::{Request, RequestInit, RequestMode, Response};

pub async fn call(request: &Request) -> Result<JsValue, JsValue> {
    let _ = request.headers().set("content-type", "application/json");
    let window = web_sys::window().unwrap();
    let resp_value = JsFuture::from(window.fetch_with_request(request)).await?;
    assert!(resp_value.is_instance_of::<Response>());
    let resp: Response = resp_value.dyn_into().unwrap();
    let json = JsFuture::from(resp.json()?).await?;
    Ok(json)            
}

pub async fn get(url: &str) -> Result<JsValue, JsValue> {
    let mut opts = RequestInit::new();
    opts.method("GET");
    opts.mode(RequestMode::Cors);
    call(&Request::new_with_str_and_init(url, &opts)?).await
}

pub async fn post(url: &str) -> Result<JsValue, JsValue> {
    let mut opts = RequestInit::new();
    opts.method("POST");
    opts.mode(RequestMode::Cors);
    call(&Request::new_with_str_and_init(url, &opts)?).await
}

pub async fn post_with_body(url: &str, body: &str) -> Result<JsValue, JsValue> {
    let mut opts = RequestInit::new();
    opts.method("POST");
    opts.mode(RequestMode::Cors);
    opts.body(Some(&wasm_bindgen::JsValue::from_str(body)));    
    call(&Request::new_with_str_and_init(url, &opts)?).await
}

