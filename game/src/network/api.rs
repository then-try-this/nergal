// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use wasm_bindgen::prelude::*;

use crate::maths::vec2::*;
use crate::game::message::*;
use crate::game::game_scores::*;
use crate::network::server::*;

const BASE_URL: &str = "https://nergal.thentrythis.org";
//const BASE_URL: &str = "http://127.0.0.1:9009";

pub async fn register_player() -> Result<JsValue, JsValue> {
    post(&format!("{}/register_player", BASE_URL)).await
}

pub async fn register_game(player_id: u64) -> Result<JsValue, JsValue> {
    post(&format!("{}/register_game/{}", BASE_URL, player_id)).await
}

pub async fn update_game(game_id: u64, theme: Theme, network: &str, values: ScoreValues) -> Result<JsValue, JsValue> {
    post_with_body(
        &format!("{}/update_game", BASE_URL),
        &format!(
        r#"{{"id": {},
            "times_ill": {},
            "nergals_infected": {},
            "nergals_talked_to": {},
            "num_friends_made": {},
            "num_friends_at_end": {},
            "lifetime": {},
            "snacks_eaten": {},
            "plants_talked_to": {},
            "clouds_talked_to": {},
            "music_made": {},
            "theme" : "{:?}",
            "network" : "{}" 
        }}"#,        
            game_id,
            values.times_ill,
            values.nergals_infected,
            values.nergals_talked_to,
            values.num_friends_made,
            values.num_friends_at_end,
            values.lifetime,
            values.snacks_eaten,
            values.plants_talked_to,
            values.clouds_talked_to,
            values.music_made,
            theme,
            network
        )
    ).await
}

pub async fn create_event(player_id: u64, player_pos: Vec2, game_id: u64, sender: &str, recipient: &str, msg: Message, valstr: String) -> Result<JsValue, JsValue> {
    let name = format!("{:?}",msg.data).replace('"',"");
    let value = valstr.replace('"',"");
    //console::log_1(&format!("create event {}",player_id).into());

    post_with_body(
        format!("{}/create_event", BASE_URL).as_str(),
        &format!(
        r#"{{"player_id": {},
            "player_pos_x": {},
            "player_pos_y": {},
            "game_id": {}, 
            "sender": "{}",
            "recipient": "{}",
            "msg": "{}",
            "ingame_sender_id": {}, 
            "ingame_recipient_id": {}, 
            "ingame_msg": {:?} 
        }}"#,        
            player_id,
            player_pos.x,
            player_pos.y,
            game_id,
            sender,
            recipient,
            value,
            msg.sender.0,
            msg.recipient.0,
            name
        )
    ).await
}

pub async fn get_averages() -> Result<JsValue, JsValue> {
    get(&format!("{}/averages/", BASE_URL)).await
}

pub async fn get_network_averages(network: &str) -> Result<JsValue, JsValue> {
    get(&format!("{}/network_averages/{}", BASE_URL, network)).await
}
