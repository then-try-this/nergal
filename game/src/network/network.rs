// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


use std::sync::{ Mutex, Arc };

use wasm_bindgen_futures::{ spawn_local };
use crate::network::api::*;
use crate::game::message::*;
use crate::game::id::*;
use crate::maths::vec2::*;
use crate::game::game_scores::*;
use crate::game::resources::*;
use crate::game::world::{ World };
use std::collections::HashMap;

use web_sys::console;

#[derive(Debug,Clone,PartialEq)]
pub struct NetworkData {
    player_id: u64,
    game_id: u64,
    averages: ScoreValues,
    network_averages: HashMap<String, ScoreValues>,
    registered: bool,
}

impl NetworkData {
    pub fn new() -> Self {
        NetworkData {
            player_id: 0,
            game_id: 0,
            averages: ScoreValues::new(),
            network_averages: HashMap::new(),
            registered: false, // player
        }
    }
}

#[derive(Debug,Clone)]
pub struct Network {
    pub shared_data: Arc<Mutex<NetworkData>>,
    active: bool
}

impl Network {
    pub fn new() -> Self {
        Network {
            shared_data: Arc::new(Mutex::new(NetworkData::new())),
            active: true
        }
    }

    pub fn register_player(&mut self) {
        let shared_data = self.shared_data.clone();
        let fetch_and_assign = async move {  
            let call = register_player().await;
            match call {
                Ok(value) => {
                    let player_id = value.as_f64().unwrap() as u64;
                    //console::log_1(&format!("player id: {:?}", player_id).into());
                    let mut lock = shared_data.lock().unwrap();
                    lock.player_id = player_id;
                    lock.registered = true;
                }
                Err(_) => {}
            }
        };    
        spawn_local(fetch_and_assign);            
    }

    pub fn register_game(&mut self) {
        let shared_data = self.shared_data.clone();
        let player_id = {
            let lock = shared_data.lock().unwrap();
            lock.player_id
        };
        let fetch_and_assign = async move {  
            let call = register_game(player_id).await;
            match call {
                Ok(value) => {
                    let game_id = value.as_f64().unwrap() as u64;
                    //console::log_1(&format!("game id: {:?}", game_id).into());
                    let mut lock = shared_data.lock().unwrap();
                    lock.game_id = game_id;
                }
                Err(_) => {}
            }
        };    
        spawn_local(fetch_and_assign);            
    }

    pub fn update_averages(&mut self) {
        let shared_data = self.shared_data.clone();
        let fetch_and_assign = async move {  
            let call = get_averages().await;
            match call {
                Ok(value) => {
                    let averages:ScoreValues = serde_wasm_bindgen::from_value(value).unwrap();
                    let mut lock = shared_data.lock().unwrap();
                    lock.averages = averages;
                }
                Err(_) => {}
            }
        };    
        spawn_local(fetch_and_assign);            
    }

    pub fn update_network_averages(&mut self, networks: &[&str]) {
        for network in networks {
            self.update_network_average(network);
        }
    }

    pub fn update_network_average(&mut self, network_: &str) {
        let shared_data = self.shared_data.clone();
        let network = (*network_).to_string();
        let fetch_and_assign = async move {  
            let call = get_network_averages(&network).await;
            match call {
                Ok(value) => {
                    let averages:ScoreValues = serde_wasm_bindgen::from_value(value).unwrap();
                    let mut lock = shared_data.lock().unwrap();
                    //console::log_1(&format!("{}: {:?}", network, averages).into());
                    lock.network_averages.insert(network, averages);
                }
                Err(_) => {}
            }
        };    
        spawn_local(fetch_and_assign);
    }

    pub fn get_averages(&self) -> ScoreValues {
       let shared_data = self.shared_data.clone();
        let lock = shared_data.lock().unwrap();
        lock.averages
    }

    pub fn get_network_averages(&self, name: &str) -> ScoreValues {
       let shared_data = self.shared_data.clone();
        let lock = shared_data.lock().unwrap();
        if let Some(averages) = lock.network_averages.get(name) {
            *averages
        } else {
            ScoreValues::new()
        }
    }

    pub fn entity_id_to_human(&self, id: EntityId, resources: &Resources) -> String {
        match id {
            NO_ID => "nobody".into(),
            WORLD_ID => "system".into(),
            SCREEN_MANAGER_ID => "system".into(),
            PLAYER_ID => format!("player ({})", resources.lookup_social_id(id).unwrap()),
            _ => {
                if let Some(social_id) = resources.lookup_social_id(id) {
                    format!("{}", social_id)
                } else {
                    "entity not in social graph".into()
                }
            }                
        }
    }

    pub fn get_pos(&self, id: EntityId, world: &World) -> Vec2 {
        if let Ok(e) = world.entity_grid.get_entity(id) {
            e.pos()
        } else {
            Vec2::zero()
        }
    }
    
    pub fn store_event(&mut self, msg: &Message, resources: &Resources, world: &World) {
        let sender = self.entity_id_to_human(msg.sender, resources);
        let recipient = self.entity_id_to_human(msg.recipient, resources);
               
        let val: String = match &msg.data {

            MessageType::InitGame(_animid, network) => {
                format!("New game using {} {:?}", network, resources.theme)
            }

            // sneezes
            MessageType::Spread(_strain) => {
                format!(
                    "Spread from {} @ {:?}",
                    sender,
                    self.get_pos(msg.sender, world)
                )                    
            }
            
            // sometimes cause infections (if in range and by chance)
            MessageType::Infect(_strain, originator) => {
                format!(
                    "Infect from {} @ {:?} to {} @ {:?}",
                    self.entity_id_to_human(*originator, resources),
                    self.get_pos(*originator, world),
                    sender,
                    self.get_pos(msg.sender, world)
                )                    
            }

            // which succeed when the owner hasn't got the disease already
            MessageType::InfectionSuccess(originator) => {
                format!(
                    "InfectionSuccess from {} @ {:?} to {} @ {:?}",
                    self.entity_id_to_human(*originator, resources),
                    self.get_pos(*originator, world),
                    sender,
                    self.get_pos(msg.sender, world)
                )                    
            }

            MessageType::Recovered => {
                format!(
                    "Recovered from illness {} @ {:?}",
                    sender,
                    self.get_pos(msg.sender, world),
                )
            }
            
            MessageType::AddRelationship(to)  => {
                format!(
                    "AddRelationship between {} @ {:?} and {} @ {:?}",
                    sender,
                    self.get_pos(msg.sender, world),
                    self.entity_id_to_human(*to, resources),
                    self.get_pos(*to, world)
                )
            }
            
            MessageType::RemoveRelationship(to) => {
                format!(
                    "RemoveRelationship between {} @ {:?} and {} @ {:?}",
                    sender,
                    self.get_pos(msg.sender, world),
                    self.entity_id_to_human(*to, resources),
                    self.get_pos(*to, world)
                )
            }
            
            MessageType::TalkTo(_id, entity, pos) => {
                let friend = if entity == "nergal" {
                    if resources.social_graph.player_friend_cache.contains(&msg.sender) {
                        ", friend"
                    } else {
                        ", stranger"
                    }
                } else {
                    ""
                };
                
                format!(
                    "TalkTo between {} and {} who is a {} at {:?} {}",
                    recipient,
                    sender,
                    entity,
                    pos,
                    friend
                )
            }

            MessageType::GameOver(reason) => {
                format!("GameOver because {:?}", reason)
            }

            MessageType::HealthChange(change, reason) => {
                if *reason == HealthReason::Tired {
                    "".into()
                } else {
                    format!(
                        "HealthChange for {} @ {:?} of {} reason {:?}",
                        recipient,
                        self.get_pos(msg.recipient, world),
                        change,
                        reason
                    )
                }
            }
            
            MessageType::PlayerInfo(text) => {
                format!("PlayerInfo {}", text)
            }

            MessageType::DeactivateRecording => {
                self.active = false;
                "".into()
            }
            
            _ => { "".into() }
        };

        // independance so we can leave her with people (only does solo walks)
        // getting her better with other dogs
        
        if self.active && !val.is_empty() {
            if let Ok(lock) = self.shared_data.lock() {
                if lock.registered {
                    let player_id = lock.player_id;
                    let game_id = lock.game_id;
                    let player_pos = resources.world_state.player_pos;
                    let msg_cpy = msg.clone();
                    
                    spawn_local(async move {
                        let _ = create_event(
                            player_id,
                            player_pos,
                            game_id,
                            &sender,
                            &recipient,
                            msg_cpy,
                            val
                        ).await;
                    });
                }
            } else {
                console::log_1(&"couldn't get lock".into());
            }
        }
    }

    pub fn update_game(&self, scores_: &GameScores, theme: Theme, network_: &str) {
        let scores = scores_.clone();
        let network: String = network_.to_string();
        if let Ok(lock) = self.shared_data.lock() {
            if lock.registered {
                let _player_id = lock.player_id;
                let game_id = lock.game_id;

                spawn_local(async move {
                    let _ = update_game(
                        game_id,
                        theme,
                        &network,
                        ScoreValues::from_scores(scores)
                    ).await;
                });       
            }            
        } else {
            console::log_1(&"couldn't get lock".into());
        }
    }
}
