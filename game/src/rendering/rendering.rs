// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use wasm_bindgen::JsCast;
use vector2math::*;
use crate::maths::vec2::{ Vec2 };

use crate::gui::highlight_text_entity::{ TextBlock };

/// Description of the different render passes we use
#[derive(Debug,Clone,Copy,PartialEq)]
pub enum RenderPass {
    Bottom,
    Shadow,
    Beauty,
    Top,
    GUI,
    Debug
}

pub fn get_context() -> web_sys::CanvasRenderingContext2d {
	let window = web_sys::window().expect("global window does not exists");    
	let document = window.document().expect("expecting a document on window");
    let canvas: web_sys::HtmlCanvasElement = document.create_element("canvas")
        .unwrap()
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .unwrap();
    canvas.get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<web_sys::CanvasRenderingContext2d>()
        .unwrap()
}


/// Utility functions for rendering
pub fn text_width(context:  &web_sys::CanvasRenderingContext2d, text: &str) -> f64 {
    if let Ok(metrics) = context.measure_text(text) {
        metrics.width()
    } else {
        0.
    }
}

pub fn text_width_with_font(text: &str, font: &str) -> f64 {
    let context = get_context();
    context.set_font(font); 
    
    if let Ok(metrics) = context.measure_text(text) {
        metrics.width()
    } else {
        0.
    }
}


pub fn text_layout_pixels(text: &str, width: f64, font: &str) -> Vec<String> {
    let context = get_context();
    context.set_font(font); 
    
    let mut lines: Vec<String> = vec![];
    let mut cur_line: String = "".into();
    let mut cur_word: String = "".into();
    
    for c in text.chars() {
        match c {
            ' ' => {
                if text_width(&context, &(cur_line.clone() + &cur_word)) > width {
                    lines.push(cur_line + " ");
                    cur_line = cur_word.clone() + " ";
                } else {
                    cur_line += &(cur_word + " ");
                }               
                cur_word = "".into();
            },
            
            _ => {
                cur_word.push(c);
            }
                
        }
    }
    
    if text_width(&context, &(cur_line.clone() + &cur_word)) > width {
        lines.push(cur_line);
        cur_line = cur_word.clone();         
    } else {
        cur_line += &(cur_word + " ");
    }
    
    lines.push(cur_line);

    lines
}

pub fn text_layout_chars(text: &str, width: usize) -> Vec<String> {
    let mut lines: Vec<String> = vec![];
    let mut cur_line: String = "".into();
    let mut cur_word: String = "".into();

    for c in text.chars() {
        match c {
            ' ' => {
                if cur_line.len() + cur_word.len() > width {
                    lines.push(cur_line+" ");
                    cur_line = cur_word+" ";
                } else {
                    cur_line += &(cur_word+" ");
                }               
                cur_word = "".into();
            },
            
            _ => {
                cur_word.push(c);
            }
                
        }
    }
    
    if cur_line.len() + cur_word.len() > width {
        lines.push(cur_line+" ");
        cur_line = cur_word.clone();         
    } else {
        cur_line += &(cur_word+" ");
    }
 
    lines.push(cur_line);
    lines
}

pub fn textblock_layout_pixels(text: &mut Vec<TextBlock>, _width: f64, font: &str) {
    let context = get_context();
    context.set_font(font); 

    let mut pos = Vec2::zero();
    
    for block in text {
        block.relative_pos = pos;
        block.width = text_width(&context, &block.text);
        pos.x += block.width;
    }
}


pub fn draw_dot(context: &web_sys::CanvasRenderingContext2d, x: f64, y: f64) {
    context.begin_path();
    context
        .arc(x,y, 5.0, 0.0, std::f64::consts::PI * 2.00)
        .unwrap();
    context.set_fill_style(&"#000".into());        
    context.fill();
}

// adjusts the position of 'inner' so that it is visible within 'outer'
// (pos, size)
pub fn constrain_position(inner: (Vec2, Vec2), outer: (Vec2, Vec2)) -> Vec2 {
    let inner_width = inner.1.x;
    let inner_height = inner.1.y;
    let inner_max_x = inner.0.x + inner.1.x;
    let inner_max_y = inner.0.y + inner.1.y;
    let outer_max_x = outer.0.x + outer.1.x;
    let outer_max_y = outer.0.y + outer.1.y;

    //console::log_1(&format!("{:?} {:?}", inner, outer).into());
    
    Vec2::new(if inner.0.x < outer.0.x { outer.0.x }
              else if inner_max_x > outer_max_x {
                  outer_max_x - inner_width                     
              }
              else { inner.0.x },
              if inner.0.y < outer.0.y { outer.0.y }
              else if inner_max_y > outer_max_y { outer_max_y - inner_height }
              else { inner.0.y }
    )
}

pub fn enclosed_by(inner: (Vec2, Vec2), outer: (Vec2, Vec2)) -> bool {
    let _inner_width = inner.1.x;
    let _inner_height = inner.1.y;
    let inner_max_x = inner.0.x + inner.1.x;
    let inner_max_y = inner.0.y + inner.1.y;
    let outer_max_x = outer.0.x + outer.1.x;
    let outer_max_y = outer.0.y + outer.1.y;

    inner.0.x > outer.0.x && inner_max_x < outer_max_x && 
        inner.0.y > outer.0.y && inner_max_y < outer_max_y 
}


pub fn vec_to_clock(vec: Vec2) -> u32 {
    let n = vec.norm();
    let degrees = (-n.x.atan2(n.y) * 57.2958) + 180.;
    println!("{:?} = {}", vec, degrees);
    let ret = (degrees / 360. * 12.) as u32;
    if ret > 11 { 0 } else { ret }
}


#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn basics() {
        let outer = (Vec2::new(10.,10.),Vec2::new(90.,140.));

        assert_eq!(constrain_position((Vec2::new(4.,10.), Vec2::new(5.,5.)), outer),
                   Vec2::new(10.,10.));
            
        assert_eq!(constrain_position((Vec2::new(11.,11.), Vec2::new(5.,5.)), outer),
                   Vec2::new(11.,11.));

        assert_eq!(constrain_position((Vec2::new(110.,160.), Vec2::new(5.,5.)), outer),
                   Vec2::new(95.,145.));

        assert_eq!(vec_to_clock(Vec2::new(0.,1.)), 6);
        assert_eq!(vec_to_clock(Vec2::new(0.,-1.)), 0);
        assert_eq!(vec_to_clock(Vec2::new(1.,0.01)), 3);
        assert_eq!(vec_to_clock(Vec2::new(-1.,0.)), 9);
        assert_eq!(vec_to_clock(Vec2::new(1.,1.)), 4);
        assert_eq!(vec_to_clock(Vec2::new(-1.,-1.)), 10);
        
/*        assert_eq!(constrain_position(
            (Vec2 { x: 8825.613244851149, y: 5855.381478495814 }/,
             Vec2 { x: 200.0, y: 228.0 }),

            (Vec2 { x: 8207.974442666755, y: 5863.587330609283 },
             Vec2 { x: 1700.0, y: 750.0 })
                
        ), Vec2::zero());
*/        
    }
}
