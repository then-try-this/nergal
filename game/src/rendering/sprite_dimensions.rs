// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use wasm_bindgen::JsCast;
use web_sys::{ HtmlImageElement };
use vector2math::*;

use crate::rendering::animation::{ AnimId };
use crate::maths::vec2::{ Vec2 };

/// sprite dimensions based on sprite sheet - this is the most common default height
const SPRITE_WIDTH: f64 = 200.;
const SPRITE_HEIGHT: f64 = 257.;

/// Positions in y of each character's feet and heads
const STICK_WIDTH: f64 = 200.;
const STICK_FEET_HEIGHT: f64 = 40.; // (distance between bottom of sprite and feet)
const STICK_BODY_HEIGHT: f64 = 167.; // (distance between feet and head)

const CLOUD_WIDTH: f64 = 220.;
const CLOUD_FEET_HEIGHT: f64 = 40.;
const CLOUD_BODY_HEIGHT: f64 = 111.;

const EGG_WIDTH: f64 = 220.;
const EGG_FEET_HEIGHT: f64 = 40.;
const EGG_BODY_HEIGHT: f64 = 140.;

const GHOST_WIDTH: f64 = 220.;
const GHOST_FEET_HEIGHT: f64 = 40.;
const GHOST_BODY_HEIGHT: f64 = 140.;

const SHADOW_TX_X: f64 = 1.56;
const SHADOW_TX_Y: f64 = 0.80;

pub struct SpriteDimensions {
    pub width: f64,
    pub feet_height: f64,
    pub body_height: f64,
    pub sprite_width: f64,
    pub sprite_height: f64,
    pub shadow_tx_x: f64,
    pub shadow_tx_y: f64
}

impl SpriteDimensions {
    pub fn new(width: f64, feet_height: f64, body_height: f64, sprite_width: f64, sprite_height: f64, shadow_tx_x: f64, shadow_tx_y: f64) -> Self {
        SpriteDimensions {
            width,
            feet_height,
            body_height,
            sprite_height,
            sprite_width,
            shadow_tx_x,
            shadow_tx_y
        }
    }
}

/// Returns spritesheet path, feet height and head height
pub fn character_params(character: AnimId) -> (&'static str, SpriteDimensions) {
    match character {
        AnimId::StickCharacter => { ("/images/sprites/characters/stick-sheet.png", SpriteDimensions::new(STICK_WIDTH, STICK_FEET_HEIGHT, STICK_BODY_HEIGHT, SPRITE_WIDTH, SPRITE_HEIGHT, SHADOW_TX_X, SHADOW_TX_Y)) },
        AnimId::CloudCharacter => { ("/images/sprites/characters/cloud-sheet.png", SpriteDimensions::new(CLOUD_WIDTH, CLOUD_FEET_HEIGHT, CLOUD_BODY_HEIGHT, SPRITE_WIDTH, SPRITE_HEIGHT, SHADOW_TX_X, SHADOW_TX_Y)) },
        AnimId::EggCharacter => { ("/images/sprites/characters/egg-sheet.png", SpriteDimensions::new(EGG_WIDTH, EGG_FEET_HEIGHT, EGG_BODY_HEIGHT, SPRITE_WIDTH, SPRITE_HEIGHT, SHADOW_TX_X, SHADOW_TX_Y)) },
        AnimId::StickStetsonCharacter => { ("/images/sprites/characters/stick-stetson.png", SpriteDimensions::new(STICK_WIDTH, STICK_FEET_HEIGHT, STICK_BODY_HEIGHT, SPRITE_WIDTH, 2057./6., 2.17, 0.85)) },
        AnimId::StickPartyCharacter => { ("/images/sprites/characters/stick-party.png", SpriteDimensions::new(STICK_WIDTH, STICK_FEET_HEIGHT, STICK_BODY_HEIGHT, SPRITE_WIDTH, 2400./6., 2.67, 0.85)) },
        AnimId::StickAcornCharacter => { ("/images/sprites/characters/stick-acorn.png", SpriteDimensions::new(STICK_WIDTH, STICK_FEET_HEIGHT, STICK_BODY_HEIGHT, SPRITE_WIDTH, 2057./6., 2.17, 0.85)) },
        AnimId::CloudPussyCharacter => { ("/images/sprites/characters/cloud-pussy.png", SpriteDimensions::new(CLOUD_WIDTH, CLOUD_FEET_HEIGHT, CLOUD_BODY_HEIGHT, SPRITE_WIDTH, SPRITE_HEIGHT, SHADOW_TX_X, SHADOW_TX_Y)) },
        AnimId::CloudStetsonCharacter => { ("/images/sprites/characters/cloud-stetson.png", SpriteDimensions::new(CLOUD_WIDTH, CLOUD_FEET_HEIGHT, CLOUD_BODY_HEIGHT, SPRITE_WIDTH, SPRITE_HEIGHT, SHADOW_TX_X, SHADOW_TX_Y)) },
        AnimId::CloudBobbleCharacter => { ("/images/sprites/characters/cloud-bobble.png", SpriteDimensions::new(CLOUD_WIDTH, CLOUD_FEET_HEIGHT, CLOUD_BODY_HEIGHT, SPRITE_WIDTH, SPRITE_HEIGHT, SHADOW_TX_X, SHADOW_TX_Y)) },
        AnimId::EggBobbleCharacter => { ("/images/sprites/characters/egg-bobble.png", SpriteDimensions::new(EGG_WIDTH, EGG_FEET_HEIGHT, EGG_BODY_HEIGHT, SPRITE_WIDTH, 2057./6., 2.17, 0.85)) },
        AnimId::EggAcornCharacter => { ("/images/sprites/characters/egg-acorn.png", SpriteDimensions::new(EGG_WIDTH, EGG_FEET_HEIGHT, EGG_BODY_HEIGHT, SPRITE_WIDTH, 2057./6., 2.17, 0.85)) },
        AnimId::EggPussyCharacter => { ("/images/sprites/characters/egg-pussy.png", SpriteDimensions::new(EGG_WIDTH, EGG_FEET_HEIGHT, EGG_BODY_HEIGHT, SPRITE_WIDTH, 2057./6., 2.17, 0.85)) }
        AnimId::GhostCharacter => { ("/images/sprites/characters/ghost-sheet.png", SpriteDimensions::new(GHOST_WIDTH, GHOST_FEET_HEIGHT, GHOST_BODY_HEIGHT, SPRITE_WIDTH, 2057./6., 2.17, 0.85)) },
        AnimId::GhostShroomCharacter => { ("/images/sprites/characters/ghost-shroom.png", SpriteDimensions::new(GHOST_WIDTH, GHOST_FEET_HEIGHT, GHOST_BODY_HEIGHT, SPRITE_WIDTH, 2057./6., 2.17, 0.85)) },
        AnimId::GhostPartyCharacter => { ("/images/sprites/characters/ghost-party.png", SpriteDimensions::new(GHOST_WIDTH, GHOST_FEET_HEIGHT, GHOST_BODY_HEIGHT, SPRITE_WIDTH, 2057./6., 2.17, 0.85)) }
    }    
}

// Relative to pos
pub fn character_bounding_box(pos: Vec2, character: AnimId) -> (Vec2, Vec2) {
    let dims = character_params(character);
    
    let left = -(SPRITE_WIDTH/4.);
    let top = -dims.1.body_height;            
    let width = SPRITE_WIDTH/2.;
    let height = dims.1.body_height;
    
    (Vec2::new(left, top).add(pos),
     Vec2::new(left + width, top + height).add(pos))
}

// (define (get-pixel img x y)
//   (let ((canvas (document.createElement "canvas")))
// 	(set! canvas.width img.width)
// 	(set! canvas.height img.height)
// 	(let ((context (canvas.getContext "2d")))
// 	  (context.drawImage img 0 0)
// 	  (let ((ret (context.getImageData x y 1 1)))
// 		ret.data))))


pub fn get_pixel(image: &HtmlImageElement, pos: Vec2) -> Vec<u8> {
	let window = web_sys::window().expect("global window does not exists");    
	let document = window.document().expect("expecting a document on window");
    let canvas: web_sys::HtmlCanvasElement = document.create_element("canvas")
        .unwrap()
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .unwrap();
    canvas.set_width(image.width());
    canvas.set_height(image.height());
    let context = canvas.get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<web_sys::CanvasRenderingContext2d>()
        .unwrap();
    let _ = context.draw_image_with_html_image_element(image, 0., 0.);
    let ret = context.get_image_data(pos.x, pos.y, 1., 1.)
        .unwrap();
    ret.data().to_vec()
}

// ;; x y in screen coords
// (define (entity-check-pixel-alpha e x y threshold)
//   (let ((clip (list-ref (entity-sprite-list e) (entity-clip e)))
//     	(posx (+ (- (vx (entity-pos e)) (vx (entity-centre e)))
//     			 (vx (entity-offset e))))
//     	(posy (+ (- (vy (entity-pos e)) (vy (entity-centre e)))
//     			 (vy (entity-offset e)))))	
//     (let ((image (anim-clip-frame
//     			  clip (entity-facing e)
//     			  (entity-frame e)))
//     	  (ix (- x posx))
//     	  (iy (- y posy)))
//       (let ((p (get-pixel image ix iy)))
//     	;; check alpha - threshold so we don't pick up shadows etc
//     	(> (list-ref p 3) threshold)))))

//pub fn check_pixel_alpha(e
