// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::cmp;
use vector2math::*;
use web_sys::HtmlImageElement;
use std::collections::HashMap;
use crate::game::sprite_sheet::{ SpriteSheet };


/// todo: move, game specific...
#[derive(Debug, Copy, Clone, Eq, Hash, PartialEq)]
pub enum AnimId {
    StickCharacter,
    CloudCharacter,
    EggCharacter,
    StickStetsonCharacter,
    StickPartyCharacter,
    StickAcornCharacter,
    CloudPussyCharacter,
    CloudStetsonCharacter,
    CloudBobbleCharacter,
    EggBobbleCharacter,
    EggAcornCharacter,
    EggPussyCharacter,
    GhostCharacter,
    GhostShroomCharacter,
    GhostPartyCharacter
}

pub fn animid_to_character_type(id: AnimId) -> String {
    match id {
        AnimId::StickCharacter => "stick",
        AnimId::CloudCharacter => "cloud",
        AnimId::EggCharacter => "egg",
        AnimId::StickStetsonCharacter => "stick",
        AnimId::StickPartyCharacter => "stick",
        AnimId::StickAcornCharacter => "stick",
        AnimId::CloudPussyCharacter => "cloud",
        AnimId::CloudStetsonCharacter => "cloud",
        AnimId::CloudBobbleCharacter => "cloud",
        AnimId::EggBobbleCharacter => "egg",
        AnimId::EggAcornCharacter => "egg",
        AnimId::EggPussyCharacter => "egg",
        AnimId::GhostCharacter => "ghost",
        AnimId::GhostShroomCharacter => "ghost",
        AnimId::GhostPartyCharacter => "ghost"
    }.into()
}

#[derive(Debug, Eq, Hash, PartialEq, Copy, Clone)]
pub enum ClipId {
    Idle,
    Dance,
    Eat,
    Ill,
    Death,
    Jump,
    Kneebend,
    Lean,
    Vomit,
    Sleep,
    Sneeze,
    Talk,
    WalkRight,
    WalkLeft,
    WalkUp,
    WalkDown
}

#[derive(Debug, Clone, Copy)]
pub enum AnimMode {
    Loop,
    OneShot,
    LoopEnd        
}

/// An anim clip is a single animation for a character, defined by a list of frames
#[derive(Debug, Clone)]
pub struct AnimClip {
    mode: AnimMode,
    speed: f64,
    frames: &'static[u32],
    offsets: &'static[[f64;2]],
    flip_x: bool,
    loop_point: usize
}

impl AnimClip {
    pub fn new(mode: AnimMode, speed: f64, frames: &'static [u32], offsets: &'static [[f64;2]], flip_x: bool, loop_point: usize) -> Self {
        if !offsets.is_empty() && offsets.len()!=frames.len() {               
            panic!("offsets need to match frames");
        }
        
        AnimClip {
            mode, speed, frames, offsets, flip_x, loop_point
        }
    }
    
    pub fn current_frame(&self, time_millis: &u128) -> usize {
        let frame = (*time_millis as f64 * (self.speed/1000.)) as usize;

        match self.mode {
            AnimMode::Loop => frame % self.frames.len(),
            AnimMode::OneShot =>  cmp::min(frame, self.frames.len()-1),
            AnimMode::LoopEnd => if frame<self.loop_point {
                frame
            } else {
                self.loop_point + (frame - self.loop_point) % (self.frames.len() - self.loop_point)
            }
        }
    }
    
    /// Render the current frame into the context
    #[allow(unused_must_use)]
    pub fn render(&self, context: &web_sys::CanvasRenderingContext2d, x: f64, y: f64, sheet: &SpriteSheet,  image: &HtmlImageElement, time_millis: &u128) {
        let index = self.current_frame(time_millis);
        
        if self.flip_x {
            context.translate(sheet.tile_w.into(),0.);
            context.scale(-1.,1.);
        }

        let mut fx = x;
        let mut fy = y;
        
        // we apply offsets to allow manual tweaking of frame positions
        // without having to change the sprite sheets, could be temporary
        // but I doubt that
        if !self.offsets.is_empty() {
            fx+=self.offsets[index][0];
            fy+=self.offsets[index][1];
        }

        let source_rect = sheet.frame_to_subimage(self.frames[index]);
        
        context.draw_image_with_html_image_element_and_sw_and_sh_and_dx_and_dy_and_dw_and_dh(
            image, source_rect[0].into(), source_rect[1].into(),
            sheet.tile_w.into(), sheet.tile_h.into(),
            fx.round(), fy.round(),
            sheet.tile_w.into(), sheet.tile_h.into()
        );
    }
}

/// An animation is a list of clips and a sprite sheet to load frames from
#[derive(Debug, Clone)]
pub struct Animation {
    pub clips: HashMap<ClipId,AnimClip>,
    pub sheet: SpriteSheet
}

impl Animation {
    pub fn new(sheet: SpriteSheet) -> Self {
        Animation {
            clips: HashMap::new(),
            sheet
        }
    }
    
    // pub fn is_finished(&self, clip_id: ClipId, time_millis: &u128, num_loops: u32) -> bool {
    //     self.clips.get(&clip_id).unwrap().is_finished(time_millis, num_loops)
    // }

    pub fn current_frame(&self, clip_id: ClipId, time_millis: &u128) -> usize {
        self.clips.get(&clip_id).unwrap().current_frame(time_millis)
    }
    
    pub fn render(&self, context: &web_sys::CanvasRenderingContext2d, x: f64, y: f64, clip_id: ClipId, image: &HtmlImageElement, time_millis: &u128) {
        self.clips.get(&clip_id).unwrap().render(context,x,y,&self.sheet,image,time_millis)        
    }
}
