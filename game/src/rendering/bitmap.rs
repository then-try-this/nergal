// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use wasm_bindgen::JsCast;
use web_sys::{ HtmlImageElement };
use crate::maths::vec2::{ Vec2 };

/// A whole thing to avoid calling get_image_data
pub struct Bitmap {
    pub width: usize,
    pub height: usize,
    pub map: Vec<[u8; 4]>,
    pub shrink_by: usize,
    pub orig_width: usize,
    pub orig_height: usize,
}

impl Bitmap {
    pub fn new(image: &HtmlImageElement, _channel: usize, shrink_by: usize) -> Self {
        let width = image.width() as usize / shrink_by;
        let height = image.height() as usize / shrink_by;        
	    let window = web_sys::window().expect("global window does not exists");    
	    let document = window.document().expect("expecting a document on window");
        let canvas: web_sys::HtmlCanvasElement = document.create_element("canvas")
            .unwrap()
            .dyn_into::<web_sys::HtmlCanvasElement>()
            .unwrap();
        canvas.set_width(width as u32);
        canvas.set_height(height as u32);
        let context = canvas.get_context("2d")
            .unwrap()
            .unwrap()
            .dyn_into::<web_sys::CanvasRenderingContext2d>()
            .unwrap();
        let _ = context.draw_image_with_html_image_element_and_dw_and_dh(
            image, 0., 0., width as f64, height as f64
        );

        let mut map = vec![];

        let data = context.get_image_data(0., 0., width as f64, height as f64)
            .unwrap()
            .data()
            .to_vec();

        let mut pos = 0;
        for _x in 0..height {
            for _y in 0..width {                
                map.push([data[pos],
                          data[pos+1],
                          data[pos+2],
                          data[pos+3]]);
                pos += 4;
            }
        }
        
        Bitmap {
            width, height, map, shrink_by,
            orig_width: image.width() as usize,
            orig_height: image.height() as usize
        }
    }

    pub fn get_pixel(&self, pos: Vec2) -> [u8; 4] {
        let x: usize = pos.x as usize / self.shrink_by;
        let y: usize = pos.y as usize / self.shrink_by;
        if x > 0 && x < self.width && y > 0 && y < self.height {
            self.map[y * self.width + x]
        } else {
            [0, 0, 0, 0]
        }
    }
}
