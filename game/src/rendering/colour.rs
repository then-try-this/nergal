// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Colour {
    pub r: f64,
    pub g: f64,
    pub b: f64,
    pub a: f64
}

#[allow(dead_code)]
impl Colour {
    pub fn new(r: f64, g: f64, b: f64, a: f64) -> Self {
        Colour { r, g, b, a }
    }
    pub fn black() -> Self {
        Colour { r: 0., g: 0., b: 0., a: 1. }
    }

    pub fn as_hex(&self) -> String {
        format!("#{:02x}{:02x}{:02x}{:02x}",
                (self.r*255.) as u16,
                (self.g*255.) as u16,
                (self.b*255.) as u16,
                (self.a*255.) as u16) 
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn basics() {
        assert_eq!(Colour::new(0.,0.,0.,1.0).as_hex(),"#000000ff");        
        assert_eq!(Colour::new(0.5,0.5,0.5,1.0).as_hex(),"#7f7f7fff");        
        assert_eq!(Colour::new(1.0,0.0,0.0,0.5).as_hex(),"#ff00007f");        
        assert_eq!(Colour::new(0.0,1.0,0.0,0.5).as_hex(),"#00ff007f");        
        assert_eq!(Colour::new(0.0,0.0,1.0,0.5).as_hex(),"#0000ff7f");        
    }
}
