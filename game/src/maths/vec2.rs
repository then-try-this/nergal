// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use vector2math::*;
use rand::Rng;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Vec2 {
    pub x: f64,
    pub y: f64,
}

#[allow(dead_code)]
impl Vec2 {
    pub fn new(x: f64, y: f64) -> Self {
        Vec2 { x, y }
    }
    pub fn zero() -> Self {
        Vec2 { x:0.0, y:0.0 }
    }
    pub fn norm(&self) -> Self {
        self.div(self.mag())
    }
//    pub fn dist(&self, other: Vec2) -> f64 {
//        self.sub(other).mag()
//    }
    // avoid a square root for comparisons etc
    pub fn dist_sq(&self, other: Vec2) -> f64 {
        let v = self.sub(other);
        v.x*v.x + v.y*v.y
    }
//    pub fn lerp(&self, other: Vec2, t: f64) -> Vec2 {
//        self.add(self.sub(other).mul(t))
//    }
    pub fn rnd() -> Self {
        let mut rng = rand::thread_rng();
        Vec2 {
            x: rng.gen_range(0.0..1.0),
            y: rng.gen_range(0.0..1.0)
        }
    }
    pub fn crnd() -> Self {
        let mut rng = rand::thread_rng();
        Vec2 {
            x: rng.gen_range(-1.0..1.0),
            y: rng.gen_range(-1.0..1.0)
        }
    }
    pub fn inside(&self, min:Vec2, max:Vec2) -> bool {
        self.x>=min.x && self.y>=min.y && self.x<max.x && self.y<max.y
    }

    pub fn inside_box(&self, bb: (Vec2, Vec2)) -> bool {
        self.x>=bb.0.x && self.y>=bb.0.y && self.x<bb.1.x && self.y<bb.1.y
    }

    pub fn clamp(&self, bb: (Vec2, Vec2)) -> Vec2 {
        Vec2::new(
            if self.x < bb.0.x {
                bb.0.x
            } else if self.x > bb.1.x {
                bb.1.x
            } else {
                self.x
            },
            if self.y < bb.0.y {
                bb.0.y
            } else if self.y > bb.1.y {
                bb.1.y
            } else {
                self.y
            }            
        )
    }

    pub fn centre(bb: (Vec2, Vec2)) -> Vec2 {
        Vec2::new(
            bb.0.x + (bb.1.x - bb.0.x) / 2.,
            bb.0.y + (bb.1.y - bb.0.y) / 2.
        )
    }

}

impl Vector2 for Vec2 {
    type Scalar = f64;
    fn new(x: f64, y: f64) -> Self {
        Vec2 { x, y }
    }
    fn x(&self) -> f64 {
        self.x
    }
    fn y(&self) -> f64 {
        self.y
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn basics() {
        assert_eq!(Vec2::new(10.0,0.0).mag(),10.0);
        let v = Vec2::new(10.0,0.0);
        assert_eq!(v.div(v.mag()).mag(),1.0);
        assert_eq!(v.norm().mag(),1.0);
        let v2 = Vec2::zero();
        assert_eq!(v.dist(v2),10.0);
        assert_eq!(v.dist_sq(v2),100.0);
        assert_eq!(v.lerp(v2,0.5),Vec2::new(5.0,0.0));
        assert_eq!(v.lerp(v2,0.25),Vec2::new(7.5,0.0));

        assert_eq!(Vec2::new(20.0,0.0).clamp(
            (Vec2::new(0.0,0.0),Vec2::new(10.0,10.0))),
                   Vec2::new(10.0,0.0));
        assert_eq!(Vec2::new(4.0,0.0).clamp(
            (Vec2::new(10.0,10.0),Vec2::new(20.0,20.0))),
                   Vec2::new(10.0,10.0));
        assert_eq!(Vec2::new(12.0,12.0).clamp(
            (Vec2::new(10.0,10.0),Vec2::new(20.0,20.0))),
                   Vec2::new(12.0,12.0));
        
    }
}
