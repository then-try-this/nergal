// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#![allow(clippy::single_match)]
#![allow(clippy::new_without_default)]
#![allow(clippy::module_inception)]
#![allow(clippy::too_many_arguments)]
#![allow(clippy::borrowed_box)]
#![forbid(unsafe_code)]

#![doc = include_str!("../../README.md")]

use wasm_bindgen::prelude::*;
use js_sys::Array;
use web_sys::UrlSearchParams;
use std::collections::hash_map::{ DefaultHasher };
use std::hash::{ Hash, Hasher };
use instant::{ Instant };
use instant::{ Duration };

/// Common game code and subsystems.
pub mod game;
/// GUI entities and screen manager.
pub mod gui;
/// All the entity types used in the game.
pub mod entities;
/// Networking code for talking to the citizen science server.
pub mod network;
/// Items that can be carried and shared between entities (including diseases)
pub mod items;
pub mod sound;
pub mod nergal;
pub mod maths;
pub mod rendering;

use crate::game::world;
use vector2math::*;

use crate::maths::vec2::{ Vec2 };
use crate::game::id::*;
use crate::game::message::*;
use crate::network::network::*;
use crate::game::resources::{ Resources };
use crate::gui::screen_manager::{ ScreenManager, ScreenState };
use crate::sound::sound::{ Sound, Priority };
use crate::game::game_scores::{ GameScores };
use crate::nergal::*;

use web_sys::console;

const MAX_DELTA_MILLIS: u128 = 1000;

/// The main game container, for the world, resources, gui and network access.
#[wasm_bindgen]
pub struct Game {
    world: world::World,
    resources: Resources,
    time: Instant,
    /// Top level storage of messages, cleaned every frame - for
    /// distribution to all game elements.
    messages: MessageVec,
    screens: ScreenManager,
    network: Network,
    sound: Sound,
    context: web_sys::CanvasRenderingContext2d
}

impl Default for Game {
    fn default() -> Self {
        Self::new()
    }
}

#[wasm_bindgen]
impl Game {
    
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        console::log_1(&"Hello from nergal".into());

        let mut network = Network::new();
        network.register_player();
        network.update_averages();
        network.update_network_averages(&[
            "dolphin",
            "badger",
            "elephant",
        ]);
                
        // setup all the things we only need to do once
        let mut resources = Resources::new();

        let world_size_x = resources.world_state.screen_size.x;
        let world_size_y = resources.world_state.screen_size.y;
        
        resources.world_state.world_size.x = world_size_x;
        resources.world_state.world_size.y = world_size_y;
        
        resources.world_state.player_pos = Vec2::new(world_size_x/2.,
                                                     world_size_y/2.);

        nergal_daytime_colours(&mut resources);
        nergal_animations(&mut resources);                
        let screens = ScreenManager::new(resources.world_state.screen_size.x as u32,
                                             resources.world_state.screen_size.y as u32);
        //nergal_screens(&resources, &mut screens);

        let document = web_sys::window().unwrap().document().unwrap();
        let canvas = document.get_element_by_id("canvas").unwrap();
        let canvas: web_sys::HtmlCanvasElement = canvas
            .dyn_into::<web_sys::HtmlCanvasElement>()
            .map_err(|_| ())
            .unwrap();

        let context = canvas
            .get_context("2d")
            .unwrap()
            .unwrap()
            .dyn_into::<web_sys::CanvasRenderingContext2d>()
            .unwrap();

        context.set_image_smoothing_enabled(false);

        resources.screen_state = ScreenState::Intro;

        let theme = if let Ok(params) = UrlSearchParams::new_with_str(
            &document
                .location()
                .unwrap()
                .search()
                .unwrap()) {        
            if let Some(theme) = params.get("theme") {
                if theme == "night" {
                    Theme::Nighttime
                } else {
                    Theme::Daytime
                }
            } else {
                Theme::Daytime
            }
        } else {
            Theme::Daytime
        };

        Game{
            world: world::World::new(), // empty world for now
            resources,
            time: Instant::now(),
            messages: vec![
                Message::new(MessageType::SetTheme(theme), NO_ID, WORLD_ID),
                // simply to trigger screen reader (if it allows it from user input)
                Message::new(
                    MessageType::ScreenChange(ScreenState::Intro),
                    WORLD_ID,
                    SCREEN_MANAGER_ID
                )                    
            ],
            screens,
            network,
            sound: Sound::new().unwrap(),
            context
        }

    }

    #[wasm_bindgen]
    pub fn init(&mut self) {
        // build screens after font has loaded to fix text wrap
        let mut screens = ScreenManager::new(self.resources.world_state.screen_size.x as u32,
                                             self.resources.world_state.screen_size.y as u32);
        nergal_screens(&self.resources, &mut screens);

        self.screens = screens;
    }
    
    #[wasm_bindgen]
    pub fn add_sprite_resource(&mut self, name: String, image: web_sys::HtmlImageElement, desc: String) {
        let handle = self.resources.add_sprite(name.clone(),image);
        if !desc.is_empty() {
            self.resources.speech.add_image_desc(handle, &desc);
        }
    }

    #[wasm_bindgen]
    pub fn add_themed_sprite_resource(&mut self, name: String, image: web_sys::HtmlImageElement, night_image: web_sys::HtmlImageElement, desc: String) {
        let handle = self.resources.add_themed_sprite(name.clone(), image, night_image);
        if !desc.is_empty() {
            self.resources.speech.add_image_desc(handle, &desc);
        }
    }
    
/*    #[wasm_bindgen]
    pub fn add_graph(&mut self, xml: String) {
        let g = self.graph.load_from_graphml(&"x".into(),&xml);
        self.world.update_entities_from_graph(&g);
        self.graph.merge(g);
        self.graph.generate_cache();
    }*/

    #[wasm_bindgen]
    pub fn add_adjacency_graph(&mut self, filename: String, text: String) {
        self.resources.add_network(filename, text);
    }

    #[wasm_bindgen]
    pub fn add_conversations(&mut self, text: String) {
        self.resources.conversations = self.resources.conversations.load_from_graphml(&text);
    }

    #[wasm_bindgen]
    pub fn add_audio_buffer(&mut self, name: String, buffer: web_sys::AudioBuffer) {
        self.sound.add_buffer(name, buffer);
    }
    
    #[wasm_bindgen]
    pub fn handle(&mut self, key: String) {
        self.messages.push(Message::new(
            MessageType::KeyPress(key.clone()), WORLD_ID, WORLD_ID)
        );        
    }

    #[wasm_bindgen]
    pub fn handle_release(&mut self, key: String) {
        self.messages.push(Message::new(
            MessageType::KeyRelease(key.clone()), WORLD_ID, WORLD_ID)
        );        
    }

    #[wasm_bindgen]
    pub fn play_sound(&mut self, _time: f64, _json: String) {
        //self.sound.play_sound_on(time, &serde_json::from_str(&json).unwrap());
        //self.sound.update();
    }

    #[wasm_bindgen]
    pub fn get_time(&self) -> f64 {
        self.sound.context.current_time()
    }

    #[wasm_bindgen]
    pub fn update_sound(&mut self) {
        self.sound.update()
    }

    #[wasm_bindgen]
    pub fn add_bitmap_resource(&mut self, name: String, image: web_sys::HtmlImageElement) {
        self.resources.add_bitmap(name, image);
    }
    
    #[wasm_bindgen]
    pub fn mouse(&mut self, vx: u32, vy: u32, event_type: String) {        
        let world_pos = Vec2::new(vx as f64 - self.world.world_offset.x,
                                  vy as f64 - self.world.world_offset.y);
        
        self.messages.push(Message::new(
            if event_type=="mousedown" {
                MessageType::MouseClick(
                    Vec2::new(vx as f64, vy as f64),
                    world_pos,
                    self.world.entity_here(world_pos)
                )
            } else {
                MessageType::MouseMove(
                    Vec2::new(vx as f64, vy as f64),
                    world_pos,
                    false
                )
            },
            WORLD_ID, WORLD_ID
        ));
    }

    #[wasm_bindgen]
    pub fn visible(&mut self, is_hidden: bool) {
        if is_hidden {
            self.sound.stop_all_sounds();
        }
    }


    #[wasm_bindgen]
    pub fn stats(&self) -> wasm_bindgen::JsValue {
        if let Ok(player) = self.world.entity_grid.get_entity(PLAYER_ID) {
            let (_x,_y) = self.world.entity_grid.address_tuple(
                player.pos()
            );
            
            return wasm_bindgen::JsValue::from(
                [            
                    format!("network: {}", self.resources.world_state.network_filename),
                    format!("pos: {},{}", player.pos().x as u32, player.pos().y as u32),
                    format!("num_friends: {}", self.resources.social_graph.player_friend_cache.len()),
                    //format!("location: {},{}", x,y),
                    //format!("grid size: {}", self.world.entity_grid.grid.len()),
                    //format!("world size: {:?}", self.resources.world_state.world_size),
                    format!("num entities: {}", self.world.entity_grid.grid.iter().fold(0,|acc,b| acc+b.entities.len())),
                    //format!("scenery size: {}", self.world.scenery_grid.grid.iter().fold(0,|acc,b| acc+b.entities.len())),
                    //format!("grid crossings: {}", self.world.entity_grid.num_crossings),
                    //format!("empty cells: {}", self.world.entity_grid.grid.iter().fold(0,|acc,b| if b.entities.is_empty() { acc+1 } else { acc })),
                    //format!("message queue: {}", self.world.num_messages),
                ]
                    .iter()
                    .map(|x| wasm_bindgen::JsValue::from_str(x))
                    .collect::<Array>());
        } else {
            return wasm_bindgen::JsValue::from(
                ["Game not running.".to_string()]
                    .iter()
                    .map(|x| wasm_bindgen::JsValue::from_str(x))
                    .collect::<Array>());
        }
    }

    #[wasm_bindgen]
    pub fn safari_hack(&mut self) {
        self.resources.safari_hack = true; 
    }

    pub fn scores(&self) -> wasm_bindgen::JsValue {
        return wasm_bindgen::JsValue::from(
            [            
                format!("times ill: {}", self.resources.world_state.scores.times_ill),
                format!("nergals infected by player: {}", self.resources.world_state.scores.nergals_infected),
                format!("nergals talked to: {}", self.resources.world_state.scores.nergals_talked_to.len()),
                format!("num friends made: {}", self.resources.world_state.scores.num_friends_made),
                format!("friends at end: {}", self.resources.world_state.scores.num_friends_at_end),
                format!("lifetime: {}", self.resources.world_state.scores.lifetime),
            ]
                .iter()
                .map(|x| wasm_bindgen::JsValue::from_str(x))
                .collect::<Array>());
    }


    #[wasm_bindgen]
    #[allow(unused_must_use)]
    pub fn render(&mut self) {

        let mut delta = self.time.elapsed();
        self.time = Instant::now();        
        let mut init_entities = false;
        let mut quit_game = false;
        
        if delta.as_millis() > MAX_DELTA_MILLIS {
            delta = Duration::from_millis(MAX_DELTA_MILLIS as u64);
        }
        
        ///////////////////////////////////////////////////////////////
        // update and collect messages        
        self.messages = self.screens.update(&mut self.messages, &self.resources, &mut self.sound, &delta); 
        
        // only update the game if we are "in" the game
        if self.screens.state == ScreenState::Game {
            self.messages = self.world.update(&mut self.messages, &mut self.resources, &mut self.sound, &delta);
        }

        for msg in &self.messages {
            match &msg.data {
                MessageType::InitGame(player_char, network) => {
                    self.world = world::World::create(&mut self.resources, &self.sound);
                    self.resources.world_state.game_time = 0;
                    self.resources.world_state.conversation_happening = false;
                    self.resources.world_state.menu_open = false;
                    self.resources.generate_graph(network.clone());
                    self.resources.world_state.network_filename = network.into(); 
                    self.world.update_entities_from_graph(&mut self.resources, *player_char);
                    self.resources.world_state.scores = GameScores::new();
                    init_entities=true;
                },

                MessageType::PlayerInfo(value) => {
                    if value == "consent_yes" {
                        self.resources.world_state.consent_given = true;
                    }
                }
                
                MessageType::SetTheme(theme) => {
                    self.resources.theme = *theme;

                    match theme {
                        Theme::Daytime => {
                            nergal_daytime_colours(&mut self.resources);
                            self.resources.speech.active = false;
                        }
                        Theme::Nighttime => {
                            nergal_nighttime_colours(&mut self.resources);
                            self.resources.speech.active = true;
                        }
                    }

                    nergal_switch_theme(&mut self.screens, &self.resources);
                }
                
                MessageType::PlaySoundId(id, duration, desc) => {
                    let mut h = DefaultHasher::new();
                    msg.sender.hash(&mut h);
                    id.hash(&mut h);
                    let sid = h.finish();
                    self.sound.register_entity(msg.sender,sid);
                    let priority = if msg.sender == PLAYER_ID {
                        Priority::High
                    } else {
                        Priority::Normal
                    };
                    self.sound.play_sound(sid, *duration, desc, priority);
                }

                MessageType::PlaySoundAt(pos, duration, desc) => {
                    let mut h = DefaultHasher::new();
                    msg.sender.hash(&mut h);
                    let sid = h.finish();
                    self.sound.register_entity(msg.sender,sid);
                    let priority = if msg.sender == PLAYER_ID {
                        Priority::High
                    } else {
                        Priority::Normal
                    };
                    self.sound.play_sound_at(sid, *duration, *pos, desc, priority);
                }

                MessageType::PlaySoundAtId(id, pos, duration, desc) => {                    
                    let mut h = DefaultHasher::new();
                    msg.sender.hash(&mut h);
                    id.hash(&mut h);
                    let sid = h.finish();
                    self.sound.register_entity(msg.sender,sid);
                    let priority = if msg.sender == PLAYER_ID {
                        Priority::High
                    } else {
                        Priority::Normal
                    };
                    self.sound.play_sound_at(sid, *duration, *pos, desc, priority);
                }

                MessageType::ModifySound(id, param_vec) => {
                    let mut h = DefaultHasher::new();
                    msg.sender.hash(&mut h);
                    id.hash(&mut h);
                    let sid = h.finish();
                    self.sound.modify_parameters(sid, param_vec);
                }

                MessageType::StopSound(id) => {
                    let mut h = DefaultHasher::new();
                    msg.sender.hash(&mut h);
                    id.hash(&mut h);
                    let sid = h.finish();
                    self.sound.unregister_entity(msg.sender,sid);
                    self.sound.stop_sound(sid);
                }

                MessageType::SpeechParam(name, value) => {
                    match name.as_str() {
                        "speed" => { self.resources.speech.speech_speed = *value; }
                        "player_pitch" => { self.resources.speech.player_pitch = *value; }
                        _ => {}
                    }
                    
                    //self.resources.speech.say(Voice::Player, "Testing speech now: one, two, three.");
                }

                MessageType::AddSpeechToQueue(voice, text) => {
                    //self.resources.speech.add_to_queue(*voice, text.to_string());
                    self.resources.speech.say(*voice, text);
                }

                MessageType::Say(voice, text) => {
                    //self.resources.speech.add_to_queue(*voice, text.to_string());
                    self.resources.speech.say(*voice, text);
                }
                
                MessageType::ScreenChange(state) => {

                    if *state == ScreenState::SelectGhost || self.resources.screen_state == ScreenState::SelectChar {
                        // register the game now, so the id is
                        // (hopefully) ready in advance of init game
                        // for logging
                        self.network.register_game();
                    }


                    // first check if we are coming out of the game,
                    // tidy up and record things here
                    if self.resources.screen_state == ScreenState::Game {
                        self.sound.stop_all_sounds();

                        self.resources.world_state.scores.lifetime =
                            self.resources.world_state.game_time;
                        self.resources.world_state.scores.num_friends_at_end =
                            self.resources.social_graph.all_connected_entity_ids(PLAYER_ID).len() as u32;

                        self.network.update_game(
                            &self.resources.world_state.scores,
                            self.resources.theme,
                            &self.resources.world_state.network_filename
                        );                        
                    }

                    if *state == ScreenState::Game {
                        nergal_init_prompts(&mut self.screens, &mut self.resources); 
                    }
                    
                    if *state == ScreenState::GameOver(GameOverReason::NoHealth) {
                        nergal_update_diagnostic(&mut self.screens, &mut self.resources);
                    }
                    
                    if *state == ScreenState::Results {
                        nergal_generate_results(
                            &mut self.screens,
                            &self.network.get_network_averages(match self.resources.world_state.network_filename.as_str() {
                                "/networks/badger_matrix.txt" => {
                                    "badger"
                                }
                                "/networks/dolphin_matrix.txt" => {
                                    "dolphin"
                                }
                                _ => {
                                    "elephant"
                                }
                            }),
                            &self.resources
                        );
                    }
                    
                    self.resources.screen_state = *state;
                    self.resources.screens_visited.insert(*state);                        

                    if *state != ScreenState::Game {
                        let world_size_x = self.resources.world_state.screen_size.x;
                        let world_size_y = self.resources.world_state.screen_size.y;
                        
                        self.resources.world_state.world_size.x = world_size_x;
                        self.resources.world_state.world_size.y = world_size_y;
                                                
                        self.resources.world_state.player_pos = Vec2::new(world_size_x/2.,
                                                                          world_size_y/2.);
                    }

                    if *state == ScreenState::AboutData {
                        nergal_update_about_data(&mut self.screens, &self.resources);
                    }
                    
                    if *state == ScreenState::SpeechOptions {
                        nergal_update_voices(&mut self.screens, &mut self.resources);
                    }

                }

                MessageType::KeyPress(key) => {
                    if key == "t" {
                        self.resources.speech.test();
                    }
                    if key == "escape" {
                        quit_game = true;
                    }
                }
                 _ => {}
            }

            self.network.store_event(msg, &self.resources, &self.world);
            self.resources.world_state.scores.update(msg);
        }

        if quit_game {
            self.messages.push(Message::new(
                MessageType::GameOver(GameOverReason::QuitButton),
                NO_ID,
                WORLD_ID
            ));

            self.messages.push(Message::new(
                MessageType::ScreenChange(ScreenState::GameOver(GameOverReason::QuitButton)),
                NO_ID,
                SCREEN_MANAGER_ID
            ));
        }
        
        /////////////////////////////////////////////////////////////
        // dispatch messages        
        self.screens.dispatch(&self.messages, &self.resources);

        if self.screens.state == ScreenState::Game {
            self.world.dispatch(&self.messages, &self.resources);
        }

        /////////////////////////////////////////////////////////////
        // render
        
        //self.context.clear_rect(0.0,0.0,1700.0,750.0);
        self.context.begin_path();
        self.context.rect(0.0, 0.0,
                     self.resources.world_state.screen_size.x,
                     self.resources.world_state.screen_size.y);
        self.context.set_fill_style(&self.resources.colour("background").into());
        self.context.fill();

        //self.world = self.world.relax();
        self.context.set_font(self.resources.attr("default_font")); 

        if self.screens.state == ScreenState::Game {
            self.world.render(&self.context, &self.resources, &delta);
        }
        
        self.screens.render(&self.context, &self.resources, &delta);

        self.sound.update();
        self.resources.speech.update();
        self.messages.clear();

        self.resources.world_state.game_time += delta.as_millis();

        // insert here to pop up the next frame and sent to gui and all entities
        // best way I could come up with to do this...
        if init_entities {
            self.messages.push(Message::new(MessageType::NewGame, WORLD_ID, NO_ID));
        }
        
    }
}



