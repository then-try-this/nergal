pub mod oscillator;
pub mod gain;
pub mod biquad_filter;
pub mod channel_merger;
pub mod sample;
pub mod panner;
pub mod envelope;
