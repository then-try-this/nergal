// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use serde_json::Value;

use crate::sound::sound::{ Sound, Buffers };
use crate::sound::synth_node::{ SynthNode };
use crate::sound::synth_node_desc::{ SynthNodeDesc };
use crate::declare_synth_node;



/// A very simple envelope with built in gain node
pub struct Envelope {
    pub node: web_sys::GainNode,
    pub children: HashMap<String, Box<dyn SynthNode>>,
    pub attack_time: f64,
    pub release_time: f64    
}

impl Envelope {
    pub fn new(sound: &Sound, desc: &SynthNodeDesc) -> Result<Self, String> {
        let mut osc = Envelope {
            node: sound.context.create_gain().unwrap(),
            children: HashMap::new(),
            attack_time: 0.01,
            release_time: 1.0
        };
        osc.node.gain().set_value(0.);
        osc.initialise(sound, desc);
        Ok(osc)
    }                         
}

impl SynthNode for Envelope {

    fn trigger(&self, time: f64) {
        let _ = self.node.gain().cancel_scheduled_values(time);
        let _ = self.node.gain().set_value_at_time(0., time);
        let _ = self.node.gain().linear_ramp_to_value_at_time(1., time + self.attack_time);
        let _ = self.node.gain().linear_ramp_to_value_at_time(0., time + self.release_time);

        for child in self.children().values() {
            child.trigger(time);
        }
    }

    fn set_parameters(&mut self, _buffers: &Buffers, parameters: &HashMap<String, Value>) {
        for (param, value) in parameters {
            match param.as_str() {
                "attack_time" => {
                    if let Value::Number(value) = value {
                        self.attack_time = value.as_f64().unwrap();
                    }
                },
                "release_time" => {
                    if let Value::Number(value) = value {
                        self.release_time = value.as_f64().unwrap();                        
                    }
                }
                _ => {}
            }
        }
    }

    fn connect_node(&self, parameter: &str, node: &web_sys::AudioNode) {
        match parameter {
            "audio" => {
                let _ = node.connect_with_audio_node(&self.node);
            }
            _ => {}
        }
    }

    declare_synth_node!();
}
