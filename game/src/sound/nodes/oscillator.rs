// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use serde_json::Value;
use web_sys::{ OscillatorType };
use crate::sound::sound::{ Sound, Buffers };
use crate::sound::synth_node::{ SynthNode };
use crate::sound::synth_node_desc::{ SynthNodeDesc };
use crate::declare_synth_node;



pub struct Oscillator {
    pub node: web_sys::OscillatorNode,
    pub children: HashMap<String, Box<dyn SynthNode>>       
}

impl Oscillator {
    pub fn new(sound: &Sound, desc: &SynthNodeDesc) -> Result<Self, String> {
        let mut osc = Oscillator {
            node: sound.context.create_oscillator().unwrap(),
            children: HashMap::new()
        };
        osc.initialise(sound, desc);
        Ok(osc)
    }                         
}

impl Drop for Oscillator {
    fn drop(&mut self) {
        let _ = self.node.stop();
    }
}

impl SynthNode for Oscillator {

    fn trigger(&self, time: f64) {
        let _ = self.node.start_with_when(time);

        for child in self.children().values() {
            child.trigger(time);
        }
    }

    
    fn set_parameters(&mut self, _buffers: &Buffers, parameters: &HashMap<String, Value>) {
        for (param, value) in parameters {
            match param.as_str() {
                "freq" => {
                    if let Value::Number(value) = value {
                        self.node.frequency().set_value(value.as_f64().unwrap() as f32);
                    }
                }
                "shape" => {
                    if let Value::String(value) = value {
                        match value.as_str() {
                            "sine" => self.node.set_type(OscillatorType::Sine),
                            "square" => self.node.set_type(OscillatorType::Square),
                            "sawtooth" => self.node.set_type(OscillatorType::Sawtooth),
                            "triangle" => self.node.set_type(OscillatorType::Triangle),
                            _ => {}
                        }
                    }
                }
                _ => {}
            }
        }
    }

    fn connect_node(&self, parameter: &str, node: &web_sys::AudioNode) {
        match parameter {
            "audio" => {
                let _ = node.connect_with_audio_node(&self.node);
            }
            "freq" => {
                let _ = node.connect_with_audio_param(&self.node.frequency());                
            }            
            _ => {}
        }
    }

    declare_synth_node!();
}
