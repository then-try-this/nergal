// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use serde_json::Value;
use web_sys::{ BiquadFilterType };
use crate::sound::sound::{ Sound, Buffers };
use crate::sound::synth_node::{ SynthNode };
use crate::sound::synth_node_desc::{ SynthNodeDesc };
use crate::declare_synth_node;



pub struct BiquadFilter {
    pub node: web_sys::BiquadFilterNode,
    pub children: HashMap<String, Box<dyn SynthNode>>       
}

impl BiquadFilter {
    pub fn new(sound: &Sound, desc: &SynthNodeDesc) -> Result<Self, String> {
        let mut filter = BiquadFilter {
            node: sound.context.create_biquad_filter().unwrap(),
            children: HashMap::new()
        };
        filter.initialise(sound, desc);
        Ok(filter)
    }                         
}

impl SynthNode for BiquadFilter {

    fn set_parameters(&mut self, _buffers: &Buffers, parameters: &HashMap<String, Value>) {
        for (param, value) in parameters {
            match param.as_str() {
                "freq" => {
                    if let Value::Number(value) = value {
                        self.node.frequency().set_value(value.as_f64().unwrap() as f32);
                    }
                }
                "detune" => {
                    if let Value::Number(value) = value {
                        self.node.detune().set_value(value.as_f64().unwrap() as f32);
                    }
                }
                "q" => {
                    if let Value::Number(value) = value {
                        self.node.q().set_value(value.as_f64().unwrap() as f32);
                    }
                }
                "gain" => {
                    if let Value::Number(value) = value {
                        self.node.gain().set_value(value.as_f64().unwrap() as f32);
                    }
                }
                "type" => {
                    if let Value::String(value) = value {
                        match value.as_str() {
                            "lowpass" => self.node.set_type(BiquadFilterType::Lowpass),
                            "highpass" => self.node.set_type(BiquadFilterType::Highpass),
                            "bandpass" => self.node.set_type(BiquadFilterType::Bandpass),                            
                            "lowshelf" => self.node.set_type(BiquadFilterType::Lowshelf),
                            "highshelf" => self.node.set_type(BiquadFilterType::Highshelf),
                            "peaking" => self.node.set_type(BiquadFilterType::Peaking),
                            "notch" => self.node.set_type(BiquadFilterType::Notch),
                            "allpass" => self.node.set_type(BiquadFilterType::Allpass),
                            _ => {}
                        }
                    }
                }
                _ => {}
            }
        }
    }

    fn connect_node(&self, parameter: &str, node: &web_sys::AudioNode) {
        match parameter {
            "audio" => {
                let _ = node.connect_with_audio_node(&self.node);
            }
            "freq" => {
                let _ = node.connect_with_audio_param(&self.node.frequency());                
            }            
            "detune" => {
                let _ = node.connect_with_audio_param(&self.node.detune());                
            }
            "q" => {
                let _ = node.connect_with_audio_param(&self.node.q());                
            }
            "gain" => {
                let _ = node.connect_with_audio_param(&self.node.gain());                
            }
            _ => {}
        }
    }

    declare_synth_node!();
}
