// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use serde_json::Value;

use crate::sound::sound::{ Sound, Buffers };
use crate::sound::synth_node::{ SynthNode };
use crate::sound::synth_node_desc::{ SynthNodeDesc };
use crate::declare_synth_node;

use web_sys::console;

pub struct ChannelMerger {
    pub node: web_sys::ChannelMergerNode,
    pub children: HashMap<String, Box<dyn SynthNode>>       
}

impl ChannelMerger {
    pub fn new(sound: &Sound, desc: &SynthNodeDesc) -> Result<Self, String> {
        match sound.context.create_channel_merger() {
            Ok(node) => {
                let mut node = ChannelMerger {
                    node,
                    children: HashMap::new()
                };
                //node.node.number_of_inputs(6);
                node.initialise(sound, desc);
                Ok(node)
            },
            Err(v) => {
                console::log_1(&v);
                Err("could not create".into())
            }
        }
    }                         
}

impl SynthNode for ChannelMerger {

    fn set_parameters(&mut self, _buffers: &Buffers, parameters: &HashMap<String, Value>) {
        for (param, value) in parameters {
            match param.as_str() {
                "channel_count" => {
                    if let Value::Number(_value) = value {
                        //self.node.set_number_of_inputs(value.as_f64().unwrap() as u32);
                    }
                }
                _ => {}
            }
        }
    }

    fn connect_node(&self, parameter: &str, node: &web_sys::AudioNode) {
        match parameter {
            "audio_0" => {
                let _ = node.connect_with_audio_node_and_output_and_input(&self.node, 0, 0);
            }
            "audio_1" => {
                let _ = node.connect_with_audio_node_and_output_and_input(&self.node, 0, 1);
            }
            "audio_2" => {
                let _ = node.connect_with_audio_node_and_output_and_input(&self.node, 0, 2);
            }
            "audio_3" => {
                let _ = node.connect_with_audio_node_and_output_and_input(&self.node, 0, 3);
            }
            "audio_4" => {
                let _ = node.connect_with_audio_node_and_output_and_input(&self.node, 0, 4);
            }
            "audio_5" => {
                let _ = node.connect_with_audio_node_and_output_and_input(&self.node, 0, 5);
            }
            "audio_6" => {
                let _ = node.connect_with_audio_node_and_output_and_input(&self.node, 0, 6);
            }
            _ => {}
        }
    }

    declare_synth_node!();
}
