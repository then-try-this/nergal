// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use serde_json::Value;

use crate::sound::sound::{ Sound, Buffers };
use crate::sound::synth_node::{ SynthNode };
use crate::sound::synth_node_desc::{ SynthNodeDesc };
use crate::declare_synth_node;



pub struct Gain {
    pub node: web_sys::GainNode,
    pub children: HashMap<String, Box<dyn SynthNode>>       
}

impl Gain {
    pub fn new(sound: &Sound, desc: &SynthNodeDesc) -> Result<Self, String> {
        let mut gain = Gain {
            node: sound.context.create_gain().unwrap(),
            children: HashMap::new()
        };
        gain.initialise(sound, desc);
        Ok(gain)
    }                         
}

impl SynthNode for Gain {

    fn set_parameters(&mut self, _buffers: &Buffers, parameters: &HashMap<String, Value>) {
        for (param, value) in parameters {
            match param.as_str() {
                "gain" => {
                    if let Value::Number(value) = value {
                        let gain = value.as_f64().unwrap() as f32;
                        self.node.gain().set_value(gain);
                    }
                }
                _ => {}
            }
        }
    }

    fn connect_node(&self, parameter: &str, node: &web_sys::AudioNode) {
        match parameter {
            "audio" => {
                let _ = node.connect_with_audio_node(&self.node);
            }
            "gain" => {
                let _ = node.connect_with_audio_param(&self.node.gain());                
            }            
            _ => {}
        }
    }

    declare_synth_node!();
}
