// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::collections::hash_map::{ DefaultHasher };
use std::hash::{ Hash, Hasher };
use serde_json::Value;

use crate::sound::sound::{ Sound, Buffers };
use crate::sound::synth_node::{ SynthNode };
use crate::sound::synth_node_desc::{ SynthNodeDesc };
use crate::declare_synth_node;

use web_sys::console;


pub struct Sample {
    pub node: web_sys::AudioBufferSourceNode,
    pub children: HashMap<String, Box<dyn SynthNode>>       
}

impl Sample {
    pub fn new(sound: &Sound, desc: &SynthNodeDesc) -> Result<Self, String> {
        match sound.context.create_buffer_source() {
            Ok(node) => {
                let mut ret = Sample {
                    node,
                    children: HashMap::new()
                };
                ret.initialise(sound, desc);
                Ok(ret)
            },
            Err(v) => {
                console::log_1(&v);
                Err("could not create".into())
            }
        }
    }                         
}

impl SynthNode for Sample {

    fn trigger(&self, time: f64) {
        let _ = self.node.start_with_when(time);

        for child in self.children().values() {
            child.trigger(time);
        }
    }

    fn stop(&self) {
        let _ = self.node.stop();
    }
    
    fn set_parameters(&mut self, buffers: &Buffers, parameters: &HashMap<String, Value>) {
        for (param, value) in parameters {
            match param.as_str() {
                "file_name" => {
                    if let Value::String(value) = value {
                        let mut h = DefaultHasher::new();
                        value.hash(&mut h);
                        if let Some(buffer) = buffers.get(&h.finish()) {
                            self.node.set_buffer(Some(buffer));
                        } else {
                            console::log_1(&format!("couldn't find sound buffer from filename: {}", value).into());
                        }
                    }
                }
                "file_handle" => {
                    if let Value::Number(value) = value {
                        if let Some(buffer) = buffers.get(&value.as_u64().unwrap()) {                       
                            self.node.set_buffer(Some(buffer));
                        } else {
                            console::log_1(&"couldn't find sound buffer from handle".into());
                        }
                    }
                }
                "detune" => {
                    if let Value::Number(value) = value {                        
                        self.node.detune().set_value(value.as_f64().unwrap() as f32);
                    }
                }
                "playback_rate" => {
                    if let Value::Number(value) = value {                        
                        self.node.playback_rate().set_value(value.as_f64().unwrap() as f32);
                    }
                }
                "loop" => {
                    if let Value::Number(value) = value {                        
                        self.node.set_loop(value.as_f64().unwrap() != 0.);
                    }
                }
                "loop_start" => {
                    if let Value::Number(value) = value {                        
                        self.node.set_loop_start(value.as_f64().unwrap());
                    }
                }
                "loop_end" => {
                    if let Value::Number(value) = value {                        
                        self.node.set_loop_end(value.as_f64().unwrap());
                    }
                }
                _ => {}
            }
        }
    }

    fn connect_node(&self, parameter: &str, node: &web_sys::AudioNode) {
        match parameter {
            "audio" => {
                let _ = node.connect_with_audio_node(&self.node);
            }
            _ => {}
        }
    }

    declare_synth_node!();

}
