// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use serde_json::Value;

use crate::sound::sound::{ Sound, Buffers };
use crate::sound::synth_node::{ SynthNode };
use crate::sound::synth_node_desc::{ SynthNodeDesc };
use crate::declare_synth_node;
use web_sys::DistanceModelType;

use web_sys::console;

pub struct Panner {
    pub node: web_sys::PannerNode,
    pub children: HashMap<String, Box<dyn SynthNode>>       
}

impl Panner {
    pub fn new(sound: &Sound, desc: &SynthNodeDesc) -> Result<Self, String> {
        match sound.context.create_panner() {
            Ok(node) => {
                let mut ret = Panner {
                    node,
                    children: HashMap::new()
                };
                ret.initialise(sound, desc);
                Ok(ret)
            },
            Err(v) => {
                console::log_1(&v);
                Err("could not create".into())
            }
        }
    }                         
}

impl SynthNode for Panner {

    fn set_parameters(&mut self, _buffers: &Buffers, parameters: &HashMap<String, Value>) {
        for (param, value) in parameters {
            match param.as_str() {
                "position" => {
                    if let Value::Array(value) = value {
                        if value.len() == 3 {                        
                            if let Some(v) = value[0].as_f64() { self.node.position_x().set_value(v as f32); }
                            if let Some(v) = value[1].as_f64() { self.node.position_y().set_value(v as f32); }
                            if let Some(v) = value[2].as_f64() { self.node.position_z().set_value(v as f32); }
                        }
                    }
                }
                "orientation" => {
                    if let Value::Array(value) = value {
                        if value.len() == 3 {          
                            if let Some(v) = value[0].as_f64() { self.node.orientation_x().set_value(v as f32); }
                            if let Some(v) = value[1].as_f64() { self.node.orientation_y().set_value(v as f32); }
                            if let Some(v) = value[2].as_f64() { self.node.orientation_z().set_value(v as f32); }
                        }
                    }
                }
                "distance_model" => {
                    if let Value::Number(value) = value {
                        if let Some(v) = value.as_i64() { 
                            match v {
                                0 => self.node.set_distance_model(DistanceModelType::Linear),
                                1 => self.node.set_distance_model(DistanceModelType::Inverse),
                                2 => self.node.set_distance_model(DistanceModelType::Exponential),
                                _ => {}
                            }
                        }
                    }
                }
                "max_distance" => {
                    if let Value::Number(value) = value {
                        if let Some(v) = value.as_i64() { self.node.set_max_distance(v as f64); }
                    }
                }
                "ref_distance" => {
                    if let Value::Number(value) = value {
                        if let Some(v) = value.as_i64() { self.node.set_ref_distance(v as f64); }
                    }
                }
                "rolloff_factor" => {
                    if let Value::Number(value) = value {
                        if let Some(v) = value.as_i64() { self.node.set_rolloff_factor(v as f64); }
                    }
                }
                _ => {}
            }
        }
    }

    fn connect_node(&self, parameter: &str, node: &web_sys::AudioNode) {
        match parameter {
            "audio" => {
                let _ = node.connect_with_audio_node(&self.node);
            }
            _ => {}
        }
    }

    declare_synth_node!();

    
}
