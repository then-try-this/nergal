// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::cell::RefCell;
use vector2math::*;
use std::collections::HashMap;
use std::collections::hash_map::{ DefaultHasher };
use std::hash::{ Hash, Hasher };
use wasm_bindgen::prelude::*;
use web_sys::{ AudioContext, AudioBuffer };
use crate::sound::synth_node::*;
use crate::sound::synth_node_desc::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::id::*;
use serde_json::Value;

const MAX_SOUNDS: usize = 100;
const MAX_SOUND_DISTANCE: f64 = 1000.;

#[derive(PartialEq)]
pub enum Priority {
    High,
    Normal,
    Low
}

pub fn midi_to_freq(note: u8) -> f32 {
    27.5 * 2f32.powf((note as f32 - 21.0) / 12.0)
}

pub type Buffers = HashMap<u64, AudioBuffer>;

pub struct RootNode {
    pub end_time: f64,
    pub root: RefCell<Box<dyn SynthNode>>
}

pub struct Sound {
    pub context: AudioContext,
    pub buffers: Buffers,
    pub synth_roots: HashMap<u64, RootNode>,    
    pub entity_to_root: HashMap<EntityId, u64>
}

impl Drop for Sound {
    fn drop(&mut self) {
        let _ = self.context.close();
    }
}

impl Sound {
    pub fn new() -> Result<Self, JsValue> {
        let context = web_sys::AudioContext::new()?;
        Ok(Sound {
            context,
            buffers: HashMap::new(),
            synth_roots: HashMap::new(),
            entity_to_root: HashMap::new()
        })
    }

    pub fn update(&mut self) {
        //let before = self.synth_roots.len();

        let t = self.context.current_time();

        for root in self.synth_roots.values() {
            if t >= root.end_time {
                root.root.borrow().stop();
            }
        }
               
        self.synth_roots.retain(|_k, v| { t < v.end_time });
        
        //let after = self.synth_roots.len();
        //console::log_1(&format!("removed {} roots, have {}", before-after, after).into());

    }

    pub fn add_buffer(&mut self, path: String, buffer: AudioBuffer) {
        let mut h = DefaultHasher::new();
        path.hash(&mut h);
        let handle = h.finish();        
        self.buffers.insert(handle, buffer); 
    }

    pub fn get_buffer_from_path(&self, path: &str) -> Option<&AudioBuffer> {
        self.buffers.get(&self.get_handle_from_path(path))
    }

    pub fn get_handle_from_path(&self, path: &str) -> u64 {
        let mut h = DefaultHasher::new();
        path.hash(&mut h);
        h.finish()        
    }

    pub fn play_sound_on(&mut self, id: u64, duration: f64, time: f64, desc: &SynthNodeDesc, priority: Priority) -> Result<u64, JsValue> {
        if priority != Priority::High && self.synth_roots.len() > MAX_SOUNDS {
            return Ok(0);
        }
            
        let node = desc.create_synth_node(self)?;

        node.trigger(time);
        let _ = node.node().connect_with_audio_node(&self.context.destination());

        self.synth_roots.insert(id, RootNode {
            root: RefCell::new(node),
            end_time: self.context.current_time() + duration
        });
        
        Ok(id)
    }

    pub fn play_sound(&mut self, id: u64, duration: f64, desc: &SynthNodeDesc, priority: Priority) -> Result<u64, JsValue> {
        if priority != Priority::High && self.synth_roots.len() > MAX_SOUNDS {
            return Ok(0);
        }

        let node = desc.create_synth_node(self)?;
        let _ = node.node().connect_with_audio_node(&self.context.destination());
        node.trigger(self.context.current_time());

        self.synth_roots.insert(id, RootNode {
            root: RefCell::new(node),
            end_time: self.context.current_time() + duration
        });
        
        Ok(id)
    }
    
    pub fn play_sound_at(&mut self, id: u64, duration: f64, pos: Vec2, desc: &SynthNodeDesc, priority: Priority) -> Result<u64, JsValue> {

        if priority != Priority::High && self.synth_roots.len() > MAX_SOUNDS {
            return Ok(0);
        }

        if pos.mag() > MAX_SOUND_DISTANCE {
            return Ok(0);
        }
        
        let node = desc.create_synth_node(self)?;

        let mut panner = SynthNodeDesc::new("panner".into())
            .create_synth_node(self)?;

        panner.set_parameter(
            &self.buffers, "position", vec![pos.x, pos.y, 0.].into()
        );

        panner.set_parameter(&self.buffers, "distance_model", 0.into());       
        panner.set_parameter(&self.buffers, "ref_distance", 250.0.into());
        // max sound distance is greater than audible distance, so
        // that sounds that start playing outside of range will become
        // audible if we move towards them/them towards us
        panner.set_parameter(&self.buffers, "max_distance", 500.0.into());       
        panner.set_parameter(&self.buffers, "rolloff_factor", 1.0.into());

        panner.connect_node("audio", node.node());
        panner.children_mut().insert("audio".into(), node);
        let _ = panner.node().connect_with_audio_node(&self.context.destination());

        panner.trigger(self.context.current_time());

        self.synth_roots.insert(id, RootNode {
            root: RefCell::new(panner),
            end_time: self.context.current_time() + duration
        });
        
        Ok(id)
    }
    
    pub fn modify_parameters(&mut self, id: u64, parameters: &HashMap<String, Value>) {
        if let Some(root) = self.synth_roots.get_mut(&id) {
            // only sets root of the tree atm
            root.root.get_mut().set_parameters(&self.buffers,parameters);
        }
    }

    pub fn stop_sound(&mut self, id: u64) {
        if let Some(root) = self.synth_roots.get(&id) {            
            root.root.borrow().stop();
        }
    }

    pub fn stop_all_sounds(&mut self) {
        for root in self.synth_roots.values() {            
            root.root.borrow().stop();
        }
    }

    pub fn register_entity(&mut self, id: EntityId, sid: u64) {
        self.entity_to_root.insert(id, sid);
    }

    pub fn unregister_entity(&mut self, id: EntityId, _sid: u64) {
        self.entity_to_root.remove(&id);        
    }

    pub fn update_position(&mut self, id: EntityId, pos: Vec2) {
        if let Some(sid) = self.entity_to_root.get(&id) {
            if let Some(root) = self.synth_roots.get_mut(sid) {
                if pos.mag() > MAX_SOUND_DISTANCE {
                    root.root.borrow().stop();
                } else {                    
                    root.root.borrow_mut().set_parameter(
                        &self.buffers,
                        "position",
                        vec![pos.x, pos.y, 0.].into()
                    );
                }
            }
        }
    }
}
