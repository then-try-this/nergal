// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// Synthesis aimed at mapping to webaudio, and focused on quantity
/// rather than depth.

/// a-rate/k-rate

/// Basic nodes:
/// Oscillator -> sine, square, sawtooth, triangle [custom]
/// BiquadFilter -> lowpass, highpass, bandpass, lowshelf, highshelf, peaking, notch, allpass
/// Gain
/// StereoPanner
/// Convolver
/// Waveshaper -> curve
/// Delay -> delaytime
/// ConstantSource
/// ChannelSplitter
/// ChannelMerger
/// AudioBufferSource
/// DynamicCompressor -> threshold, knee, ratio, reduction, attack, release 
/// IIRFilter
/// Analyser (vocoder?)
/// Panner <- final 3D positioning

/// More complex nodes also possible, combining the above...

use std::collections::HashMap;

use serde_json::Value;
use crate::sound::sound::{ Sound, Buffers };
use crate::sound::synth_node_desc::{ SynthNodeDesc };

#[macro_export]
macro_rules! declare_synth_node {
    () => {
        fn node(&self) -> &web_sys::AudioNode { &self.node }
        fn children(&self) -> &HashMap<String, Box<dyn SynthNode>> { &self.children }
        fn children_mut(&mut self) -> &mut HashMap<String, Box<dyn SynthNode>> { &mut self.children }
    }
}


/// A node in a synth tree. Contains the descendants of this node.
pub trait SynthNode {
    fn node(&self) -> &web_sys::AudioNode;
    fn children(&self) -> &HashMap<String,  Box<dyn SynthNode>>;
    fn children_mut(&mut self) -> &mut HashMap<String,  Box<dyn SynthNode>>;
    fn set_parameters(&mut self, buffers: &Buffers, parameters: &HashMap<String, Value>);
    fn connect_node(&self, parameter: &str, node: &web_sys::AudioNode);

    fn trigger(&self, time: f64) {
        for child in self.children().values() {
            child.trigger(time);
        }
    }

    fn stop(&self) {
        for child in self.children().values() {
            child.stop();
        }
    }
    
    /// Helper to set a single parameter
    fn set_parameter(&mut self, buffers: &Buffers, name: &str, value: Value) {
        let mut p = HashMap::new();
        p.insert(name.into(), value);
        self.set_parameters(buffers, &p);
    }

    /// Initialise the node with a bunch of parameters in one go
    fn initialise(&mut self, sound: &Sound, desc: &SynthNodeDesc) {
        for (param, desc_node) in desc.children.iter() {
            let child = desc_node.create_synth_node(sound).unwrap();
            self.connect_node(param, child.node());
            self.children_mut().insert(param.clone(), child);
        }
        
        self.set_parameters(&sound.buffers, &desc.parameters);
    }
        
}
