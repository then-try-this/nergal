// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use serde::{ Serialize, Deserialize };
use serde_json::Value;

use crate::sound::sound::{ Sound };
use crate::sound::synth_node::{ SynthNode };
use crate::sound::nodes::oscillator::{ Oscillator };
use crate::sound::nodes::gain::{ Gain };
use crate::sound::nodes::biquad_filter::{ BiquadFilter };
use crate::sound::nodes::channel_merger::{ ChannelMerger };
use crate::sound::nodes::sample::{ Sample };
use crate::sound::nodes::panner::{ Panner };
use crate::sound::nodes::envelope::{ Envelope };




#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SynthNodeDesc {
    pub name: String,
    pub parameters: HashMap<String, Value>,
    pub children: HashMap<String, Box<SynthNodeDesc>>
}

impl SynthNodeDesc {
    pub fn new(name: String) -> Self {
        SynthNodeDesc {
            name,
            parameters: HashMap::new(),
            children: HashMap::new()
        }
    }

    /// Make them easier to build in code
    pub fn build(name: &str, param_vec: &[(&str, Value)], child_vec: &[(&str, SynthNodeDesc)]) -> Self {
        let mut parameters: HashMap<String, Value> = HashMap::new();
        for param in param_vec {
            parameters.insert(param.0.into(), param.1.clone());
        }

        let mut children: HashMap<String, Box<SynthNodeDesc>> = HashMap::new();
        for child in child_vec {
            children.insert(child.0.into(), Box::new(child.1.clone()));
        }
        
        SynthNodeDesc {
            name: name.into(),
            parameters,
            children
        }
    }
    
    pub fn create_synth_node(&self, sound: &Sound) -> Result<Box<dyn SynthNode>, String> {
        //console::log_1(&format!("creating {}", self.name).into());
        match self.name.as_str() {
            "osc" => { Ok(Box::new(Oscillator::new(sound, self)?)) },
            "gain" => { Ok(Box::new(Gain::new(sound, self)?)) },
            "biquad_filter" => { Ok(Box::new(BiquadFilter::new(sound, self)?)) },
            "channel_merger" => { Ok(Box::new(ChannelMerger::new(sound, self)?)) },
            "sample" => { Ok(Box::new(Sample::new(sound, self)?)) },
            "panner" => { Ok(Box::new(Panner::new(sound, self)?)) },
            "envelope" => { Ok(Box::new(Envelope::new(sound, self)?)) },
            &_ => Err("unknown node name".into())
        }
    }
}
