// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::collections::VecDeque;

use web_sys::SpeechSynthesisUtterance;
use web_sys::SpeechSynthesis;
use web_sys::console;


use web_sys::SpeechSynthesisVoice;

#[derive(Debug, Copy, Clone)]
pub enum Voice {
    Narrator,
    Player,
    Nergal,
    Plant,
    Cloud
}

pub struct Speech {    
    #[cfg(target_family = "wasm")]
    pub synth: SpeechSynthesis,
    pub image_desc: HashMap<u64, String>,
    pub speech_speed: f32,
    pub player_pitch: f32,
    pub speech_volume: f32,
    pub active: bool,
    pub queue: VecDeque<(Voice, String)>,
    pub voices: Vec<SpeechSynthesisVoice>
}


impl Speech {

    #[cfg(target_family = "wasm")]
    pub fn new() -> Self {
        let window = web_sys::window().expect("global window does not exists");    
        let synth = window.speech_synthesis().expect("no speech synth found in window");

        let mut voices = vec![];
        
        //console::log_1(&"Voice information from speech synth".into());
        for v in synth.get_voices() {
            if let Ok(lang) = js_sys::Reflect::get(&v, &"lang".into()) {
                if lang == "en" {
                    //console::log_1(&v);
                    voices.push(v.into());
                }
            }
        }
        
        Speech {
            synth,
            image_desc: HashMap::new(),
            speech_speed: 1.2,
            player_pitch: 1.0,
            speech_volume: 1.0,
            active: false,
            queue: VecDeque::from(vec![]),
            voices
        }        
    }

    #[cfg(target_family = "unix")]
    pub fn new() -> Self {
        Speech {
            image_desc: HashMap::new(),
            speech_speed: 1.2,
            player_pitch: 1.0,
            speech_volume: 1.0,
            active: false,
            queue: VecDeque::from(vec![]),
            voices: vec![]
        }
    }

    #[cfg(target_family = "wasm")]
    pub fn get_voices(&mut self) {
        self.voices = vec![];        
        //console::log_1(&"Voice information from speech synth".into());
        for v in self.synth.get_voices() {
            if let Ok(lang) = js_sys::Reflect::get(&v, &"lang".into()) {
                if lang == "en" {
                    //console::log_1(&v);
                    self.voices.push(v.into());
                }
            }            
        }
        //console::log_1(&format!("Loaded {} voices", self.voices.len()).into());
    }

    
    pub fn add_image_desc(&mut self, handle: u64, desc: &str) {
        self.image_desc.insert(handle, desc.into());
    }
    
    #[cfg(target_family = "wasm")]
    pub fn test(&self) {
        if !self.active { return }
        self.synth.cancel();
        
        //if let Ok(utterance) = SpeechSynthesisUtterance::new_with_text("test 1,2,3.") {
        //    self.synth.cancel();
        //    console::log_1(&format!("saying").into());
        //    console::log_1(&format!("{:?}", self.synth.speak(&utterance)).into());
       // }
    }

    #[cfg(target_family = "wasm")]
    pub fn say(&self, voice: Voice, text: &str) {
        if !self.active { return }
        if text == "" { return }
            
        self.synth.cancel();

        // split text into sentences
        let sentences = text.split(".");
        
        for sentence in sentences {
            if sentence.len() > 1 {
                //console::log_1(&format!("saying: {}", sentence).into());
                if let Ok(utterance) = SpeechSynthesisUtterance::new_with_text(sentence) {
                    utterance.set_volume(self.speech_volume);
                    utterance.set_rate(self.speech_speed);
                    utterance.set_pitch(match voice {
                        Voice::Narrator => 1.0,
                        Voice::Player => self.player_pitch,
                        Voice::Nergal => 1.5,
                        Voice::Plant => 1.3,
                        Voice::Cloud => 0.7                    
                    });
                    self.synth.speak(&utterance);
                } else {
                    console::log_1(&"problem with utterance".into());
                }
            }
        }
    }

    #[cfg(target_family = "wasm")]
    pub fn say_pitch(&self, pitch: f32, text: &str) {
        if !self.active { return }
        if text == "" { return }
            
        self.synth.cancel();

        // split text into sentences
        let sentences = text.split(".");
        
        for sentence in sentences {
            if sentence.len() > 1 {
                //console::log_1(&format!("saying: {}", sentence).into());
                if let Ok(utterance) = SpeechSynthesisUtterance::new_with_text(sentence) {
                    utterance.set_volume(self.speech_volume);
                    utterance.set_rate(self.speech_speed);
                    utterance.set_pitch(pitch);
                    self.synth.speak(&utterance);
                } else {
                    console::log_1(&"problem with utterance".into());
                }
            }
        }
    }

    pub fn add_to_queue(&mut self, voice: Voice, text: String) {
        self.queue.push_front((voice, text));
    }

    #[cfg(target_family = "wasm")]
    pub fn update(&mut self) {
        if !self.queue.is_empty() && !self.synth.pending() {
            //console::log_1(&format!("saying {} from queue", self.queue[0].1).into());
            self.say(self.queue[0].0, &self.queue[0].1);
            self.queue.pop_front();
        }

        //if self.synth.pending() {
            //console::log_1(&"utterance pending".into());
        //}
    }
    
    
    #[cfg(target_family = "unix")]
    pub fn say(&self, _voice: Voice, _text: &str) {
    }

    #[cfg(target_family = "unix")]
    pub fn say_pitch(&self, _pitch: f32, _text: &str) {
    }
    
    #[cfg(target_family = "unix")]
    pub fn test(&self) {
    }

    #[cfg(target_family = "unix")]
    pub fn update(&mut self) {
    }

    #[cfg(target_family = "unix")]
    pub fn get_voices(&mut self) {
    }
    
}

