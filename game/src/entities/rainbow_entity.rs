// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::any::Any;
use instant::{ Duration };
use crate::game::id::*;
use vector2math::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::message::{ Message, MessageVec };
use crate::game::resources::{ Resources };
use crate::rendering::rendering::{ RenderPass };

use crate::declare_any;
use crate::declare_message_reader;

const SOUND_TRIGGER_DIST: f64 = 200.;
const SOUND_TRIGGER_PERIOD: u128 = 5000; 

#[derive(Debug,Clone)]
pub struct RainbowEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub timer: u128,
    pub image_handle: u64,
    pub state: EntityState
}

/// Like a rock, but you can walk through it.
impl RainbowEntity {
    pub fn at_pos(id: EntityId, pos: Vec2, image_handle: u64) -> Self {
        RainbowEntity {
            id,
            pos,
            timer: 0,
            image_handle,
            state: EntityState::Idle
        }   
    }
}

impl Entity for RainbowEntity {
    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    fn cat(&self) -> EntityCat { EntityCatFlags::MINERAL }

    fn describe(&self, resources: &Resources) -> String {
        if let Some(text) = resources.speech.image_desc.get(&self.image_handle) {
            text.clone()
        } else {
            "Bug: someone needs to describe this rainbow".into()
        }
    }

    fn update(&self, _entities: &EntityRefVec, resources: &Resources, delta: &Duration) -> (Box<dyn Entity>,MessageVec) {
        let new_timer = self.timer + delta.as_millis();

        match self.state {
            EntityState::Idle => {
                if self.pos.dist(resources.world_state.player_pos) < SOUND_TRIGGER_DIST {
                    return (Box::new(RainbowEntity {
                        state: EntityState::Activity,
                        timer: 0,
                        ..*self }),
                            vec![
                                self.play_sound_msg(resources, "/sounds/rainbow-1.mp3", 10.0)
                            ]);
                }
            }

            EntityState::Activity => {
                if self.timer > SOUND_TRIGGER_PERIOD {
                    return (Box::new(RainbowEntity {
                        state: EntityState::Idle,
                        ..*self }), vec![]
                    );
                }
            }

            _ => {}
        }


        (Box::new(RainbowEntity{
            timer: new_timer,
            ..*self }),
         vec![])
    }

    fn apply_message(&self, _msg: &Message, _resources: &Resources) -> Box<dyn Entity> {
        Box::new(self.clone())
    }

    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        

        match pass {
            RenderPass::Beauty => {
                if let Some(image) = resources.sprite(&self.image_handle) {
                    let x: i32 = self.pos.x as i32 - image.width() as i32 / 2;
                    let y: i32 = self.pos.y as i32 - image.height() as i32 + 20;
                    context.draw_image_with_html_image_element(image, x as f64, y as f64);
                }
            },
            
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
