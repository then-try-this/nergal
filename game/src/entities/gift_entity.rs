// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::any::Any;
use vector2math::*;
use rand::Rng;
use rand::prelude::SliceRandom;
use instant::{ Duration };

use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::message::*;
use crate::game::resources::{ Resources };
use crate::rendering::rendering::{ RenderPass };
use crate::game::constants::*;

use crate::declare_any;
use crate::declare_message_reader;



const GIFT_PIXELS_PER_MILLI: f64 = 0.05;
const GIFT_CLOSE_DISTANCE: f64 = 20.;

#[derive(Debug,Copy,Clone)]
pub enum GiftType {
    Banana,
    Blueberries,
    Ramen,
    Cupcake,
    Mango,
    Carrot,
    Beetroot,
    Popcorn,
    Hazelnuts
}

#[derive(Debug,Copy,Clone)]
/// A gift (e.g. snack) that can be passed from one character to another.
pub struct GiftEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub recipient: EntityId,
    pub image_handle: u64
}

impl GiftEntity {
    pub fn at_pos(id: EntityId, pos: Vec2, recipient: EntityId, gift_type: GiftType, resources: &Resources) -> Self {
        GiftEntity {
            id,
            pos,
            recipient,
            image_handle: resources.get_handle_from_path(
                match gift_type {
                    GiftType::Banana => "/images/sprites/gifts/banana.png",
                    GiftType::Beetroot => "/images/sprites/gifts/beetroot.png",
                    GiftType::Blueberries => "/images/sprites/gifts/blueberries.png",
                    GiftType::Carrot => "/images/sprites/gifts/carrot.png",
                    GiftType::Cupcake => "/images/sprites/gifts/cupcake.png",
                    GiftType::Hazelnuts => "/images/sprites/gifts/hazenuts.png",
                    GiftType::Mango => "/images/sprites/gifts/mango.png",
                    GiftType::Popcorn => "/images/sprites/gifts/popcorn.png",
                    GiftType::Ramen => "/images/sprites/gifts/ramen.png",
                }
            )            
        }   
    }
}

impl Entity for GiftEntity {
    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    fn cat(&self) -> EntityCat { EntityCatFlags::VEGETABLE }
    
    fn update(&self, entities: &EntityRefVec, resources: &Resources, delta: &Duration) -> (Box<dyn Entity>,MessageVec) {      
        if let Some(p) = self.find_entity(self.recipient, entities) {
            let target = Vec2::centre(p.bb());
            
           (Box::new(GiftEntity{
               pos: self.pos
                   .add(target
                        .sub(self.pos)
                        .norm()
                        .mul(GIFT_PIXELS_PER_MILLI*delta.as_millis() as f64)
                   ),  
               ..*self
           }), if self.pos.dist(target)<GIFT_CLOSE_DISTANCE {
               if p.state() != EntityState::Dead {
                   // we have arrived, and target is still alive
                   let mut rng = rand::thread_rng();
                   vec![
                       Message::new(MessageType::RemoveMe, self.id(), WORLD_ID),
                       Message::new(MessageType::HealthChange(SNACK_SIZE, HealthReason::Snack), self.id, self.recipient),
                       self.play_sound_msg(resources,"/sounds/notify-5.mp3", 1.0),
                       self.play_sound_msg(
                           resources,
                           ["/sounds/nomnom-1.mp3",
                            "/sounds/nomnom-2.mp3"]
                               .choose(&mut rng)
                               .unwrap(),
                           rng.gen_range(1.4..1.8)
                       )
                   ]                 
               } else {
                   vec![Message::new(MessageType::RemoveMe, self.id(), WORLD_ID)]
               }
           } else {
               vec![]
           })
        } else {
            (Box::new(GiftEntity {
                ..*self
            }), vec![])
        }
    }

    fn apply_message(&self, _msg: &Message, _resources: &Resources) -> Box<dyn Entity> {
        Box::new(*self)
    }

    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        match pass {
            // render on top for clarity
            RenderPass::GUI => {
                if let Some(image) = resources.sprite(&self.image_handle) {
                    context.draw_image_with_html_image_element(
                        image,
                        (self.pos.x-Into::<f64>::into(image.width()/2)).round(),
                        (self.pos.y-Into::<f64>::into(image.height()/2)).round());
                }
            }
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
