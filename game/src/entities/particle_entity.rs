// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::any::Any;
use std::cell::RefCell;
use rand::Rng;
use instant::{ Duration };
use crate::game::id::*;
use vector2math::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::message::{ Message, MessageVec };
use crate::game::resources::{ Resources };
use crate::rendering::rendering::{ RenderPass };

use crate::declare_any;
use crate::declare_message_reader;

#[derive(Debug, Clone, Copy)]
pub enum Movement {
    Drift,
    Rise,
    Explode
}

#[derive(Debug, Clone, Copy)]
pub struct Particle {
    pub id: usize,
    pub pos: Vec2,
    pub vel: Vec2,
    pub t: u128,
}

impl Particle {
    pub fn update(&mut self, movement: Movement, delta: &Duration) {
        match movement {
            Movement::Rise => {
                self.vel.x = f64::sin((self.t as f64) * 0.002 + self.id as f64) * 0.1;                
            },
            _ => {        
            }
        }
        self.pos = self.pos.add(self.vel.mul(delta.as_millis() as f64));
        self.t += delta.as_millis();
    }
}

#[derive(Debug,Clone)]
pub struct ParticleEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub image_handle: u64,
    pub timer: u128,
    pub lifetime: u128,
    pub state: EntityState,
    pub movement: Movement,
    pub particles: RefCell<Vec<Particle>>        
}

/// Entity representing a particle system of some kind
impl ParticleEntity {

    pub fn at_pos(id: EntityId, pos: Vec2, image_handle: u64, movement: Movement, num_particles: usize, lifetime: u128) -> Self {

        let mut particles = vec![];
        let rnd = &mut rand::thread_rng();
            
        for i in 0..num_particles {
            particles.push(Particle {
                id: i,
                pos: Vec2::zero(),
                vel: match movement {
                    Movement::Drift => { Vec2::zero() },
                    Movement::Rise => { Vec2::new(0., rnd.gen_range(-0.2..-0.1)) },
                    Movement::Explode => { Vec2::zero() }
                },
                t: 0
            });
        }

        ParticleEntity {
            id,
            pos,
            image_handle,
            timer: 0,
            lifetime,
            state: EntityState::Idle,
            movement,
            particles: RefCell::new(particles)
        }   
    }
}

impl Entity for ParticleEntity {
    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    fn state(&self) -> EntityState { self.state }
    fn cat(&self) -> EntityCat {
        EntityCatFlags::IMAGINARY 
    }
    
    fn update(&self, _entities: &EntityRefVec, _resources: &Resources, delta: &Duration) -> (Box<dyn Entity>,MessageVec) {

        let new_timer = self.timer + delta.as_millis();

        for p in &mut *self.particles.borrow_mut() {
            p.update(self.movement, delta);
        }
        
        (Box::new(ParticleEntity{
            timer: new_timer,
            ..self.clone() }),
         vec![])
    }
    
    fn apply_message(&self, _msg: &Message, _resources: &Resources) -> Box<dyn Entity> {
        Box::new(self.clone())     
    }

    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        
        // context.begin_path();
        // context
        //     .arc(self.pos.x,self.pos.y, 5.0, 0.0, std::f64::consts::PI * 2.00)
        //     .unwrap();
        // context.set_fill_style(&"#000".into());        
        // context.fill();
        
        match pass {
            RenderPass::GUI => {
                if let Some(image) = resources.sprite(&self.image_handle) {

                    for p in &*self.particles.borrow() {                    
                        context.draw_image_with_html_image_element(
                            image,
                            (self.pos.x + p.pos.x - image.width() as f64 / 2.).round(),
                            (self.pos.y + p.pos.y).round()
                        );
                    }
                }
            },

            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
