// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::any::Any;
use vector2math::*;
use instant::{ Duration };
use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::message::*;
use crate::game::resources::{ Resources };
use crate::rendering::rendering::{ RenderPass };
use crate::sound::synth_node_desc::{ SynthNodeDesc };

use crate::declare_any;
use crate::declare_message_reader;

#[derive(Debug, Copy, Clone)]
pub struct PianoEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub image_handle: u64,
    pub notes_image_handle: u64,
    pub sample_handles: [u64;7],
    pub debounce: bool
}

impl PianoEntity {
    pub fn at_pos(id: EntityId, pos: Vec2, image_handle: u64, notes_image_handle: u64, sample_handles: [u64;7]) -> Self {
        PianoEntity {
            id,
            pos,
            image_handle,
            notes_image_handle,
            sample_handles,
            debounce: false
        }   
    }
}

impl Entity for PianoEntity {
    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }

    fn describe(&self, resources: &Resources) -> String {
        if let Some(text) = resources.speech.image_desc.get(&self.image_handle) {
            text.clone()
        } else {
            "Bug: someone needs to describe this piano".into()
        }
    }
    
    fn update(&self, _entities: &EntityRefVec, resources: &Resources, _delta: &Duration) -> (Box<dyn Entity>,MessageVec) {
        if let Some(bitmap) = resources.bitmaps.get(&self.notes_image_handle) {

            let pos = self.pos.sub(Vec2::new(bitmap.orig_width as f64 / 2.,
                                             bitmap.orig_height as f64 / 2.));
            
            let pixel_colours = if resources.world_state.player_pos.inside_box(
                (pos,
                 pos.add(Vec2::new(bitmap.orig_width as f64,
                                   bitmap.orig_height as f64)))) {
                bitmap.get_pixel(resources.world_state.player_pos.sub(pos))                    
            } else {
                [0, 0, 0, 0]
            };
        
            let sample_played = if pixel_colours[3]>0 {
                (pixel_colours[0] as i32 * 7) / 256
            } else {
                -1
            };
            
            return (Box::new(PianoEntity {
                debounce: if self.debounce && sample_played == -1 {
                    false
                } else if sample_played != -1 {
                    true
                } else {
                    self.debounce
                },
                ..*self
            }), if sample_played > -1 && !self.debounce {
                vec![Message::new(
                    MessageType::PlaySoundAt(
                        self.pos.sub(resources.world_state.player_pos),
                        2.0,
                        SynthNodeDesc::build("sample", &[
                            ("file_handle", self.sample_handles[sample_played as usize].into()),
                            ("playback_rate", 0.5.into())
                        ], &[])
                    ), self.id, WORLD_ID
                )]
            } else {
                vec![]
            });
        }
    
        (Box::new(PianoEntity {
            ..*self
        }), vec![])
    }
    
    fn apply_message(&self, _msg: &Message, _resources: &Resources) -> Box<dyn Entity> {
        Box::new(*self)
    }

    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        match pass {
            RenderPass::Bottom => {
                if let Some(image) = resources.sprite(&self.image_handle) {
                    context.draw_image_with_html_image_element(
                        image,
                        (self.pos.x-Into::<f64>::into(image.width()/2)).round(),
                        (self.pos.y-Into::<f64>::into(image.height()/2)).round());
                }
            }
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
