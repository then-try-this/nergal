// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::any::Any;
use vector2math::*;
use instant::{ Duration };
use std::collections::HashMap;
use serde_json::Value;

use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::message::*;
use crate::game::resources::{ Resources };
use crate::rendering::rendering::{ RenderPass };
use crate::sound::synth_node_desc::{ SynthNodeDesc };

use crate::declare_any;
use crate::declare_message_reader;

#[derive(Debug, Copy, Clone)]
pub enum PlayingState {
    Stop,
    Stopped,
    Play,
    Playing,
    Pause,
    Paused,
    Unpause
}

#[derive(Debug, Copy, Clone)]
/// A non-interactive background scenery item.
pub struct SceneryEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub image_handle: u64,
    pub sample_handle: u64,
    pub playing_state: PlayingState
}

impl SceneryEntity {
    pub fn at_pos(id: EntityId, pos: Vec2, image_handle: u64, sample_handle: u64) -> Self {
        SceneryEntity {
            id,
            pos,
            image_handle,
            sample_handle,
            playing_state: PlayingState::Stopped
        }   
    }
}

impl Entity for SceneryEntity {
    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }

    fn describe(&self, resources: &Resources) -> String {
        if let Some(text) = resources.speech.image_desc.get(&self.image_handle) {
            text.clone()
        } else {
            "Bug: someone needs to describe this scenery item.".into()
        }
    }
    
    fn update(&self, _entities: &EntityRefVec, resources: &Resources, _delta: &Duration) -> (Box<dyn Entity>,MessageVec) {

        // the bitmaps are the same images as the rendered
        // ones, so the handles will be the same
        if let Some(bitmap) = resources.bitmaps.get(&self.image_handle) {

            let pos = self.pos.sub(Vec2::new(bitmap.orig_width as f64 / 2.,
                                             bitmap.orig_height as f64 / 2.));            
            let pixel_value = if resources.world_state.player_pos.inside_box(
                (pos,
                 pos.add(Vec2::new(bitmap.orig_width as f64,
                                   bitmap.orig_height as f64)))) {
                
                bitmap.get_pixel(resources.world_state.player_pos.sub(pos))                    
            } else {
                [0, 0, 0, 0]
            };

            return (Box::new(SceneryEntity {
                playing_state: match self.playing_state {
                PlayingState::Stopped => {
                    if pixel_value[3] > 0 {
                        PlayingState::Play
                    } else {
                        PlayingState::Stopped
                    }
                }
                PlayingState::Playing => {
                    if pixel_value[3] == 0 {
                        PlayingState::Stop
                    } else if !self.state_is_moving(resources.world_state.player_state) {
                        PlayingState::Pause
                    } else {
                        PlayingState::Playing
                    }
                }
                PlayingState::Paused => {
                    if self.state_is_moving(resources.world_state.player_state) {
                        PlayingState::Unpause
                    } else if pixel_value[3] == 0 {
                        // probably shouldn't happen if the player is not
                        // moving, but just to be sure
                        PlayingState::Stop
                    } else {
                        PlayingState::Paused
                    }
                }
                PlayingState::Play => { PlayingState::Playing }
                PlayingState::Pause => { PlayingState::Paused }
                PlayingState::Unpause => { PlayingState::Playing }
                PlayingState::Stop => { PlayingState::Stopped }
                },
                ..*self
            }), match self.playing_state {
                PlayingState::Play => {
                    vec![Message::new(
                        MessageType::PlaySoundId(
                            "lake_splash".into(),
                            99.0,
                            SynthNodeDesc::build("sample", &[
                                ("file_handle", self.sample_handle.into()),
                                ("playback_rate", 1.0.into()),
                                ("loop", 1.0.into())
                            ], &[])
                        ), self.id, WORLD_ID
                    )]
                }
                PlayingState::Pause => {
                    let mut parameters: HashMap<String, Value> = HashMap::new();
                    parameters.insert(
                        "playback_rate".into(),
                        0.0.into()
                    );
                    
                    vec![Message::new(
                        MessageType::ModifySound(
                            "lake_splash".into(),
                            parameters
                        ), self.id, WORLD_ID
                    )]
                }
                PlayingState::Unpause => {
                    let mut parameters: HashMap<String, Value> = HashMap::new();
                    parameters.insert(
                        "playback_rate".into(),
                        1.0.into()
                    );
                    
                    vec![Message::new(
                        MessageType::ModifySound(
                            "lake_splash".into(),
                            parameters
                        ), self.id, WORLD_ID
                    )]
                }
                PlayingState::Stop => {
                    vec![Message::new(
                        MessageType::StopSound("lake_splash".into()),
                        self.id, WORLD_ID
                    )]
                }
                _ =>  vec![]                    
            });
        }
    
        (Box::new(SceneryEntity {
            ..*self
        }), vec![])

    }

    fn apply_message(&self, _msg: &Message, _resources: &Resources) -> Box<dyn Entity> {
        Box::new(*self)
    }

    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        match pass {
            RenderPass::Bottom => {
                if let Some(image) = resources.sprite(&self.image_handle) {
                    context.draw_image_with_html_image_element(
                        image,
                        (self.pos.x-Into::<f64>::into(image.width()/2)).round(),
                        (self.pos.y-Into::<f64>::into(image.height()/2)).round());
                }
            }
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
