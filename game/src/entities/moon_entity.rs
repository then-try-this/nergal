// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use vector2math::*;
use std::any::Any;
use std::rc::Rc;

use rand::prelude::SliceRandom;
use instant::{ Duration };
use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::message::*;
use crate::game::resources::{ Resources };
use crate::rendering::rendering::{ RenderPass };
use crate::gui::menu_item::{ MenuItem };
use crate::game::constants::*;
use crate::sound::speech::{ Voice };

use crate::declare_any;
use crate::declare_message_reader;


    
#[derive(Debug,Clone,Copy)]
pub struct MoonEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub image_handle: u64,
    pub state: EntityState,
    pub timer: u128,
    pub conversation_cursor: u64
}

/// A moon entity.
impl MoonEntity {
    pub fn new(id: EntityId, pos: Vec2, image_handle: u64) -> Self {
        MoonEntity {
            id,
            pos,
            image_handle,
            state: EntityState::Idle,
            timer: 0,
            conversation_cursor: 0
        }   
    }
}

impl Entity for MoonEntity {
    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    fn bb(&self) -> (Vec2, Vec2) {
        let tl = self.pos.add(Vec2::new(-150.,-150.));
        (tl, tl.add(Vec2::new(250.,170.)))
    }
    fn cat(&self) -> EntityCat {
        EntityCatFlags::MINERAL
    }

    fn describe(&self, _resources: &Resources) -> String {
        "The moon.".into()
    }
    
    fn update(&self, _entities: &EntityRefVec, resources: &Resources, delta: &Duration) -> (Box<dyn Entity>,MessageVec) {
        let mut rng = rand::thread_rng();
        let new_timer = self.timer + delta.as_millis();
        
        match self.state {

            EntityState::OpenMenu(from_key) => {
                return (Box::new(MoonEntity {
                    state: EntityState::WaitMenu,
                    ..*self
                }), vec![
                    Message::new(
                        MessageType::OpenMenu("Moon menu".into(), Rc::new(
                            vec![
                                MenuItem::new(
                                    match resources.theme {
                                        Theme::Daytime => "Talk to moon".into(),
                                        Theme::Nighttime => "Approach and talk to moon".into(),
                                    }, 
                                    Rc::new(vec![
                                        Message::new(MessageType::TalkTo(self.id, "moon".into(), self.pos), self.id, PLAYER_ID)
                                    ])
                                )
                            ]
                            ), 100., from_key),
                        self.id(), WORLD_ID
                    ),
                    self.play_sound_msg(resources,"/sounds/notify.mp3", 1.0)
                ]);
            }

            EntityState::WaitMenu => {
                if self.timer > MENU_WAIT_TIME_MILLIS {                    
                    return (Box::new(MoonEntity {
                        state: EntityState::Idle,
                        ..*self
                    }), vec![])
                }
            }

            EntityState::Listening => {
                if self.timer > resources.time("listening_time") {
                    return (Box::new(MoonEntity {                    
                        state: EntityState::Answer,
                        ..*self
                    }), vec![])
                }
            },

            EntityState::Answer => {
                let choices = resources.conversations.next_ids_from_cursor(self.conversation_cursor);

                if choices.is_empty() {
                    return (Box::new(MoonEntity {
                        state: EntityState::Idle,
                        timer: 0,
                        conversation_cursor: 0, // clear conversaton
                                                // so we know it's
                                                // finished
                        ..*self
                    }), vec![]);                    
                } else {

                    // todo: use likelihood here
                    let cursor = *choices.choose(&mut rng).unwrap();
                    return (Box::new(MoonEntity {
                        state: EntityState::Talking,
                        timer: 0,
                        conversation_cursor: cursor,                        
                        ..*self
                    }), vec![
                        Message::new(
                            MessageType::SpeechBubble(
                                resources.conversations.text_from_cursor(cursor).into(),
                                Voice::Cloud,
                                PLAYER_ID
                            ),
                            self.id(),
                            WORLD_ID
                        )
                    ]);
                }
            },
            
            EntityState::Talking => {
                if self.timer > resources.time("speaking_time") {
                    return (Box::new(MoonEntity {                    
                        state: EntityState::WaitMenu,
                        conversation_cursor: 0,
                        ..*self
                    }), vec![
                        Message::new(
                            MessageType::Question(self.conversation_cursor),
                            self.id(),
                            PLAYER_ID, 
                        ),
                    ]);
                }                
            },

            _ => {}
        }        

        (Box::new(MoonEntity {
            timer: new_timer,
            ..*self }),
         vec![])
    }

    fn apply_message(&self, msg: &Message, resources: &Resources) -> Box<dyn Entity> {
        match &msg.data {
            
            // open the menu when a player clicks on us
            MessageType::MouseClick(_, pos, _) => {
                // don't open the menu if it's already open
                if !resources.world_state.menu_open && pos.inside_box(self.bb()) {
                    return Box::new(MoonEntity {
                        state: EntityState::OpenMenu(false),
                        ..*self
                    });              
                }               
            },            

            MessageType::MenuGone => {
                if self.state == EntityState::WaitMenu && msg.recipient == self.id {
                    return Box::new(MoonEntity {
                        state: EntityState::Idle,
                        ..*self
                    });
                }
            }

            MessageType::Question(cursor) => { 
                if msg.recipient == self.id {
                    return Box::new(MoonEntity {
                        state: EntityState::Listening,
                        timer: 0,
                        conversation_cursor: *cursor,
                        ..*self
                    });
                } 
            }
            
            MessageType::KeyPress(key) => {                
                if !resources.world_state.conversation_happening && is_trigger_menu_key(key) && self.pos.dist(resources.world_state.player_pos) < MENU_TRIGGER_DISTANCE {
                    return Box::new(MoonEntity {
                        state: EntityState::OpenMenu(true),
                        ..*self
                    });                                      
                }
            }
            
            _ => {}
        }

        Box::new(*self)
    }

    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        

        // context.begin_path();
        // let bb = self.bb();
        // context.rect(bb.0.x, bb.0.y, bb.1.x - bb.0.x, bb.1.y - bb.0.y);
        // context.set_stroke_style(&"#f00".into());
        // context.stroke();                                
        //context.set_fill_style(&"#000".into());
        //context.fill_text(&format!("{:?} {}", self.state(), self.conversation_cursor), self.pos.x-25.0, self.pos.y-90.0).unwrap();

        match pass {
            RenderPass::Beauty => {
                if let Some(image) = resources.sprite(&self.image_handle) {
                    context.save();
                    context.translate(-Into::<f64>::into(image.width()/2),
                                      -Into::<f64>::into(image.height())+20.);     
                    context.translate(self.pos.x,self.pos.y);
                    context.draw_image_with_html_image_element(image, 0., 0.);
                    context.restore();
                }
            },
            
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
