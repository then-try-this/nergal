// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use rand::Rng;
use std::any::Any;
use std::rc::Rc;
use rand::prelude::SliceRandom;
use crate::game::id::*;
use vector2math::*;
use crate::maths::vec2::{ Vec2 };
use instant::{ Duration };
use crate::game::entity::*;
use crate::game::inventory::*;

use crate::items::disease_item::*;
use crate::game::inventory_item::{ InventoryItemType };
use crate::game::message::*;
use crate::game::message::MessageType::*;
use crate::game::resources::{ Resources };
use crate::rendering::rendering::*;
use crate::rendering::animation::{ ClipId, AnimId };
use crate::entities::social_entity::{ SocialEntity };
use crate::entities::plant_entity::{ PlantEntity };
use crate::rendering::sprite_dimensions::*;
use crate::gui::menu_item::{ MenuItem };

use crate::gui::screen_manager::{ ScreenState };
use crate::game::constants::*;
use crate::sound::speech::{ Voice };
use crate::game::world_state::{ WorldPhase };

use crate::declare_any;
use crate::declare_message_reader;

const SYMPTOM_DURATION_MILLIS: u128 = 1500;
const CONVERSATION_GAP: f64 = 120.;
const TOO_CLOSE_DISTANCE: f64 = 50.0;
const AVOID_DISTANCE: f64 = 10.0;
const DEAD_WAIT_TIME_MILLIS: u128 = 5000;

/// The player character, can carry items and disease, have
/// conversations with other characters, and is part of a social
/// network.
#[derive(Clone)]
pub struct PlayerEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub inventory: Rc<InventoryVec>,
    pub state: EntityState,
    pub anim: ClipId,
    pub timer: u128,
    pub target: Vec2,
    pub character: AnimId,
    pub health: f64,
    pub conversation_cursor: u64,
    pub conversation_target: EntityId,
    pub sent_health_warning: bool,
    pub movement_keys_pressed: u32,
    pub met_friend: bool,
    pub met_stranger: bool
}


impl PlayerEntity {
    pub fn new(id: EntityId, pos: Vec2, character: AnimId) -> Self {
        PlayerEntity {
            id,
            pos,
            // put 'heavier' items behind Rc so they are not deeply cloned
            inventory: Rc::new(vec![]),
            //inventory: Rc::new(vec![Rc::new(DiseaseItem::new(DiseaseStrain::DiseaseA,0.into()))]),
            state: EntityState::Idle,
            anim: ClipId::Idle,
            timer: 0,
            target: Vec2::zero(),
            character,
            health: 100.,
            conversation_cursor: 0,
            conversation_target: 0.into(),
            sent_health_warning: false,
            movement_keys_pressed: 0,
            met_friend: false,
            met_stranger: false
        }   
    }

    fn start_walking(&self, messages: &MessageVec, _delta: &Duration, resources: &Resources, next_state: EntityState) -> (Box<dyn Entity>, MessageVec) {
        (Box::new(PlayerEntity {
            state: next_state,
            ..self.clone()
        }), msg_concat(
            messages,
            &vec![
                // stop any previously triggered walking sound
                Message::new(
                    MessageType::StopSound("walking".into()), self.id, WORLD_ID
                ),
                self.play_looped_sound_msg(
                    "walking",
                    resources,
                    "/sounds/steps-normal-fast-player.mp3",
                    1.
                )
            ]
        ))            
    }
    
    fn walk(&self, messages: &MessageVec, delta: &Duration, resources: &Resources, next_state: EntityState) -> (Box<dyn Entity>, MessageVec) {
        let v = self.target.sub(self.pos);
        let pixels_per_frame = WALK_SPEED * delta.as_millis() as f64;

        // source of NaNs
        let m = if v.mag() > 0. {
            v.norm().mul(pixels_per_frame)
        } else {
            Vec2::zero()
        };

        // are we outside the world space?
        let outside_world = !self.pos.add(m).inside_box(resources.world_state.accessible_area);
        
        // have we reached our destination?
        let stop_walking = v.mag() < pixels_per_frame || outside_world;

        let mut new_messages = if stop_walking {
            msg_cons(
                messages,
                Message::new(
                    MessageType::StopSound("walking".into()), self.id, WORLD_ID
                )
            )
        } else {
            messages.clone()
        };
        
        if outside_world {
            new_messages.push(
                Message::new(MessageType::PlayerOutsideWorld, self.id, WORLD_ID)
            );
        }
        
        (Box::new(PlayerEntity {                    
            state: if stop_walking { next_state } else { self.state },
            anim: if stop_walking { self.anim_idle_or_ill() } else { self.anim },
            pos: self.pos.add(m).clamp(resources.world_state.accessible_area),
            ..self.clone()
        }), new_messages)
    }

    /// Convert the actions from conversations into messages to dispatch
    fn messages_from_action(&self, cursor: u64, resources: &Resources) -> Vec<Message> {
        match resources.conversations.action_from_cursor(cursor) {
            "friend" => { vec![Message::new(AddRelationship(self.conversation_target), self.id, WORLD_ID)] }
            //"unfriend" => { vec![Message::new(RemoveRelationship(self.conversation_target), self.id, WORLD_ID)] }
            _ => vec![]
        }        
    }

    fn anim_idle_or_ill(&self) -> ClipId {
        if self.inventory_contains(InventoryItemType::Disease) {
            ClipId::Ill
        } else {
            ClipId::Idle
        }
    }

    fn key_to_direction(&self, key: &str) -> Vec2 {
        match key {
            "a" => Vec2::new(-3000.,0.),
            "d" => Vec2::new(3000.,0.),
            "w" => Vec2::new(0.,-3000.),
            "s" => Vec2::new(0.,3000.), 
            _ => Vec2::zero()                        
        }
    }
}

impl Inventory for PlayerEntity {
    fn items(&self) -> &InventoryVec {
        &self.inventory
    }
}

impl Entity for PlayerEntity {
    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    fn bb(&self) -> (Vec2, Vec2) {
        character_bounding_box(self.pos, self.character)
    }
    fn state(&self) -> EntityState { self.state }
    fn set_state(&mut self, state: EntityState) { self.state=state }
    fn cat(&self) -> EntityCat {
        EntityCatFlags::ANIMAL |
        EntityCatFlags::SOLID |
        EntityCatFlags::SENTIENT |
        EntityCatFlags::MOVABLE
    }
    fn describe(&self, _resources: &Resources) -> String {
        "You, the player.".into()
    }

    fn update(&self, entities: &EntityRefVec, resources: &Resources, delta: &Duration) -> (Box<dyn Entity>,MessageVec) {
        // update the inventory, and distribute any messages from
        // items we hold
        let (updated_inventory, mut messages) = self.inventory_update(self, resources, delta);
        let mut rng = rand::thread_rng();

        let mut sent_health = false;
        if self.health < HEALTH_WARNING_PERCENT && !self.sent_health_warning {
            messages.push(Message::new(
                MessageType::PlayerHealthWarning, self.id, WORLD_ID
            ));
            sent_health = true;
        }
        
        let updated_entity = PlayerEntity {
            timer: self.timer+delta.as_millis(),
            inventory: Rc::new(updated_inventory),
            sent_health_warning: if self.health > HEALTH_WARNING_PERCENT {
                false
            } else {
                sent_health || self.sent_health_warning
            },
            ..self.clone()
        };

        if self.health < 1. && self.state != EntityState::Dead {
            return (Box::new(PlayerEntity {                
                state: EntityState::Dead,
                anim: ClipId::Death,
                timer: 0,
                ..updated_entity
            }), msg_cons(
                &messages,                
                Message::new(PlayerDied, self.id, WORLD_ID)
            )); 
        }
        
        // check collisions
        if self.is_moving() && self.state != EntityState::Avoid && self.state != EntityState::StartAvoid {
            let close = self.within_radius_of(
                entities, TOO_CLOSE_DISTANCE, EntityCatFlags::SOLID_TO_PLAYER
            );

            if !close.is_empty() {                
                // if we are approaching someone to talk and we hit
                // and obstacle on the way, then just stop walking and
                // start up the conversation anyway
                if self.state == EntityState::Approach {
                    return (Box::new(PlayerEntity {
                        state: EntityState::Question,
                        ..updated_entity
                    }), messages);
                } else {
                    let target = self.pos().add(close.iter().fold(Vec2::zero(),|acc,e| {
                        acc.add(self.pos().sub(e.pos()).norm().mul(AVOID_DISTANCE))
                    }));

                    let sound = if close[0].cat() & EntityCatFlags::VEGETABLE != 0 {
                        let filename = format!("/sounds/hit-plant-{}.mp3", rng.gen_range(1..4));
                        
                        if let Some(plant) = close[0].as_any().downcast_ref::<PlantEntity>() {                            
                            // set pitch based on species of plant
                            self.play_sound_msg(
                                resources,
                                &filename,
                                plant.size_to_pitch()
                            )
                        } else {
                            self.play_sound_msg(
                                resources,
                                &filename,
                                3.0
                            )
                        }
                    } else if close[0].cat() & EntityCatFlags::SENTIENT != 0 {
                        let filename = format!("/sounds/bump-{}.mp3", rng.gen_range(1..6));
                        self.play_sound_msg(
                            resources,
                            &filename,
                            2.0
                        )
                    } else {
                        let filename = format!("/sounds/hit-rock-{}.mp3", rng.gen_range(1..4));
                        self.play_sound_msg(
                            resources,
                            &filename,
                            1.0
                        )
                    };
                    
                    return (Box::new(PlayerEntity {
                        state: EntityState::StartAvoid,
                        anim: self.anim_for_target(target),
                        timer: 0,
                        target,
                        ..updated_entity
                    }), msg_cons(
                        &messages,
                        sound                            
                    ));
                }
            }
        }
       
        match self.state {
            EntityState::Idle => {
                return (Box::new(updated_entity), messages)                                        
            },
            
            // make a menu with all the possible conversation answers to choose
            EntityState::Answer => {
                let (_sprite_sheet, dims) = character_params(self.character);

                let responses = resources.conversations.next_ids_from_cursor(self.conversation_cursor);

                if responses.is_empty() {
                    return (Box::new(PlayerEntity {
                        state: EntityState::Idle,
                        anim: self.anim_idle_or_ill(),
                        timer: 0,
                        // clear conversation if ended
                        conversation_cursor: 0,
                        ..updated_entity
                    }), msg_concat(
                        &messages,
                        &vec![
                            Message::new(ConversationOver, self.id, self.conversation_target)
                        ])
                    );                    
                } else {                
                    return (Box::new(PlayerEntity {
                        state: EntityState::WaitMenu,
                        anim: self.anim_idle_or_ill(),
                        timer: 0,
                        // clear conversation if ended
                        conversation_cursor: self.conversation_cursor,
                        ..updated_entity
                    }), msg_concat(
                        &messages,
                        &vec![
                            Message::new(
                                OpenMenu("Chat responses menu".into(), Rc::new(
                                    responses
                                        .iter()
                                        .map(|cursor| -> MenuItem {
                                            MenuItem::new(
                                                resources.conversations.text_from_cursor(*cursor).into(),
                                                Rc::new(msg_cons(
                                                    &self.messages_from_action(*cursor, resources),
                                                    Message::new(Answer(*cursor), self.id(), self.id())                                                
                                                )),
                                            )
                                        })
                                        .collect::<Vec<_>>()
                                ), dims.body_height, false),
                                self.id(), WORLD_ID
                            ),
                            self.play_sound_msg(resources,"/sounds/notify.mp3", 1.0)
                        ])
                    );
                }
            },
            
            EntityState::Question => {                                
                return (Box::new(PlayerEntity {
                    state: EntityState::Idle,
                    anim: ClipId::Talk,
                    timer: 0,
                    ..updated_entity
                }), msg_concat(
                    &messages,
                    &vec![
                        Message::new(
                            Question(self.conversation_cursor),
                            self.id(), self.conversation_target
                        ),
                        Message::new(
                            SpeechBubble(
                                resources.conversations.text_from_cursor(self.conversation_cursor).into(),
                                Voice::Player,
                                self.conversation_target
                            ),
                            self.id(),
                            WORLD_ID
                        ),
                        self.play_sound_msg(
                            resources,
                            ["/sounds/chatter-1.mp3",
                             "/sounds/chatter-2.mp3",
                             "/sounds/chatter-3.mp3"]
                                .choose(&mut rng)
                                .unwrap(),
                            rng.gen_range(1.4..1.8))
                    ])
                );               
            },

/*            EntityState::OpenMenu => {                
                let (sprite_sheet, dims) = character_params(self.character);

                return (Box::new(PlayerEntity {
                    state: EntityState::WaitMenu,
                    anim: self.anim_idle_or_ill(),
                    timer: 0,
                    ..updated_entity
                }), msg_concat(
                    &messages,
                    &vec![
                        Message::new(
                            OpenMenu(Rc::new(vec![
                                MenuItem::new(
                                    "Quit".into(),
                                    Rc::new(vec![
                                        Message::new(
                                            ScreenChange(ScreenState::GameOver),
                                            self.id,
                                            SCREEN_MANAGER_ID
                                        )
                                    ]),
                                ),
                            ]),
                            dims.body_height),
                            self.id(), WORLD_ID                            
                        ),
                        self.play_sound_msg(resources,"/sounds/notify.mp3", 1.0)
                    ])
                );
            }
*/
            EntityState::WaitMenu => {
                if self.timer>MENU_WAIT_TIME_MILLIS {
                    return (Box::new(PlayerEntity {
                        state: EntityState::Idle,
                        conversation_cursor: 0,
                        ..updated_entity
                    }), messages)
                }
            }
            
            EntityState::StartApproach => return updated_entity.start_walking(&messages, delta, resources, EntityState::Approach),
            EntityState::Approach => return updated_entity.walk(&messages, delta, resources, EntityState::Question),

            EntityState::StartWalkTo => return updated_entity.start_walking(&messages, delta, resources, EntityState::WalkTo),
            EntityState::WalkTo => return updated_entity.walk(&messages, delta, resources, EntityState::Idle),

            EntityState::StartAvoid => return updated_entity.start_walking(&messages, delta, resources, EntityState::Avoid),
            EntityState::Avoid => return updated_entity.walk(&messages, delta, resources, EntityState::Idle),

            EntityState::Symptomatic => {
                if self.timer>SYMPTOM_DURATION_MILLIS {                
                    return (Box::new(PlayerEntity {
                        state: EntityState::Idle,
                        timer: 0,
                        anim: self.anim_idle_or_ill(),
                        ..updated_entity
                    }), messages)
                }
            }

            EntityState::Dead => {
                if self.timer>DEAD_WAIT_TIME_MILLIS {
                    return (Box::new(updated_entity),
                     msg_concat(
                         &messages,
                         &vec![
                             Message::new(
                                 ScreenChange(ScreenState::GameOver(GameOverReason::NoHealth)),
                                 self.id,
                                 SCREEN_MANAGER_ID
                             ),
                             Message::new(
                                 MessageType::GameOver(GameOverReason::NoHealth),
                                 NO_ID,
                                 WORLD_ID
                             )
                         ]
                     ));
                }
            }

            EntityState::Describe => {

                let connected = resources.social_graph.all_connected_entity_ids(self.id);

                let close = entities.iter().filter(|e| {
                    e.pos().dist(self.pos) < DESCRIPTION_DISTANCE && e.id() != PLAYER_ID && e.describe(resources) != ""
                }).cloned().collect::<EntityRefVec>();

                let non_nergals = close.iter().filter(|e| {
                    match e.as_any().downcast_ref::<SocialEntity>() { 
                        Some(_n) => false,
                        None => true
                    }
                }).cloned().collect::<EntityRefVec>();
                
                let nergals = close.iter().fold(vec![],|mut acc, e| {
                    match e.as_any().downcast_ref::<SocialEntity>() { 
                        Some(n) => { acc.push(n); acc },
                        None => acc
                    }
                });

                let friends = nergals.iter().filter(|e| {
                    connected.contains(&e.id())
                }).collect::<Vec<_>>().len();

                let strangers = nergals.len() - friends;

                let mut text: String = "".into();

                let t = ((GAME_TIME_LENGTH - resources.world_state.game_time) as f64 / 1000.) as u32;        
                let minutes = t / 60;
                let seconds = t % 60;

                let minutes_text = if minutes == 1 {
                    format!("{} minute", minutes)
                } else {
                    format!("{} minutes", minutes)
                };

                let seconds_text = if seconds == 1 {
                    format!("{} second", seconds)
                } else {
                    format!("{} seconds", seconds)
                };
                
                text = format!(
                    "{} My health is {} percent and there are {} {} left.",
                    text,
                    self.health as u32,
                    minutes_text,
                    seconds_text
                );
                
                // don't count ourselves
                if close.len() == 1 {                    
                    text = format!("{} Around me is {} thing: ", text, close.len())
                } else {
                    text = format!("{} Around me are {} things: ", text, close.len())
                };

                let nergal_desc_threshold = 6;
                
                if nergals.len() > nergal_desc_threshold  {
                    if friends == 1 {
                        text = format!("{} {} friend, ", text, friends);
                    } else {
                        text = format!("{} {} friends, ", text, friends);
                    }

                    if strangers == 1 {
                        text = format!("{} {} stranger, ", text, strangers);
                    } else {
                        text = format!("{} {} strangers, ", text, strangers);
                    }
                }

                let mut things = vec![];

                if nergals.len() <= nergal_desc_threshold {
                    for e in nergals {
                        let dir = e.pos().sub(self.pos());
                        let paces = (dir.mag() / PIXELS_PER_PACE) as u32;
                        things.push((vec_to_clock(dir), paces, e.describe(resources)));
                    }                    
                }
                
                for e in non_nergals {
                    let dir = e.pos().sub(self.pos());
                    let paces = (dir.mag() / 20.) as u32; // pixels per pace
                    things.push((vec_to_clock(dir), paces, e.describe(resources)));
                }

                // close to far
                things.sort_by(|a, b| a.1.cmp(&b.1));

                for (dir,paces,thing) in things {
                    text = format!("{} At {} paces at {} oclock, {}.", text, paces, if dir == 0 { 12 } else { dir }, thing);                
                }

                resources.speech.say(Voice::Player, &text);
                
                return (Box::new(PlayerEntity {
                    state: EntityState::Idle,
                    timer: 0,
                    anim: self.anim_idle_or_ill(),
                    ..updated_entity
                }), messages)                    
            }
            
            _ => {}
        }

        (Box::new(updated_entity), messages)
    }    
    
    /// For the player we need to detect and act on user input as well
    /// as messages from other entities
    fn apply_message(&self, msg: &Message, resources: &Resources) -> Box<dyn Entity> {

        if self.state == EntityState::Dead {
            // don't accept messages if in death phase
            return Box::new(self.clone());
        }
        
        match &msg.data {
            MouseClick(_, pos, entity_here) => {
                // walk to clicked on target position
                // if there is a (sentient) entity here
                // then ignore as we are opening a menu
                if !entity_here {
                    return Box::new(PlayerEntity {
                        state: EntityState::StartWalkTo,
                        target: *pos,
                        anim: self.anim_for_target(*pos),
                        timer: 0,
                        ..self.clone()
                    });
                }
            },

            KeyPress(key) => {
                if is_describe_key(key) {
                    return Box::new(PlayerEntity {
                        state: EntityState::Describe,
                        ..self.clone()
                    });
                }

                if ["w","a","s","d"].contains(&key.as_str()) {                
                    if self.state!=EntityState::WalkTo {
                        let new_pos = self.pos.add(self.key_to_direction(&key));
 
                        // walk to clicked on target position
                        return Box::new(PlayerEntity {
                            state: EntityState::StartWalkTo,
                            target: new_pos,
                            anim: self.anim_for_target(new_pos),
                            timer: 0,
                            movement_keys_pressed: self.movement_keys_pressed + 1,
                            ..self.clone()
                        });
                    } else {
                        // add target to old one
                        let new_target = self.target.add(self.key_to_direction(&key));

                        return Box::new(PlayerEntity {
                            target: new_target,
                            anim: self.anim_for_target(new_target),
                            movement_keys_pressed: self.movement_keys_pressed + 1,
                            ..self.clone()
                        });
                    }
                }
            },
            
            KeyRelease(key) => {
                if ["w","a","s","d"].contains(&key.as_str()) {                
                    if self.state==EntityState::WalkTo {                        
                        // remove direction from target
                        let new_target = self.target.sub(self.key_to_direction(&key));

                        // still movement keys down? just update the direction
                        if self.movement_keys_pressed != 1 {
                            return Box::new(PlayerEntity {
                                target: new_target,
                                anim: self.anim_for_target(new_target),
                                movement_keys_pressed: self.movement_keys_pressed - 1,
                                ..self.clone()
                            });
                        } else {
                            // no keys down, stop moving
                            return Box::new(PlayerEntity {
                                state: EntityState::Idle,
                                anim: self.anim_idle_or_ill(),
                                movement_keys_pressed: self.movement_keys_pressed - 1,
                                ..self.clone()
                            });
                        }
                    } else {
                        return Box::new(PlayerEntity {
                            movement_keys_pressed: self.movement_keys_pressed - 1,
                            ..self.clone()
                        });
                    }
                    
                }
            },
            
            TalkTo(id, entity, mut pos) => {
                let connected = resources.social_graph.all_connected_entity_ids(self.id);               
                let starting_cursor = resources.conversations.get_rnd_root(                    
                    match entity.as_str() {
                        "nergal" => {
                            if connected.contains(id) {
                                if self.met_friend { "friend" } else { "first_friend" }
                            } else {
                                if self.met_stranger { "stranger" } else { "first_stranger" }
                            }
                        }
                        _ => entity.as_str()
                    }
                );

                let is_friend = resources
                    .social_graph
                    .player_friend_cache
                    .contains(&id);
                
                // walk to one side of the target or the other
                if pos.x < self.pos.x {
                    pos.x += CONVERSATION_GAP;
                } else {
                    pos.x -= CONVERSATION_GAP;
                }
                
                // walk to clicked on target position
                return Box::new(PlayerEntity {
                    state: EntityState::StartApproach,
                    target: pos,
                    anim: self.anim_for_target(pos),
                    timer: 0,
                    met_friend: if entity == "nergal" && is_friend { true } else { self.met_friend },
                    met_stranger: if entity == "nergal" && !is_friend { true } else { self.met_stranger },
                    conversation_cursor: starting_cursor, 
                    conversation_target: *id,
                    ..self.clone()
                });
            },            

            // Questions come from other entities
            Question(cursor) => {
                // check the entity is the one we are currently having
                // a chat with - this will be wrong if we switch mid
                // conversation to a new nergal and the old one
                // replies, we ignore them
                if msg.recipient == self.id && msg.sender == self.conversation_target  {
                    return Box::new(PlayerEntity {
                        state: EntityState::Answer,
                        conversation_cursor: *cursor,
                        ..self.clone()
                    });
                }                
            }

            // Answer messages are sent to ourselves from the menu item
            Answer(cursor) => { 
                if msg.recipient == self.id { 
                    return Box::new(PlayerEntity {
                        state: EntityState::Question,
                        conversation_cursor: *cursor,
                        ..self.clone()
                    });
                }                
            }

            Infect(disease, source) => {
                if msg.recipient == self.id {
                    return Box::new(PlayerEntity {
                        inventory: Rc::new(self.inventory_add(
                            DiseaseItem::new(*disease, *source)
                        )),
                        ..self.clone() 
                    });
                }
            }

            Spread(_strain) => {
                if msg.recipient == self.id && self.state != EntityState::Dead {
                    return Box::new(PlayerEntity {
                        state: EntityState::Idle,
                        timer: 0,
                        anim: match resources.world_state.phase() {
                            WorldPhase::Manic => ClipId::Vomit,
                            _ => ClipId::Sneeze
                        },
                        ..self.clone() 
                    });
                }
            },
            
            HealthChange(change, reason) => {
                if msg.recipient == self.id {
                    return Box::new(PlayerEntity {
                        health: f64::min(100., f64::max(0., self.health + change)),
                        anim: match reason {
                            HealthReason::Snack => ClipId::Eat,
                            _ => self.anim
                        },
                        ..self.clone() 
                    });
                }
            }

            MenuGone => {
                if msg.recipient == self.id {
                    return Box::new(PlayerEntity {
                        state: EntityState::Idle,
                        anim: self.anim_idle_or_ill(),
                        timer: 0,
                        ..self.clone()
                    });
                }
            }
            
            _ => {} 
        }

        Box::new(self.clone())

    }


    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {
        let (sprite_sheet, dims) = character_params(self.character);

        // context.begin_path();
        // context
        //     .arc(self.pos.x,self.pos.y, 5.0, 0.0, std::f64::consts::PI * 2.00)
        //     .unwrap();
        // context.set_fill_style(&"#F00".into());        
        // context.fill();

        match pass {
            RenderPass::Beauty => {
                if let Some(image) = resources.get_sprite_from_path(sprite_sheet) {
                    if let Some(anim) = resources.animation.get(&self.character) {
                        anim.render(
                            context,
                            self.pos.x - (dims.sprite_width/2.),
                            self.pos.y - dims.sprite_height+dims.feet_height,
                            self.anim,
                            image,
                            &self.timer
                        );
                    }
                }
            },
            
            RenderPass::Shadow => {                
                if let Some(image) = resources.get_sprite_from_path(sprite_sheet) {
                    context.save();
                    context.translate(
                        self.pos.x - (dims.sprite_width/2.),
                        self.pos.y - dims.sprite_height + dims.feet_height
                    );
                    context.transform(1., 0., -0.75, 0.5, 1., 1.);
                    context.translate(dims.sprite_width * dims.shadow_tx_x,
                                      dims.sprite_height * dims.shadow_tx_y);               
                    
                    if let Some(anim) = resources.animation.get(&self.character) {
                        anim.render(
                            context,
                            0., 0.,
                            self.anim,
                            image,
                            &self.timer
                        );
                    }
                    context.restore();
                }
            }


            RenderPass::Debug => {           
                /*let mut pos=-180.0;
                for message in self.memory.iter() {
                    context.fill_text(message, self.pos.x-30.0, self.pos.y+pos).unwrap();
                    pos-=15.0;
                }*/
                context.begin_path();
                let bb = self.bb();
                context.rect(bb.0.x, bb.0.y, bb.1.x - bb.0.x, bb.1.y - bb.0.y);
                context.set_stroke_style(&"#f00".into());
                context.stroke();                

                context.set_fill_style(&"#000".into());
                context.fill_text(format!("{:?} {:?}", self.state, self.conversation_cursor).as_str(), self.pos.x-30., self.pos.y-190.).unwrap();        
                //context.fill_text("player", self.pos.x-30.0, self.pos.y-190.0).unwrap();
            }

            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
