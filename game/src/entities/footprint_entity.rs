// Nergal Copyright (C) 2025 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::any::Any;
use rand::Rng;
use instant::{ Duration };
use crate::game::id::*;
use vector2math::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::message::{ Message, MessageVec };
use crate::game::resources::{ Resources };
use crate::rendering::rendering::{ RenderPass };

use crate::declare_any;
use crate::declare_message_reader;

const FOOTPRINT_DURATION: u128 = 200;
const NUM_FOOTPRINTS: usize = 10;

#[derive(Debug, Clone, Copy)]
pub struct Footprint {
    pub pos: Vec2,
    pub t: u128,
}

#[derive(Debug,Copy,Clone)]
pub struct FootprintEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub timer: u128,
    pub parent_id: EntityId,
    pub image_handle: u64,
    pub footprints: [Footprint; NUM_FOOTPRINTS],
    pub index: usize
}

/// Entity representing a particle system of some kind
impl FootprintEntity {

    pub fn new(id: EntityId, pos: Vec2, parent_id: EntityId, image_handle: u64) -> Self {
        FootprintEntity {
            id,
            pos,
            timer: 0,
            parent_id,
            image_handle,
            footprints: [Footprint { pos: Vec2::zero(), t: 0 }; NUM_FOOTPRINTS],
            index: 0
        }   
    }
}

impl Entity for FootprintEntity {
    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    fn cat(&self) -> EntityCat {
        EntityCatFlags::IMAGINARY 
    }
    
    fn update(&self, entities: &EntityRefVec, _resources: &Resources, delta: &Duration) -> (Box<dyn Entity>,MessageVec) {
        if let Some(parent) = self.find_entity(self.parent_id, entities) {
            
            if self.state_is_moving(parent.state()) && self.timer > FOOTPRINT_DURATION {
                let mut rng = rand::thread_rng();

                let mut new_footprints = self.footprints;
                
                new_footprints[self.index].pos = parent.pos().add(if self.index % 2 == 0 {
                    Vec2::new(rng.gen_range(-10.0..-5.0), rng.gen_range(-10.0..-5.0) - 10.)
                } else {
                    Vec2::new(rng.gen_range(5.0..10.0), rng.gen_range(5.0..10.0) - 10.)
                });
                
                new_footprints[self.index].t = 0;

                (Box::new(FootprintEntity{
                    timer: 0,
                    pos: parent.pos(),
                    footprints: new_footprints,
                    index: if self.index >= NUM_FOOTPRINTS - 1 { 0 } else { self.index + 1 },
                    ..*self
                }),
                 vec![])
            } else {                
                (Box::new(FootprintEntity{
                    timer: self.timer + delta.as_millis(),
                    pos: parent.pos(),
                    ..*self
                }),
                 vec![])
            }
        } else {
            (Box::new(*self), vec![])
        }
    }
    
    fn apply_message(&self, _msg: &Message, _resources: &Resources) -> Box<dyn Entity> {
        Box::new(self.clone())     
    }

    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
                
        match pass {
            RenderPass::Shadow => {
                if let Some(image) = resources.sprite(&self.image_handle) {
                    context.save();
                    context.set_filter("brightness(100)");

                    for (i, f) in self.footprints.iter().enumerate() {
                        let mut a: i32 = (self.index as i32 - i as i32) - 1;
                        if a < 0 { a += NUM_FOOTPRINTS as i32 }

                        context.set_global_alpha((1.0 - (a as f64 / NUM_FOOTPRINTS as f64) as f64) / 3.0);
                        
                        context.draw_image_with_html_image_element(
                            image,
                            f.pos.x.round(),
                            f.pos.y.round()
                        );
                    }

                    context.restore();

                }
            },

            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
