// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// Characters that are part of a social network (aka 'Nergals').
pub mod social_entity;
/// A character who is part of a social network, but controlled by the player.
pub mod player_entity;
/// Non-interactive scenery items.
pub mod scenery_entity;
/// Stationary (and partially sentient) characters.
pub mod plant_entity;
/// Stationary, non-interactive items with depth.
pub mod rock_entity;
/// Moving (and partially sentient) characters.
pub mod cloud_entity;
/// A gift (e.g. snack) that can be passed from one character to another.
pub mod gift_entity;
/// Like a rock, but you can walk through it.
pub mod rainbow_entity;
/// A general purpose particle system entity.
pub mod particle_entity;
/// An entity that plays notes when you walk on it.
pub mod piano_entity;
/// A moon entity that moves across the sky.
pub mod moon_entity;
/// Rendering foot prints.
pub mod footprint_entity;
