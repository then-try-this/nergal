// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::any::Any;
use rand::Rng;
use rand::prelude::SliceRandom;
use instant::{ Duration };
use std::rc::Rc;
use crate::game::id::*;
use vector2math::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::message::*;
use crate::game::resources::{ Resources };
use crate::rendering::rendering::{ RenderPass };
use crate::gui::menu_item::{ MenuItem };
use crate::game::constants::*;
use crate::sound::speech::{ Voice };
use crate::sound::synth_node_desc::{ SynthNodeDesc };

use lazy_static::lazy_static;

use crate::declare_any;
use crate::declare_message_reader;



const SOUND_TRIGGER_PERIOD: u128 = 3000; 

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Species {
    Apple,
    Banyan,
    Baobab,
    Begonia,
    Bulbinetorta,
    Calathea,
    Dollseye,
    Dragonblood,
    Drosera,
    Fishbone,
    Jasmine,
    Lecanopteris,
    Monkeymask,
    Ponytailpalm,
    Stefonia,
    Strelizia,
    Tillandsia
}

#[derive(Debug, Copy, Clone)]
pub struct PlantEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub image_handle: u64,
    pub sample_handle: u64,
    pub timer: u128,
    pub state: EntityState,
    pub species: Species
}

/// Entity representing a plant, a sentient character with a fixed position
impl PlantEntity {
    pub fn at_pos(id: EntityId, pos: Vec2, image_handle: u64, sample_handle: u64, species: Species) -> Self {
        PlantEntity {
            id,
            pos,
            image_handle,
            sample_handle,
            timer: 0,
            state: EntityState::Idle,
            species               
        }   
    }

    pub fn size_to_pitch(&self) -> f64 {
        (match self.species {
            Species::Apple => 1.,
            Species::Banyan => 0.75,
            Species::Baobab => 0.6,
            Species::Begonia => 2.,
            Species::Bulbinetorta => 2.5,
            Species::Calathea => 1.5,
            Species::Dollseye => 2.2,
            Species::Dragonblood => 0.8,
            Species::Drosera => 1.2,
            Species::Fishbone => 1.3,
            Species::Jasmine => 2.1,
            Species::Lecanopteris => 2.2,
            Species::Monkeymask => 3.,
            Species::Ponytailpalm => 2.3,
            Species::Stefonia => 2.4,
            Species::Strelizia => 1.8,
            Species::Tillandsia => 2.7,
        }) * 2.
    }    
}

lazy_static! {

    static ref PLANT_REPLIES: HashMap<Species, Vec<&'static str>> = {
        let mut m = HashMap::new();
    
        m.insert(Species::Apple, vec![
            "I’m an apple tree! I’m originally from central Asia, but I’ve been grown for thousands of years all over Asia, Europe and North America.",
            "I’m an apple tree! There are over 7,500 cultivars of apples, bred for eating, cooking, cider and juice.",
            "I’m an apple tree! In Norse mythology, the goddess Iðunn gave apples to the gods to give them eternal youthfulness."
        ]);

        m.insert(Species::Banyan, vec![
            "I'm a banyan tree. I'm a type of fig and I'm the national tree of India.",
            "I'm a banyan tree. I make roots from my branches that grow towards the ground and turn into new trunks, so I can spread outwards indefinitely.",
            "I'm a banyan tree. We can grow huge, with one tree covering acres of land.",
            "I'm a banyan tree. We keep making new trunks - the Great Banyan of Kolkata has about 2,880 trunks."
        ]);

        m.insert(Species::Baobab, vec![
            "I’m a baobab tree. I’m native to Madagascar, Africa and Australia.",
            "I’m a baobab tree. I’m one of the most long lived plants, and I have great cultural significance, acting as the site of communal gatherings, storytelling, and rituals.",
            "I’m a baobab tree. I have big flowers that open quickly at dusk, and I’m mostly pollinated by bats."
        ]);
        
        m.insert(Species::Begonia, vec![
            "I’m a Begonia maculata, the polka dot begonia, and I’m native to southeast Brazil."
        ]);

        m.insert(Species::Bulbinetorta, vec![
            "I'm a Bulbine torta! I live in rocky dry places in Namaqualand and the Cederberg (right at the South of the African continent).",
            "I'm a Bulbine torta! I grow from a flat based tuber and have twisted thin leaves.",
            "I'm a Bulbine torta! In the right environment (not inside a computer) I have yellowy orange flowers with fluffy yellow stamens.",
            "I'm a Bulbine torta! When I'm growing in the wild I bloom between July and September."
        ]);

        m.insert(Species::Calathea, vec![
            "I’m a Calathea ornata Sanderiana! I have large glossy green leaves with pink stripes. The humans who made this game have one in their studio, so they put me in.",
            "I’m a Calathea ornata Sanderiana! I’m a type of prayer plant. At night my leaves fold up, and in the morning they unfurl – this is called ‘nyctinasty’.",
            "I’m a Calathea ornata Sanderiana! I live in shady forests under the trees, and I’m native to the tropical Americas."
        ]);
        
        m.insert(Species::Dollseye, vec![
            "I’m a doll’s-eye plant, sorry if I frightened you. My official name is Actaea pachypoda.",
            "I’m a doll’s eye plant. I’m native to North America. I make white berries with a black dot, which look like eyeballs.",
            "I’m a doll’s eye plant. I’m incredibly toxic to humans, but birds like to eat me."
        ]);

        m.insert(Species::Dragonblood, vec![
            "I’m a dragon blood tree! My fancy name is Dracaena cinnabar.",
            "I’m a dragon blood tree! I’m the national tree of Yemen.",
            "I’m a dragon blood tree! My sap is red like blood."
        ]);

        m.insert(Species::Drosera, vec![
            "I’m a Drosera, also known as a sundew. There are 194 species of us all around the world… and we’re carnivorous.",
            "I’m a Drosera. I lure, capture and digest insects using stalked mucilaginous glands on my leaves."
        ]);

        m.insert(Species::Fishbone, vec![
            "I'm a fishbone cactus! My fancy name is Epiphyllum anguliger.",
            "I'm a fishbone cactus! I'm from Mexico and you can find me in evergreen forests near there.",
            "I'm a fishbone cactus! I live high up, at elevations between 1100 and 1800 metres."
        ]);

        m.insert(Species::Jasmine, vec![
            "I’m a jasmine! There are over 200 species of jasmine, so I’m not sure exactly who I am.",
            "I’m a jasmine! I’m closely related to olives.",
            "I’m a jasmine! I smell beautiful, so Nergals use me to make perfume.",
            "I’m a jasmine! I’m the national flower of Tunisia, Indonesia, and the Philippines."
        ]);

        m.insert(Species::Lecanopteris, vec![
            "I’m a Lecanopteris! We’re a group of ferns from Southeast Asia and New Guinea.",
            "I’m a Lecanopteris! I have swollen hollow roots that provide homes for ants."
        ]);

        m.insert(Species::Monkeymask, vec![
            "I’m a Monstera adansonii, sometimes known as a monkey mask plant.I’m from South and Central America.",
            "I’m a Monstera adansonii, I’m a vine and I live in tropical forests that are hot and humid."
        ]);

        m.insert(Species::Ponytailpalm, vec![
            "I’m a ponytail palm! My other name is elephant’s foot. Both describe me quite well, except I’m not actually a true palm.",
            "I’m a ponytail palm! I’m threatened with extinction because people like digging me up to have in their homes."
        ]);
        
        m.insert(Species::Stefonia, vec![
            "I’m a Stephania! I’m native to southern Asia and Australia.",
            "I’m a Stephania! I’m a vine and I have a large tuber, I can grow to about 4 metres tall."
        ]);

        m.insert(Species::Strelizia, vec![
            "I’m a Strelizia, otherwise known as a bird of paradise plant or crane flower.",
            "I’m a Strelizia. I’m native to South Africa and I’m pollinated by birds."
        ]);

        m.insert(Species::Tillandsia, vec![
            "I’m a Tillandsia! My leaves are covered in special cells that can rapidly absorb water.",
            "I’m a Tillandsia! We’re usually known as air plants. We get our nutrients and water from the air.",
            "I’m a Tillandsia! We don’t need to grow in soil, we can cling to telephone wires, bare rocks, tree branches, anything we like."
        ]);

        m
    };    
}
    
impl Entity for PlantEntity {
    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    fn bb(&self) -> (Vec2, Vec2) {
        let tl = self.pos.add(Vec2::new(-150.,-150.));
        (tl, tl.add(Vec2::new(250.,170.)))
    }
    fn state(&self) -> EntityState { self.state }
    fn cat(&self) -> EntityCat {
        EntityCatFlags::VEGETABLE |
        EntityCatFlags::SENTIENT |
        EntityCatFlags::SOLID |
        EntityCatFlags::SOLID_TO_PLAYER
    }
    
    fn describe(&self, _resources: &Resources) -> String {
        format!("A {}",
                match self.species {
                    Species::Apple => "apple tree",
                    Species::Banyan => "banyan tree",
                    Species::Baobab => "baobab tree",
                    Species::Begonia => "begonia plant",
                    Species::Bulbinetorta => "bulbine torta plant",
                    Species::Calathea => "calathea plant",
                    Species::Dollseye => "dollseye plant",
                    Species::Dragonblood => "dragon blood tree",
                    Species::Drosera => "drosera plant",
                    Species::Fishbone => "fishbone cactus",
                    Species::Jasmine => "jasmine plant",
                    Species::Lecanopteris => "lecanopteris fern",
                    Species::Monkeymask => "monkeymask plant",
                    Species::Ponytailpalm => "ponytail palm",
                    Species::Stefonia => "stefonia plant",
                    Species::Strelizia => "strelizia plant",
                    Species::Tillandsia => "tillandsia plant"
                }
        )
    }
    
    fn update(&self, _entities: &EntityRefVec, resources: &Resources, delta: &Duration) -> (Box<dyn Entity>,MessageVec) {

        let mut rng = rand::thread_rng();
        let new_timer = self.timer + delta.as_millis();
        
        match self.state {
            EntityState::Idle => {
                if self.pos.dist(resources.world_state.player_pos) < SOUND_TRIGGER_DIST {
                    return (Box::new(PlantEntity {
                        state: EntityState::Activity,
                        timer: 0,
                        ..*self }),
                            vec![
                                Message::new(
                                    MessageType::PlaySoundAt(
                                        self.pos().sub(resources.world_state.player_pos),
                                        10.0,
                                        SynthNodeDesc::build("sample", &[
                                            ("file_handle", self.sample_handle.into()),
                                            ("playback_rate", rng.gen_range(0.9..1.3).into())
                                        ], &[])
                                    ), self.id, WORLD_ID
                                )
                            ]);
                }
            }

            EntityState::Activity => {
                if self.timer > SOUND_TRIGGER_PERIOD {
                    return (Box::new(PlantEntity {
                        state: EntityState::Idle,
                        ..*self }), vec![]
                    );
                }
            }

            EntityState::OpenMenu(from_key) => {
                return (Box::new(PlantEntity {
                    state: EntityState::WaitMenu,
                    ..*self
                }), vec![
                    Message::new(
                        MessageType::OpenMenu("Plant menu".into(), Rc::new(
                            vec![
                                MenuItem::new(
                                    match resources.theme {
                                        Theme::Daytime => "Talk to plant".into(),
                                        Theme::Nighttime => "Approach and talk to plant".into(),
                                    }, 
                                    Rc::new(vec![
                                        Message::new(MessageType::TalkTo(self.id, "plant".into(), self.pos), self.id, PLAYER_ID)
                                    ])
                                )
                            ]
                            ), 100., from_key),
                        self.id(), WORLD_ID
                    ),
                    self.play_sound_msg(resources,"/sounds/notify.mp3", 1.0)
                ]);
            }

            EntityState::WaitMenu => {
                if self.timer > MENU_WAIT_TIME_MILLIS {                    
                    return (Box::new(PlantEntity {
                        state: EntityState::Idle,
                        ..*self
                    }), vec![])
                }
            }


            EntityState::Listening => {
                if self.timer > resources.time("listening_time") {
                    return (Box::new(PlantEntity {                    
                        state: EntityState::Answer,
                        ..*self
                    }), vec![])
                }
            },

            EntityState::Answer => {
                //let cursor = *choices.choose(&mut rng).unwrap();
                    
                return (Box::new(PlantEntity {
                    state: EntityState::Idle,
                    timer: 0,                    
                    ..*self
                }), vec![
                    // no more conversation from a plant, so stop
                    // the player talking immediately (no need to wait)
                    Message::new(MessageType::Question(0), self.id(), PLAYER_ID),
                    Message::new(
                        MessageType::SpeechBubble(
                            PLANT_REPLIES
                                .get(&self.species)
                                .unwrap()
                                .choose(&mut rng)
                                .unwrap()
                                .to_string(),
                            Voice::Plant,
                            PLAYER_ID
                        ),
                        self.id(),
                        WORLD_ID
                    ),
                    
                    // self.play_sound_msg(
                    //     resources,
                    //     *["/sounds/chatter-1.mp3",
                    //       "/sounds/chatter-2.mp3",
                    //       "/sounds/chatter-3.mp3"]
                    //         .choose(&mut rng)
                    //         .unwrap(),
                    //     rng.gen_range(1.4..1.8)
                    // )
                ]);            
            }
                        
            _ => {}
        }

        (Box::new(PlantEntity{
            timer: new_timer,
            ..*self }),
         vec![])
    }

    fn apply_message(&self, msg: &Message, resources: &Resources) -> Box<dyn Entity> {
        match &msg.data {

            // open the menu when a player clicks on us
            MessageType::MouseClick(_, pos, _) => {
                // don't open the menu if it's already open
                if !resources.world_state.menu_open && pos.inside_box(self.bb()) {
                    return Box::new(PlantEntity {
                        state: EntityState::OpenMenu(false),
                        ..*self
                    });              
                }               
            },            

            MessageType::MenuGone => {
                if self.state == EntityState::WaitMenu && msg.recipient == self.id {
                    return Box::new(PlantEntity {
                        state: EntityState::Idle,
                        ..*self
                    });
                }
            }

            MessageType::Question(_cursor) => { 
                if msg.recipient == self.id {
                    return Box::new(PlantEntity {
                        state: EntityState::Listening,
                        timer: 0,
                        ..*self
                    });
                } 
            }

            MessageType::KeyPress(key) => {                
                if !resources.world_state.conversation_happening && is_trigger_menu_key(key) && self.pos.dist(resources.world_state.player_pos) < MENU_TRIGGER_DISTANCE {
                    return Box::new(PlantEntity {
                        state: EntityState::OpenMenu(true),
                        ..*self
                    });                                      
                }
            }
            
            _ => {}
        }
        
        Box::new(*self)
    }

    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        
        // context.begin_path();
        // context
        //     .arc(self.pos.x,self.pos.y, 5.0, 0.0, std::f64::consts::PI * 2.00)
        //     .unwrap();
        // context.set_fill_style(&"#000".into());        
        // context.fill();

/*        context.begin_path();
        let bb = self.bb();
        context.rect(bb.0.x, bb.0.y, bb.1.x - bb.0.x, bb.1.y - bb.0.y);
        context.set_stroke_style(&"#f00".into());
        context.stroke();                                
  */      
        match pass {
            RenderPass::GUI => {
                if resources.world_state.current_entity_selection == self.id {                    
                    if let Some(image) = resources.get_sprite_from_path("/images/gui/down-arrow.png") {
                        context.draw_image_with_html_image_element(image, self.pos.x - 50., self.pos.y - 200.);
                    }
                }
            }
            RenderPass::Beauty => {
                if let Some(image) = resources.sprite(&self.image_handle) {
                    let x: i32 = self.pos.x as i32 - image.width() as i32 / 2;
                    let y: i32 = self.pos.y as i32 - image.height() as i32 + 20;
                    context.draw_image_with_html_image_element(image, x as f64, y as f64);
                }
            },

            RenderPass::Shadow => {                
                if let Some(image) = resources.sprite(&self.image_handle) {
                    let mut x: i32 = self.pos.x as i32 - image.width() as i32 / 2;
                    let mut y: i32 = self.pos.y as i32 - image.height() as i32 + 20;

                    x+=(Into::<f64>::into(image.height()) * 0.72) as i32;
                    y+=(Into::<f64>::into(image.height()) * 0.47) as i32;

                    context.save();
                    context.translate(
                        x as f64,
                        y as f64
                    );
                    context.transform(1., 0., -0.75, 0.5, 1., 1.);                    
                    context.draw_image_with_html_image_element(image, 0., 0.);
                    context.restore();
                }
            }
            
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
