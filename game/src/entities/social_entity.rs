// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use rand::Rng;
use std::rc::Rc;
use std::any::Any;
use vector2math::*;
use rand::prelude::SliceRandom;
use instant::{ Duration };

use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::inventory::*;

use crate::items::disease_item::*;
use crate::game::inventory_item::*;
use crate::game::message::*;
use crate::game::message::MessageType::*;
use crate::game::resources::{ Resources };
use crate::rendering::animation::{ ClipId, AnimId };
use crate::rendering::rendering::{ RenderPass };
use crate::entities::plant_entity::{ PlantEntity };
use crate::rendering::sprite_dimensions::*;
use crate::gui::menu_item::MenuItem;

use crate::entities::gift_entity::{ GiftType };
use crate::game::constants::*;
use crate::sound::speech::{ Voice };
use crate::rendering::animation::{ animid_to_character_type };
use crate::game::world_state::{ WorldPhase };

use crate::declare_any;
use crate::declare_message_reader;

const TOO_CLOSE_DISTANCE: f64 = 50.0;
const FRIEND_DISTANCE: f64 = 200.0;
const STRANGER_DISTANCE: f64 = 300.0;
const WALK_LIKELIHOOD: u32 = 10;
const ACTIVITY_LIKELIHOOD: u32 = 50; // 500
const ACTIVITY_MILLIS: u128 = 5000;
const SYMPTOM_DURATION_MILLIS: u128 = 1500;

/// A character that is part of a social network and is
/// autonomous. Also known as 'Nergals', these characters can catch
/// diseases and talk to the player, who may or may not be part of
/// their social network.
#[derive(Clone)]
pub struct SocialEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub vel: Vec2,
    pub inventory: Rc<InventoryVec>,
    pub state: EntityState,
    pub timer: u128,
    pub target: Vec2,
    pub anim: ClipId,
    pub character: AnimId,
    pub conversation_cursor: u64
}

#[allow(dead_code)]
impl SocialEntity {
    pub fn new(id: EntityId) -> Self {
        SocialEntity {
            id,
            pos: Vec2::zero(),
            vel: Vec2::zero(),
            inventory: Rc::new(vec![]),
            state: EntityState::Idle,
            timer: 0,
            target: Vec2::zero(),
            anim: ClipId::Idle,
            character: AnimId::StickCharacter,
            conversation_cursor: 0
        }   
    }
    
    pub fn at_pos(id: EntityId, pos: Vec2, character: AnimId) -> Self {
        SocialEntity {
            id,
            pos,
            vel: Vec2::zero(),
            inventory: Rc::new(vec![]),
            state: EntityState::Idle,
            timer: 0,
            target: Vec2::zero(),
            anim: ClipId::Idle,
            character,
            conversation_cursor: 0
        }   
    }

    fn start_walking(&self, messages: &MessageVec, _delta: &Duration, resources: &Resources, next_state: EntityState) -> (Box<dyn Entity>, MessageVec) {
        (Box::new(SocialEntity {
            state: next_state,
            ..self.clone()
        }), msg_concat(
            messages,
            &vec![
                // stop any previously triggered walking sound
                Message::new(
                    MessageType::StopSound("walking".into()), self.id, WORLD_ID
                ),
                self.play_looped_sound_msg(
                    "walking",
                    resources,
                    "/sounds/steps-normal-fast.mp3",
                    1.
                )
            ]
        ))            
    }

    fn walk(&self, messages: &MessageVec, delta: &Duration, next_state: EntityState) -> (Box<dyn Entity>,MessageVec) {
        let v = self.target.sub(self.pos);
        let m = v.norm().mul(WALK_SPEED*delta.as_millis() as f64);
        let stop_walking = v.mag()<SPEAK_DISTANCE;
        (Box::new(SocialEntity {                    
            state: if stop_walking { next_state } else { self.state },
            anim: if stop_walking { self.anim_idle_or_ill() } else { self.anim },
            pos: self.pos.add(m),
            inventory: self.inventory.clone(),
            ..*self
        }), if stop_walking {
            msg_cons(
                messages,
                Message::new(
                    MessageType::StopSound("walking".into()), self.id, WORLD_ID
                )
            )
        } else {
            messages.clone()
        })
    }
    
    /// Convert the actions from conversations into messages to dispatch
    fn messages_from_action(&self, cursor: u64, resources: &Resources) -> Vec<Message> {
        match resources.conversations.action_from_cursor(cursor) {
            "snack" => {
                // use the entity id to choose the gift
                // so each character has a specific one
                // the provide to the player
                let snack = [GiftType::Banana,
                             GiftType::Blueberries,
                             GiftType::Ramen,
                             GiftType::Cupcake,
                             GiftType::Mango,
                             GiftType::Carrot,
                             GiftType::Beetroot,
                             GiftType::Popcorn,
                             GiftType::Hazelnuts][self.id.0 % 8];

                vec![
                    Message::new(
                        SpeechBubble(
                            match snack {
                                GiftType::Banana => "I hope you like bananas",
                                GiftType::Blueberries => "Have some delicious blueberries I gathered from the forest",
                                GiftType::Ramen => "Have some ramen to fuel your noodle",
                                GiftType::Cupcake => "You're my cupcake",
                                GiftType::Mango => "A sweet mango for a sweet Nergal",
                                GiftType::Carrot => "You can have this fabulous carrot I grew",
                                GiftType::Beetroot => "Here's a lovely beetroot, I'm rooting for you",
                                GiftType::Popcorn => "Popcorn is one of the only situations in which you eat the result of an explosion",
                                GiftType::Hazelnuts => "I squirrelled away these hazelnuts just for you"
                            }.to_string(),
                            Voice::Nergal,
                            PLAYER_ID
                        ),
                        self.id(),
                        WORLD_ID
                    ),
                                        
                    Message::new(
                        SendGift(
                            snack,
                            PLAYER_ID),
                        self.id, WORLD_ID)
                ]}            
            _ => vec![]
        }        
    }

    fn anim_idle_or_ill(&self) -> ClipId {
        if self.inventory_contains(InventoryItemType::Disease) {
            ClipId::Ill
        } else {
            ClipId::Idle
        }
    }

}

impl Inventory for SocialEntity {
    fn items(&self) -> &InventoryVec {
        &self.inventory
    }
}

impl Entity for SocialEntity {
    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    fn bb(&self) -> (Vec2, Vec2) {
        character_bounding_box(self.pos, self.character)
    }
    fn state(&self) -> EntityState { self.state }
    fn set_state(&mut self, state: EntityState) { self.state=state }
    fn cat(&self) -> EntityCat {
        EntityCatFlags::ANIMAL |
        EntityCatFlags::SOLID |
        EntityCatFlags::SENTIENT |
        EntityCatFlags::MOVABLE
    }

    fn describe(&self, resources: &Resources) -> String {
        format!("A {} shaped {} nergal, who is {}",
                animid_to_character_type(self.character),
                if resources.social_graph.is_player_connected(self.id) {
                    "friend"
                } else {
                    "stranger"
                },
                match self.anim {
                    ClipId::Idle => "standing still",
                    ClipId::Dance => "dancing",
                    ClipId::Eat => "eating",
                    ClipId::Ill => "ill looking",
                    ClipId::Death => "dead",
                    ClipId::Jump => "jumping",
                    ClipId::Kneebend => "bending their knees",
                    ClipId::Lean => "leaning left and right",
                    ClipId::Vomit => "vomiting",
                    ClipId::Sleep => "sleeping",
                    ClipId::Sneeze => "sneezing",
                    ClipId::Talk => "talking",
                    ClipId::WalkRight => "walking right",
                    ClipId::WalkLeft => "walking left",
                    ClipId::WalkUp => "walking upwards",
                    ClipId::WalkDown => "walking downwards"
                }
        )
    }
    
    fn update(&self, entities: &EntityRefVec, resources: &Resources, delta: &Duration) -> (Box<dyn Entity>, MessageVec) {
        let mut rng = rand::thread_rng();

        // update the inventory, and distribute any messages from
        // items we hold
        let (updated_inventory, messages) = self.inventory_update(self, resources, delta);

        let connected = resources.social_graph.all_connected_entity_ids(self.id);
        
        let updated_entity = SocialEntity {
            timer: self.timer + delta.as_millis(),
            inventory: Rc::new(updated_inventory),
            ..*self
        };
        
        // first priority is avoiding others, if not doing something else
        if !self.do_not_disturb() && self.timer > 3000 && self.state != EntityState::Avoid && self.state != EntityState::StartAvoid && self.state != EntityState::Symptomatic {
            let close  = self.within_radius_of(
                entities, TOO_CLOSE_DISTANCE, EntityCatFlags::SOLID
            );
            
            // any close by?
            if !close.is_empty() {
                // target based on multiple collisions?
                let target = self.pos().add(close.iter().fold(Vec2::zero(),|acc,e| {
                    if self.pos().dist(e.pos()) > 1. {
                        acc.add(self.pos().sub(e.pos()).norm().mul(
                            if connected.contains(&e.id()) {
                                FRIEND_DISTANCE
                            } else {
                                STRANGER_DISTANCE
                            }))
                    } else {
                        acc.add(Vec2::rnd())
                    }
                }));

                let sound = if close[0].cat() & EntityCatFlags::VEGETABLE != 0 {
                    let filename = format!("/sounds/hit-plant-{}.mp3", rng.gen_range(1..4));

                    if let Some(plant) = close[0].as_any().downcast_ref::<PlantEntity>() {                            
                        // set pitch based on species of plant
                        self.play_sound_msg(
                            resources,
                            &filename,
                            plant.size_to_pitch()
                        )
                    } else {
                        self.play_sound_msg(
                            resources,
                            &filename,
                            3.0
                        )
                    }
                } else if close[0].cat() & EntityCatFlags::SENTIENT != 0 {
                    let filename = format!("/sounds/bump-{}.mp3", rng.gen_range(1..6));
                    self.play_sound_msg(
                        resources,
                        &filename,
                        rng.gen_range(1.7..2.4)
                    )
                } else {
                    let filename = format!("/sounds/hit-rock-{}.mp3", rng.gen_range(1..4));
                    self.play_sound_msg(
                        resources,
                        &filename,
                        1.0
                    )
                };
                
                return (Box::new(SocialEntity {
                    state: EntityState::StartAvoid,
                    anim: self.anim_for_target(target),
                    timer: 0,
                    target,
                    ..updated_entity
                }), msg_concat(
                    &messages,
                    &vec![
                        Message::new(
                            MessageType::StopSound("walking".into()), self.id, WORLD_ID
                        ),
                        sound                            
                    ]
                ));
            }
        }
        
        // the state machine 
        match self.state {

            // main idle state
            EntityState::Idle => {
                // choose a new 'friend' to approach 
                if rng.gen_range(0..WALK_LIKELIHOOD)==1 {
                    if let Ok(choice) = self.choose_connected_prioritise_player(entities, &connected) {
                        // only need to approach if we are too far away
                        if choice.pos().dist(self.pos) > FRIEND_DISTANCE {
                            return (Box::new(SocialEntity {
                                state: EntityState::StartApproach,
                                target: choice.pos(),
                                anim: self.anim_for_target(choice.pos()),
                                timer: 0,
                                ..updated_entity
                            }), messages);
                        }
                    } else if !connected.is_empty() {
                        // there are no friends nearby, ask the world
                        // where a random connected entity is 'globally'
                        return (Box::new(
                            SocialEntity {
                                state: EntityState::Idle,
                                ..updated_entity
                            }), msg_cons(
                            &messages,
                            Message::new(
                                WhereIs(*connected.choose(&mut rng).unwrap()),
                                self.id,
                                WORLD_ID                                
                            )
                        ))
                    }
                } 

                // carry out an activity
                if rng.gen_range(0..ACTIVITY_LIKELIHOOD)==1 {
                    let new_anim = [ClipId::Sleep,
                                    ClipId::Jump,
                                    ClipId::Dance,
                                    ClipId::Kneebend,
                                    ClipId::Lean,
                                    ClipId::Talk,
                                    ClipId::Eat
                    ].choose(&mut rng).unwrap();

                    return (Box::new(SocialEntity {
                        state: EntityState::Activity,
                        anim: *new_anim,
                        timer: 0,
                        ..updated_entity
                    }), match *new_anim {
                        ClipId::Talk => msg_cons(
                            &messages,
                            self.play_sound_msg(
                                resources,
                                ["/sounds/chatter-1.mp3",
                                 "/sounds/chatter-2.mp3",
                                 "/sounds/chatter-3.mp3"]
                                    .choose(&mut rng)
                                    .unwrap(),
                                rng.gen_range(1.4..1.8)
                            )
                        ),
                        // ClipId::Sleep => msg_cons(
                        //     &messages,
                        //     self.play_sound_msg(
                        //         resources,
                        //         ["/sounds/snore-1.mp3",
                        //          "/sounds/snore-2.mp3"]
                        //             .choose(&mut rng)
                        //             .unwrap(),
                        //         rng.gen_range(2.4..2.8)
                        //     )
                        // ),
                        ClipId::Eat => msg_cons(
                            &messages,
                            self.play_sound_msg(
                                resources,
                                ["/sounds/nomnom-1.mp3",
                                 "/sounds/nomnom-2.mp3"]
                                    .choose(&mut rng)
                                    .unwrap(),
                                rng.gen_range(1.4..1.8)
                            )
                        ),
                        _ => vec![] 
                    });             
                }
            },
            
            EntityState::Listening => {
                if self.timer > resources.time("listening_time") {
                    return (Box::new(SocialEntity {                    
                        state: EntityState::Answer,
                        anim: self.anim_idle_or_ill(),                      
                        ..updated_entity
                    }), messages)
                }
            },

            EntityState::Answer => {
                let choices = resources.conversations.next_ids_from_cursor(self.conversation_cursor);

                if choices.is_empty() {
                    return (Box::new(SocialEntity {
                        state: EntityState::Idle,
                        timer: 0,
                        conversation_cursor: 0, // clear conversaton
                                                // so we know it's
                                                // finished
                        ..updated_entity
                    }), messages);                    
                } else {
                    // todo: use likelihood here
                    let cursor = *choices.choose(&mut rng).unwrap();
                    return (Box::new(SocialEntity {
                        state: EntityState::Talking,
                        timer: 0,
                        anim: ClipId::Talk,
                        conversation_cursor: cursor,                        
                        ..updated_entity
                    }), msg_concat(
                        &messages,
                        &msg_concat(
                            &self.messages_from_action(cursor, resources),
                            &vec![
                                Message::new(
                                    SpeechBubble(
                                        resources.conversations.text_from_cursor(cursor).into(),
                                        Voice::Nergal,
                                        PLAYER_ID
                                    ),
                                    self.id(),
                                    WORLD_ID
                                ),
                                
                                self.play_sound_msg(
                                    resources,
                                    ["/sounds/chatter-1.mp3",
                                     "/sounds/chatter-2.mp3",
                                     "/sounds/chatter-3.mp3"]
                                        .choose(&mut rng)
                                        .unwrap(),
                                    rng.gen_range(1.4..1.8)
                                )
                            ]
                        )
                    ));
                }
            },

            EntityState::Talking => {
                if self.timer > resources.time("speaking_time") {
                    return (Box::new(SocialEntity {                    
                        state: EntityState::WaitMenu,
                        anim: self.anim_idle_or_ill(),
                        conversation_cursor: 0,
                        ..updated_entity
                    }), msg_concat(
                        &messages, &vec![
                            Message::new(
                                Question(self.conversation_cursor),
                                self.id(),
                                PLAYER_ID, 
                            ),
                            Message::new(
                                HealthChange(SOCIAL_AWARD_HEALTH, HealthReason::Social),
                                self.id(),
                                PLAYER_ID
                            ),
                            self.play_sound_msg(resources,"/sounds/notify-5.mp3", 1.0)
                        ]
                    ));
                }                
            },

            EntityState::StartApproach => return updated_entity.start_walking(&messages, delta, resources, EntityState::Approach),
            EntityState::Approach => return updated_entity.walk(&messages,delta,EntityState::Idle),

            EntityState::StartAvoid => return updated_entity.start_walking(&messages, delta, resources, EntityState::Avoid),
            EntityState::Avoid => return updated_entity.walk(&messages,delta,EntityState::Idle),

            EntityState::Activity => {
                if self.timer > ACTIVITY_MILLIS {
                    return (Box::new(SocialEntity {
                        state: EntityState::Idle,
                        timer: 0,
                        anim: self.anim_idle_or_ill(),
                        ..updated_entity
                    }), messages)
                }                
            }

            EntityState::Symptomatic => {
                if self.timer>SYMPTOM_DURATION_MILLIS {                
                    return (Box::new(SocialEntity {
                        state: EntityState::Idle,
                        timer: 0,
                        anim: self.anim_idle_or_ill(),
                        ..updated_entity
                    }), messages)
                }
            }

            EntityState::OpenMenu(from_key) => {
                let (_sprite_sheet, dims) = character_params(self.character);
                let friend = resources.social_graph.is_player_connected(self.id);
                return (Box::new(SocialEntity {
                    state: EntityState::WaitMenu,
                    anim: self.anim_idle_or_ill(),
                    timer: 0,
                    ..updated_entity
                }), msg_concat(
                    &messages,
                    &vec![
                        Message::new(
                            OpenMenu(
                                if friend {
                                    "Friend menu".into()
                                } else {
                                    "Stranger menu".into()
                                },
                                Rc::new(
                                    if friend {
                                        vec![
                                            MenuItem::new(
                                                match resources.theme {
                                                     Theme::Daytime => "Talk to friend".into(),
                                                    Theme::Nighttime => "Approach and talk to friend".into(),
                                                }, 
                                                Rc::new(vec![
                                                    Message::new(TalkTo(self.id, "nergal".into(), self.pos), self.id, PLAYER_ID)
                                                ])
                                            ),
                                            MenuItem::new(
                                                "Unfriend".into(),
                                                Rc::new(vec![
                                                    Message::new(RemoveRelationship(PLAYER_ID), self.id, WORLD_ID)
                                                ])
                                            )
                                        ]
                                    } else {
                                        vec![
                                            MenuItem::new(
                                                match resources.theme {
                                                    Theme::Daytime => "Talk to stranger".into(),
                                                    Theme::Nighttime => "Approach and talk to stranger".into(),
                                                },
                                                Rc::new(vec![
                                                    Message::new(TalkTo(self.id, "nergal".into(), self.pos), self.id, PLAYER_ID)
                                                ])
                                            )
                                        ]
                                    }), dims.body_height, from_key),
                            self.id(), WORLD_ID
                        ),
                        self.play_sound_msg(resources,"/sounds/notify.mp3", 1.0)
                    ])
                );
            }

            EntityState::WaitMenu => {
                if self.timer > MENU_WAIT_TIME_MILLIS {                    
                    return (Box::new(SocialEntity {
                        state: EntityState::Idle,
                        conversation_cursor: 0,
                        ..updated_entity
                    }), messages)
                }
            }

            _ => {}
        }
        (Box::new(updated_entity), messages)
    }

    fn apply_message(&self, msg: &Message, resources: &Resources) -> Box<dyn Entity> {
        match &msg.data {
            
            Question(cursor) => { 
                if msg.recipient == self.id {
                    return Box::new(SocialEntity {
                        state: EntityState::Listening,
                        timer: 0,
                        anim: self.anim_idle_or_ill(),
                        conversation_cursor: *cursor,
                        ..self.clone()
                    });
                } 
            }

            ConversationOver => { 
                if msg.recipient == self.id {
                    return Box::new(SocialEntity {
                        conversation_cursor: 0,
                        ..self.clone()
                    });
                } 
            }

            // response from server as to where the friend entity is
            // in the world (not part of a conversation)
            Response(response) => {
                if msg.recipient == self.id {
                    let target = response.answer_as::<Vec2>();
                    
                    return Box::new(SocialEntity {
                        state: EntityState::StartApproach,
                        target: *target,
                        anim: self.anim_for_target(*target),
                        timer: 0,
                        ..self.clone()
                    });
                } 
            }
            
            Infect(disease, source) => {
                if msg.recipient == self.id {
                    return Box::new(SocialEntity {
                        inventory: Rc::new(self.inventory_add(
                            DiseaseItem::new(*disease, *source)
                        )),
                        ..self.clone() 
                    });
                }
            },

            Spread(_strain) => {
                // just replace the current animation with a sneeze -
                // as long as we aren't walking
                if msg.recipient == self.id {
                    return Box::new(SocialEntity {
                        state: if self.do_not_disturb() { self.state } else { EntityState::Symptomatic },
                        anim: match resources.world_state.phase() {
                            WorldPhase::Manic => ClipId::Vomit,
                            _ => ClipId::Sneeze
                        },
                        timer: 0, // is this right? slows down conversation?
                        ..self.clone() 
                    });
                }
            },

            // open the menu when a player clicks on us
            MouseClick(_, pos, _) => {
                let bb = character_bounding_box(self.pos, self.character);
                // don't open the menu if it's already open
                if !resources.world_state.menu_open && pos.inside_box(bb) {
                    return Box::new(SocialEntity {
                        state: EntityState::OpenMenu(false),
                        ..self.clone()
                    });              
                }               
            },            

            KeyPress(key) => {
                // don't check if someone is already having a conversation
                if !resources.world_state.conversation_happening && is_trigger_menu_key(key) && self.pos.dist(resources.world_state.player_pos) < MENU_TRIGGER_DISTANCE {
                    return Box::new(SocialEntity {
                        state: EntityState::OpenMenu(true),
                        ..self.clone()
                    });                                      
                }
            },

            MenuGone => {
                if self.state == EntityState::WaitMenu && msg.recipient == self.id {
                    return Box::new(SocialEntity {
                        state: EntityState::Idle,
                        ..self.clone()
                    });
                }
            }
            
            _ => {}
        }     
        Box::new(self.clone())
    }
    

    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {
        let (sprite_sheet, dims) = character_params(self.character);

        let offset_x = -(dims.sprite_width/2.);
        let offset_y = -dims.sprite_height + dims.feet_height;                     
        
        match pass {
            RenderPass::GUI => {
                if resources.world_state.current_entity_selection == self.id {                    
                    if let Some(image) = resources.get_sprite_from_path("/images/gui/down-arrow.png") {
                        context.draw_image_with_html_image_element(image, self.pos.x - 50., self.pos.y - 200.);
                    }
                }
            }
            RenderPass::Beauty => {
                if let Some(image) = resources.get_sprite_from_path(sprite_sheet) {
                    if let Some(anim) = resources.animation.get(&self.character) {
                        anim.render(
                            context,
                            self.pos.x + offset_x,
                            self.pos.y + offset_y,
                            self.anim,
                            image,
                            &self.timer
                        );

                        // display a heart if they are a friend of the player
                        // todo: (could easily cache this and remove the need for the search)
                        if resources.social_graph.is_player_connected(self.id) {
                            if let Some(image) = resources.get_sprite_from_path("/images/sprites/heart.png") {
                                let bb = self.bb();
                                let mut pos = Vec2::new(bb.0.x + (bb.1.x - bb.0.x) / 2. - 15.,
                                                        bb.0.y - 40.);

                                // make the heart move up and down when the character is jumping
                                if self.anim == ClipId::Jump {
                                    let frame = anim.current_frame(self.anim, &self.timer);
                                    if frame < 5 {
                                        pos.y -= [0., 20., 40., 40., 20., 0.][frame];
                                    }
                                }
                                
                                context.draw_image_with_html_image_element(image, pos.x.round(), pos.y.round());
                            }
                        }
                    }
                }
            }
            
            RenderPass::Shadow => {                
                if let Some(image) = resources.get_sprite_from_path(sprite_sheet) {
                    context.save();              
                    context.translate(
                        self.pos.x + offset_x,
                        self.pos.y + offset_y
                    );
                    context.transform(1., 0., -0.75, 0.5, 1., 1.);
                    // magic numbers here
                    context.translate(dims.sprite_width * dims.shadow_tx_x,
                                      dims.sprite_height * dims.shadow_tx_y);               
                    
                    if let Some(anim) = resources.animation.get(&self.character) {
                        anim.render(
                            context,
                            0., 0.,
                            self.anim,
                            image,
                            &self.timer
                        );
                    }
                    
                    context.restore();
                }
            }
            
            RenderPass::Debug => {
                /*let mut pos=-180.;
                for message in &*self.memory {
                    context.fill_text(&message, self.pos.x-20.0, self.pos.y+pos).unwrap();
                    pos-=15.0;
                }*/

                context.begin_path();
                let bb = character_bounding_box(self.pos, self.character);
                context.rect(bb.0.x, bb.0.y, bb.1.x - bb.0.x, bb.1.y - bb.0.y);
                context.set_stroke_style(&"#f00".into());
                context.stroke();                                
                context.set_fill_style(&"#000".into());
                context.fill_text(&format!("{:?} {}", self.state(), self.conversation_cursor), self.pos.x-25.0, self.pos.y-90.0).unwrap();
                
                //      let idstr = format!("{}",self.id.0);
                //      context.fill_text(&idstr, self.pos.x-25.0, self.pos.y-120.0).unwrap();
            }
            _ => {}
        }
    }

    declare_any!();
    declare_message_reader!();
}


#[cfg(test)]
mod tests {
    use super::*;
    use instant::Instant;
    use std::ptr;
    use crate::game::conversation::ConversationGraph;
    
    #[test]
    fn basics() {
        
        /*let mut r = Resources::new();
        r.social_graph.generate_cache();
        r.conversations = r.conversations.load_from_graphml(&r#"
<graphml>
  <key attr.name="weight" attr.type="double" for="edge" id="likelihood" />
  <graph edgedefault="undirected">
    <node id="1" text_en="Hello, friend how are you?"/>
    <node id="2" text_en="I'm great thank you, have you seen anyone sneezing?"/>

    <edge source="1" target="2"> <data key="likelihood">0.5</data> </edge>

  </graph>
</graphml>
        "#.to_string());

        
        r.social_graph = r.social_graph.load_from_block(
            &r#"0 0 0
                0 1 0     
                0 0 1"#.into()
        );

        
        println!("hello");
        
        
        let mut s = Box::new(SocialEntity::new(1.into()));
        assert_eq!(s.id, 1.into());
        assert_eq!(s.state, EntityState::Idle);

        let delta = Instant::now().elapsed();
        
        // check responding to conversations

        let e: EntityVec = vec![Box::new(SocialEntity::at_pos(2.into(), Vec2::new(300.0,0.0), AnimId::StickCharacter))];
        let er: EntityRefVec = e.iter().collect();
        let c: Vec<EntityId> = vec![2.into()];
        
        let inventory_ptr = ptr::addr_of!(*s.inventory);
        {
            let (new_s,messages) = s.update(&er,&r,&delta);
            s = Box::new(new_s.as_any().downcast_ref::<SocialEntity>().unwrap().clone());
        }
            
            
        assert_eq!(s.state(), EntityState::Idle);

        s = Box::new(s.dispatch(&vec![Message {
            sender: 2.into(),
            recipient: 1.into(),
            data: Question(1 as u64),
        }], &r).as_any().downcast_ref::<SocialEntity>().unwrap().clone());
        
        assert_eq!(s.state, EntityState::Listening);

        {
            let delta = delta + Duration::new(20, 0);
            let (new_s,messages) = s.update(&er,&r,&delta);
            s = Box::new(new_s.as_any().downcast_ref::<SocialEntity>().unwrap().clone());
            let (new_s,messages) = s.update(&er,&r,&delta);
            s = Box::new(new_s.as_any().downcast_ref::<SocialEntity>().unwrap().clone());

            assert_eq!(s.state,EntityState::Answer); 
            assert_eq!(s.conversation_cursor, 1); 

            let (new_s,messages) = s.update(&er,&r,&delta);
            s = Box::new(new_s.as_any().downcast_ref::<SocialEntity>().unwrap().clone());

            assert_eq!(s.state,EntityState::Idle); 
            //assert_eq!(messages[0].sender, 1.into());            
        }
            
        
        s.inventory = Rc::new(s.inventory_add(DiseaseItem::new(DiseaseStrain::DiseaseA, NO_ID)));
        
        {
            let (new_s,messages) = s.update(&er,&r,&Duration::from_millis(300));
            s = Box::new(new_s.as_any().downcast_ref::<SocialEntity>().unwrap().clone());
        }
        
        println!("{:?}",s.filter_items(InventoryItemType::Disease)
                 .iter()
                 .map(|ii| { item_as::<DiseaseItem>(ii).unwrap() })
                 .collect::<Vec<_>>());
        
        assert_eq!(item_as::<DiseaseItem>(&s.inventory[0]).unwrap().timer, 300);
        
        {
            let (new_s,messages) = s.update(&er,&r,&Duration::from_millis(300));
            s = Box::new(new_s.as_any().downcast_ref::<SocialEntity>().unwrap().clone());
        }

        println!("{:?}",s.filter_items(InventoryItemType::Disease)
                 .iter()
                 .map(|ii| { item_as::<DiseaseItem>(ii).unwrap() })
                 .collect::<Vec<_>>());
        
        assert_eq!(item_as::<DiseaseItem>(&s.inventory[0]).unwrap().timer, 600);
        
        */
    }
}



