// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;
use rand::Rng;
use std::any::Any;
use instant::{ Duration };
use rand::prelude::SliceRandom;
use vector2math::Vector2;


use crate::game::id::*;
use crate::game::message::*;
use crate::game::message::MessageType::*;
use crate::game::entity::{ Entity };
use crate::game::inventory_item::*;
use crate::game::inventory::{ InventoryVec };
use crate::sound::synth_node_desc::{ SynthNodeDesc };
use crate::game::resources::{ Resources };
use crate::game::world_state::{ WorldPhase };

use crate::item_declare_any;



const DURATION_VARIATION: i64 = 2_000;
const INCUBATION_DURATION: u128 = 10_000;
const SYMPTOMATIC_DURATION: u128 = 10_000;
const CONVALESCENCE_DURATION: u128 = 10_000;

const SNEEZE_DURATION: u128 = 3_000;
pub const INFECTION_RADIUS: f64 = 200.;
const HEALTH_DROP_PER_SNEEZE: f64 = -5.;

fn calc_duration(dur: u128) -> u128 {
    (dur as i64 +
     rand::thread_rng().gen_range(-DURATION_VARIATION..DURATION_VARIATION)
     as i64) as u128
}

#[derive(Debug,Clone,Copy,PartialEq)]
pub enum DiseaseStrain {
    DiseaseA,
    DiseaseB
}

#[derive(Debug,Clone,Copy,PartialEq)]
pub enum DiseaseStage {
    Spawned,
    Incubating,
    Symptomatic,    
    Convalescent,
    Recovered,
    Finished
}

#[derive(Debug,Clone,Copy)]
pub struct DiseaseItem {
    pub strain: DiseaseStrain,
    pub stage: DiseaseStage,
    pub timer: u128,
    pub stage_duration: u128,
    pub sneeze_timer: u128,
    pub is_fatal: bool,
    pub source: EntityId
}

impl DiseaseItem {    
    pub fn new(strain: DiseaseStrain, source: EntityId) -> Self {
        DiseaseItem {
            strain,
            stage: DiseaseStage::Spawned,
            timer: 0,
            stage_duration: 0,
            sneeze_timer: 0,
            is_fatal: false,
            source
        }
    }
}

impl InventoryItem for DiseaseItem {

    fn item_type(&self) -> InventoryItemType {
        InventoryItemType::Disease
    }

    fn delete_me(&self) -> bool {
        self.stage == DiseaseStage::Finished
    }
    
    fn is_dup_of(&self, other: &Rc<dyn InventoryItem>) -> bool {
        if let Some(other) = item_as::<DiseaseItem>(other) {
            if self.strain == other.strain {
                return true;
            }
        }
        false   
    }

    fn update(&self, _inventory: &InventoryVec, owner: &dyn Entity, resources: &Resources, delta: &Duration) -> (Rc<dyn InventoryItem>, MessageVec) {

        //if owner.id() == PLAYER_ID { console::log_1(&format!("stage: {:?} {:?}", self.stage, self.stage_duration).into()); }

        (Rc::new(DiseaseItem {
            timer: if self.timer > self.stage_duration {
                0
            } else {
                self.timer + delta.as_millis()
            },
            stage_duration: if self.timer > self.stage_duration || self.stage == DiseaseStage::Spawned {
                match self.stage {
                    DiseaseStage::Spawned => calc_duration(INCUBATION_DURATION), 
                    DiseaseStage::Incubating => calc_duration(SYMPTOMATIC_DURATION),
                    DiseaseStage::Symptomatic => calc_duration(CONVALESCENCE_DURATION),
                    DiseaseStage::Convalescent => 0,
                    DiseaseStage::Recovered => 0,
                    DiseaseStage::Finished => 0
                }
            } else {
                self.stage_duration
            },
            sneeze_timer: if self.stage == DiseaseStage::Symptomatic {
                if self.sneeze_timer > SNEEZE_DURATION {
                    0
                } else {
                    self.sneeze_timer + delta.as_millis()
                }
            } else {
                0
            },
            stage: if self.stage == DiseaseStage::Spawned {
                DiseaseStage::Incubating
            } else if self.timer > self.stage_duration {
                match self.stage {
                    DiseaseStage::Spawned => { panic!() } // shouldn't get here
                    DiseaseStage::Incubating => DiseaseStage::Symptomatic,
                    DiseaseStage::Symptomatic => DiseaseStage::Convalescent,
                    DiseaseStage::Convalescent => DiseaseStage::Recovered,
                    DiseaseStage::Recovered => DiseaseStage::Finished,
                    DiseaseStage::Finished => DiseaseStage::Finished,
                }
            } else {
                self.stage
            },
            ..*self
        }),
         if self.stage == DiseaseStage::Spawned {
             // to record the scores
             vec![Message::new(InfectionSuccess(self.source), owner.id(), WORLD_ID)]
         } else if self.stage == DiseaseStage::Recovered {         
             vec![Message::new(Recovered, owner.id(), WORLD_ID)]
         } else if self.sneeze_timer > SNEEZE_DURATION {

             let audio_name = match resources.world_state.phase() {
                 WorldPhase::Manic => "sick",
                 _ => "sneeze"
             };
             
             // only send events if we have just changed state
             // check *new* state
             vec![
                 // need to tell the owner to sneeze
                 Message::new(Spread(self.strain), 0.into(), owner.id()),
                 // and the game to infect surrounding nergals
                 Message::new(Spread(self.strain), owner.id(), WORLD_ID),
                 // and reduce health of owner
                 Message::new(HealthChange(HEALTH_DROP_PER_SNEEZE, HealthReason::Disease), 0.into(), owner.id()),
                 
                 Message::new(MessageType::PlaySoundAt(
                     owner.pos().sub(resources.world_state.player_pos),
                     2.0,
                     SynthNodeDesc::build(
                         "sample",
                         &[
                             if owner.id() == PLAYER_ID {
                                 ("file_name", format!("/sounds/{}-{}.mp3", audio_name, [1,2].choose(&mut rand::thread_rng()).unwrap()).into())
                             } else {
                                 ("file_name", format!("/sounds/{}-{}.mp3", audio_name, [3,4].choose(&mut rand::thread_rng()).unwrap()).into())
                             },
                             ("playback_rate", rand::thread_rng().gen_range(1.4..2.0).into())
                         ], &[])
                 ), NO_ID, WORLD_ID)
             ]
         } else {
             vec![]
         })
    }
    
    item_declare_any!();
}
