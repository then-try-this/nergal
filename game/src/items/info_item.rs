// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;
use std::any::Any;
use instant::Duration;
use crate::game::entity::{ Entity };
use crate::game::inventory::{ InventoryVec };
use crate::game::inventory_item::*;
use crate::game::message::{ MessageVec };
use crate::game::resources::{ Resources };

use crate::item_declare_any;



/// An inventory item that holds a simple piece of information.
#[derive(Debug, Clone, Hash)]
pub struct InfoItem {
    pub text: String
}

impl InfoItem {
    pub fn new(text: String) -> Self {
        InfoItem {
            text
        }
    }
}

impl InventoryItem for InfoItem {
    fn item_type(&self) -> InventoryItemType {
        InventoryItemType::Info
    }
    
    fn is_dup_of(&self, other: &Rc<dyn InventoryItem>) -> bool {
        if let Some(other) = item_as::<InfoItem>(other) {
            if self.text == other.text {
                return true;
            }
        }
        false   
    }
    
    fn update(&self, _inventory: &InventoryVec, _owner: &dyn Entity, _resources: &Resources, _delta: &Duration) -> (Rc<dyn InventoryItem>, MessageVec) {
        (Rc::new(self.clone()), vec![])
    }

    item_declare_any!();
}
