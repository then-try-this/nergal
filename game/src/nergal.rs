// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;



use vector2math::*;
use rand::prelude::SliceRandom;

use crate::maths::vec2::{ Vec2 };
use crate::game::id::*;

use crate::game::message::*;

use crate::game::resources::{ Resources };
use crate::game::sprite_sheet::{ SpriteSheet };
use crate::rendering::animation::*;
use crate::gui::screen_manager::{ ScreenManager, ScreenState };
use crate::game::bucket::{ Bucket };

use crate::game::entity::*;
use crate::game::game_scores::*;
use crate::gui::button_entity::{ ButtonEntity, ButtonType };
use crate::gui::gfx_entity::{ GfxEntity, GfxEntityData };
use crate::gui::text_entity::{ TextEntity, Alignment };
use crate::gui::highlight_text_entity::{ HighlightTextEntity, TextBlock };
use crate::gui::healthbar_entity::{ HealthBarEntity };
use crate::gui::clock_entity::{ ClockEntity };

use crate::sound::sound::{ Sound };
use crate::game::biome_layout::*;
use crate::sound::speech::{ Voice };

use crate::entities::social_entity::{ SocialEntity };
use crate::entities::scenery_entity::{ SceneryEntity };
use crate::entities::plant_entity::{ PlantEntity, Species };
use crate::entities::rock_entity::{ RockEntity };
use crate::entities::cloud_entity::{ CloudEntity };
use crate::entities::rainbow_entity::{ RainbowEntity };
use crate::entities::piano_entity::{ PianoEntity };
use crate::entities::moon_entity::{ MoonEntity };
use crate::entities::footprint_entity::{ FootprintEntity };

use crate::gui::prompt_entity::*;
use crate::game::constants::*;

use web_sys::console;

const NORMAL_TEXT_SIZE: u32 = 30;
const TEXT_AREA_WIDTH: f64 = 1000.;
const CIRCLE_BUTTON_Y: f64 = 350.;

pub fn nergal_daytime_colours(resources: &mut Resources) {
    resources.colours.insert("background".into(), "#f8f8f8ff".into());
    resources.colours.insert("results_background".into(), "#eeded5".into());
    resources.colours.insert("graph_background".into(), "#f9f3df".into());
    resources.colours.insert("default_text".into(), "#2a3a4a".into());
    resources.colours.insert("results_text".into(), "#2a3a4a".into());
    resources.colours.insert("button_bg".into(), "#d6c3c9".into());
    resources.colours.insert("button_hover".into(), "#cfd2b2".into());
    resources.colours.insert("button_text".into(), "#2a3a4a".into());
    resources.colours.insert("menu_bg".into(), "#fab4ab".into());
    resources.colours.insert("menu_hover".into(), "#ffffc9".into());
    resources.colours.insert("menu_text".into(), "#2a3a4a".into());
    resources.colours.insert("bubble_text".into(), "#2a3a4a".into());
    resources.colours.insert("dropdown_menu_text".into(), "#2a3a4a".into());
    resources.colours.insert("healthbar".into(), "#2e3f4f".into());
    resources.colours.insert("clock".into(), "#f0b046".into());
    resources.colours.insert("node_colour".into(), "#808ecc".into());
    resources.colours.insert("inverted_button_bg".into(), "#2a3a4a".into());
    resources.colours.insert("inverted_button_text".into(), "#f8f8f8".into());
    resources.colours.insert("inverted_button_hover".into(), "#565640".into());
    resources.colours.insert("highlight".into(), "#d1ff22".into());    
    resources.colours.insert("highlight_text".into(), "#2a3a4a".into());    
    resources.colours.insert("prompt_bg".into(), "#f8f8f8ff".into());    
    resources.colours.insert("prompt_text".into(), "#2a3a4a".into());    
        
    resources.attrs.insert("button_font".into(), "40px capriola".into());
    resources.attrs.insert("small_button_font".into(), "30px capriola".into());
    resources.attrs.insert("default_font".into(), "20px capriola".into());

    resources.times.insert("speaking_time".into(), 2500);
    resources.times.insert("listening_time".into(), 2000);
}

pub fn nergal_nighttime_colours(resources: &mut Resources) {
    resources.colours.insert("background".into(), "#2a3a4aff".into());
    resources.colours.insert("results_background".into(), "#d7c4ca".into());
    resources.colours.insert("graph_background".into(), "#f9f3df".into());
    resources.colours.insert("default_text".into(), "#fbf8f0".into());
    resources.colours.insert("results_text".into(), "#fbf8f0".into());
    resources.colours.insert("button_bg".into(), "#f8f8f8".into());
    resources.colours.insert("button_hover".into(), "#d0ff26".into());
    resources.colours.insert("button_text".into(), "#253748".into());
    resources.colours.insert("menu_bg".into(), "#fab4ab".into());
    resources.colours.insert("menu_hover".into(), "#ffffc9".into());
    resources.colours.insert("menu_text".into(), "#2a3a4a".into());
    resources.colours.insert("bubble_text".into(), "#2a3a4a".into());     
    resources.colours.insert("dropdown_menu_text".into(), "#2a3a4a".into());
    resources.colours.insert("healthbar".into(), "#fbf8f0".into());
    resources.colours.insert("clock".into(), "#f0b046".into());
    resources.colours.insert("inverted_button_bg".into(), "#f8f8f8".into());
    resources.colours.insert("inverted_button_text".into(), "#253748".into());
    resources.colours.insert("inverted_button_hover".into(), "#d0ff26".into());
    resources.colours.insert("highlight".into(), "#d1ff22".into());    
    resources.colours.insert("highlight_text".into(), "#2a3a4a".into());    
    resources.colours.insert("prompt_bg".into(), "#f8f8f8ff".into());    
    resources.colours.insert("prompt_text".into(), "#2a3a4a".into());    

    resources.attrs.insert("button_font".into(), "40px capriola".into());
    resources.attrs.insert("small_button_font".into(), "30px capriola".into());
    resources.attrs.insert("default_font".into(), "20px capriola".into());

    resources.times.insert("speaking_time".into(), 5000);
    resources.times.insert("listening_time".into(), 4000);
}

pub fn nergal_animations(resources: &mut Resources) {
    let mut a = Animation::new(SpriteSheet::new(7,6,1400,1543));
    a.clips.insert(ClipId::Idle, AnimClip::new(AnimMode::Loop, 5.0, &[24], &[], false, 0));
    a.clips.insert(ClipId::Dance, AnimClip::new(AnimMode::Loop, 5.0, &[24,0,24,1,24,0,24,1,24,0], &[], false, 0));
    a.clips.insert(ClipId::Eat, AnimClip::new(AnimMode::Loop, 5.0, &[2,3,2,3,2,3], &[], false, 0));
    a.clips.insert(ClipId::Ill, AnimClip::new(AnimMode::Loop, 5.0, &[4], &[], false, 0));
    a.clips.insert(ClipId::Death, AnimClip::new(AnimMode::Loop, 5.0, &[6], &[], false, 0));
    a.clips.insert(ClipId::Jump, AnimClip::new(AnimMode::Loop, 5.0, &[7,8,9,8,7,24], &[], false, 0));
    a.clips.insert(ClipId::Kneebend, AnimClip::new(AnimMode::Loop, 5.0, &[24,10,24,10,24], &[], false, 0));
    a.clips.insert(ClipId::Lean, AnimClip::new(AnimMode::Loop, 5.0, &[11,24,12,24], &[], false, 0));
    a.clips.insert(ClipId::Vomit, AnimClip::new(AnimMode::OneShot, 5.0, &[32,13,14,15,16,17,32], &[], false, 0));
    a.clips.insert(ClipId::Sleep, AnimClip::new(AnimMode::LoopEnd, 1.0, &[5,18,18,18], &[], false, 1));
    a.clips.insert(ClipId::Sneeze, AnimClip::new(AnimMode::OneShot, 5.0, &[19,20,21,22,23,4], &[], false, 0));
    a.clips.insert(ClipId::Talk, AnimClip::new(AnimMode::OneShot, 5.0, &[25,26,25,26,25,26,25,26,25,26,24], &[], false, 0));        
    a.clips.insert(ClipId::WalkUp, AnimClip::new(AnimMode::Loop, 10.0, &[35,36], &[], false, 0));
    a.clips.insert(ClipId::WalkDown, AnimClip::new(AnimMode::Loop, 10.0, &[27,28], &[], false, 0));
    a.clips.insert(ClipId::WalkRight, AnimClip::new(AnimMode::LoopEnd, 10.0, &[32,33,34], &[], false, 1)); // add loop range - for all walk chars
    a.clips.insert(ClipId::WalkLeft, AnimClip::new(AnimMode::LoopEnd, 10.0, &[31,30,29], &[], false, 1));
    
    resources.add_animation(AnimId::StickCharacter,a.clone());

    let mut stick_acorn = a.clone();
    stick_acorn.sheet = SpriteSheet::new(7,6,1400,2057);
    resources.add_animation(AnimId::StickAcornCharacter,stick_acorn);
    
    let mut stick_stetson = a.clone();
    stick_stetson.sheet = SpriteSheet::new(7,6,1400,2057);
    resources.add_animation(AnimId::StickStetsonCharacter,stick_stetson);

    let mut stick_party = a.clone();
    stick_party.sheet = SpriteSheet::new(7,6,1400,2400);
    resources.add_animation(AnimId::StickPartyCharacter,stick_party);
    
    let mut b = Animation::new(SpriteSheet::new(7,6,1400,1543));
    b.clips.insert(ClipId::Idle, AnimClip::new(AnimMode::Loop, 5.0, &[24], &[], false, 0));
    b.clips.insert(ClipId::Dance, AnimClip::new(AnimMode::Loop, 5.0, &[24,0,24,1,24,0,24,1,24,0], &[], false, 0));
    b.clips.insert(ClipId::Eat, AnimClip::new(AnimMode::Loop, 5.0, &[2,3,4,3,4,3,4,3,2], &[], false, 0));
    b.clips.insert(ClipId::Ill, AnimClip::new(AnimMode::OneShot, 5.0, &[5], &[], false, 0));
    b.clips.insert(ClipId::Death, AnimClip::new(AnimMode::OneShot, 5.0, &[7], &[], false, 0));
    b.clips.insert(ClipId::Jump, AnimClip::new(AnimMode::Loop, 5.0, &[8,9,10,9,8,24], &[], false, 0));
    b.clips.insert(ClipId::Kneebend, AnimClip::new(AnimMode::Loop, 5.0, &[11,24,11,24], &[], false, 0));
    b.clips.insert(ClipId::Lean, AnimClip::new(AnimMode::Loop, 5.0, &[24,12,24,13], &[], false, 0));
    b.clips.insert(ClipId::Vomit, AnimClip::new(AnimMode::OneShot, 5.0, &[34,14,15,16,17,18,34], &[], false, 0));
    b.clips.insert(ClipId::Sleep, AnimClip::new(AnimMode::LoopEnd, 1.0, &[6,19,19,19], &[], false, 1));
    b.clips.insert(ClipId::Sneeze, AnimClip::new(AnimMode::OneShot, 5.0, &[20,21,22,23,5], &[], false, 0));
    b.clips.insert(ClipId::Talk, AnimClip::new(AnimMode::OneShot, 5.0, &[25,26,25,26,25,26,25,26,25,26,24], &[], false, 0));        
    b.clips.insert(ClipId::WalkDown, AnimClip::new(AnimMode::Loop, 10.0, &[31,32], &[], false, 0));
    b.clips.insert(ClipId::WalkUp, AnimClip::new(AnimMode::Loop, 10.0, &[35,36], &[], false, 0));
    b.clips.insert(ClipId::WalkRight, AnimClip::new(AnimMode::LoopEnd, 10.0, &[34,30,28], &[], false, 1));
    b.clips.insert(ClipId::WalkLeft, AnimClip::new(AnimMode::LoopEnd, 10.0, &[33,29,27], &[], false, 1));
    
    resources.add_animation(AnimId::CloudCharacter,b.clone());

    resources.add_animation(AnimId::CloudPussyCharacter,b.clone());
    resources.add_animation(AnimId::CloudStetsonCharacter,b.clone());
    resources.add_animation(AnimId::CloudBobbleCharacter,b.clone());
    
    let mut a = Animation::new(SpriteSheet::new(7,6,1400,1543));
    a.clips.insert(ClipId::Idle, AnimClip::new(AnimMode::Loop, 5.0, &[24], &[], false, 0));
    a.clips.insert(ClipId::Dance, AnimClip::new(AnimMode::Loop, 5.0, &[24,0,24,1,24,0,24,1,24,0], &[], false, 0));
    a.clips.insert(ClipId::Eat, AnimClip::new(AnimMode::Loop, 5.0, &[2,3,2,3,2,3], &[], false, 0));
    a.clips.insert(ClipId::Ill, AnimClip::new(AnimMode::OneShot, 5.0, &[4], &[], false, 0));
    a.clips.insert(ClipId::Death, AnimClip::new(AnimMode::OneShot, 5.0, &[6], &[], false, 0));
    a.clips.insert(ClipId::Jump, AnimClip::new(AnimMode::Loop, 5.0, &[7,8,9,8,7,24], &[], false, 0));
    a.clips.insert(ClipId::Kneebend, AnimClip::new(AnimMode::Loop, 5.0, &[24,10,24,10,24], &[], false, 0));
    a.clips.insert(ClipId::Lean, AnimClip::new(AnimMode::Loop, 5.0, &[11,24,12], &[], false, 0));
    a.clips.insert(ClipId::Vomit, AnimClip::new(AnimMode::OneShot, 5.0, &[34,13,14,15,16,17,34], &[], false, 0));
    a.clips.insert(ClipId::Sleep, AnimClip::new(AnimMode::LoopEnd, 1.0, &[5,18,18,18], &[], false, 1)); 
    a.clips.insert(ClipId::Sneeze, AnimClip::new(AnimMode::OneShot, 5.0, &[19,20,21,22,23,4], &[], false, 0));
    a.clips.insert(ClipId::Talk, AnimClip::new(AnimMode::OneShot, 5.0, &[25,26,25,26,25,26,25,26,25,26,24], &[], false, 0));        
    a.clips.insert(ClipId::WalkDown, AnimClip::new(AnimMode::Loop, 10.0, &[31,32], &[], false, 0));
    a.clips.insert(ClipId::WalkUp, AnimClip::new(AnimMode::Loop, 10.0, &[35,36], &[], false, 0));
    a.clips.insert(ClipId::WalkRight, AnimClip::new(AnimMode::LoopEnd, 10.0, &[34,30,28], &[], false, 1));
    a.clips.insert(ClipId::WalkLeft, AnimClip::new(AnimMode::LoopEnd, 10.0, &[33,29,27], &[], false, 1));
    
    resources.add_animation(AnimId::EggCharacter,a.clone());

    let mut egg_hat = a.clone();
    egg_hat.sheet = SpriteSheet::new(7,6,1400,2057);
    resources.add_animation(AnimId::EggBobbleCharacter,egg_hat.clone());
    resources.add_animation(AnimId::EggAcornCharacter,egg_hat.clone());
    resources.add_animation(AnimId::EggPussyCharacter,egg_hat.clone());

    let mut a = Animation::new(SpriteSheet::new(7,6,1400,2057));
    a.clips.insert(ClipId::Idle, AnimClip::new(AnimMode::Loop, 5.0, &[25], &[], false, 0));
    a.clips.insert(ClipId::Dance, AnimClip::new(AnimMode::Loop, 5.0, &[25,0,25,1,25,0,25,1,25,0], &[], false, 0));
    a.clips.insert(ClipId::Eat, AnimClip::new(AnimMode::Loop, 5.0, &[2,3,4,3], &[], false, 0));
    a.clips.insert(ClipId::Ill, AnimClip::new(AnimMode::Loop, 5.0, &[5], &[], false, 0));
    a.clips.insert(ClipId::Death, AnimClip::new(AnimMode::Loop, 5.0, &[7], &[], false, 0));
    a.clips.insert(ClipId::Jump, AnimClip::new(AnimMode::Loop, 5.0, &[8,9,10,9,8,25], &[], false, 0));
    a.clips.insert(ClipId::Kneebend, AnimClip::new(AnimMode::Loop, 5.0, &[25,11,25,11,25], &[], false, 0));
    a.clips.insert(ClipId::Lean, AnimClip::new(AnimMode::Loop, 5.0, &[12,25,13,25], &[], false, 0));
    a.clips.insert(ClipId::Vomit, AnimClip::new(AnimMode::OneShot, 5.0, &[33,14,15,16,17,18,33], &[], false, 0));
    a.clips.insert(ClipId::Sleep, AnimClip::new(AnimMode::LoopEnd, 1.0, &[6,19,19,19], &[], false, 1));
    a.clips.insert(ClipId::Sneeze, AnimClip::new(AnimMode::OneShot, 5.0, &[20,21,22,23,24,5], &[], false, 0));
    a.clips.insert(ClipId::Talk, AnimClip::new(AnimMode::OneShot, 5.0, &[26,27,26,27,26,27,26,27,26,27,26], &[], false, 0));        
    a.clips.insert(ClipId::WalkUp, AnimClip::new(AnimMode::Loop, 10.0, &[36,37], &[], false, 0));
    a.clips.insert(ClipId::WalkDown, AnimClip::new(AnimMode::Loop, 10.0, &[28,29], &[], false, 0));
    a.clips.insert(ClipId::WalkRight, AnimClip::new(AnimMode::LoopEnd, 10.0, &[33,34,35], &[], false, 1)); // add loop range - for all walk chars
    a.clips.insert(ClipId::WalkLeft, AnimClip::new(AnimMode::LoopEnd, 10.0, &[32,31,30], &[], false, 1));
    
    resources.add_animation(AnimId::GhostCharacter,a.clone());

    let mut ghost_hat = a.clone();
    ghost_hat.sheet = SpriteSheet::new(7,6,1400,2057);
    resources.add_animation(AnimId::GhostShroomCharacter,ghost_hat.clone());
    resources.add_animation(AnimId::GhostPartyCharacter,ghost_hat.clone());
}

pub fn nergal_quit_button() -> Box<dyn Entity> {
    Box::new(ButtonEntity::new(
        EntityId::new(),
        ButtonType::SmallTextButton,
        Vec2::new(70., 690.),
        Vec2::new(200., 50.),
        "Quit",
        | button, _resources | -> MessageVec {                    
            vec![
                Message::new(
                    MessageType::ScreenChange(ScreenState::Intro),
                    button.id(),
                    SCREEN_MANAGER_ID
                )
            ]
        }
    ))
}

pub fn nergal_leave_speech_message(resources: &Resources) -> Message {
    Message::new(
        MessageType::ScreenChange(
            if resources.world_state.consent_given {
                match resources.theme {
                    Theme::Daytime => ScreenState::SelectChar,
                    Theme::Nighttime => ScreenState::SelectGhost
                }
            } else {
                ScreenState::Consent
            }
        ),
        NO_ID,
        SCREEN_MANAGER_ID
    )                    
}

pub fn nergal_enter_game(resources: &Resources, button: &dyn Entity, character: AnimId) -> MessageVec {    
    vec![
        Message::new(
            MessageType::InitGame(character, resources.pick_network().into()),
            button.id(),
            WORLD_ID
        ),
        Message::new(
            MessageType::ScreenChange(if resources.screens_visited.contains(&ScreenState::GameInfo(resources.theme)) {
                ScreenState::Game
            } else {
                ScreenState::GameInfo(resources.theme)
            }),
            button.id(),
            SCREEN_MANAGER_ID
        )
    ]
}

pub fn nergal_screens(resources: &Resources, screens: &mut ScreenManager) {

    let centre_x = 1700./2.;

    screens.entity_lookup.insert("daynight".into(), EntityId::new());
    screens.entity_lookup.insert("daynight_text".into(), EntityId::new());

    screens.screens.insert(ScreenState::Intro, Bucket::build(vec![
        
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,160.),
            "nergal",
            180, 35., 200., Alignment::Centre
        )),
        
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,290.),
            "Explore a strange world full of friendly creatures and the occasional ominous disease. Includes a bit of light peril, but you'll be contributing to important health research just by pottering around.",
            NORMAL_TEXT_SIZE, 35., 1500., Alignment::Centre
        )),

        Box::new(TextEntity::new(
            *screens.entity_lookup.get("daynight_text").unwrap(),
            Vec2::new(centre_x, 380.),
            "",
            NORMAL_TEXT_SIZE, 35., 1500., Alignment::Centre
        )),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x-400., 480.),
            Vec2::new(200., 60.),
            "Play!",
            | button, resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::ScreenChange(
                            match resources.theme {                                
                                Theme::Daytime => {
                                    if resources.world_state.consent_given {
                                        ScreenState::SelectChar
                                    } else {
                                        ScreenState::Consent
                                    }
                                },
                                Theme::Nighttime => ScreenState::KeyControls
                            }                            
                        ),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x-95.,480.),
            Vec2::new(200., 60.),
            "Read more",
            | button, _resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::ScreenChange(ScreenState::About),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            }
        )),

        Box::new(ButtonEntity::new(
            *screens.entity_lookup.get("daynight").unwrap(),
            ButtonType::InvertedTextButton,
            Vec2::new(centre_x+300.,480.),
            Vec2::new(200., 60.),
            "Night mode",
            | button, resources | -> MessageVec {
                let theme_msg = Message::new(
                    MessageType::SetTheme(match resources.theme {
                        Theme::Daytime => Theme::Nighttime,
                        Theme::Nighttime => Theme::Daytime
                    }),
                    button.id(),
                    SCREEN_MANAGER_ID
                );
                
                if resources.theme == Theme::Daytime {
                    // send this to trigger the screen reader for the intro screen
                    vec![
                        Message::new(
                            MessageType::ScreenChange(ScreenState::Intro),
                            WORLD_ID,
                            SCREEN_MANAGER_ID
                        ), theme_msg
                    ]
                } else {
                    vec![theme_msg]
                }
            }
        )),
        
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(300., 40.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/moon.png"))
            ]))
        )),        

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(130., 100.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/clouds/cloud-5.png")                
            )
        )),
 
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(1250., 50.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/clouds/cloud-2.png")                
            )
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(1200., 30.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-5.png"))
            ]))
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(50., 20.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-5.png"))
            ]))
        )),       

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(400.,720.),
            AnimId::EggCharacter
        )),
        
        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(600.,720.),
            AnimId::CloudCharacter
        )),
        
        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1300.,720.),
            AnimId::CloudCharacter
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(30., 430.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_strelizia.png")
            )
        )),
        
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(1450., 420.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_calathea.png")
            )
        )),

    ]));

    screens.screens.insert(ScreenState::About, Bucket::build(vec![
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(1100., 50.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/gui/photos.png")
            )
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(1120., 430.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/gui/logos.png")
            )
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(70.,70.),
            "Nergal is a game world that you can explore, and your movements and decisions in the game generate useful data for scientific research. Our real life social networks and the decisions we make can change how diseases spread around our communities. Most research on this is based on computer simulations because it's really hard to track people's interactions in real life. These simulations often assume people make perfect decisions every time, which definitely isn't how the world works, so this game might give us some better data.",
            25, 35., 970., Alignment::Left
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(70.,400.),
            "We've built this game world as a safe way to see what people do when there are diseases around. The results will fill a gap in our understanding of the world, and maybe even help us fight diseases like Covid-19 better in future. The researchers behind Nergal are Dr. Matt Silk and Nitara Wijayatilake (in the photo!) at the University of Edinburgh, the game was made by the non-profit organisation Then Try This, and it was funded by The Royal Society and Impetus For Citizen Science.",
            25, 35., 970., Alignment::Left
        )),

        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x - 500., 650.),
            Vec2::new(200., 60.),
            "Back to start",
            | button, _resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::ScreenChange(ScreenState::Intro),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x - 100., 650.),
            Vec2::new(200., 60.),
            "Credits",
            | button, _resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::ScreenChange(ScreenState::Credits),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            }
        ))
            
    ]));

    screens.screens.insert(ScreenState::Credits, Bucket::build(vec![
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,70.),
            "Nergal has been brought to you by:",
            40, 35., 970., Alignment::Centre
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(centre_x - 80., 80.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/gui/ttt.png")
            )
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 200.),
            "Made by: Dr Amber Griffiths and Dave Griffiths at Then Try This.",
            25, 35., 970., Alignment::Centre
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(centre_x - 80., 200.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/gui/edin.png")
            )
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 330.),
            "Scientifically tweaked and analysed by: Dr Matthew Silk and Nitara Wijayatilake at the Univerisity of Edinburgh",
            25, 35., 970., Alignment::Centre
        )),
        
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(centre_x - 100., 380.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/gui/isight.png")
            )
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 470.),
            "Advised and tested by: Haydn Uren, Stuart Crowle, Beatrix Love, Jahred Love, James Smithson, Bethan Perry, Sophie Butcher, Dom Hall, Lauren Rudkin and Carole Theobald at iSightCornwall",
            25, 35., 970., Alignment::Centre
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 580.),
            "Including the sounds:",
            15, 35., 970., Alignment::Centre
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 600.),
            "Water Wading.wav by Motion_S -- https://freesound.org/s/221764/ -- License: Attribution 4.0",
            15, 35., 970., Alignment::Centre
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 620.),
            "Aspen tree in strong wind.wav by juskiddink -- https://freesound.org/s/78955/ -- License: Attribution 4.0",
            15, 35., 970., Alignment::Centre
        )),  
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x, 650.),
            Vec2::new(200., 60.),
            "Back to start",
            | button, _resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::ScreenChange(ScreenState::Intro),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            }
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(200.,720.),
            AnimId::EggCharacter
        )),
        
        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(300.,420.),
            AnimId::StickCharacter
        )),
        
        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1500.,720.),
            AnimId::CloudCharacter
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1400.,620.),
            AnimId::EggCharacter
        )),
        
    ]));
    
    let text_gap = 75.;
    
    screens.screens.insert(ScreenState::KeyControls, Bucket::build(vec![        
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 100.),
            "KEY CONTROLS FOR THE GAME",
            50, 35., 1500., Alignment::Centre
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(100., 200.),
            "To hear a description of what's on a screen, or around you in the game, press the J key",
            NORMAL_TEXT_SIZE, 35., 1500., Alignment::Left
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(100., 200. + text_gap),
            "To move around, use the cursor keys or W, A, S and D keys.",
            NORMAL_TEXT_SIZE, 35., 1500., Alignment::Left
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(100., 200. + text_gap * 2.),
            "To move through each button in a screen, item in a menu, or character around you - press the F key.",
            NORMAL_TEXT_SIZE, 35., 2500., Alignment::Left
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(100., 200. + text_gap * 3.),
            "To activate a button, menu item, or character press the Spacebar.",
            NORMAL_TEXT_SIZE, 35., 1500., Alignment::Left
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(100., 200. + text_gap * 4.),
            "To quit the game, press Escape.",
            NORMAL_TEXT_SIZE, 35., 1500., Alignment::Left
        )),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x-300., 650.),
            Vec2::new(200., 60.),
            "Next",
            | button, resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::ScreenChange(
                            if resources.world_state.consent_given {
                                match resources.theme {
                                    Theme::Daytime => ScreenState::SelectChar,
                                    Theme::Nighttime => ScreenState::SelectGhost                                
                                }
                            } else {
                                ScreenState::Consent
                            }
                        ),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x+100., 650.),
            Vec2::new(200., 60.),
            "Speech options",
            | button, _resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::ScreenChange(ScreenState::SpeechOptions),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(1350., 0.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-1.png"))
            ]))
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(1550., 80.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-5.png"))
            ]))
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(50., 10.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-2.png"))
            ]))
        )),       

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(255., 15.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-5.png"))
            ]))
        )),       
                       
    ]));
    
    let buttons_y = 200.;
    let button_gap = 80.;
        
    screens.screens.insert(ScreenState::SpeechOptions, Bucket::build(vec![
                               
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 80.),
            "SPEECH SPEED",
            50, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 150.),
            "You can change how fast the speech is in the game.",
            NORMAL_TEXT_SIZE, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),

        Box::new(ButtonEntity::new_with_quiet(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y), Vec2::new(200., 50.),
            "Slow",
            | button, resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::SpeechParam("speed".into(), 1.0), button.id(), WORLD_ID
                    ),
                    nergal_leave_speech_message(resources)
                ]
            },
            | button, _resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::SpeechParam("speed".into(), 1.0), button.id(), WORLD_ID
                    ),
                    Message::new(
                        MessageType::Say(Voice::Narrator, "Slow speech speed.".into()), button.id(), WORLD_ID
                    )
                ]
            },
            true
        )),

        Box::new(ButtonEntity::new_with_quiet(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y + button_gap), Vec2::new(200., 50.),
            "Normal",
            | button, resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::SpeechParam("speed".into(), 1.2), button.id(), WORLD_ID
                    ),
                    nergal_leave_speech_message(resources)
                ]
            },
            | button, _resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::SpeechParam("speed".into(), 1.2), button.id(), WORLD_ID
                    ),
                    Message::new(
                        MessageType::Say(Voice::Narrator, "Normal speech speed.".into()), button.id(), WORLD_ID
                    )
                ]
            },
            true
        )),

        Box::new(ButtonEntity::new_with_quiet(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y + button_gap * 2.), Vec2::new(200., 50.),
            "Fast",
            | button, resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::SpeechParam("speed".into(), 1.3), button.id(), WORLD_ID
                    ),
                    nergal_leave_speech_message(resources)
                ]
            },
            | button, _resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::SpeechParam("speed".into(), 1.3), button.id(), WORLD_ID
                    ),
                    Message::new(
                        MessageType::Say(Voice::Narrator, "Fast speech speed.".into()), button.id(), WORLD_ID
                    )
                ]
            },
            true
        )),

        Box::new(ButtonEntity::new_with_quiet(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y + button_gap * 3.), Vec2::new(200., 50.),
            "Very fast",
            | button, resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::SpeechParam("speed".into(), 1.4), button.id(), WORLD_ID
                    ),
                    nergal_leave_speech_message(resources)
                ]
            },
            | button, _resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::SpeechParam("speed".into(), 1.4), button.id(), WORLD_ID
                    ),
                    Message::new(
                        MessageType::Say(Voice::Narrator, "Very fast speech speed.".into()), button.id(), WORLD_ID
                    ),
                ]
            },
            true
        )),

        Box::new(ButtonEntity::new_with_quiet(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y + button_gap * 4.), Vec2::new(200., 50.),
            "Ultra fast",
            | button, resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::SpeechParam("speed".into(), 1.5), button.id(), WORLD_ID
                    ),
                    nergal_leave_speech_message(resources)
                ]
            },
            | button, _resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::SpeechParam("speed".into(), 1.5), button.id(), WORLD_ID
                    ),
                    Message::new(
                        MessageType::Say(Voice::Narrator, "Ultra fast speech speed.".into()), button.id(), WORLD_ID
                    ),
                ]
            },
            true
        )),

        
        

        
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(1350., 0.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-1.png"))
            ]))
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(1550., 80.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-5.png"))
            ]))
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(50., 10.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-2.png"))
            ]))
        )),       

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(255., 15.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-5.png"))
            ]))
        )),       

    ]));
    
    screens.screens.insert(ScreenState::Consent, Bucket::build(vec![
        
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,180.),
            "While you play, your decisions and movements in the game will be stored as data for scientific research. This is completely anonymous and we have no way of knowing who you are.",
            NORMAL_TEXT_SIZE, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),


        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,320.),
            "Are you over 18 and happy to go ahead? If you are under 18 you can still play, but your data won't be stored.",
            NORMAL_TEXT_SIZE, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::SmallTextButton,
            Vec2::new(centre_x-150., 400.),
            Vec2::new(200., 50.),
            "Yes",
            | button, _resources | -> MessageVec {
                vec![
                    Message::new(
                        // also picked up by outer loop to record consent
                        MessageType::PlayerInfo("consent_yes".into()),
                        button.id(),
                        WORLD_ID
                    ),
                    Message::new(
                        MessageType::ScreenChange(ScreenState::PlayedBefore),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::SmallTextButton,
            Vec2::new(centre_x+150., 400.),
            Vec2::new(200., 50.),
            "No",
            | button, _resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::PlayerInfo("consent_no".into()),
                        button.id(),
                        WORLD_ID
                    ),
                    Message::new(
                        MessageType::ScreenChange(ScreenState::Intro),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),
        
        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1430., 650.),
            AnimId::StickCharacter
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1230., 630.),
            AnimId::StickCharacter
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1330., 655.),
            AnimId::StickCharacter
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(30., 250.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_baobab.png")
            )
        )),
        
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(270., 490.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_bulbinetortab.png")
            )
        )),
        
    ]));
    
    screens.screens.insert(ScreenState::PlayedBefore, Bucket::build(vec![
        
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,250.),
            "Have you played before?",
            NORMAL_TEXT_SIZE, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::SmallTextButton,
            Vec2::new(centre_x-150., 350.),
            Vec2::new(200., 50.),
            "Yes",
            | button, resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::PlayerInfo("played_before_yes".into()),
                        button.id(),
                        WORLD_ID
                    ),
                    Message::new(
                        MessageType::ScreenChange(match resources.theme {
                            Theme::Daytime => ScreenState::SelectChar,
                            Theme::Nighttime => ScreenState::SelectGhost
                        }),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::SmallTextButton,
            Vec2::new(centre_x+150., 350.),
            Vec2::new(200., 50.),
            "No",
            | button, resources | -> MessageVec {
                console::log_1(&format!("{:?}", resources.theme).into());
                
                vec![
                    Message::new(
                        MessageType::PlayerInfo("played_before_no".into()),
                        button.id(),
                        WORLD_ID
                    ),
                    Message::new(
                        MessageType::ScreenChange(match resources.theme {
                            Theme::Daytime => ScreenState::SelectChar,
                            Theme::Nighttime => ScreenState::SelectGhost
                        }),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),
        
        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1430., 650.),
            AnimId::StickCharacter
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1230., 630.),
            AnimId::StickCharacter
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1330., 655.),
            AnimId::StickCharacter
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(30., 250.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_baobab.png")
            )
        )),
        
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(270., 490.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_bulbinetortab.png")
            )
        )),            
        
    ]));
    
    screens.screens.insert(ScreenState::SelectChar, Bucket::build(vec![
        
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,250.),
            "Which Nergal would you like to be today?",
            NORMAL_TEXT_SIZE, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),
        

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::ImageButton,
            Vec2::new(centre_x-350., CIRCLE_BUTTON_Y),
            Vec2::new(206.,211.),
            "/images/gui/stick-button.png",
            | button, _resources | -> MessageVec {                    
                vec![
                    Message::new(
                        MessageType::ScreenChange(ScreenState::SelectHat(0)),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::ImageButton,
            Vec2::new(centre_x-100., CIRCLE_BUTTON_Y),
            Vec2::new(206.,211.),
            "/images/gui/cloud-button.png",
            | button, _resources | -> MessageVec {                    
                vec![
                    Message::new(
                        MessageType::ScreenChange(ScreenState::SelectHat(1)),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::ImageButton,
            Vec2::new(centre_x+150., CIRCLE_BUTTON_Y),
            Vec2::new(206.,211.),
            "/images/gui/egg-button.png",
            | button, _resources | -> MessageVec {                    
                vec![
                    Message::new(
                        MessageType::ScreenChange(ScreenState::SelectHat(2)),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),

        nergal_quit_button()
            
    ]));

    screens.screens.insert(ScreenState::SelectHat(0), Bucket::build(vec![

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,250.),
            "You can now choose your hat - this is how you will tell yourself apart from all the other Nergals.",
            NORMAL_TEXT_SIZE, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::ImageButton,
            Vec2::new(centre_x-350., CIRCLE_BUTTON_Y),
            Vec2::new(206.,211.),
            "/images/gui/stick-stetson.png",
            | button, resources | -> MessageVec {
                nergal_enter_game(resources, button, AnimId::StickStetsonCharacter)
            })
        ),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::ImageButton,
            Vec2::new(centre_x-100., CIRCLE_BUTTON_Y),
            Vec2::new(206.,211.),
            "/images/gui/stick-party.png",
            | button, resources | -> MessageVec {                    
                nergal_enter_game(resources, button, AnimId::StickPartyCharacter)
            })
        ),

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::ImageButton,
            Vec2::new(centre_x+150., CIRCLE_BUTTON_Y),
            Vec2::new(206.,211.),
            "/images/gui/stick-acorn.png",
            | button, resources | -> MessageVec {                    
                nergal_enter_game(resources, button, AnimId::StickAcornCharacter)
            })
        ),

        nergal_quit_button()
 
    ]));
    
    screens.screens.insert(ScreenState::SelectHat(1), Bucket::build(vec![
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,250.),
            "You can now choose your hat - this is how you will tell yourself apart from all the other Nergals.",
            NORMAL_TEXT_SIZE, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),
        

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::ImageButton,
            Vec2::new(centre_x-350., CIRCLE_BUTTON_Y),
            Vec2::new(206.,211.),
            "/images/gui/cloud-pussy.png",
            | button, resources | -> MessageVec {                    
                nergal_enter_game(resources, button, AnimId::CloudPussyCharacter)
            })
        ),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::ImageButton,
            Vec2::new(centre_x-100., CIRCLE_BUTTON_Y),
            Vec2::new(206.,211.),
            "/images/gui/cloud-stetson.png",
            | button, resources | -> MessageVec {                    
                nergal_enter_game(resources, button, AnimId::CloudStetsonCharacter)
            })
        ),

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::ImageButton,
            Vec2::new(centre_x+150., CIRCLE_BUTTON_Y),
            Vec2::new(206.,211.),
            "/images/gui/cloud-bobble.png",
            | button, resources | -> MessageVec {                    
                nergal_enter_game(resources, button, AnimId::CloudBobbleCharacter)
            })
        ),

        nergal_quit_button()
 
    ]));

    screens.screens.insert(ScreenState::SelectHat(2), Bucket::build(vec![
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,250.),
            "You can now choose your hat - this is how you will tell yourself apart from all the other Nergals.",
            NORMAL_TEXT_SIZE, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::ImageButton,
            Vec2::new(centre_x-350., CIRCLE_BUTTON_Y),
            Vec2::new(206.,211.),
            "/images/gui/egg-bobble.png",
            | button, resources | -> MessageVec {                    
                nergal_enter_game(resources, button, AnimId::EggBobbleCharacter)
            })
        ),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::ImageButton,
            Vec2::new(centre_x-100., CIRCLE_BUTTON_Y),
            Vec2::new(206.,211.),
            "/images/gui/egg-acorn.png",
            | button, resources | -> MessageVec {                    
                nergal_enter_game(resources, button, AnimId::EggAcornCharacter)
            })
        ),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::ImageButton,
            Vec2::new(centre_x+150., CIRCLE_BUTTON_Y),
            Vec2::new(206.,211.),
            "/images/gui/egg-pussy.png",
            | button, resources | -> MessageVec {                    
                nergal_enter_game(resources, button, AnimId::EggPussyCharacter)
            })
        ),

        nergal_quit_button()
 
    ]));
    
    screens.screens.insert(ScreenState::SelectGhost, Bucket::build(vec![
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,150.),
            "Congratulations, you are a night Nergal!",
            NORMAL_TEXT_SIZE, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,200.),
            "You can now choose your hat - this is how you will tell yourself apart from all the other Nergals.",
            NORMAL_TEXT_SIZE, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),
        
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,275.),
            "If you can't see the hats, don't worry, they each make your voice different so pick the voice you prefer.",
            NORMAL_TEXT_SIZE, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),

        Box::new(ButtonEntity::new_with_quiet(
            EntityId::new(),
            ButtonType::ImageButton,
            Vec2::new(centre_x-300., CIRCLE_BUTTON_Y),
            Vec2::new(206.,211.),
            "/images/gui/ghost-shroom.png",
            | button, resources | -> MessageVec {
                vec![
                    Message::new(
                        MessageType::InitGame(AnimId::GhostShroomCharacter, resources.pick_network().into()),
                        button.id(),
                        WORLD_ID
                    ),
                    Message::new(
                        MessageType::ScreenChange(if resources.screens_visited.contains(&ScreenState::GameInfo(resources.theme)) {
                            ScreenState::Game
                        } else {
                            ScreenState::GameInfo(resources.theme)
                        }),
                        button.id(),
                        SCREEN_MANAGER_ID
                    ),
                    Message::new(
                        MessageType::SpeechParam("player_pitch".into(), 0.6), button.id(), WORLD_ID
                    )
                ]
            },
            | _button, resources: &Resources | -> MessageVec {
                resources.speech.say_pitch(0.6, "Red mushroom hat with a low voice.");
                vec![]
            },
            true
        )),
        
        Box::new(ButtonEntity::new_with_quiet(
            EntityId::new(),
            ButtonType::ImageButton,
            Vec2::new(centre_x+100., CIRCLE_BUTTON_Y),
            Vec2::new(206.,211.),
            "/images/gui/ghost-party.png",
            | button, resources | -> MessageVec {                    
                vec![
                    Message::new(
                        MessageType::InitGame(AnimId::GhostPartyCharacter, resources.pick_network().into()),
                        button.id(),
                        WORLD_ID
                    ),
                    Message::new(                        
                        MessageType::ScreenChange(if resources.screens_visited.contains(&ScreenState::GameInfo(resources.theme)) {
                            ScreenState::Game
                        } else {
                            ScreenState::GameInfo(resources.theme)
                        }),
                        button.id(),
                        SCREEN_MANAGER_ID
                    ),
                    Message::new(
                        MessageType::SpeechParam("player_pitch".into(), 1.3), button.id(), WORLD_ID
                    )
                ]
            },
            | _button, resources: &Resources | -> MessageVec {
                resources.speech.say_pitch(1.5, "Party hat with a high voice.");
                vec![]
            },
            true
        )),

        nergal_quit_button()

    ]));


    screens.screens.insert(ScreenState::GameInfo(Theme::Daytime), Bucket::build(vec![

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 100.),
            "HOW TO PLAY",
            50, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),
            
        Box::new(TextEntity::new(
            EntityId::new(), 
            Vec2::new(500., 180.),
            "What you do is up to you, there's no right or wrong, the only aim is to survive the game.",
            30, 35., 700., Alignment::Left
        )),
        
        Box::new(TextEntity::new(
            EntityId::new(), 
            Vec2::new(500., 280.),
            "Click where you want to go and on things you want to explore.",
            30, 35., 700., Alignment::Left
        )),
        
        Box::new(TextEntity::new(
            EntityId::new(), 
            Vec2::new(500., 380.),
            "Click on other nergals to talk to them or unfriend them if you don't want them to follow you.",
            30, 35., 700., Alignment::Left
        )),
        
        Box::new(TextEntity::new(
            EntityId::new(), 
            Vec2::new(500., 510.),
            "Talk to friends to get snacks to increase your health, talk to strangers to make new friends.",
            30, 35., 700., Alignment::Left
        )),
        

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x,600.),
            Vec2::new(200.,60.),
            "Next",
            | button, _resources | -> MessageVec {
                // do something
                vec![
                    Message::new(
                        MessageType::ScreenChange(ScreenState::Game),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(100., 430.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_dollseye.png")
            )
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(1330., 250.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_apple.png")
            )
        )),
    ]));

    screens.screens.insert(ScreenState::GameInfo(Theme::Nighttime), Bucket::build(vec![

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 100.),
            "HOW TO PLAY",
            50, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),
            
        Box::new(TextEntity::new(
            EntityId::new(), 
            Vec2::new(500., 180.),
            "What you do is up to you, there's no right or wrong, the only aim is to survive the game.",
            30, 35., 700., Alignment::Left
        )),
        
        Box::new(TextEntity::new(
            EntityId::new(), 
            Vec2::new(500., 280.),
            "Move where you want to go and explore the world.",
            30, 35., 700., Alignment::Left
        )),
        
        Box::new(TextEntity::new(
            EntityId::new(), 
            Vec2::new(500., 380.),
            "Select other nergals to talk to them or unfriend them if you don't want them to follow you.",
            30, 35., 700., Alignment::Left
        )),
        
        Box::new(TextEntity::new(
            EntityId::new(), 
            Vec2::new(500., 510.),
            "Talk to friends to get snacks to increase your health, talk to strangers to make new friends.",
            30, 35., 700., Alignment::Left
        )),
        

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x,600.),
            Vec2::new(200.,60.),
            "Next",
            | button, _resources | -> MessageVec {
                // do something
                vec![
                    Message::new(
                        MessageType::ScreenChange(ScreenState::Game),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(100., 430.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_dollseye.png")
            )
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(1330., 250.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_apple.png")
            )
        )),
    ]));

    screens.entity_lookup.insert("prompts".into(), EntityId::new());

    screens.screens.insert(ScreenState::Game, Bucket::build(vec![
        Box::new(HealthBarEntity::new(
            EntityId::new(),
            Vec2::new(100.,10.)                                
        )),

        Box::new(ClockEntity::new(
            EntityId::new(),
            Vec2::new(1600., 60.),
            GAME_TIME_LENGTH
        )),

        Box::new(PromptEntity::new(
            *screens.entity_lookup.get("prompts").unwrap(),
            Vec2::new(500., 50.),
            Rc::new(vec![])
        )),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::SmallTextButton,
            Vec2::new(70., 690.),            
            Vec2::new(200., 50.),
            "Quit",
            | button, _resources | -> MessageVec {                    
                vec![
                    Message::new(
                        MessageType::GameOver(GameOverReason::QuitButton),
                        button.id(),
                        WORLD_ID
                    ),
                    Message::new(
                        MessageType::ScreenChange(ScreenState::GameOver(GameOverReason::QuitButton)),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            }
        ))
    ]));

    screens.screens.insert(ScreenState::GameOver(GameOverReason::QuitButton), Bucket::build(vec![
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 200.),
            "GAME OVER",
            50, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),

        Box::new(TextEntity::new(
            EntityId::new(), 
            Vec2::new(centre_x, 300.),
            "You exited the game early!",
            30, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x,400.),
            Vec2::new(200.,60.),
            "Next",
            | button, resources | -> MessageVec {
                // do something
                vec![
                    Message::new(
                        MessageType::ScreenChange(if resources.screens_visited.contains(&ScreenState::Demographics(resources.theme)) {
                            ScreenState::Results                                
                        } else {
                            ScreenState::Demographics(resources.theme)
                        }),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1300.,520.),
            AnimId::StickCharacter
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(200., 330.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_begonia.png")
            )
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(330., 300.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_bulbinetortab.png")
            )
        )),
    ]));

    screens.screens.insert(ScreenState::GameOver(GameOverReason::OutOfTime), Bucket::build(vec![
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 200.),
            "YOU SURVIVED",
            50, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),

        Box::new(TextEntity::new(
            EntityId::new(), 
            Vec2::new(centre_x, 300.),
            "Well done, you survived until the end of the game!",
            30, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x,400.),
            Vec2::new(200.,60.),
            "Next",
            | button, resources | -> MessageVec {
                // do something
                vec![
                    Message::new(
                        MessageType::ScreenChange(if resources.screens_visited.contains(&ScreenState::Demographics(resources.theme)) {
                            ScreenState::Results
                        } else {
                            ScreenState::Demographics(resources.theme)
                        }),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1200.,620.),
            AnimId::EggCharacter
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1300.,680.),
            AnimId::StickCharacter
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(400., 350.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Daytime, resources.get_handle_from_path("/images/sprites/rainbows/rainbow2trans.png")),
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-1.png"))
            ]))
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(80., 500.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Daytime, resources.get_handle_from_path("/images/sprites/rainbows/rainbow3trans.png")),
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-2.png"))
            ]))
        )),
        
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(20., 10.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/moon.png"))
            ]))
        )),

    ]));

    screens.entity_lookup.insert("diagnostic_text".into(), EntityId::new());

    screens.screens.insert(ScreenState::GameOver(GameOverReason::NoHealth), Bucket::build(vec![
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 200.),
            "GAME OVER",
            50, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),

        Box::new(TextEntity::new(
            *screens.entity_lookup.get("diagnostic_text").unwrap(),
            Vec2::new(centre_x, 300.),
            "",
            30, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x, 400.),
            Vec2::new(200.,60.),
            "Next",
            | button, resources | -> MessageVec {
                // do something
                vec![
                    Message::new(
                        MessageType::ScreenChange(if resources.screens_visited.contains(&ScreenState::Demographics(resources.theme)) {
                            ScreenState::Results
                        } else {
                            ScreenState::Demographics(resources.theme)
                        }),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1300.,620.),
            AnimId::StickCharacter
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(200., 430.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_begonia.png")
            )
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(330., 400.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_bulbinetortab.png")
            )
        )),
 
    ]));
    
    screens.screens.insert(ScreenState::Demographics(Theme::Daytime), Bucket::build(vec![

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 200.),
            "It's helpful to know some things about you. This means we can understand more about the scientific data collected in the game. These three questions are optional and anonymous.",
            30, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x,350.),
            Vec2::new(200.,60.),
            "Next",
            | button, _resources | -> MessageVec {
                // do something
                vec![
                    Message::new(
                        MessageType::ScreenChange(ScreenState::DemographicsAge),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1200.,620.),
            AnimId::EggCharacter
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1300.,680.),
            AnimId::StickCharacter
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(400., 350.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Daytime, resources.get_handle_from_path("/images/sprites/rainbows/rainbow2trans.png")),
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-1.png"))
            ]))
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(80., 500.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Daytime, resources.get_handle_from_path("/images/sprites/rainbows/rainbow3trans.png")),
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-2.png"))
            ]))
        )),
        
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(20., 10.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/moon.png"))
            ]))
        )),

    ]));

    screens.screens.insert(ScreenState::Demographics(Theme::Nighttime), Bucket::build(vec![

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 200.),
            "It's helpful to know some things about you. This means we can understand more about the scientific data collected in the game. These four questions are optional and anonymous.",
            30, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x,350.),
            Vec2::new(200.,60.),
            "Next",
            | button, _resources | -> MessageVec {
                // do something
                vec![
                    Message::new(
                        MessageType::ScreenChange(ScreenState::DemographicsAge),
                        button.id(),
                        SCREEN_MANAGER_ID
                    )
                ]
            })
        ),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1200.,620.),
            AnimId::EggCharacter
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1300.,680.),
            AnimId::StickCharacter
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(400., 350.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Daytime, resources.get_handle_from_path("/images/sprites/rainbows/rainbow2trans.png")),
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-1.png"))
            ]))
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(80., 500.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Daytime, resources.get_handle_from_path("/images/sprites/rainbows/rainbow3trans.png")),
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-2.png"))
            ]))
        )),
        
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(20., 10.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/moon.png"))
            ]))
        )),

    ]));

    screens.screens.insert(ScreenState::DemographicsAge, Bucket::build(vec![
                               
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 100.),
            "How old are you?",
            30, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y - 50.), Vec2::new(200., 50.),
            "Under 18",
            | button, _resources | -> MessageVec {
                vec![
                    // server will delete any prior data when getting this message
                    Message::new(MessageType::PlayerInfo("age_under_18".into()), button.id(), WORLD_ID),
                    // deactivate recording and skip to the end
                    Message::new(MessageType::DeactivateRecording, button.id(), WORLD_ID),
                    Message::new(MessageType::ScreenChange(ScreenState::Results), button.id(), SCREEN_MANAGER_ID)
                ]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y - 50. + button_gap), Vec2::new(200., 50.),
            "18 to 24",
            | button, _resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("age_18_24".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(ScreenState::DemographicsGender), button.id(), SCREEN_MANAGER_ID)]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y - 50. + button_gap * 2.), Vec2::new(200., 50.),
            "25 to 49",
            | button, _resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("age_25_49".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(ScreenState::DemographicsGender), button.id(), SCREEN_MANAGER_ID)]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y - 50. + button_gap * 3.), Vec2::new(200., 50.),
            "50 to 64",
            | button, _resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("age_50_64".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(ScreenState::DemographicsGender), button.id(), SCREEN_MANAGER_ID)]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y - 50. + button_gap * 4.), Vec2::new(200., 50.),
            "65 to 79",
            | button, _resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("age_65_79".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(ScreenState::DemographicsGender), button.id(), SCREEN_MANAGER_ID)]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y - 50. + button_gap * 5.), Vec2::new(200., 50.),
            "80 or over",
            | button, _resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("age_80_over".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(ScreenState::DemographicsGender), button.id(), SCREEN_MANAGER_ID)]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y - 50. + button_gap * 6.), Vec2::new(200., 50.),
            "Prefer not to say",
            | button, _resources | -> MessageVec {
                vec![
                    Message::new(MessageType::PlayerInfo("age_ns".into()), button.id(), WORLD_ID),
                    Message::new(MessageType::ScreenChange(ScreenState::DemographicsGender), button.id(), SCREEN_MANAGER_ID)
                ]
            }
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1200.,620.),
            AnimId::EggCharacter
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1300.,680.),
            AnimId::StickCharacter
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(400., 350.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Daytime, resources.get_handle_from_path("/images/sprites/rainbows/rainbow2trans.png")),
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-1.png"))
            ]))
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(80., 500.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Daytime, resources.get_handle_from_path("/images/sprites/rainbows/rainbow3trans.png")),
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-2.png"))
            ]))
        )),
        
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(20., 10.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/moon.png"))
            ]))
        )),

    ]));
    
    let button_gap = 100.;
    
    screens.screens.insert(ScreenState::DemographicsGender, Bucket::build(vec![

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 100.),
            "What's your gender?",
            30, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y), Vec2::new(200., 50.),
            "Woman/girl (including trans)",
            | button, _resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("gender_wt".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(ScreenState::DemographicsPandemic), button.id(), SCREEN_MANAGER_ID)]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y + button_gap), Vec2::new(200., 50.),
            "Man/boy (including trans)",
            | button, _resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("gender_mt".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(ScreenState::DemographicsPandemic), button.id(), SCREEN_MANAGER_ID)]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y + button_gap * 2.), Vec2::new(200., 50.),
            "Non-binary",
            | button, _resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("gender_nb".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(ScreenState::DemographicsPandemic), button.id(), SCREEN_MANAGER_ID)]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y + button_gap * 3.), Vec2::new(200., 50.),
            "Another gender not listed",
            | button, _resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("gender_ag".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(ScreenState::DemographicsPandemic), button.id(), SCREEN_MANAGER_ID)]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y + button_gap * 4.), Vec2::new(200., 50.),
            "Prefer not to say",
            | button, _resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("gender_nr".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(ScreenState::DemographicsPandemic), button.id(), SCREEN_MANAGER_ID)]
            }
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1200.,620.),
            AnimId::EggCharacter
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1300.,680.),
            AnimId::StickCharacter
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(400., 350.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Daytime, resources.get_handle_from_path("/images/sprites/rainbows/rainbow2trans.png")),
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-1.png"))
            ]))
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(80., 500.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Daytime, resources.get_handle_from_path("/images/sprites/rainbows/rainbow3trans.png")),
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-2.png"))
            ]))
        )),
        
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(20., 10.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/moon.png"))
            ]))
        )),

    ]));

    screens.screens.insert(ScreenState::DemographicsPandemic, Bucket::build(vec![

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 100.),
            "During the Covid-19 pandemic, were you clinically vulnerable or caring for someone who was?",
            30, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),
        
        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y), Vec2::new(200., 50.),
            "I was/am clinically vulnerable",
            | button, resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("pandemic_clinically_vulnerable".into()), button.id(), WORLD_ID),                     
                     Message::new(MessageType::ScreenChange(
                         match resources.theme {
                             Theme::Daytime => ScreenState::Results,
                             Theme::Nighttime => ScreenState::SightInfo
                         }), button.id(), SCREEN_MANAGER_ID
                     ),
                ]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y + button_gap), Vec2::new(200., 50.),
            "I cared for someone vulnerable",
            | button, resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("pandemic_carer".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(
                         match resources.theme {
                             Theme::Daytime => ScreenState::Results,
                             Theme::Nighttime => ScreenState::SightInfo
                         }), button.id(), SCREEN_MANAGER_ID
                     ),
                ]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y + button_gap * 2.), Vec2::new(200., 50.),
            "Neither of these",
            | button, resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("pandemic_none".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(
                         match resources.theme {
                             Theme::Daytime => ScreenState::Results,
                             Theme::Nighttime => ScreenState::SightInfo
                         }), button.id(), SCREEN_MANAGER_ID
                     ),
                ]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y + button_gap * 3.), Vec2::new(200., 50.),
            "Prefer not to say",
            | button, resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("pandemic_nr".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(
                         match resources.theme {
                             Theme::Daytime => ScreenState::Results,
                             Theme::Nighttime => ScreenState::SightInfo
                         }), button.id(), SCREEN_MANAGER_ID
                     ),
                ]
            }
        )),
        
        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1200.,620.),
            AnimId::EggCharacter
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1300.,680.),
            AnimId::StickCharacter
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(400., 350.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Daytime, resources.get_handle_from_path("/images/sprites/rainbows/rainbow2trans.png")),
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-1.png"))
            ]))
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(80., 500.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Daytime, resources.get_handle_from_path("/images/sprites/rainbows/rainbow3trans.png")),
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-2.png"))
            ]))
        )),
        
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(20., 10.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/moon.png"))
            ]))
        )),        

    ]));

    screens.screens.insert(ScreenState::SightInfo, Bucket::build(vec![

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 100.),
            "Can you tell us why you chose night mode? This helps us understand the results better.",
            30, 35., TEXT_AREA_WIDTH, Alignment::Centre
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y), Vec2::new(200., 50.),
            "I have no sight at all",
            | button, _resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("sight_ns".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(ScreenState::Results), button.id(), SCREEN_MANAGER_ID)]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y + button_gap), Vec2::new(200., 50.),
            "I have limited sight",
            | button, _resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("sight_l".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(ScreenState::Results), button.id(), SCREEN_MANAGER_ID)]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y + button_gap * 2.), Vec2::new(200., 50.),
            "Any other reason",
            | button, _resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("sight_ao".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(ScreenState::Results), button.id(), SCREEN_MANAGER_ID)]
            }
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(), ButtonType::SmallTextButton, Vec2::new(centre_x, buttons_y + button_gap * 3.), Vec2::new(200., 50.),
            "Prefer not to say",
            | button, _resources | -> MessageVec {
                vec![Message::new(MessageType::PlayerInfo("sight_nr".into()), button.id(), WORLD_ID),
                     Message::new(MessageType::ScreenChange(ScreenState::Results), button.id(), SCREEN_MANAGER_ID)]
            }
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1200.,620.),
            AnimId::EggCharacter
        )),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1300.,680.),
            AnimId::StickCharacter
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(400., 350.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Daytime, resources.get_handle_from_path("/images/sprites/rainbows/rainbow2trans.png")),
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-1.png"))
            ]))
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(80., 500.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Daytime, resources.get_handle_from_path("/images/sprites/rainbows/rainbow3trans.png")),
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/star-2.png"))
            ]))
        )),
        
        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(20., 10.),
            GfxEntityData::ThemedImage(Box::new([
                (Theme::Nighttime, resources.get_handle_from_path("/images/sprites/astronomical/moon.png"))
            ]))
        )),

    ]));
    
    screens.entity_lookup.insert("results_bg".into(), EntityId::new());
    screens.entity_lookup.insert("sociability_biscuit".into(), EntityId::new());
    screens.entity_lookup.insert("sociability_text_1".into(), EntityId::new());
    screens.entity_lookup.insert("sociability_text_2".into(), EntityId::new());
    screens.entity_lookup.insert("health_biscuit".into(), EntityId::new());
    screens.entity_lookup.insert("health_text_1".into(), EntityId::new());
    screens.entity_lookup.insert("health_text_2".into(), EntityId::new());
    screens.entity_lookup.insert("other_biscuit".into(), EntityId::new());
    screens.entity_lookup.insert("other_text".into(), EntityId::new());
    screens.entity_lookup.insert("results_nergal".into(), EntityId::new());

    let biscuit_x = 280.;
    let sociability_y = 120.;
    let health_y = sociability_y + 140.;
    let other_y = health_y + 140.;
    
    screens.screens.insert(ScreenState::Results, Bucket::build(vec![
     
        Box::new(GfxEntity::new(
            *screens.entity_lookup.get("results_bg").unwrap(),
            Vec2::new(centre_x - 600., 30.),
            GfxEntityData::DomedRect(
                Vec2::new(1200., 500.),
                "results_background".into()
            )
        )),

        Box::new(TextEntity::coloured(
            EntityId::new(), Vec2::new(centre_x, 100.),
            "YOUR TIME AS A NERGAL",
            50, 35., TEXT_AREA_WIDTH, Alignment::Centre, "results_text"
        )),

        Box::new(GfxEntity::new(
            *screens.entity_lookup.get("sociability_biscuit").unwrap(),
            Vec2::new(biscuit_x, sociability_y),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/gui/biscuits/cloud_social.png")
            )
        )),

        Box::new(HighlightTextEntity::coloured(
            *screens.entity_lookup.get("sociability_text_1").unwrap(),
            Vec2::new(biscuit_x + 200. , sociability_y + 40.),
            30, 35., 500., Alignment::Left, "results_text"
        )),

        Box::new(TextEntity::coloured(
            *screens.entity_lookup.get("sociability_text_2").unwrap(),
            Vec2::new(biscuit_x + 200. ,sociability_y + 80.),
            "x",
            30, 35., 500., Alignment::Left, "results_text"
        )),
        
        Box::new(GfxEntity::new(
            *screens.entity_lookup.get("health_biscuit").unwrap(),
            Vec2::new(biscuit_x, health_y),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/gui/biscuits/cloud_sick.png")
            )
        )),

        Box::new(HighlightTextEntity::coloured(
            *screens.entity_lookup.get("health_text_1").unwrap(),
            Vec2::new(biscuit_x + 200., health_y + 40.),
            30, 35., 500., Alignment::Left, "results_text"
        )),

        Box::new(TextEntity::coloured(
            *screens.entity_lookup.get("health_text_2").unwrap(),
            Vec2::new(biscuit_x + 200., health_y + 80.),
            "x",
            30, 35., 500., Alignment::Left, "results_text"
        )),

        Box::new(GfxEntity::new(
            *screens.entity_lookup.get("other_biscuit").unwrap(),
            Vec2::new(biscuit_x, other_y),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/gui/biscuits/cloud_snacks.png")
            )
        )),

        Box::new(HighlightTextEntity::coloured(
            *screens.entity_lookup.get("other_text").unwrap(),
            Vec2::new(biscuit_x + 200. , other_y + 60.),
            30, 35., 500., Alignment::Left, "results_text"
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,580.),
            "Playing the game generated important data for research on how diseases spread, thank you!",
            30, 35., 1500., Alignment::Centre
        )),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x - 400.,640.),
            Vec2::new(200., 60.),
            "More about your data",
            | button, _resources | -> MessageVec {                    
                vec![Message::new(
                    MessageType::ScreenChange(ScreenState::AboutData),
                    button.id(),
                    SCREEN_MANAGER_ID
                )]
            })
        ),

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x + 220. ,640.),
            Vec2::new(200., 60.),
            "Play again",
            | button, resources | -> MessageVec {                    
                vec![Message::new(
                    MessageType::ScreenChange(match resources.theme {
                        Theme::Daytime => ScreenState::SelectChar,
                        Theme::Nighttime => ScreenState::SelectGhost
                    }),
                    button.id(),
                    SCREEN_MANAGER_ID
                )]
            })
        ),

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x + 600., 640.),
            Vec2::new(200., 60.),
            "Quit",
            | button, _resources | -> MessageVec {                    
                vec![Message::new(
                    MessageType::ScreenChange(ScreenState::Intro),
                    button.id(),
                    SCREEN_MANAGER_ID
                )]
            })
        )
    ]));
    

    screens.entity_lookup.insert("network_type".into(), EntityId::new());

    screens.screens.insert(ScreenState::AboutData, Bucket::build(vec![

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x,70.),
            "MORE ABOUT YOUR DATA",
            50, 35., 1000., Alignment::Centre
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(200.,115.),
            "The number of nergals you started the game with, and how they were connected, were based on real data from different animal species.",
            30, 35., 1300., Alignment::Left
        )),

        Box::new(HighlightTextEntity::new(
            *screens.entity_lookup.get("network_type").unwrap(),
            Vec2::new(200.,220.),
            30, 35., 1300., Alignment::Left
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(200., 280.),
            "During the game, you could make and break friendships. People often change their social networks when there is a disease around, and this changes how diseases spread. We've tracked your social decisions and the spread of disease during your game. When enough people have played, we'll analyse all the data together to try and understand these dynamics better.",
            30, 35., 1300., Alignment::Left
        )),

        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(200., 480.),
            "If you'd like to be sent a copy of the results when it's ready (it might be a couple of years!), you can add your email address using the button below.",
            30, 35., 1300., Alignment::Left
        )),

       
        Box::new(TextEntity::new(
            EntityId::new(), Vec2::new(centre_x, 580.),
            "Thank you for contributing!",
            30, 35., 700., Alignment::Centre
        )),

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x - 520. ,640.),
            Vec2::new(200., 60.),
            "My time as a Nergal",
            | button, _resources | -> MessageVec {                    
                vec![Message::new(
                    MessageType::ScreenChange(ScreenState::Results),
                    button.id(),
                    SCREEN_MANAGER_ID
                )]
            })
        ),

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x - 80. ,640.),
            Vec2::new(200., 60.),
            "Play again",
            | button, resources | -> MessageVec {                    
                vec![Message::new(
                    MessageType::ScreenChange(match resources.theme {
                        Theme::Daytime => ScreenState::SelectChar,
                        Theme::Nighttime => ScreenState::SelectGhost
                    }),
                    button.id(),
                    SCREEN_MANAGER_ID
                )]
            })
        ),
        
        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x + 340., 640.),
            Vec2::new(200., 60.),
            "Signup for results",
            | _button, _resources | -> MessageVec {                    
                let _ = web_sys::window().unwrap().open_with_url_and_target(
                    "https://forms.gle/PBoU3dtwEQ2vM3zK6", "_blank"
                );
                vec![]
            })
        ),        

        Box::new(ButtonEntity::new(
            EntityId::new(),
            ButtonType::TextButton,
            Vec2::new(centre_x + 690., 640.),
            Vec2::new(200., 60.),
            "Quit",
            | button, _resources | -> MessageVec {                    
                vec![Message::new(
                    MessageType::ScreenChange(ScreenState::Intro),
                    button.id(),
                    SCREEN_MANAGER_ID
                )]
            })
        ),

        Box::new(SocialEntity::at_pos(
            EntityId::new(),
            Vec2::new(1600.,520.),
            AnimId::StickCharacter
        )),

        Box::new(GfxEntity::new(
            EntityId::new(),
            Vec2::new(30., 330.),
            GfxEntityData::Image(
                resources.get_handle_from_path("/images/sprites/plants/plant_begonia.png")
            )
        )),

    ]));

}



pub fn nergal_entities(resources: &Resources, sound: &Sound, centre: &Vec2) -> Vec<Box<dyn Entity>> {
    // place entities in vectors before positioning
    let mut entity_vec: Vec<Box<dyn Entity>> = vec![];
    let area_box = resources.world_state.accessible_area;
    
    for _ in 0..600 {

        let res = [("/images/sprites/plants/plant_apple.png", "/sounds/tree-1.mp3", Species::Apple),
                   ("/images/sprites/plants/plant_banyan.png", "/sounds/tree-2.mp3", Species::Banyan ),
                   ("/images/sprites/plants/plant_baobab.png", "/sounds/tree-3.mp3", Species::Baobab ),
                   ("/images/sprites/plants/plant_begonia.png", "/sounds/leaves-vlarge.mp3", Species::Begonia ),
                   ("/images/sprites/plants/plant_bulbinetortaa.png", "/sounds/leaves-small.mp3", Species::Bulbinetorta ),
                   ("/images/sprites/plants/plant_bulbinetortab.png", "/sounds/leaves-small.mp3", Species::Bulbinetorta ),
                   ("/images/sprites/plants/plant_bulbinetortac.png", "/sounds/leaves-small.mp3", Species::Bulbinetorta ),
                   ("/images/sprites/plants/plant_bulbinetortad.png", "/sounds/leaves-small.mp3", Species::Bulbinetorta ),
                   ("/images/sprites/plants/plant_calathea.png", "/sounds/leaves-large.mp3", Species::Calathea ),
                   ("/images/sprites/plants/plant_dollseye.png", "/sounds/leaves-medium.mp3", Species::Dollseye ),
                   ("/images/sprites/plants/plant_dragonblood.png", "/sounds/tree-2.mp3", Species::Dragonblood ),
                   ("/images/sprites/plants/plant_drosera.png", "/sounds/leaves-medium.mp3", Species::Drosera ),
                   ("/images/sprites/plants/plant_fishbone.png", "/sounds/leaves-large.mp3", Species::Fishbone ),
                   ("/images/sprites/plants/plant_jasmine.png", "/sounds/leaves-medium.mp3", Species::Jasmine ),
                   ("/images/sprites/plants/plant_lecanopteris.png", "/sounds/leaves-large.mp3", Species::Lecanopteris ),
                   ("/images/sprites/plants/plant_monkeymask.png", "/sounds/leaves-small.mp3", Species::Monkeymask ),
                   ("/images/sprites/plants/plant_ponytailpalm.png", "/sounds/leaves-small.mp3", Species::Ponytailpalm ),
                   ("/images/sprites/plants/plant_stefonia.png", "/sounds/leaves-small.mp3", Species:: Stefonia),
                   ("/images/sprites/plants/plant_strelizia.png", "/sounds/leaves-medium.mp3", Species::Strelizia ),
                   ("/images/sprites/plants/plant_tillandsia.png", "/sounds/leaves-small.mp3", Species::Tillandsia )]
            .choose(&mut rand::thread_rng()).unwrap();
        
        entity_vec.push(Box::new(PlantEntity::at_pos(
            EntityId::new(),
            Vec2::zero(),
            resources.get_handle_from_path(res.0),
            resources.get_handle_from_path(res.1),
            res.2
        )));            
    }

    for _ in 0..100 {
        entity_vec.push(Box::new(CloudEntity::at_pos(
            EntityId::new(),
            area_box.0.add(Vec2::rnd().mul2(area_box.1)),
            resources.get_handle_from_path(
                ["/images/sprites/clouds/cloud-1.png",
                 "/images/sprites/clouds/cloud-2.png",
                 "/images/sprites/clouds/cloud-3.png",
                 "/images/sprites/clouds/cloud-4.png",
                 "/images/sprites/clouds/cloud-5.png",
                 "/images/sprites/clouds/cloud-6.png"]
                    .choose(&mut rand::thread_rng()).unwrap()
            )
        )));            
    }
    
    for _ in 0..100 {
        let assets = [("/images/scenery/lake_mediumgreen.png", "/sounds/steps-splashy-3.mp3"),
                      ("/images/scenery/lake_bigyellow.png", "/sounds/steps-splashy-3.mp3"),
                      ("/images/scenery/lake_mediumpurple.png", "/sounds/steps-splashy-3.mp3"),
                      ("/images/scenery/lake_smallpink.png", "/sounds/steps-splashy-3.mp3"),
                      ("/images/scenery/lake_smallblue.png", "/sounds/steps-splashy-3.mp3"),
                      ("/images/scenery/lake_mediumpalepink.png", "/sounds/steps-splashy-3.mp3"),
                      ("/images/scenery/lake_mediumdarkpink.png", "/sounds/steps-splashy-3.mp3"),
                      ("/images/scenery/lake_bigdarkgreen.png", "/sounds/steps-splashy-3.mp3")]
            .choose(&mut rand::thread_rng()).unwrap();
        
        entity_vec.push(Box::new(SceneryEntity::at_pos(
            EntityId::new(),
            Vec2::zero(),
            resources.get_handle_from_path(assets.0),
            sound.get_handle_from_path(assets.1)
        )));            
    }

    for _ in 0..100 {
        let notes = ["/sounds/piano-1.mp3",
                     "/sounds/piano-2.mp3",
                     "/sounds/piano-3.mp3",
                     "/sounds/piano-4.mp3",
                     "/sounds/piano-5.mp3",
                     "/sounds/piano-6.mp3",
                     "/sounds/piano-7.mp3"];
        
        let assets = [("/images/scenery/stripes_bigyellow.png",
                       "/images/scenery/stripes_bigyellow_notes.png"),
                      ("/images/scenery/stripes_smallpink.png",
                       "/images/scenery/stripes_smallpink_notes.png"),
                      ("/images/scenery/stripes_bigorange.png",
                       "/images/scenery/stripes_bigorange_notes.png"),
                      ("/images/scenery/stripes_mediumblue.png",
                       "/images/scenery/stripes_mediumblue_notes.png"),
                      ("/images/scenery/stripes_mediumpink.png",
                       "/images/scenery/stripes_mediumpink_notes.png"),
                      ("/images/scenery/stripes_mediumpurple.png",
                       "/images/scenery/stripes_mediumpurple_notes.png")                    
        ].choose(&mut rand::thread_rng()).unwrap();
        
        entity_vec.push(Box::new(PianoEntity::at_pos(
            EntityId::new(),
            Vec2::zero(),
            resources.get_handle_from_path(assets.0),
            resources.get_handle_from_path(assets.1),
            notes.map(|path| {
                sound.get_handle_from_path(path)
            })
        )));            
    }   

    // stuff only in daytime
    if resources.theme == Theme::Daytime {    
        for _ in 0..200 {
            entity_vec.push(Box::new(RockEntity::at_pos(
                EntityId::new(),
                Vec2::zero(),
                resources.get_handle_from_path(
                    ["/images/sprites/rocks/rock1.png",
	                 "/images/sprites/rocks/rock2.png",
	                 "/images/sprites/rocks/rock3.png",
	                 "/images/sprites/rocks/rock4.png",
	                 "/images/sprites/rocks/rock5.png",
	                 "/images/sprites/rocks/rock6.png"]
                        .choose(&mut rand::thread_rng()).unwrap()
                )
            )));            
        }

        for _ in 0..20 {
            entity_vec.push(Box::new(RainbowEntity::at_pos(
                EntityId::new(),
                Vec2::zero(),
                resources.get_handle_from_path(
                    ["/images/sprites/rainbows/rainbow1trans.png",
                     "/images/sprites/rainbows/rainbow2trans.png",
                     "/images/sprites/rainbows/rainbow3trans.png",
                     "/images/sprites/rainbows/rainbow4trans.png"]
                        .choose(&mut rand::thread_rng()).unwrap()
                )
            )));            
        }
    } else {
        // night time entities
        entity_vec.push(Box::new(MoonEntity::new(
            EntityId::new(),
            centre.add(Vec2::new(0., -300.)),
            resources.get_handle_from_path("/images/sprites/astronomical/moon.png"
            )
        )));

        for _ in 0..100 {
            entity_vec.push(Box::new(RainbowEntity::at_pos(
                EntityId::new(),
                Vec2::zero(),
                resources.get_handle_from_path(
                    ["/images/sprites/astronomical/star-1.png",
                     "/images/sprites/astronomical/star-2.png",
                     "/images/sprites/astronomical/star-3.png",
                     "/images/sprites/astronomical/star-4.png",
                     "/images/sprites/astronomical/star-5.png"]
                        .choose(&mut rand::thread_rng()).unwrap()
                )
            )));            
        }

    }

    entity_vec.push(Box::new(FootprintEntity::new(
        EntityId::new(),
        *centre,
        PLAYER_ID,
        resources.get_handle_from_path("/images/sprites/footprint.png")
    )));
    
    let mut grid = create_layout_hexgrid(area_box, 200., true);
    layout_into_grid::<PlantEntity>(&mut entity_vec, &mut grid);
    layout_into_grid::<RockEntity>(&mut entity_vec, &mut grid);
    layout_into_grid::<RainbowEntity>(&mut entity_vec, &mut grid);

    //console::log_1(&format!("{} grid positions left over", grid.len()).into());
    
    let mut grid = create_layout_hexgrid(area_box, 800., false);
    layout_into_grid::<SceneryEntity>(&mut entity_vec, &mut grid);
    layout_into_grid::<PianoEntity>(&mut entity_vec, &mut grid);
        
    //console::log_1(&format!("{} scenery grid positions left over", grid.len()).into());

    entity_vec
}

pub fn nergal_init_prompts(screens: &mut ScreenManager, resources: &Resources) {
    let screen = screens.screens.get_mut(&ScreenState::Game).unwrap();
    
    screen.get_entity_mut_as::<PromptEntity>(*screens.entity_lookup.get("prompts").unwrap())
        .unwrap().prompts = Rc::new(vec![
            Prompt::new(match resources.theme {
                Theme::Daytime => "Click on things to interact with them, or on empty space to move around the world",
                Theme::Nighttime => "Select things to interact with them, or use cursor keys to move around the world"
            }.into(), PromptPriority::High),
        ].into());
    
}

pub fn nergal_update_diagnostic(screens: &mut ScreenManager, resources: &Resources) {
    let screen = screens.screens.get_mut(&ScreenState::GameOver(GameOverReason::NoHealth)).unwrap();

    screen.get_entity_mut_as::<TextEntity>(*screens.entity_lookup.get("diagnostic_text").unwrap())
        .unwrap().set_text(
            &resources.world_state.scores.diagnostic(),
            30, TEXT_AREA_WIDTH,
        );
    
}

pub fn nergal_generate_results(screens: &mut ScreenManager, averages: &ScoreValues, resources: &Resources) {

    let screen = screens.screens.get_mut(&ScreenState::Results).unwrap();

    let player_type = animid_to_character_type(resources.world_state.player_character);
    
    screen.get_entity_mut_as::<GfxEntity>(*screens.entity_lookup.get("results_bg").unwrap())
        .unwrap().
        update_data(match resources.theme {
            Theme::Daytime => GfxEntityData::DomedRect(
                Vec2::new(1200., 500.),
                "results_background".into()
            ),
            Theme::Nighttime => GfxEntityData::RoundRect(
                Vec2::new(1200., 500.),
                "results_background".into()
            )
        });
    
    screen.get_entity_mut_as::<GfxEntity>(*screens.entity_lookup.get("sociability_biscuit").unwrap())
        .unwrap().
        update_data(GfxEntityData::Image(
            resources.get_handle_from_path(&format!("/images/gui/biscuits/{}_social.png", player_type))
        ));
    
    screen.get_entity_mut_as::<HighlightTextEntity>(*screens.entity_lookup.get("sociability_text_1").unwrap())
        .unwrap().set_text(
            &resources.world_state.scores.interpret_sociability_1(averages),
            30, 700.
        );

    screen.get_entity_mut_as::<TextEntity>(*screens.entity_lookup.get("sociability_text_2").unwrap())
        .unwrap().set_text(
            &resources.world_state.scores.interpret_sociability_2(averages),
            30, 700.
        );

    screen.get_entity_mut_as::<TextEntity>(*screens.entity_lookup.get("sociability_text_2").unwrap())
        .unwrap().set_text(
            &resources.world_state.scores.interpret_sociability_2(averages),
            30, 700.
        );

    screen.get_entity_mut_as::<GfxEntity>(*screens.entity_lookup.get("health_biscuit").unwrap())
        .unwrap().
        update_data(GfxEntityData::Image(
            resources.get_handle_from_path(&format!("/images/gui/biscuits/{}_sick.png", player_type))
        ));

    screen.get_entity_mut_as::<HighlightTextEntity>(*screens.entity_lookup.get("health_text_1").unwrap())
        .unwrap().set_text(
            &resources.world_state.scores.interpret_health_1(averages),
            30, 700.
        );

    screen.get_entity_mut_as::<TextEntity>(*screens.entity_lookup.get("health_text_2").unwrap())
        .unwrap().set_text(
            &resources.world_state.scores.interpret_health_2(averages),
            30, 700.
        );

    let other_type = ["clouds", "plants", "snacks"].choose(&mut rand::thread_rng()).unwrap();
    
    screen.get_entity_mut_as::<GfxEntity>(*screens.entity_lookup.get("other_biscuit").unwrap())
        .unwrap().
        update_data(GfxEntityData::Image(
            resources.get_handle_from_path(&format!("/images/gui/biscuits/{}_{}.png", player_type, other_type))
        ));

    screen.get_entity_mut_as::<HighlightTextEntity>(*screens.entity_lookup.get("other_text").unwrap())
        .unwrap().set_text(
            &resources.world_state.scores.interpret_other(averages, other_type),
            30, 700.
        );

    let results_nergal = *screens.entity_lookup.get("results_nergal").unwrap();
    screen.remove_entity(results_nergal);
    screen.entities.push(Box::new(SocialEntity::at_pos(
        results_nergal,
        Vec2::new(1300., 350.),
        resources.world_state.player_character
    )));

}

// pub fn nergal_generate_graphs(screens: &mut ScreenManager, resources: &Resources) {
//     let screen = screens.screens.get_mut(&ScreenState::SocialNetworks).unwrap();
//     screen.get_entity_mut_as::<GraphEntity>(*screens.entity_lookup.get("network_before").unwrap())
//         .unwrap().fill(&resources.starting_social_graph);

//     screen.get_entity_mut_as::<GraphEntity>(*screens.entity_lookup.get("network_after").unwrap())
//         .unwrap().fill(&resources.social_graph);

//     screen.get_entity_mut_as::<TextEntity>(*screens.entity_lookup.get("network_before_text").unwrap())
//         .unwrap().set_text(&format!(
//             "The number of nergals you start with and how they are connected is picked randomly from real data on different animal species. You started with a social network based on {}! It looked like this:",
//             match resources.world_state.network_filename.as_str() {
//                 "/networks/badger_matrix.txt" => {
//                     "badgers"
//                 }
//                 "/networks/dophin_matrix.txt" => {
//                     "dolphins"
//                 }
//                 "/networks/elephant_matrix.txt" => {
//                     "elephants"
//                 }            
//                 _ => "an unknown animal (!)"
//             }
//         ), 20, 700.);    
// }

pub fn nergal_update_about_data(screens: &mut ScreenManager, resources: &Resources) {
    let screen = screens.screens.get_mut(&ScreenState::AboutData).unwrap();

    screen.get_entity_mut_as::<HighlightTextEntity>(*screens.entity_lookup.get("network_type").unwrap())
        .unwrap().set_text(
            &[
                TextBlock::new("You started with a social network based on ".into(), false),
                
                TextBlock::new(match resources.world_state.network_filename.as_str() {
                    "/networks/badger_matrix.txt" => {
                        "badgers".into()
                    }
                    "/networks/dolphin_matrix.txt" => {
                        "dolphins".into()
                    }
                    "/networks/elephant_matrix.txt" => {
                        "elephants".into()
                    }            
                    _ => resources.world_state.network_filename.clone()
                }, true),
            ], 30, 1000.);    
}

pub fn nergal_switch_theme(screens: &mut ScreenManager, resources: &Resources) {

    let screen = screens.screens.get_mut(&ScreenState::Intro).unwrap();                    
    screen.get_entity_mut_as::<ButtonEntity>(*screens.entity_lookup.get("daynight").unwrap())
        .unwrap().set_text(
            match resources.theme {
                Theme::Daytime => "Night mode",
                Theme::Nighttime => "Day mode"
            }
        );

    screen.get_entity_mut_as::<TextEntity>(*screens.entity_lookup.get("daynight_text").unwrap())
        .unwrap().set_text(
            match resources.theme {
                Theme::Daytime => "This game is not designed for use on phones. Night Mode is a higher contrast version that can be played with sound only, this is designed for players with low or no vision.",
                Theme::Nighttime => "Night mode can be played using sound only, use the F key to cycle through buttons and the space bar to activate them. Press the J key to hear a description of what's on the screen (using Firefox or most Chrome based browsers)."
            },
            NORMAL_TEXT_SIZE, 1500.
        );


    for screen in screens.screens.values_mut() { 
        for e in &mut screen.entities {
            if let Some(nergal) = e.as_any_mut().downcast_mut::<SocialEntity>() {
                match resources.theme {
                    Theme::Daytime => {
                        nergal.character = *[
                            AnimId::StickCharacter,
                            AnimId::CloudCharacter,
                            AnimId::EggCharacter,                            
                        ].choose(&mut rand::thread_rng()).unwrap()
                    }
                    Theme::Nighttime => {
                        nergal.character = AnimId::GhostCharacter
                    }
                }
            }
        }
    }
}

pub fn nergal_update_voices(_screens: &mut ScreenManager, resources: &mut Resources) {
    resources.speech.get_voices();    
}
