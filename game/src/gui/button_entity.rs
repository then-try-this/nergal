// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


use std::any::Any;
use vector2math::*;
use instant::{ Duration };
use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::message::*;
use crate::game::resources::{ Resources };
use crate::rendering::rendering::*;
use crate::sound::synth_node_desc::{ SynthNodeDesc };
use crate::sound::speech::{ Voice };
use crate::game::constants::{ is_select_key };

use crate::declare_any;
use crate::declare_message_reader;



const BUTTON_PADDING: f64 = 50.;

pub type CallbackType = fn(button: &dyn Entity, resources: &Resources) -> MessageVec;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum ButtonType {
    TextButton,
    SmallTextButton,
    InvertedTextButton,
    ImageButton
}

/// A clickable button, fixed in screen coordinates.
#[derive(Debug, Clone)]
pub struct ButtonEntity {
    pub id: EntityId,
    pub button_type: ButtonType,
    pub text: String,
    pub pos: Vec2,
    pub size: Vec2,
    pub timer: u128,
    pub clicked: bool,
    pub callback: CallbackType,
    pub hover: bool,
    pub hover_callback: CallbackType,
    pub call_hover_callback: bool,
    pub quiet: bool
}

impl ButtonEntity {
    pub fn new(id: EntityId, button_type: ButtonType, pos: Vec2, size: Vec2, text: &str, callback: CallbackType) -> Self {
        ButtonEntity::new_with_hover_callback(
            id, button_type, pos, size, text, callback,
            |_button: &dyn Entity, _resources: &Resources| -> MessageVec { vec![] }
        )
    }

    pub fn new_with_hover_callback(id: EntityId, button_type: ButtonType, pos: Vec2, size: Vec2, text: &str, callback: CallbackType, _hover_callback: CallbackType) -> Self {
        ButtonEntity::new_with_quiet(
            id, button_type, pos, size, text, callback,
            |_button: &dyn Entity, _resources: &Resources| -> MessageVec { vec![] },
            false
        )
    }
    
    pub fn new_with_quiet(id: EntityId, button_type: ButtonType, pos: Vec2, size: Vec2, text: &str, callback: CallbackType, hover_callback: CallbackType, quiet: bool) -> Self {
        ButtonEntity {
            id,
            button_type,
            text: text.to_string(),  
            pos: match button_type {
                ButtonType::TextButton => Vec2::new(pos.x - (text_width_with_font(text, "50px capriola") + BUTTON_PADDING) / 2., pos.y),
                ButtonType::InvertedTextButton => Vec2::new(pos.x - (text_width_with_font(text, "50px capriola") + BUTTON_PADDING) / 2., pos.y),
                ButtonType::SmallTextButton => Vec2::new(pos.x - (text_width_with_font(text, "30px capriola") + BUTTON_PADDING) / 2., pos.y),
                ButtonType::ImageButton => pos
            },
            size: match button_type {
                ButtonType::TextButton => Vec2::new(text_width_with_font(text, "50px capriola") + BUTTON_PADDING, size.y),
                ButtonType::InvertedTextButton => Vec2::new(text_width_with_font(text, "50px capriola") + BUTTON_PADDING, size.y),
                ButtonType::SmallTextButton => Vec2::new(text_width_with_font(text, "30px capriola") + BUTTON_PADDING, size.y),
                ButtonType::ImageButton => size
            },
            timer: 0,
            clicked: false,
            callback,
            hover: false,
            hover_callback,
            call_hover_callback: false,
            quiet
        }
    }

    pub fn set_text(&mut self, text: &str) {
        self.text = text.to_string();
    }
}

impl Entity for ButtonEntity {

    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    fn bb(&self) -> (Vec2, Vec2) {
        (self.pos, self.pos.add(self.size))
    }
    fn cat(&self) -> EntityCat {
        EntityCatFlags::FOCUSABLE |
        EntityCatFlags::QUIET
    }
        
    fn describe(&self, resources: &Resources) -> String {
        match self.button_type {
            ButtonType::TextButton => format!("{} button.", self.text),
            ButtonType::InvertedTextButton => format!("{} button.", self.text),
            ButtonType::SmallTextButton => format!("{} button.", self.text),
            ButtonType::ImageButton => {
                if let Some(desc) = resources.speech.image_desc.get(
                    &resources.get_handle_from_path(&self.text)
                ) {              
                    format!("{} button.", desc)                
                } else {                    
                    "".into()
                }
            }
        }
    }

    fn update(&self, _entities: &EntityRefVec, resources: &Resources, delta: &Duration) -> (Box<dyn Entity>,MessageVec) {
        (Box::new(ButtonEntity{
            timer: self.timer+delta.as_millis(),
            call_hover_callback: false,
            clicked: false,
            ..self.clone()}),
         if self.clicked {
             // call the callback here, pass in resources and
             // dispatch any messages it produces
             msg_cons(&(self.callback)(self, resources),
                      Message::new(MessageType::PlaySoundAt(
                          Vec2::zero(),
                          1.0,
                          SynthNodeDesc::build(
                              "sample",
                              &[
                                  ("file_name", "/sounds/notify-3.mp3".into()),
                                  ("playback_rate", 1.into())
                              ], &[])
                      ), NO_ID, WORLD_ID)
             )
         } else if self.call_hover_callback {
             (self.hover_callback)(self, resources)                          
         } else {                 
             vec![]
         })
    }

    fn apply_message(&self, msg: &Message, resources: &Resources) -> Box<dyn Entity> {
        match &msg.data {
            MessageType::MouseClick(click, _, _) => {
                if click.inside_box((self.pos, self.pos.add(self.size))) {
                    return Box::new(ButtonEntity {
                        clicked: true,                                
                        ..self.clone()
                    })
                }
            },
            
            MessageType::MouseMove(click, _, _) => {
                let _item_pos = self.pos;

                if click.inside_box((self.pos, self.pos.add(self.size))) {

                    //if !self.hover {
                    //    resources.speech.say(Voice::Narrator, &self.describe(resources));
                    //}
                    
                    return Box::new(ButtonEntity {
                        hover: true,
                        call_hover_callback: !self.hover,
                        ..self.clone()
                    })
                } else {
                    return Box::new(ButtonEntity {
                        hover: false,                                
                        ..self.clone()
                    })
                }
            }
            
            MessageType::Highlight => {
                if msg.recipient == self.id {
                    if !self.hover && !self.quiet {
                        resources.speech.say(Voice::Narrator, &self.describe(resources));
                    }
                    
                    return Box::new(ButtonEntity {
                        hover: true,
                        call_hover_callback: true,
                        ..self.clone()
                    })
                }
            }

            MessageType::Unhighlight => {
                if msg.recipient == self.id {
                    return Box::new(ButtonEntity {
                        hover: false,                                
                        ..self.clone()
                    })
                }
            }

            MessageType::ScreenChange(_state) => {
                return Box::new(ButtonEntity {
                    hover: false,                                
                    ..self.clone()
                })                
            }

            MessageType::KeyPress(key) => {
                if is_select_key(key) && self.hover {
                    return Box::new(ButtonEntity {
                        clicked: true,                                
                        ..self.clone()
                    })
                }
            }
            
            _ => {}
        }
        
        Box::new(self.clone())
    }

    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        match pass {
            RenderPass::GUI => {
                match self.button_type {

                    ButtonType::ImageButton => {
                        if let Some(image) = resources.get_sprite_from_path(&self.text) { 

                            if self.hover {
                                context.save();                            
                                if resources.safari_hack {
                                    context.set_filter("brightness(120%)");
                                } else {
                                    context.set_filter("brightness(1.2)");
                                }
                            }
                            
                            context.draw_image_with_html_image_element(image, self.pos.x.round(), self.pos.y.round());
                            
                            if self.hover {
                                context.restore();
                            }                            
                        } else {
                            //console::log_1(&format!("{} not found", self.text).into());
                        }
                    }

                    _ => {                
                        if self.button_type == ButtonType::SmallTextButton {
                            context.set_font(resources.attr("small_button_font"));
                        } else {
                            context.set_font(resources.attr("button_font"));
                        }
                        
                        let text_width = text_width(context, &self.text);
                        
                        context.begin_path();
                        context.round_rect_with_f64(self.pos.x, self.pos.y, self.size.x, self.size.y, 49.);
                        context.set_fill_style(&resources.colour(if self.hover {
                            if self.button_type == ButtonType::InvertedTextButton {
                                "inverted_button_hover"
                            } else {
                                "button_hover"
                            }
                        } else if self.button_type == ButtonType::InvertedTextButton {
                            "inverted_button_bg"
                        } else {
                            "button_bg"
                        }).into());
                        context.fill();
                        
                        let middle = self.size.x/2.;
                        let text_middle = text_width/2.;
                        
                        context.set_fill_style(&resources.colour(
                            if self.button_type == ButtonType::InvertedTextButton {
                                "inverted_button_text"
                            } else {
                                "button_text"
                            }
                        ).into());
                        context.fill_text(&self.text, self.pos.x - text_middle + middle,
                                          self.pos.y + self.size.y - 15.).unwrap();
                    }
                    
                }                
            }
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
