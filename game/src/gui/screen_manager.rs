// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use instant::{ Duration };
use vector2math::*;
use crate::game::bucket::{ Bucket };
use crate::game::message::{ MessageVec };
use crate::game::resources::{ Resources };
use crate::rendering::rendering::{ RenderPass };
use crate::game::entity::*;

use crate::game::message::*;
use crate::game::constants::*;
use crate::game::id::*;
use crate::sound::sound::{ Sound };
use crate::gui::menu_entity::{ MenuEntity };

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash)]
pub enum ScreenState {
    Intro,
    About,
    Credits,
    Consent,
    PlayedBefore,
    KeyControls,
    SpeechOptions,
    ExtraSpeechOptions,
    SightInfo,
    SelectChar,
    SelectHat(u32),
    SelectGhost,
    GameInfo(Theme),
    Game,
    GameOver(GameOverReason),
    Demographics(Theme),
    DemographicsAge,
    DemographicsGender,
    DemographicsPandemic,
    Results,
    AboutData
}

/// A manager for screens of gui entities. Effectively a map of states
/// to buckets containing gui entities
pub struct ScreenManager {
    pub width: u32,
    pub height: u32,
    pub screens: HashMap<ScreenState, Bucket>,
    pub state: ScreenState,
    pub entity_lookup: HashMap<String, EntityId>,
    pub focus_index: i32,
    pub tab_captured: bool
}

impl ScreenManager {
    pub fn new(width: u32, height: u32) -> Self {
        ScreenManager {
            width, height,
            screens: HashMap::new(),
            state: ScreenState::Intro,
            entity_lookup: HashMap::new(),
            focus_index: -1,
            tab_captured: false
        }
    }
   
    pub fn update(&mut self, outside_messages: &mut MessageVec, resources: &Resources, sound: &mut Sound, delta: &Duration) -> MessageVec {        
        let _surrounding: EntityRefVec = vec![];

        // look for the current screen
        if let Some(screen) = self.screens.get_mut(&self.state) {
            // just send in all the entities in this screen as the surrounding ones
            let (new_screen, mut messages) = screen.update(screen.entities.iter().collect(), resources, sound, delta);
            *screen = new_screen;
            messages.append(outside_messages);
            return messages;
        }

        outside_messages.clone()
    }

    pub fn dispatch(&mut self, messages: &MessageVec, resources: &Resources) {                
        
        // update based on the messages collected
        // send to just the current one      
        if let Some(screen) = self.screens.get_mut(&self.state) {
            let mut tacked_on_msgs = vec![];
            
            for msg in messages {
                if msg.recipient == SCREEN_MANAGER_ID {
                    match &msg.data {
                        MessageType::RemoveMe => {
                            screen.remove_entity(msg.sender);
                        }
                        MessageType::StateChange(state) => {                            
                            if let Ok(entity) = screen.get_entity_mut(msg.sender) {
                                entity.set_state(*state);
                            } 
                        }
                        // for buttons moving from one state to another
                        MessageType::ScreenChange(state) => {
                            self.focus_index = -1;
                            self.tab_captured = false;
                            self.state = *state;                            
                        }
                        MessageType::OpenMenu(desc, menu_items, y_offset, _from_key) => {
                            if let Ok(sender) = screen.get_entity(msg.sender) {

                                let menu = Box::new(MenuEntity::new(
                                    EntityId::new(),
                                    desc.into(),
                                    sender.pos(),
                                    *y_offset,
                                    msg.sender,
                                    SCREEN_MANAGER_ID,
                                    menu_items.clone()
                                ));
                                
                                screen.entities.push(menu);                               
                            }

                            self.tab_captured = true;
                            
                            // turn off focus when we open a menu
                            // so we can focus within the menu
                            if self.focus_index != -1 {
                                let focusable_entities = screen.entities.iter().filter(|e| {
                                    e.cat() & EntityCatFlags::FOCUSABLE != 0
                                }).collect::<Vec<_>>();
                                
                                tacked_on_msgs.push(Message::new(
                                    MessageType::Unhighlight,
                                    SCREEN_MANAGER_ID,
                                    focusable_entities[self.focus_index as usize].id()
                                ));
                                
                                self.focus_index = -1;
                            }
                        }
                        
                        MessageType::MenuGone => {
                            self.tab_captured = false;
                        }
                        _ => {}
                    }
                } else { // snooping on other messages
                    match &msg.data {
                        MessageType::KeyPress(key) => {
                            if self.state != ScreenState::Game && is_next_key(key) && !self.tab_captured {
                                let last_focus = self.focus_index;
                                self.focus_index += 1;
                                
                                let focusable_entities = screen.entities.iter().filter(|e| {
                                    e.cat() & EntityCatFlags::FOCUSABLE != 0
                                }).collect::<Vec<_>>();

                                if self.focus_index as usize >= focusable_entities.len() {
                                    self.focus_index = 0;
                                }

                                if last_focus != -1 {
                                    tacked_on_msgs.push(Message::new(
                                        MessageType::Unhighlight,
                                        SCREEN_MANAGER_ID,
                                        focusable_entities[last_focus as usize].id()
                                    ));
                                }
                                
                                tacked_on_msgs.push(Message::new(
                                    MessageType::Highlight,
                                    SCREEN_MANAGER_ID,
                                    focusable_entities[self.focus_index as usize].id()
                                ));
                                
                            }

                        }

                        // not included above as we need to do this
                        // with messages sent to the game system too
                        // (to deselect quit button)
                        MessageType::OpenMenu(_desc, _menu_items, _y_offset, _from_key) => {
                            // ignore open menu messages from game
                            // entities (who send to world_id) on GUI
                            // screens
                            if msg.recipient != WORLD_ID || self.state == ScreenState::Game {                                       
                                self.tab_captured = true;
                                
                                // turn off focus when we open a menu
                                // so we can focus within the menu
                                if self.focus_index != -1 {
                                    let focusable_entities = screen.entities.iter().filter(|e| {
                                        e.cat() & EntityCatFlags::FOCUSABLE != 0
                                    }).collect::<Vec<_>>();
                                    
                                    tacked_on_msgs.push(Message::new(
                                        MessageType::Unhighlight,
                                        SCREEN_MANAGER_ID,
                                        focusable_entities[self.focus_index as usize].id()
                                    ));
                                    
                                    self.focus_index = -1;
                                }
                            }
                        }
                        _ => {}
                    }
                }
            }

            tacked_on_msgs.extend(messages.iter().cloned());
            
            *screen = screen.dispatch(&tacked_on_msgs, resources);            
            
            for msg in messages {
                match &msg.data {
                    MessageType::ScreenChange(state) => {
                        if let Some(screen) = self.screens.get(state) {
                            screen.describe(resources);
                        }
                    }
                    MessageType::KeyPress(key) => {
                        if is_describe_key(key) {
                            if let Some(screen) = self.screens.get(&self.state) {
                                screen.describe(resources);
                            }
                        }
                    }
                    _ => {}
                }                  
            }
        }
    }
    
    #[allow(unused_must_use)]
    pub fn render(&self, context: &web_sys::CanvasRenderingContext2d, resources: &Resources, delta: &Duration) {
        context.save();        

        // set up for shadow pass
        context.save();
        context.set_global_alpha(0.10);
        if resources.safari_hack {
            context.set_filter("brightness(0%)");
        } else {
            context.set_filter("brightness(0)");
        }
        // render shadows
        for entity in &self.screens[&self.state].entities {
            entity.render(context,RenderPass::Shadow,resources,delta);
        }
        context.restore();

        for entity in &self.screens[&self.state].entities {
            entity.render(context,RenderPass::Bottom,resources,delta);
        }

        for entity in &self.screens[&self.state].entities {
            entity.render(context,RenderPass::Beauty,resources,delta);
        }

        for entity in &self.screens[&self.state].entities {
            entity.render(context,RenderPass::Top,resources,delta);
        }

        for entity in &self.screens[&self.state].entities {
            entity.render(context,RenderPass::GUI,resources,delta);
        }
        /*for entity in &self.screens[&self.state].entities {
            entity.render(context,RenderPass::Debug,resources,delta);
        }*/
        context.restore();
    }
}
