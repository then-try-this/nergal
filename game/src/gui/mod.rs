// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/// A temporary speech bubble entity in game space, that follows a character around.
pub mod bubble_entity;
/// A entity in game space, that is a temporary interactive collection of menu items.
pub mod menu_entity;
/// A clickable menu item in game space (not an entity)
pub mod menu_item;
/// A clickable button, fixed in screen coordinates.
pub mod button_entity;
/// A manager for screens of gui entities.
pub mod screen_manager;
/// A general purpose graphics entitiy.
pub mod gfx_entity;
/// An entity that renders lines of text.
pub mod text_entity;
/// An entity that renders lines of text with parts highlighted.
pub mod highlight_text_entity;
/// A healthbar that tracks the health of a character.
pub mod healthbar_entity;
/// A game timer to show how long you have survived.
pub mod clock_entity;
/// A graph visualisation.
pub mod graph_entity;
/// Helpful player prompts.
pub mod prompt_entity;

