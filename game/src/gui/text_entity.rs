// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;
use std::any::Any;
use vector2math::*;
use instant::{ Duration };
use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };

use crate::game::entity::*;
use crate::game::message::{ Message, MessageVec };
use crate::game::resources::{ Resources };
use crate::rendering::rendering::*;

use crate::declare_any;
use crate::declare_message_reader;

#[derive(Debug, Clone, Copy)]
pub enum Alignment {
    Left,
    Centre
}

/// An entity for formatting text in the GUI
#[derive(Debug, Clone)]
pub struct TextEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub align: Alignment,
    pub line_height: f64,
    pub colour: String,
    pub size: u32,
    pub text: Rc<Vec<String>>
}

impl TextEntity {
    pub fn new(id: EntityId, pos: Vec2, text: &str, size: u32,
               line_height: f64, width: f64, align: Alignment) -> Self {
        let font = format!("{}px capriola", size);
        TextEntity {
            id, pos, align, line_height, size,
            colour: "default_text".into(),
            text: Rc::new(text_layout_pixels(text, width, &font))
        }
    }

    pub fn coloured(id: EntityId, pos: Vec2, text: &str, size: u32,
               line_height: f64, width: f64, align: Alignment, colour: &str) -> Self {
        let font = format!("{}px capriola", size);
        TextEntity {
            id, pos, align, line_height, size,
            colour: colour.into(),
            text: Rc::new(text_layout_pixels(text, width, &font))
        }
    }

    pub fn set_text(&mut self, text: &str, size: u32, width: f64) {
        let font = format!("{}px capriola", size);
        self.text = Rc::new(text_layout_pixels(text, width, &font))
    }
}

impl Entity for TextEntity {
    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }

    fn describe(&self, _resources: &Resources) -> String {
        let desc = self.text.iter().fold(String::new(),|acc,line| format!("{} {}", acc, line));

        if !desc.is_empty() && !desc.ends_with('.') {
            format!("{}.", desc.replace("NERGAL", "nergal"))
        } else {
            desc
        }            
    }
    
    fn update(&self, _entities: &EntityRefVec, _resources: &Resources, _delta: &Duration) -> (Box<dyn Entity>,MessageVec) {
        (Box::new(self.clone()), vec![])
    }

    fn apply_message(&self, _msg: &Message, _resources: &Resources) -> Box<dyn Entity> {
        Box::new(self.clone())
    }
    
    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        match pass {
            RenderPass::GUI => {
                context.set_fill_style(&resources.colour(&self.colour).into());
                let font = format!("{}px capriola", self.size);
                context.set_font(&font); 

                match self.align {
                    Alignment::Left => {                
                        for (i,line) in self.text.iter().enumerate() {                            
                            context.fill_text(line, self.pos.x, self.pos.y+i as f64*self.line_height).unwrap();
                        }
                    },

                    Alignment::Centre => {
                        for (i,line) in self.text.iter().enumerate() {
                            let width = text_width(context, line);
                            context.fill_text(line, self.pos.x-width/2., self.pos.y+i as f64*self.line_height).unwrap();
                        }
                    }
                    
                    
                }
            }
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
