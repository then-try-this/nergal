// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


use std::any::Any;
use vector2math::*;
use instant::{ Duration };
use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };

use crate::game::entity::*;
use crate::game::message::{ Message, MessageVec };
use crate::game::resources::{ Resources };
use crate::rendering::rendering::*;
use crate::game::message::{ Theme };

use crate::declare_any;
use crate::declare_message_reader;

#[derive(Debug, Clone)]
pub enum GfxEntityData {
    Rect(Vec2, String),
    RoundRect(Vec2, String),
    DomedRect(Vec2, String),
    Image(u64),
    ThemedImage(Box<[(Theme, u64)]>),
    SimpleText(String, u32, String)
}



/// A low level entity for general purpose graphics in the gui
#[derive(Debug, Clone)]
pub struct GfxEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub data: GfxEntityData
}

impl GfxEntity {
    pub fn new(id: EntityId, pos: Vec2, data: GfxEntityData) -> Self {
        GfxEntity {
            id,
            pos,
            data
        }
    }

    pub fn update_data(&mut self, data: GfxEntityData) {
        self.data = data;
    }
}

impl Entity for GfxEntity {

    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }

    fn describe(&self, _resources: &Resources) -> String {
        match &self.data {
            GfxEntityData::Rect(_,_) => { "".into() },
            GfxEntityData::RoundRect(_,_) => { "".into() },
            GfxEntityData::DomedRect(_,_) => { "".into() },
            GfxEntityData::Image(_) => { "".into() },
            GfxEntityData::ThemedImage(_) => { "".into() },
            GfxEntityData::SimpleText(text, _, _) => { format!("{}.", text) } 
        }
    }
    
    fn update(&self, _entities: &EntityRefVec, _resources: &Resources, _delta: &Duration) -> (Box<dyn Entity>,MessageVec) {
        (Box::new(self.clone()), vec![])
    }

    fn apply_message(&self, _msg: &Message, _resources: &Resources) -> Box<dyn Entity> {
        Box::new(self.clone())
    }
    
    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        match pass {
            RenderPass::Bottom => {

                match &self.data {
                    GfxEntityData::Rect(size, colour) => {
                        context.set_stroke_style(&"#000".into());
                        context.begin_path();
                        context.rect(self.pos.x, self.pos.y, size.x, size.y);
                        context.set_fill_style(&resources.colour(colour).into());
                        context.fill();
                    },
                    GfxEntityData::RoundRect(size, colour) => {
                        context.begin_path();
                        context.round_rect_with_f64(self.pos.x, self.pos.y, size.x, size.y, 50.);
                        context.set_stroke_style(&resources.colour(colour).into());
                        context.set_line_width(5.);
                        context.stroke();
                        context.set_stroke_style(&"#000".into());
                        context.set_line_width(1.);
                    },
                    GfxEntityData::DomedRect(size, colour) => {
                        context.set_stroke_style(&"#000".into());
                        context.begin_path();
                        context.round_rect_with_f64(self.pos.x, self.pos.y, size.x, size.y, 50.);
                        context.rect(self.pos.x, self.pos.y + size.y / 2., size.x, size.y / 2.);
                        context.set_fill_style(&resources.colour(colour).into());
                        context.fill();
                    },
                    GfxEntityData::SimpleText(text, size, colour) => {
                        let font = format!("{}px capriola", size);
                        context.set_font(&font); 
                        //context.set_fill_style(&colour.as_hex().into());
                        context.set_fill_style(&resources.colour(colour).into());
                        context.fill_text(text, self.pos.x,  self.pos.y).unwrap();
                    },
                    GfxEntityData::Image(handle) => {
                        if let Some(image) = resources.sprite(handle) {
                            context.draw_image_with_html_image_element(image, self.pos.x, self.pos.y);
                        }
                    }
                    GfxEntityData::ThemedImage(list) => {
                        if let Some(for_theme) = list.iter().find(|&x| x.0 == resources.theme) {
                            if let Some(image) = resources.sprite(&for_theme.1) {
                                context.draw_image_with_html_image_element(image, self.pos.x, self.pos.y);
                            }
                        }
                    }
                }
            }
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
