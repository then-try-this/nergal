// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;
use vector2math::Vector2;
use instant::{ Duration };
use crate::game::resources::{ Resources };
use crate::maths::vec2::{ Vec2 };
use crate::game::message::{ Message };
use crate::rendering::rendering::{ RenderPass, text_width, text_layout_pixels };
use crate::gui::menu_entity::{ MenuStyle };

#[derive(Debug, PartialEq)]
pub enum Order {
    Only,
    First,
    Middle,
    Last,
    Dropdown
}

/// A clickable menu item in game space (not an entity)
#[derive(Debug, Clone)]
pub struct MenuItem {
    pub text: String,
    pub text_lines: Rc<Vec<String>>,
    pub messages: Rc<Vec<Message>>,
    pub size: Vec2,
}

pub const MENU_ITEM_WIDTH: f64 = 350.;
pub const MENU_ITEM_PADDING: f64 = 15.;

pub const GAME_MENU_HEIGHT: f64 = 25.;
pub const DROPDOWN_MENU_HEIGHT: f64 = 35.;

impl MenuItem {
    pub fn new(name: String, messages: Rc<Vec<Message>>) -> Self {
        MenuItem::new_style(name, MenuStyle::Game, messages)
    }

    pub fn new_style(name: String, style: MenuStyle, messages: Rc<Vec<Message>>) -> Self {
        
        let text_lines = if style == MenuStyle::Game {
            Rc::new(text_layout_pixels(&name, 300., "20px capriola"))
        } else {
            // to match small button
            Rc::new(text_layout_pixels(&name, 300., "30px capriola"))
        };

        let line_height = match style {
            MenuStyle::Game => GAME_MENU_HEIGHT,
            MenuStyle::Dropdown => DROPDOWN_MENU_HEIGHT
        };
        
        let text_height = text_lines.len() as f64 * line_height;        

        MenuItem {
            text: name,
            text_lines,
            messages,
            size: Vec2::new(MENU_ITEM_WIDTH, text_height + MENU_ITEM_PADDING * 2.),
        }
    }

    pub fn inside(&self, pos: Vec2, click: Vec2) -> bool {
        click.inside(pos,pos.add(self.size))
    }

    pub fn height(&self) -> f64 {
        self.size.y
    }
    
    #[allow(unused_must_use)]
    pub fn render(&self, context: &web_sys::CanvasRenderingContext2d, pos: Vec2, order: Order, hover: bool, _pass: RenderPass, resources: &Resources, _delta: &Duration) {
        // only called (so far) in GUI pass from the entity, so ignore
        // the pass for now

        let fill = resources.colour(if hover {
            "menu_hover"
        } else {
            "menu_bg"
        });

        context.set_line_width(2.);
        context.set_stroke_style(&resources.colour("menu_text").into());

        let px = pos.x.round();
        let py = pos.y.round();
        let sx = self.size.x.round();
        let sy = self.size.y.round();
        
        match order {
            Order::Only => {
                // rounded top and point
                if let Some(image) = resources.get_sprite_from_path(if hover {
                    "/images/gui/menu-point-hover.png"
                } else {
                    "/images/gui/menu-point.png"
                }) {
                    context.set_line_width(4.);
                    context.set_fill_style(&fill.into());
                    context.begin_path();
                    context.round_rect_with_f64(px, py, sx, sy, 50.);
                    context.rect(px, py + (sy / 2.).round(), sx, (sy / 2.).round());
                    context.stroke();
                    context.set_global_composite_operation("destination-out");
                    context.fill();
                    context.set_global_composite_operation("source-over");
                    context.fill();

                    // stick a point on it
                    let x_offset = (self.size.x / 2. - 49. / 2.).round();
                    
                    context.draw_image_with_html_image_element(
                        image,
                        px + x_offset,
                        sy + py - 0.5
                    );
                }
            }
            Order::First => {
                // rounded top only
                context.set_line_width(4.);
                context.set_fill_style(&fill.into());
                context.begin_path();
                context.round_rect_with_f64(px+1.5, py, sx-3., sy, 50.);
                context.rect(px+1.5, py + (sy / 2.).round(), sx-3., (sy / 2.).round());
                context.stroke();
                context.set_global_composite_operation("destination-out");
                context.fill();
                context.set_global_composite_operation("source-over");
                context.fill();
            }
            Order::Middle => {
                // boring square box
                context.set_fill_style(&fill.into());
                context.begin_path();
                context.rect(px, py, sx, sy);
                context.fill();
                context.set_stroke_style(&resources.colour("menu_text").into());
                context.stroke();
            }
            Order::Last => {
                // add the point
                if let Some(image) = resources.get_sprite_from_path(if hover {
                    "/images/gui/menu-point-hover.png"
                } else {
                    "/images/gui/menu-point.png"
                }) {
                    // normal square box
                    context.set_fill_style(&fill.into());
                    context.begin_path();
                    context.rect(px, py, sx, sy);
                    context.fill();
                    context.set_stroke_style(&resources.colour("menu_text").into());
                    context.stroke();

                    // stick a point on it
                    let x_offset = (self.size.x/2. - 49./2.).round();
                    
                    context.draw_image_with_html_image_element(
                        image, px + x_offset, sy + py - 2.5
                    );
                }
            }
            Order::Dropdown => {
                // rounded rect
                context.set_fill_style(&fill.into());
                context.begin_path();
                context.round_rect_with_f64(px, py, sx, sy, 50.);
                context.fill();
            }
        }
                                     
        // draw the text
        if order == Order::Dropdown {
            context.set_font(resources.attr("small_button_font"));
            context.set_fill_style(&resources.colour("menu_text").into());
        } else {
            context.set_fill_style(&resources.colour("dropdown_menu_text").into());
        }
        
        let line_height = match order {
            Order::Dropdown => DROPDOWN_MENU_HEIGHT,
            _ => GAME_MENU_HEIGHT
        };
        
        let _text_height = self.text_lines.len() as f64 * line_height;
        
        let middle = self.size.x/2.;
        for (i,line) in self.text_lines.iter().enumerate() {                    
            let text_middle = (text_width(context, line) / 2.).round();
            context.fill_text(
                line,
                px - text_middle + middle,
                py + (i as f64 * line_height) + line_height + MENU_ITEM_PADDING - 5.
            ).unwrap();
        }
        
        //context.fill_text(&self.name, pos.x - text_middle + middle,
        //                  pos.y + self.size.y - 15.).unwrap();        
        
        
    }
}
