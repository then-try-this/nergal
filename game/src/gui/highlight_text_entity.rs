// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;
use std::any::Any;
use vector2math::*;
use instant::{ Duration };
use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };

use crate::game::entity::*;
use crate::game::message::{ Message, MessageVec };
use crate::game::resources::{ Resources };
use crate::rendering::rendering::*;
use crate::gui::text_entity::{ Alignment };

use crate::declare_any;
use crate::declare_message_reader;



#[derive(Debug, Clone)]
pub struct TextBlock {
    pub text: String,
    pub relative_pos: Vec2,
    pub width: f64,
    pub highlight: bool
}

impl TextBlock {
    pub fn new(text: String, highlight: bool) -> Self {
        TextBlock {
            text,
            relative_pos: Vec2::zero(),
            width: 0.,
            highlight
        }
    }
}

/// An entity for formatting text in the GUI
#[derive(Debug, Clone)]
pub struct HighlightTextEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub align: Alignment,
    pub line_height: f64,
    pub size: u32,
    pub colour: String,
    pub text: Rc<Vec<TextBlock>>
}

impl HighlightTextEntity {
    pub fn new(id: EntityId, pos: Vec2, size: u32,
               line_height: f64, _width: f64, align: Alignment) -> Self {
        let _font = format!("{}px capriola", size);
        HighlightTextEntity {
            id, pos, align, line_height, size,
            colour: "default_text".into(),
            text: Rc::new(vec![])
        }
    }

    pub fn coloured(id: EntityId, pos: Vec2, size: u32,
                    line_height: f64, _width: f64, align: Alignment, colour: &str) -> Self {
        let _font = format!("{}px capriola", size);
        HighlightTextEntity {
            id, pos, align, line_height, size,
            colour: colour.into(),
            text: Rc::new(vec![])
        }
    }

    pub fn set_text(&mut self, text: &[TextBlock], size: u32, width: f64) {
        let font = format!("{}px capriola", size);
        let mut t = text.to_owned();
        textblock_layout_pixels(&mut t, width, &font);
        self.text = Rc::new(t);
    }
}

impl Entity for HighlightTextEntity {
    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }

    fn describe(&self, _resources: &Resources) -> String {
        self.text.iter().fold(String::new(),|acc,line| format!("{} {}", acc, line.text)) 
    }
    
    fn update(&self, _entities: &EntityRefVec, _resources: &Resources, _delta: &Duration) -> (Box<dyn Entity>,MessageVec) {
        (Box::new(self.clone()), vec![])
    }

    fn apply_message(&self, _msg: &Message, _resources: &Resources) -> Box<dyn Entity> {
        Box::new(self.clone())
    }
    
    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        match pass {
            RenderPass::GUI => {
                let font = format!("{}px capriola", self.size);
                context.set_font(&font); 

                for block in self.text.iter() {                            
                    if block.highlight {
                        context.begin_path();
                        context.rect(self.pos.x + block.relative_pos.x - 5.,
                                     self.pos.y - self.line_height + 5.,
                                     block.width + 10.,
                                     self.line_height);
                        context.set_fill_style(&resources.colour("highlight").into());
                        context.fill();
                    }
                    if block.highlight {
                        context.set_fill_style(&resources.colour("highlight_text").into());
                    } else {
                        context.set_fill_style(&resources.colour(&self.colour).into());
                    }
                    context.fill_text(
                        &block.text,
                        self.pos.x + block.relative_pos.x,
                        self.pos.y 
                    ).unwrap();
                    
                }

            }
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
