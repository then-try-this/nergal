// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


use std::any::Any;
use vector2math::*;
use instant::{ Duration };
use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };

use crate::game::entity::*;
use crate::game::message::*;
use crate::game::resources::{ Resources };
use crate::rendering::rendering::*;

use crate::declare_any;
use crate::declare_message_reader;



const CHANGE_SPEED: f64 = 0.01;

// A health bar GUI element
#[derive(Debug, Clone, Copy)]
pub struct HealthBarEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub entity_id: EntityId,
    pub health: f64,
    pub render_health: f64,
    pub timer: u128,
    pub last_change: f64
}

impl HealthBarEntity {
    pub fn new(id: EntityId, pos: Vec2) -> Self {
        HealthBarEntity {
            id,
            pos,
            entity_id: PLAYER_ID,
            health: 100.,
            render_health: 100.,
            timer: 0,
            last_change: 0.
        }
    }
}

impl Entity for HealthBarEntity {

    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    
    fn update(&self, _entities: &EntityRefVec, _resources: &Resources, delta: &Duration) -> (Box<dyn Entity>,MessageVec) {

        (Box::new(HealthBarEntity{
            timer: self.timer + delta.as_millis(),
            render_health: self.render_health * (1. - CHANGE_SPEED) + self.health * CHANGE_SPEED,
            ..*self
        }), vec![])
    }
    
    fn apply_message(&self, msg: &Message, _resources: &Resources) -> Box<dyn Entity> {
        match msg.data {
            MessageType::NewGame => {
                Box::new(HealthBarEntity{
                    health: 100.,
                    render_health: 100.,
                    timer: 0,
                    ..*self
                })
            }
            
            // tracks the health of the entity by snooping on messages
            // rather than read it directly
            MessageType::HealthChange(change, _reason) => {                
                if msg.recipient == self.entity_id {                    
                    Box::new(HealthBarEntity{
                        health: f64::min(100., f64::max(0., self.health + change)),
                        timer: 0,
                        last_change: change,
                        ..*self
                    })
                } else {
                    Box::new(*self)
                }
            }
                
            _ => {
                Box::new(*self)
            }
        }
    }
    
    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        match pass {
            RenderPass::GUI => {
                context.begin_path();
                context.rect(self.pos.x, self.pos.y, self.render_health * 3., 30.);

                context.set_fill_style(&resources.colour("healthbar").into());
                context.fill();
                
                context.begin_path();
                context.rect(self.pos.x, self.pos.y, 300., 30.);
                context.set_stroke_style(&"#000".into());
                context.stroke();

                context.set_font("30px capriola"); 
                context.set_fill_style(&resources.colour("default_text").into());
                context.fill_text("Life:",
                                  self.pos.x - 70.,
                                  self.pos.y + 26.).unwrap();

            }
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
