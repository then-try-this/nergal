// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;
use std::any::Any;
use std::collections::HashMap;
use vector2math::*;
use instant::{ Duration };
use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };

use crate::game::entity::*;
use crate::game::message::{ Message, MessageVec };
use crate::game::resources::{ Resources };
use crate::rendering::rendering::*;
use crate::game::social_graph::*;

use crate::game::graph_layout::*;

use crate::declare_any;
use crate::declare_message_reader;




#[derive(Debug, Clone)]
pub struct GraphEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub size: Vec2,
    pub graph_centre: Vec2,
    pub nodes: Rc<HashMap<EntityId, LayoutNode>>,
    pub edges: Rc<Vec<(EntityId, EntityId)>>
}

impl GraphEntity {
    pub fn new(id: EntityId, pos: Vec2, size: Vec2) -> Self {
        GraphEntity {
            id,
            pos,
            size,
            graph_centre: Vec2::zero(),
            nodes: Rc::new(HashMap::new()),
            edges: Rc::new(vec![])
        }
    }

    pub fn fill(&mut self, graph: &SocialGraph) {
        let mut ego_graph = graph.ego_graph(PLAYER_ID, 2);
        ego_graph.generate_cache();
        
        self.nodes = Rc::new(
            deterministic_layout_nodes(
                &ego_graph,
                (self.pos, self.pos.add(self.size))
            )
        );

        let mut edges = vec![];
        for (id, n) in self.nodes.iter() {
            for other in &n.connections {
                edges.push((*id, *other))
            }
        }

        self.edges = Rc::new(edges);

        
        
        // prerun

        let mut iterations = 500;

        let new_centre = self.graph_centre;
        
        while iterations > 0 {
            let (new_nodes, _new_centre) = iterate_layout_nodes(
                &self.nodes,
                self.pos.add(self.size.mul(0.5)),
                0.99,   // friction
                75.,    // edge_length
                0.0,    // centre_attraction
                0.1,    // other_repel
                0.1,    // edge_attraction
                2.0     // max_speed
            );
            self.nodes = new_nodes.clone().into();
            iterations -= 1;
        }

        self.graph_centre = new_centre;
        
    }
    
}

impl Entity for GraphEntity {

    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    
    fn describe(&self, _resources: &Resources) -> String {
        "".into()
    }
    
    fn update(&self, _entities: &EntityRefVec, _resources: &Resources, _delta: &Duration) -> (Box<dyn Entity>,MessageVec) {
        let (new_nodes, new_centre) = iterate_layout_nodes(
            &self.nodes,
            self.pos.add(self.size.mul(0.5)),
            0.99,   // friction
            75.,    // edge_length
            0.0,   // centre_attraction
            0.01,   // other_repel
            0.01, // edge_attraction
            2.0     // max_speed
        );

        (Box::new(GraphEntity {
            nodes: Rc::new(new_nodes),
            graph_centre: new_centre.add(self.pos.add(self.size.mul(0.5))),
            ..self.clone()
        }), vec![])
    }

    fn apply_message(&self, _msg: &Message, _resources: &Resources) -> Box<dyn Entity> {
        Box::new(self.clone())
    }
    
    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        match pass {
            RenderPass::GUI => {
                let bb = (self.pos, self.pos.add(self.size));

                context.begin_path();
                context.rect(self.pos.x, self.pos.y, self.size.x, self.size.y);
                context.set_fill_style(&resources.colour("graph_background").into());
                context.fill();

                for edge in self.edges.iter() {
                    let n1 = self.nodes.get(&edge.0).unwrap();
                    let n2 = self.nodes.get(&edge.1).unwrap();
                    let p1 = self.graph_centre.sub(n1.pos);
                    let p2 = self.graph_centre.sub(n2.pos);
                    
                    if p1.inside_box(bb) && p2.inside_box(bb) {
                        context.begin_path();
                        context.move_to(p1.x, p1.y);
                        context.line_to(p2.x, p2.y);
                        context.stroke();
                    }
                }                
                
                for (id, node) in self.nodes.iter() {
                    let p = self.graph_centre.sub(node.pos);
                    if p.inside_box(bb) {
                        context.begin_path();
                        context
                            .arc(p.x, p.y,
                                 10.0, 0.0, std::f64::consts::PI * 2.00)
                            .unwrap();
                        if *id == PLAYER_ID {
                            context.set_fill_style(&"#f00".into());        
                        } else {
                            context.set_fill_style(&resources.colour("node_colour").into());        
                        }
                        context.fill();
                    }
                }

            }
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
