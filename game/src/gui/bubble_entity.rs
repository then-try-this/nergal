// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;
use std::any::Any;
use vector2math::*;
use instant::{ Duration };
use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::message::*;
use crate::game::resources::{ Resources };
use crate::rendering::rendering::*;
use crate::game::constants::*;

use crate::declare_any;
use crate::declare_message_reader;



const BUBBLE_PADDING_Y: f64 = 30.;
const BUBBLE_TEXT_WIDTH: f64 = 180.;

#[derive(Debug, Clone, Copy)]
pub enum Direction {
    Left,
    Right,
    Pointless
}

/// A temporary speech bubble entity in game space, that follows a character around.
#[derive(Debug, Clone)]
pub struct BubbleEntity {
    pub id: EntityId,
    pub parent: EntityId,
    pub recipient: EntityId,
    pub message_lines: Rc<Vec<String>>,
    pub pos: Vec2,
    pub timer: u128,
    pub direction: Direction,
    pub size: Vec2,
    pub lifetime: u128
}

impl BubbleEntity {
    pub fn new(id: EntityId, pos: Vec2, parent: EntityId, recipient: EntityId, message: &str) -> Self {
        let message_lines = Rc::new(text_layout_pixels(message, BUBBLE_TEXT_WIDTH, "20px capriola"));
        let line_height = 25.;                    
        let text_height = message_lines.len() as f64 * line_height;
        let bubble_height = 103.; // from image
        let num_lines = message_lines.len();
        
        BubbleEntity {
            id,
            parent,
            recipient,
            message_lines,
            pos,
            timer: 0,
            direction: Direction::Left,
            size: Vec2::new(200., text_height + bubble_height - 40.),
            lifetime: if num_lines < 6 {
                BUBBLE_LIFETIME_MILLIS
            } else if num_lines > 7 {                    
                BUBBLE_LIFETIME_MILLIS_VLONG
            } else {
                BUBBLE_LIFETIME_MILLIS_LONG
            }
        }
    }

    pub fn bb_for(&self, pos: Vec2) -> (Vec2, Vec2) {
        let x = match self.direction {
            Direction::Left => pos.x - 200. + 20.,
            Direction::Right => pos.x - 20.,
            Direction::Pointless => pos.x - 200. + 20.0
        };
        let p = Vec2::new(x, pos.y - self.size.y);        
        (p, p.add(self.size))        
    }

    pub fn bb_to_pos(&self, bb_pos: Vec2) -> Vec2 {
        let x = match self.direction {
            Direction::Left => bb_pos.x + 200. - 20.,
            Direction::Right => bb_pos.x + 20.,
            Direction::Pointless => bb_pos.x + 200. - 20.0
        };
        Vec2::new(x, bb_pos.y + self.size.y) 
    }
}

impl Entity for BubbleEntity {

    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    fn bb(&self) -> (Vec2, Vec2) {
        self.bb_for(self.pos())
    }

    fn describe(&self, _resources: &Resources) -> String {
        self.message_lines.iter().fold(
            String::new(), |acc, line| {
                format!("{} {}", acc, line)
            }
        )
    }
    
    fn update(&self, entities: &EntityRefVec, resources: &Resources, delta: &Duration) -> (Box<dyn Entity>,MessageVec) {

        // find out where the recipient of this speech is,
        // so that we can orient the bubble correctly
        let recipient_pos = if self.recipient == PLAYER_ID {
            resources.world_state.player_pos
        } else if let Some(e) = self.find_entity(self.recipient, entities) {
            e.pos()
        } else {
            Vec2::zero()
        };
        
        let mut new_pos = if let Some(p) = self.find_entity(self.parent, entities) {
            // position based on owners bbox
            let bb = p.bb();
            Vec2::new(
                bb.0.x + (bb.1.x - bb.0.x) / 2.,
                bb.0.y - BUBBLE_PADDING_Y
            )
        } else {
            self.pos
        };

        // get the bounding box for the new position
        let new_bb = self.bb_for(new_pos);
        // modify the position if it's offscreen
        // todo: new graphics needed if this is the case
        // as the point doesn't line up any more
        let mut screen_box = resources.world_state.screen_box();
        // move top lower so we don't go behind gui elements on top of
        // screen
        screen_box.0.y += 80.;
        screen_box.1.y -= 80.;
        new_pos = self.bb_to_pos(
            constrain_position((new_bb.0, new_bb.1.sub(new_bb.0)),
                               screen_box)
        );
        
        (Box::new(BubbleEntity{
            pos: new_pos,            
            timer: self.timer+delta.as_millis(),
            message_lines: self.message_lines.clone(),
            direction: if !enclosed_by((new_bb.0, new_bb.1.sub(new_bb.0)), screen_box) {
                // if we've been moved away from our start position
                // the point will no longer point at the nergal so
                // remove it
                Direction::Pointless
            } else if self.pos.x < recipient_pos.x {
                Direction::Left
            } else {
                Direction::Right
            },
            ..*self }),
         if self.timer > if resources.theme == Theme::Daytime { self.lifetime } else { self.lifetime * NIGHTTIME_TALK_MULTIPLIER } {
             vec![
                 Message::new(MessageType::RemoveMe, self.id(), WORLD_ID)
             ]
         } else {
             vec![]
         })
    }

    fn apply_message(&self, _msg: &Message, _resources: &Resources) -> Box<dyn Entity> {
        Box::new(self.clone())
    }

    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        // skip rendering first frame before we are positioned
        // properly
        if self.timer == 0 { return };

        match pass {
            RenderPass::GUI => {
                if let Some(image) = resources.get_sprite_from_path(match self.direction {
                    Direction::Left => "/images/gui/bubble-left.png",
                    Direction::Right => "/images/gui/bubble-right.png",
                    Direction::Pointless => "/images/gui/bubble-pointless.png",
                }) {
                    let line_height = 25.;                    
                    let text_height = self.message_lines.len() as f64 * line_height;
                    
                    let depth = text_height - 40.;
                    
                    let w = image.width() as f64;
                    let h = image.height() as f64;
                    
                    let x = match self.direction {
                        Direction::Left => self.pos.x - w + 20.,
                        Direction::Right => self.pos.x - 20.,
                        Direction::Pointless => self.pos.x - w / 2.                            
                    };

                    // position such that the pointy bit is at head height
                    let y = self.pos.y - (h + depth);
                    
                    let split = h/2.;

                    // Draw top part of bubble
                    context.draw_image_with_html_image_element_and_sw_and_sh_and_dx_and_dy_and_dw_and_dh(
                        image,
                        0., 0., w, split,
                        x, y, w, split
                    );

                    // Draw expanding middle slice
                    context.draw_image_with_html_image_element_and_sw_and_sh_and_dx_and_dy_and_dw_and_dh(
                        image,
                        0., split, w, 1.,
                        x, y+split, w, depth
                    );

                    // Draw buttom half with point
                    context.draw_image_with_html_image_element_and_sw_and_sh_and_dx_and_dy_and_dw_and_dh(
                        image,
                        0., split, w, split,
                        x, y+split+depth, w, split
                    );

                    // Draw the text
                    context.set_font(resources.attr("default_font")); 
                    context.set_fill_style(&resources.colour("bubble_text").into());

                    let middle = self.size.x/2.;
                    for (i,line) in self.message_lines.iter().enumerate() {                    
                        let text_middle = text_width(context, line)/2.;
                        context.fill_text(line,
                                          x - text_middle + middle + 3.,
                                          self.pos.y + (i as f64 * line_height) - text_height - 20.).unwrap();
                    }
                }
                
                
            }
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
