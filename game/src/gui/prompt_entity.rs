// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;
use std::any::Any;
use vector2math::*;
use instant::{ Duration };
use std::collections::HashSet;
use std::collections::VecDeque;
use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::message::*;
use crate::game::resources::{ Resources };
use crate::rendering::rendering::*;
use crate::sound::speech::*;
use crate::game::constants::*;

use crate::declare_any;
use crate::declare_message_reader;

const LIFETIME_MILLIS: u128 = 5000;
const PROMPT_TEXT_WIDTH: f64 = 900.;

#[derive(Eq, Hash, PartialEq, Debug, Copy, Clone)]
pub enum PlayerExperience {
    TalkedToNergal,
    TalkedToPlant,
    TalkedToCloud,
    MovedAroundWorld,
    MadeFirstFriend,
    MadeFriends,
    AteSnack,
    DiseaseIntroduced,
    CaughtDisease,
    Recovered,
    LostHealthFromDisease,
    TimeWarning,
    HealthWarning,
    GameOverNoHealth,
    OutsideWorld,
    NoExperience,
}

#[derive(Debug, Copy, Clone)]
pub enum PromptPriority {
    High,
    Low
}

#[derive(Debug, Clone)]
pub struct Prompt {
    pub message: String,
    pub message_lines: Rc<Vec<String>>,
    pub priority: PromptPriority,
    pub spoken: bool
}

impl Prompt {
    pub fn new(message: String, priority: PromptPriority) -> Self {
        Prompt {
            message: message.clone(),
            message_lines: Rc::new(
                text_layout_pixels(&message, PROMPT_TEXT_WIDTH, "30px capriola")
            ),
            priority,
            spoken: false
        }
    }

    pub fn spoken(message: String, priority: PromptPriority) -> Self {
        let mut ret = Prompt::new(message, priority);
        ret.spoken = true;
        ret
    }
}

/// An entity to display prompts to the user
#[derive(Debug, Clone)]
pub struct PromptEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub timer: u128,
    pub prompts: Rc<VecDeque<Prompt>>,
    pub player_experience: Rc<HashSet<PlayerExperience>>,
    pub latest_experience: PlayerExperience,
    pub last_prompt_text: String
}

impl PromptEntity {
    pub fn new(id: EntityId, pos: Vec2, prompts: Rc<Vec<Prompt>>) -> Self {        
        PromptEntity {
            id,
            pos,
            timer: 0,
            prompts: VecDeque::from((*prompts).clone()).into(),
            player_experience: Rc::new(HashSet::new()),
            latest_experience: PlayerExperience::NoExperience,
            last_prompt_text: "".into()
        }
    }

    pub fn first_time(&self, experience: PlayerExperience) -> PlayerExperience {
        if self.player_experience.contains(&experience) {
            PlayerExperience::NoExperience
        } else {
            experience
        }
    }

    pub fn first_time_or(&self, experience_a: PlayerExperience, experience_b: PlayerExperience) -> PlayerExperience {
        if self.player_experience.contains(&experience_a) {
            experience_b
        } else {
            experience_a
        }
    }

    pub fn push_override(&self, prompts: &mut VecDeque<Prompt>, new_msg: &str, remove_msg: &str) {
        prompts.retain(|p| p.message!=remove_msg);
        prompts.retain(|p| p.message!=new_msg);
        prompts.push_front(Prompt::new(new_msg.into(), PromptPriority::High));
    }
}

impl Entity for PromptEntity {

    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    
    fn describe(&self, _resources: &Resources) -> String {
        if !self.prompts.is_empty() {
            self.prompts[0].message_lines.iter().fold(
                String::new(), |acc, line| {
                    format!("{} {}", acc, line)
                }
            )
        } else {
            "".into()
        }        
    }
    
    fn update(&self, _entities: &EntityRefVec, resources: &Resources, delta: &Duration) -> (Box<dyn Entity>,MessageVec) {

        // remove old prompts
        let mut new_prompts: VecDeque<Prompt> = (*self.prompts).clone();

        if self.timer > LIFETIME_MILLIS && !new_prompts.is_empty() {
            new_prompts.pop_front();
        }

        // add new ones based on the latest (new) experience
        match self.latest_experience {
            PlayerExperience::TalkedToNergal => {
                new_prompts.push_front(
                    Prompt::new("Being sociable is good for your health.".into(), PromptPriority::High),
                );
            },
            PlayerExperience::TalkedToPlant => {}
            PlayerExperience::TalkedToCloud => {}
            PlayerExperience::MovedAroundWorld => {}
            PlayerExperience::MadeFirstFriend => {
                new_prompts.push_front(
                    Prompt::new("Friends sometimes give you snacks!".into(), PromptPriority::High),
                );
                new_prompts.push_front(
                    Prompt::new("You just made a new friend!".into(), PromptPriority::High),
                );
            }
            PlayerExperience::MadeFriends => {
                new_prompts.push_front(
                    Prompt::new("You just made a new friend!".into(), PromptPriority::High),
                );
            }
            PlayerExperience::AteSnack => {
                new_prompts.push_front(
                    Prompt::new("Yum! Snacks increase your energy.".into(), PromptPriority::High),
                );
            }
            PlayerExperience::DiseaseIntroduced => {
                new_prompts.push_front(
                    Prompt::new("There’s a disease going around… be careful!".into(), PromptPriority::High),
                );
            }
            PlayerExperience::CaughtDisease => {
                self.push_override(
                    &mut new_prompts,
                    "Oh no, you caught a bug from another nergal!",
                    "You got better, and are no longer ill."
                ); 
            }
            PlayerExperience::Recovered => {
                self.push_override(
                    &mut new_prompts,
                    "You got better, and are no longer ill.",
                    "Oh no, you caught a bug from another nergal!"
                ); 
            }
            PlayerExperience::LostHealthFromDisease => {
                new_prompts.push_front(
                    Prompt::new("Oh no, diseases reduce your energy!".into(), PromptPriority::High),
                );
            }
            PlayerExperience::TimeWarning => {
                new_prompts = vec![
                    Prompt::new("Only 10 seconds left!".into(), PromptPriority::High),
                ].into();
            }
            PlayerExperience::HealthWarning => {
                new_prompts.push_front(
                    Prompt::new(format!("You only have {}% health remaining!", HEALTH_WARNING_PERCENT as u32), PromptPriority::High),
                );
            }
            PlayerExperience::GameOverNoHealth => {
                new_prompts = vec![
                    Prompt::new("Game over, you ran out of health!".into(), PromptPriority::High),
                ].into();
            }
            PlayerExperience::OutsideWorld => {
                let msg = "You've reached the edge of the world!";
                // say it now, so each time we press the key it speaks
                resources.speech.say(Voice::Narrator, msg);
                
                // but avoid duplicates with this one, as it fills up the queue
                if new_prompts.is_empty() || new_prompts[0].message != msg {
                    new_prompts.push_front(
                        Prompt::spoken(msg.into(), PromptPriority::High),
                    );
                }
            }
            PlayerExperience::NoExperience => {}                    
        }

        if resources.world_state.conversation_happening {
            // completely pause the prompts if we are having a conversation
            // but still collect any new prompts to show later on
            return (Box::new(
                PromptEntity {
                    prompts: new_prompts.clone().into(),
                    latest_experience: PlayerExperience::NoExperience,
                    ..self.clone()
                }
            ), vec![])
        }
        
        let messages = if !new_prompts.is_empty() && !new_prompts[0].spoken {
            new_prompts[0].spoken = true;
            vec![Message::new(MessageType::AddSpeechToQueue(Voice::Narrator, new_prompts[0].message.clone()), self.id, WORLD_ID)]
        } else {
            vec![]
        };
        
        (Box::new(PromptEntity{
            timer: if self.timer > LIFETIME_MILLIS || self.latest_experience != PlayerExperience::NoExperience {
                0
            } else {
                self.timer+delta.as_millis()
            },
            prompts: new_prompts.clone().into(),
            latest_experience: PlayerExperience::NoExperience,
            last_prompt_text: if !new_prompts.is_empty() {
                new_prompts[0].message.clone()
            } else {
                self.last_prompt_text.clone()
            },
            ..self.clone()}),         
         messages)
    }
    
    fn apply_message(&self, msg: &Message, _resources: &Resources) -> Box<dyn Entity> {
        // convert message to experiences
        let new_experience = match &msg.data {            
            MessageType::TalkTo(_id, entity, _pos) => {
                match entity.as_str() {
                    "nergal" => self.first_time(PlayerExperience::TalkedToNergal),
                    "plant" => self.first_time(PlayerExperience::TalkedToPlant),
                    _ => self.first_time(PlayerExperience::TalkedToCloud)
                }
            }
            
            MessageType::AddRelationship(_id) => {
                self.first_time_or(PlayerExperience::MadeFirstFriend,
                                   PlayerExperience::MadeFriends)
            }
            
            MessageType::SendGift(_gift, _id) => {
                self.first_time(PlayerExperience::AteSnack)                
            }
            
            MessageType::InfectionSuccess(_id) => {
                if msg.sender == PLAYER_ID {
                    // do this every time
                    PlayerExperience::CaughtDisease
                } else {
                    self.first_time(PlayerExperience::DiseaseIntroduced)
                }
            }

            MessageType::Recovered => {
                if msg.sender == PLAYER_ID {
                    PlayerExperience::Recovered
                } else {
                    PlayerExperience::NoExperience
                } 
            }

            MessageType::HealthChange(_change, reason) => {
                if msg.recipient == PLAYER_ID && *reason == HealthReason::Disease {
                    // do this every time
                    self.first_time(PlayerExperience::LostHealthFromDisease)
                } else {
                    PlayerExperience::NoExperience
                }
            }                
            
            MessageType::TimeWarning => {
                PlayerExperience::TimeWarning
            }

            MessageType::PlayerOutsideWorld => {
                PlayerExperience::OutsideWorld
            }

            MessageType::PlayerHealthWarning => {
                PlayerExperience::HealthWarning
            }
            
            MessageType::PlayerDied => {
                PlayerExperience::GameOverNoHealth
            }

            _ => { PlayerExperience::NoExperience }
        };

        match &msg.data {
            MessageType::GameOver(_reason) => {
                Box::new(PromptEntity {
                    // clear old prompts when a new game is started
                    prompts: Rc::new(vec![].into()),
                    latest_experience: PlayerExperience::NoExperience,
                    ..self.clone()
                })
            },
            
            _ => {
                if new_experience != PlayerExperience::NoExperience {
                    let mut new_experiences = (*self.player_experience).clone();
                    new_experiences.insert(new_experience);
                    
                    Box::new(PromptEntity {
                        player_experience: new_experiences.into(),
                        latest_experience: new_experience,
                        ..self.clone()
                    })
                } else {
                    Box::new(self.clone())
                }
            }
        }
    }

    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        if self.prompts.is_empty() { return; }

        match pass {
            RenderPass::GUI => {
                let line_height = 30.;
                let box_height = line_height * (self.prompts[0].message_lines.len() as f64 + 1.);                    
                let padding = 20.;
                
                context.begin_path();
                context.round_rect_with_f64(self.pos.x - padding,
                                            self.pos.y - (line_height + padding / 2.),
                                            PROMPT_TEXT_WIDTH + padding * 2.,
                                            box_height, 39.);
                context.set_fill_style(&resources.colour("prompt_bg").into());
                context.fill();

                // Draw the text                
                context.set_font("30px capriola"); 
                context.set_fill_style(&resources.colour("prompt_text").into());
                let line_height = 30.;
                for (i,line) in self.prompts[0].message_lines.iter().enumerate() {                    
                    let width = text_width(context, line);
                    context.fill_text(line,
                                      self.pos.x - width /2. + PROMPT_TEXT_WIDTH / 2.,
                                      self.pos.y + i as f64 * line_height).unwrap();
                }
            }
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
