// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;
use std::any::Any;
use vector2math::*;
use instant::{ Duration };
use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::message::*;
use crate::game::constants::*;
use crate::game::message::MessageType::*;
use crate::game::resources::{ Resources };
use crate::rendering::rendering::{ RenderPass, constrain_position };
use crate::gui::menu_item::{ MenuItem, MENU_ITEM_WIDTH, Order };
use crate::sound::speech::{ Voice };

use crate::declare_any;
use crate::declare_message_reader;

const LIFETIME_MILLIS: u128 = 15000;
const TIME_PER_ITEM_MILLIS: u128 = 50;
const MENU_PADDING_Y: f64 = 50.;
const ITEM_PADDING_Y: f64 = 10.;

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum MenuStyle {
    Game,
    Dropdown
}

/// A floating menu that sends messages on mouse clicks
#[derive(Debug, Clone)]
pub struct MenuEntity {
    pub id: EntityId,
    pub desc: String,
    pub style: MenuStyle,
    pub parent: EntityId,
    pub items: Rc<Vec<MenuItem>>,
    pub clicked: bool,
    pub selected: usize,
    pub hover: i32,
    pub pos: Vec2,
    pub y_offset: f64,
    pub timer: u128,
    pub size: Vec2,
    // used both in the world and screens
    pub system_id: EntityId
}

impl MenuEntity {
    pub fn new(id: EntityId, desc: String, pos: Vec2, y_offset: f64, parent: EntityId, system_id: EntityId, items: Rc<Vec<MenuItem>>) -> Self {

        let mut height = 0.;
        for item in &*items {
            height += item.height() + ITEM_PADDING_Y;
        }        

        let size = Vec2::new(MENU_ITEM_WIDTH, height);

        // although we overwrite this in the first update, it's needed to be
        // correct so we get put in the right bucket
        let mut new_pos = pos;
        new_pos.x -= MENU_ITEM_WIDTH / 2.;
        new_pos.y -= size.y;
        new_pos.y -= y_offset + MENU_PADDING_Y;                      

        let mut sorted_items = (*items).clone();
        // check for yes/no
        if sorted_items.len()==2 {
            if sorted_items[0].text.starts_with("Yes") && sorted_items[1].text.starts_with("No") || sorted_items[1].text.starts_with("Yes") && sorted_items[0].text.starts_with("No") {                
                // make Yes first
                sorted_items.sort_by(|a, b| b.text.cmp(&a.text));
            }
        }
        
        MenuEntity {
            id,
            desc,
            style: if system_id == WORLD_ID {
                MenuStyle::Game
            } else {
                MenuStyle::Dropdown
            },
            parent,
            items: sorted_items.into(),
            clicked: false,
            selected: 0,
            hover: -1,
            pos: new_pos,
            y_offset,
            timer: 0,
            size,
            system_id
        }
    }

    /*pub fn add_item(&mut self, name: String, msg: Rc<Vec<Message>>) {
        if let Some(items) = Rc::get_mut(&mut self.items) {
            items.push(MenuItem::new(name, msg));
        }
    }*/

    pub fn bb_for(&self, pos: Vec2) -> (Vec2, Vec2) {
        let p = Vec2::new(pos.x, pos.y);        
        (p, p.add(self.size))        
    }    
}

impl Entity for MenuEntity {

    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    fn cat(&self) -> EntityCat { EntityCatFlags::MENU }
    fn bb(&self) -> (Vec2, Vec2) {
        (self.pos, self.pos.add(self.size))
    }

    fn describe(&self, _resources: &Resources) -> String {
        if self.items.len() > 1 {
            format!("{} with {} items", self.desc, self.items.len())
        } else {
            format!("{} with {} item", self.desc, self.items.len())
        }
    }
    
    fn update(&self, entities: &EntityRefVec, resources: &Resources, delta: &Duration) -> (Box<dyn Entity>,MessageVec) {
        let mut new_pos = self.pos;

        if let Some(p) = self.find_entity(self.parent, entities) {
            if self.style == MenuStyle::Game {
                new_pos = p.pos();
                new_pos.x -= MENU_ITEM_WIDTH / 2.;
                new_pos.y -= self.size.y;
                new_pos.y -= self.y_offset + MENU_PADDING_Y;                                      
            } else {
                let bb = p.bb();
                new_pos = bb.0.add(bb.1).mul(0.5);
                new_pos.x -= MENU_ITEM_WIDTH / 2.;
                //new_pos.y -= self.size.y;
                new_pos.y -= self.y_offset + MENU_PADDING_Y;                                      
            }
        }

        // make sure we stay on the screen
        if self.style == MenuStyle::Game {
            // get the bounding box for the new position
            let new_bb = self.bb_for(new_pos);
            // modify the position if it's offscreen
            // todo: new graphics needed if this is the case
            // as the point doesn't line up any more
            let mut screen_box = resources.world_state.screen_box();
            // move top lower so we don't go behind gui elements on top of
            // screen
            screen_box.0.y += 80.;
            screen_box.1.y -= 80.;
            new_pos = constrain_position((new_bb.0, new_bb.1.sub(new_bb.0)),
                                         screen_box);
        }
        
        // pick up clicked
        if self.clicked {
            if self.selected<self.items.len() {
                // pick up the messages from the menu item
                let mut messages = self.items[self.selected].messages.to_vec();
                // add one to remove the menu
                messages.push(Message::new(RemoveMe, self.id(), self.system_id));
                // one to tell the world the menu has gone
                messages.push(Message::new(MenuGone, self.id(), self.system_id));
            
                // send selected messages out
                return (Box::new(MenuEntity{
                    clicked: false,
                    selected: 0,
                    ..self.clone()
                }), msg_cons(
                    &messages,
                    self.play_sound_msg(
                        resources, "/sounds/notify-2.mp3", 1.0
                    )
                ));
            } else {
                // clicked outside the menu, remove it
                // in case of double clicking, delay this happening
                if self.timer > 100 {
                    return (Box::new(MenuEntity{items: self.items.clone(), ..self.clone() }),                     
                            vec![
                                Message::new(RemoveMe, self.id(), self.system_id),
                                // tell both the owner and the world
                                // the menu has finished
                                Message::new(MenuGone, self.id(), self.parent),
                                Message::new(MenuGone, self.id(), self.system_id),
                                Message::new(ConversationOver, self.id, WORLD_ID)
                            ]);
                }
            }            
        }
        
        (Box::new(MenuEntity{
            pos: new_pos,
            timer: self.timer+delta.as_millis(),
            ..self.clone() }),
         if self.timer > if resources.theme == Theme::Daytime { LIFETIME_MILLIS } else { LIFETIME_MILLIS * NIGHTTIME_TALK_MULTIPLIER } {
             vec![
                 Message::new(RemoveMe, self.id(), self.system_id),
                 Message::new(MenuGone, self.id(), self.system_id),
                 // we will be sending these for timeouts on entity menus as well
                 // as player (we could check) but this doesn't matter, we just need
                 // to pick them up in the world to clear the convo flag
                 Message::new(ConversationOver, self.id, WORLD_ID)
             ]
         } else {
             vec![]
         })
    }

    /// Check mouse clicks against our menu items
    fn apply_message(&self, msg: &Message, resources: &Resources) -> Box<dyn Entity> {
        match &msg.data {
            MouseClick(screen_pos, world_pos, _) => {
                let mut item_pos = self.pos; 
                let click = match self.style {
                    MenuStyle::Game => world_pos,
                    MenuStyle::Dropdown => screen_pos
                };
                
                if click.inside_box(self.bb()) {
                    // check each menu item...
                    for (index, item) in (*self.items).iter().enumerate() {
                        if item.inside(item_pos, *click) {
                            //console::log_1(&format!("picked {}",index).into());
                            
                            return Box::new(MenuEntity {
                                clicked: true,
                                selected: index,
                                ..self.clone()
                            })
                        }
                        item_pos.y += item.size.y() + ITEM_PADDING_Y;
                    }
                } else {
                    return Box::new(MenuEntity {
                        clicked: true,
                        selected: self.items.len(),
                        ..self.clone()
                    })
                }   
            }

            MouseMove(screen_pos, world_pos, _) => {
                let mut item_pos = self.pos;                 

                let click = match self.style {
                    MenuStyle::Game => world_pos,
                    MenuStyle::Dropdown => screen_pos
                };

                if click.inside_box(self.bb()) {
                    // check each menu item...
                    for (index, item) in (*self.items).iter().enumerate() {
                        if item.inside(item_pos, *click) {
                            return Box::new(MenuEntity {
                                hover: index as i32,                                
                                ..self.clone()
                            })
                        }
                        item_pos.y += item.size.y();
                    }
                } else {
                    return Box::new(MenuEntity {
                        hover: -1,
                        ..self.clone()
                    })
                }   
            }

            MessageType::KeyPress(key) => {
                if is_next_key(key) {
                    let hover = if self.hover as usize >= self.items.len()-1 {
                        0
                    } else {
                        self.hover + 1
                    }; 

                    resources.speech.say(Voice::Narrator, &self.items[hover as usize].text);
                    
                    return Box::new(MenuEntity {
                        hover,
                        ..self.clone()
                    })                                     
                }
                if is_select_key(key) && self.hover != -1 {
                    return Box::new(MenuEntity {
                        clicked: true,
                        selected: self.hover as usize,
                        ..self.clone()
                    })
                }
            }
            
            _ => {}
        }
        
        Box::new(self.clone())
    }

    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, delta: &Duration) {        
        match pass {
            RenderPass::GUI => {

                let mut pos = self.pos;

                let num_items_to_draw = (self.timer / TIME_PER_ITEM_MILLIS) as usize;
                
                context.set_font(resources.attr("default_font")); 
                context.set_fill_style(&resources.colour("default_text").into());
                for (i,item) in (*self.items).iter().enumerate() {
                    if i<num_items_to_draw {
                        let order = match self.style {
                            MenuStyle::Dropdown => { Order::Dropdown },
                            MenuStyle::Game => {
                                if i==0 {
                                    if self.items.len() == 1 {
                                        Order::Only
                                    } else {
                                        Order::First
                                    }                        
                                } else if i==self.items.len()-1 {
                                    Order::Last
                                } else {
                                    Order::Middle
                                }
                            }
                        };
                        item.render(context, pos, order, i as i32 == self.hover, pass, resources, delta);
                        pos.y += item.size.y + ITEM_PADDING_Y;
                    }
                }                
            }
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
