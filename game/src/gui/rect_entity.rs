// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::rc::Rc;
use std::any::Any;
use vector2math::*;
use instant::{ Duration };
use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };
use crate::game::colour::{ Colour };
use crate::game::entity::*;
use crate::game::message::{ Message, MessageType, MessageVec };
use crate::game::resources::{ Resources };
use crate::rendering::rendering::*;

use crate::declare_any;
use crate::declare_message_reader;

pub enum GfxEntityData {
    Rect(Vec2,Colour),
    Image(String),
    Text(String)
}

use web_sys::console;

/// A low level entity for general purpose graphics in the gui
#[derive(Debug, Clone, Copy)]
pub struct GfxEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub data: GfxEntityData
}

impl GfxEntity {
    pub fn new(id: EntityId, pos: Vec2, data: GfxEntityData) -> Self {
        GfxEntity {
            id,
            pos,
            data
        }
    }
}

impl Entity for GfxEntity {
    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    
    fn update(&self, entities: &EntityRefVec, _connected: &Vec<EntityId>, delta: &Duration) -> (Box<dyn Entity>,MessageVec) {
        (Box::new(*self), vec![])
    }

    fn apply_message(&self, msg: &Message, _connected: &Vec<EntityId>) -> Box<dyn Entity> {
        Box::new(*self)
    }
    
    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        match pass {
            RenderPass::GUI => {

                match self.data {
                    GfxEntityData::Rect(size, colour) => {
                        context.set_stroke_style(&"#000".into());
                        context.begin_path();
                        context.rect(self.pos.x, self.pos.y, size.x, size.y);
                        context.set_fill_style(&colour.as_hex().into());
                        context.fill();
                    }
                    _ => {}
                }
            }
            _ => {}
        }
    }
    
    declare_any!();
    declare_message_reader!();
}
