// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


use std::any::Any;
use vector2math::*;
use instant::{ Duration };
use crate::game::id::*;
use crate::maths::vec2::{ Vec2 };

use crate::game::entity::*;
use crate::game::message::*;
use crate::game::resources::{ Resources };
use crate::rendering::rendering::*;
use crate::gui::screen_manager::{ ScreenState };


use crate::declare_any;
use crate::declare_message_reader;



const WARNING_TIME: u128 = 10*1000;

/// A game timer to show how long you have survived.
#[derive(Debug, Copy, Clone)]
pub struct ClockEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub max_time: u128,
    pub warning_time: u128,
    pub warning_fired: bool        
}

impl ClockEntity {
    pub fn new(id: EntityId, pos: Vec2, max_time: u128) -> Self {
        ClockEntity {
            id,
            pos,
            max_time,
            warning_time: max_time - WARNING_TIME,
            warning_fired: false
        }
    }
}

impl Entity for ClockEntity {

    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    
    fn update(&self, _entities: &EntityRefVec, resources: &Resources, _delta: &Duration) -> (Box<dyn Entity>,MessageVec) {
        let time = resources.world_state.game_time;
        
        (Box::new(ClockEntity {
            warning_fired: time > self.warning_time, 
            ..*self
        }), if time > self.max_time && resources.world_state.player_state != EntityState::Dead {
            // if we are dead, wait for the player entity to finish the game
            vec![
                Message::new(
                    MessageType::GameOver(GameOverReason::OutOfTime),
                    self.id, WORLD_ID
                ),                             
                Message::new(
                    MessageType::ScreenChange(ScreenState::GameOver(GameOverReason::OutOfTime)),
                    self.id, SCREEN_MANAGER_ID
                )
            ]
        } else if time > self.warning_time && !self.warning_fired {
            vec![Message::new(MessageType::TimeWarning, self.id, WORLD_ID)]
        } else {
            vec![]
        })
    }

    fn apply_message(&self, _msg: &Message, _resources: &Resources) -> Box<dyn Entity> {
        Box::new(*self)
    }
    
    #[allow(unused_must_use)]
    fn render(&self, context: &web_sys::CanvasRenderingContext2d, pass: RenderPass, resources: &Resources, _delta: &Duration) {        
        match pass {
            RenderPass::GUI => {
                context.set_font("30px capriola"); 
                context.set_fill_style(&resources.colour("clock").into());
                context.begin_path();
                
                let _t = resources.world_state.game_time as f64 / self.max_time as f64;

                context.move_to(self.pos.x, self.pos.y);
                context.arc(self.pos.x, self.pos.y,
                            50.,
                            f64::PI * -0.5 +
                            (resources.world_state.game_time as f64 * f64::PI * 2.) / self.max_time as f64,
                            f64::PI * -0.5,);
                
                context.close_path();
                context.fill();
                context.set_stroke_style(&resources.colour("default_text").into());
                context.stroke();
                context.set_line_width(2.);
                context.set_fill_style(&resources.colour("default_text").into());
                context.fill_text("Time left",
                                  self.pos.x - 60.,
                                  self.pos.y + 75.).unwrap();
            }            
            _ => {}
        }

    }
    
    declare_any!();
    declare_message_reader!();
}
