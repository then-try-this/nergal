// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::f64;
use wasm_bindgen::prelude::*;
mod game;
use crate::game::network;
//use crate::game::vec2::{ Vec2 };
use crate::game::entity::{ EntityState, PLAYER_ID };
use crate::game::player_entity::PlayerEntity;
use crate::game::social_entity::SocialEntity;

use web_sys::console;

#[wasm_bindgen]
pub struct Game {
    net: network::Network,
}

#[wasm_bindgen]
impl Game {
    
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        console::log_1(&"Hello from nergal".into());

        let mut net = network::Network::simple();

        net.get_entity_mut_as::<SocialEntity>(3).unwrap().memory.push("🍈".into());
        net.get_entity_mut_as::<SocialEntity>(14).unwrap().memory.push("🍎".into());
        net.get_entity_mut_as::<SocialEntity>(16).unwrap().memory.push("🥥".into());
        net.get_entity_mut_as::<SocialEntity>(20).unwrap().memory.push("🥝".into());
        net.get_entity_mut_as::<SocialEntity>(29).unwrap().memory.push("🫐".into());
        Game{ net }
    }

    #[wasm_bindgen]
    pub fn handle(&mut self, key: String) {
        if key=="a" {
            self.net.get_entity_mut_as::<PlayerEntity>(PLAYER_ID).unwrap().pos.x+=-2.0;
        }
        if key=="d" {
            self.net.get_entity_mut_as::<PlayerEntity>(PLAYER_ID).unwrap().pos.x+=2.0;
        }
        if key=="w" {
            self.net.get_entity_mut_as::<PlayerEntity>(PLAYER_ID).unwrap().pos.y+=-2.0;
        }
        if key=="s" {
            self.net.get_entity_mut_as::<PlayerEntity>(PLAYER_ID).unwrap().pos.y+=2.0;
        }
        if key==" " {
            self.net.get_entity_mut_as::<PlayerEntity>(PLAYER_ID).unwrap().state=EntityState::Speak;
        }
        if key=="x" {
            
            
            //self.net.relationships = self.net.disconnect_all(self.net.relationships,PLAYER_ID);
        }

    }
    
    #[wasm_bindgen]
    pub fn render(&mut self) {

        let document = web_sys::window().unwrap().document().unwrap();
        let canvas = document.get_element_by_id("canvas").unwrap();
        let canvas: web_sys::HtmlCanvasElement = canvas
            .dyn_into::<web_sys::HtmlCanvasElement>()
            .map_err(|_| ())
            .unwrap();

        let context = canvas
            .get_context("2d")
            .unwrap()
            .unwrap()
            .dyn_into::<web_sys::CanvasRenderingContext2d>()
            .unwrap();


        self.net = self.net.update();
        
        context.clear_rect(0.0,0.0,1000.0,500.0);

        //self.net = self.net.relax();
        context.set_font("15px sans");
 
        for rel in &self.net.relationships {

            let from = self.net.get_entity(rel.from).unwrap();
            let to = self.net.get_entity(rel.to).unwrap();
            
            context.begin_path();
            context.move_to(from.pos().x, from.pos().y);
            context.line_to(to.pos().x, to.pos().y);
            context.stroke();
        }

        for entity in &self.net.entities {
            let x=entity.pos().x;
            let y=entity.pos().y;

            if entity.id()==PLAYER_ID {
                context.begin_path();
                context
                    .arc(x,y, 30.0, 0.0, f64::consts::PI * 2.00)
                    .unwrap();
 
                context.set_stroke_style(&"#ddd".into());
                context.stroke();
                context.set_stroke_style(&"#000".into());
            }

            context.begin_path();
            context
                .arc(x,y, 15.0, 0.0, f64::consts::PI * 2.00)
                .unwrap();
            if entity.id()==PLAYER_ID {
                context.set_fill_style(&"#0f0".into());
            } else {
                context.set_fill_style(&"#fff".into());
            }
            context.fill();
            context.set_fill_style(&"#000".into());
            context.stroke();


            if entity.id()==PLAYER_ID {
                context.fill_text("player", x-25.0, y+30.0).unwrap();
            }
            
            context.set_fill_style(&"#f00".into());
            let mut pos=0.0;
            let a = entity.as_any();
            let p = a.downcast_ref::<SocialEntity>();
            match p {
                Some(se) =>
                    for message in &se.memory {
                        context.fill_text(&message, x-10.0, y+pos).unwrap();
                        pos+=10.0;
                    },
                None => {}
            }
            context.set_fill_style(&"#000".into());

            context.fill_text(
                match entity.state() {
                    EntityState::Idle => "",
                    EntityState::Speak => "speak",
                    EntityState::Spoken => "speak",
                    EntityState::StartWalk => "start walk",
                    EntityState::Walk => "walk",
                    EntityState::Avoid => "avoid"
                }, x-25.0, y-25.0).unwrap();

        }


    }
}

#[wasm_bindgen(start)]
fn start() {
}

