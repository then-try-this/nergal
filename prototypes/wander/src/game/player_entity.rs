// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::any::Any;
use vector2math::*;
//use rand::prelude::SliceRandom;
use crate::game::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::message::{ Message, MessageType };

#[derive(Debug,Clone)]
pub struct PlayerEntity {
    pub pos: Vec2,
    pub memory: Vec<String>,
    pub state: EntityState,
}

impl PlayerEntity {
    pub fn new() -> Self {
        PlayerEntity {
            pos: Vec2{x: 100.0, y: 100.0},
            memory: vec![],
            state: EntityState::Idle,
        }   
    }
}

impl Entity for PlayerEntity {
    fn id(&self) -> i32 { PLAYER_ID }
    fn pos(&self) -> Vec2 { self.pos }
    fn state(&self) -> EntityState { self.state }
    fn update(&self, _entities: &EntityVec, _connected: &Vec<i32>) -> (Box<dyn Entity>,Vec<Message>) {
        let memory = self.memory.clone();
        let messages: Vec<Message> = vec![];
        
        match self.state {
            EntityState::Idle => {
                
            },
            EntityState::Speak => {
                return (Box::new(PlayerEntity {
                    state: EntityState::Spoken,
                    memory: memory.clone(),
                    ..*self
                }), vec![Message{
                    sender: self.id(),
                    recipient: -1,
                    name: MessageType::Broadcast,
                    value: "X".into() //memory.choose(&mut rand::thread_rng()).unwrap().clone(),
                }]);
            },
            EntityState::Spoken => {
                return (Box::new(PlayerEntity {                    
                    state: EntityState::Spoken,                    
                    memory,
                    ..*self
                }), messages)
            },
            _ => {}
        }
        return (Box::new(PlayerEntity {
            memory,
            ..*self
        }), messages)
    }

    fn read_messages(&self, messages: &Vec<Message>, connected: &Vec<i32>) -> Box<dyn Entity> {
        // search messages from ids connected to us
        let messages: Vec<&Message> = messages.iter().filter(|msg| {
            for connection in connected {
                if connection == &msg.sender { return true }
            }
            false
        }).collect();

        Box::new(PlayerEntity {
            memory: messages.iter().fold(self.memory.clone(),|mut acc, msg| {
                if msg.name == MessageType::Broadcast {
                    if !self.memory.contains(&msg.value) {
                        acc.push(msg.value.clone());
                    }
                    return acc;
                }             
                acc
            }),                        
            ..*self
        })
    }
    fn as_any(&self) -> &dyn Any { self }
    fn as_any_mut(&mut self) -> &mut dyn Any { self }
}
