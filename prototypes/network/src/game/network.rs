// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use rand::Rng;
use vector2math::*;
use crate::game::vec2::{ Vec2 };
use crate::game::entity::{ Entity, PLAYER_ID };
use crate::game::social_entity::{ SocialEntity };
use crate::game::relationship::{ Relationship };
use crate::game::message::{ Message };
//use web_sys::console;

const DEFAULT_EDGE_LENGTH: f64 = 50.0;
const MAX_SPEED: f64 = 2.0;
const PLAYER_OVERLAP: f64 = 45.0;

#[derive(Debug)]
pub struct Network {
    pub entities: Vec<SocialEntity>,
    pub relationships: Vec<Relationship>,
    pub centre: Vec2,
}

#[allow(dead_code)]
impl Network {
    pub fn new() -> Self {
        Network {
            entities: vec![],
            relationships: vec![],
            centre: Vec2::new(500.0,250.0)
        }
        
    }

    /// Randomised constructor for testing purposes
    pub fn rand() -> Self {
        let mut rng = rand::thread_rng();
        let centre = Vec2::new(500.0,250.0);
        let mut entities = vec![];
        for i in 0..20 {
            entities.push(SocialEntity::at_pos(i,Vec2::new(rng.gen_range(-100.0..100.0),
                                                rng.gen_range(-100.0..100.0)).add(centre)));
        }

        let mut relationships = vec![];
        for i in 0..20 {
            relationships.push(Relationship{
                id: i,
                from: rng.gen_range(0..20),
                to: rng.gen_range(0..20),
                length: DEFAULT_EDGE_LENGTH
            });
        }

        Network {
            entities: entities,
            relationships: relationships,
            centre
        }
        
    }

    /// Simple constructor for testing purposes
    pub fn simple() -> Self {
        let centre =  Vec2::new(500.0,250.0);

        let mut entities = vec![];
        entities.push(SocialEntity::at_pos(PLAYER_ID, Vec2::rnd().add(centre)));

        for i in 0..30 {
            entities.push(SocialEntity::at_pos(i, Vec2::rnd().add(centre)));            
        }
        
        entities[0].memory.push("🍉".into());
        entities[3].memory.push("🍈".into());
        entities[14].memory.push("🍎".into());
        entities[16].memory.push("🥥".into());
        entities[20].memory.push("🥝".into());
        entities[29].memory.push("🫐".into());
            
        Network {
            entities,
            relationships: vec![
                Relationship{id: 1, from: 1, to: 2, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 2, from: 2, to: 3, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 3, from: 3, to: 4, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 4, from: 4, to: 1, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 5, from: 5, to: 1, length: DEFAULT_EDGE_LENGTH},

                Relationship{id: 6, from: 5, to: 6, length: DEFAULT_EDGE_LENGTH},

                Relationship{id: 7, from: 6, to: 7, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 8, from: 7, to: 8, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 9, from: 8, to: 9, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 10, from: 9, to: 10, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 11, from: 10, to: 6, length: DEFAULT_EDGE_LENGTH},

                Relationship{id: 12, from: 10, to: 11, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 13, from: 5, to: 12, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 14, from: 3, to: 13, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 15, from: 8, to: 14, length: DEFAULT_EDGE_LENGTH},

                Relationship{id: 16, from: 15, to: 16, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 17, from: 16, to: 17, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 18, from: 17, to: 18, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 19, from: 18, to: 15, length: DEFAULT_EDGE_LENGTH},


                Relationship{id: 20, from: 20, to: 21, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 21, from: 21, to: 22, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 22, from: 22, to: 23, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 23, from: 23, to: 24, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 24, from: 24, to: 25, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 25, from: 25, to: 26, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 26, from: 26, to: 27, length: DEFAULT_EDGE_LENGTH},
                Relationship{id: 27, from: 27, to: 20, length: DEFAULT_EDGE_LENGTH},

                
            ],
            centre
        }
        
    }

    pub fn get_entity(&self, id: i32) -> Result<&SocialEntity,&str> {
        for entity in &self.entities {
            if entity.id == id {
                return Ok(entity);
            }
        }
        return Err("not found");
    }

    pub fn get_entity_mut(&mut self, id: i32) -> Result<&mut SocialEntity,&str> {
        for entity in &mut self.entities {
            if entity.id == id {
                return Ok(entity);
            }
        }
        return Err("not found");
    }

    pub fn get_entities_nearby(&self, exclude: i32, pos: Vec2, dist: f64) -> Vec<&SocialEntity> {        
        self.entities.iter().fold(vec![],|mut acc,entity| {
            if entity.id!=exclude && entity.pos.sub(pos).mag()<dist {
                acc.push(entity);               
            }
            acc
        })
    }
    
    pub fn connected_entities(&self, entity_id: i32) -> Vec<&SocialEntity> {
        self.relationships.iter().fold(vec![], |mut acc:Vec<&SocialEntity>, edge| {           
            if edge.from==entity_id {
                match self.get_entity(edge.to) {
                    Ok(centity) => { acc.push(centity); return acc; },
                    Err(_) => { return acc; }
                };
            }
            if edge.to==entity_id {
                match self.get_entity(edge.from) {
                    Ok(centity) => { acc.push(centity); return acc; },
                    Err(_) => { return acc; }
                };
            }
            return acc;
        })
    }

/*    pub fn disconnect_all(&self, relationships: Vec<Relationship>, entity_id: i32) -> Vec<Relationship> {
        relationships.iter().filter(|rel| {
            rel.from != entity_id && rel.to != entity_id           
        }).collect()
    }*/
    
    fn relax_entity(&self, entity: &SocialEntity) -> SocialEntity {
        let mut delta = entity.vel.mul(0.99);
        
        delta = delta.add(self.centre.sub(entity.pos).mul(0.0005));

        delta = delta.add(self.entities.iter()
                          .fold(Vec2::zero(),|acc,other_entity| -> Vec2 {
                              if other_entity.id!=entity.id {
                                  // repel from each other  
                                  let r = entity.pos.sub(other_entity.pos);
                                  if r.mag()>0.0 {
                                      return acc.add(r.norm().mul(0.004))
                                  }
                              }
                              acc
                          }));
        
        delta = delta.add(self.connected_entities(entity.id).iter()
                          .fold(Vec2::zero(),|acc,other_entity| -> Vec2 {
                              if other_entity.id!=entity.id {
                                  // try 
                                  let b = other_entity.pos.sub(entity.pos);
                                  let distance = b.mag()-DEFAULT_EDGE_LENGTH;
                                  if distance>0.0 {
                                      return acc.add(b.norm().mul((distance*distance)*0.001))
                                  }
                              }
                              acc
                          }));

        if delta.mag()>MAX_SPEED { delta = delta.norm().mul(MAX_SPEED); }
        
        SocialEntity {
            id: entity.id,
            pos: entity.pos.add(delta),
            vel: delta,
            memory: entity.memory.clone(),
            state: entity.state,
            timer: entity.timer
        }
    }

    
    pub fn relax(&self) -> Self {
        return Network {
            entities: self.entities.iter().map(|entity| {
                if entity.id==PLAYER_ID {
                    entity.clone()
                } else {
                    self.relax_entity(entity)
                }
            }).collect::<Vec<SocialEntity>>(),
            relationships: self.relationships.clone(),
            centre: self.centre
        };                          
    }

    /// add new connections to the player by sniffing the message list
    pub fn add_player_connections(&self,messages: &Vec<Message>, relationships: &Vec<Relationship>) -> Vec<Relationship> {
        let player_msgs: Vec<&Message> = messages.iter().filter(|msg| msg.sender==PLAYER_ID).collect();
        let mut new_relationships = vec![];
        
        // we don't actually need (currently) to look at any other messages
        // as just having one is all we need to trigger this (one day check type)
        if player_msgs.len()>0 {
            let nearby_entities = self.get_entities_nearby(
                PLAYER_ID,
                self.get_entity(PLAYER_ID).unwrap().pos,
                PLAYER_OVERLAP);
            
            let connected_already:Vec<i32> = self.connected_entities(PLAYER_ID).iter()
                .map(|e| e.id).collect();

            if nearby_entities.len()>0 {
                new_relationships = nearby_entities.iter()
                    // remove already existing connections
                    .filter(|e| !connected_already.contains(&e.id))                
                    // create a relationship with every nearby entity
                    .map(|near| 
                        Relationship {
                            id: 999, // not actually used.. fixme
                            from: PLAYER_ID,
                            to: near.id,
                            length: DEFAULT_EDGE_LENGTH
                        }
                    ).collect();                
            }

            let mut newrel = relationships.clone();
            newrel.extend(new_relationships);
            return newrel;
        }
        relationships.clone()
    }
    
    pub fn update(&self) -> Self {
        let mut messages: Vec<Message> = vec![];

        // first pass, update and collect messages
        let updated_entities = self.entities.iter().map(|entity| {
            let (entity,msg) = entity.update();
            messages.extend(msg);
            entity
        }).collect::<Vec<SocialEntity>>();
        
        //console::log_1(&self.relationships.len().into());
        
        Network {
            // second pass - do messages
            entities: updated_entities.iter().map(|entity| {
                entity.update_messages(&messages,
                                       // collapse connected entities into
                                       // list of ids so we don't need to deal
                                       // with the types in entity call
                                       self.connected_entities(entity.id).iter()
                                       .map(|c| c.id).collect())
            }).collect::<Vec<SocialEntity>>(),            
            // snoop on player speak and create new connections
            relationships: self.add_player_connections(&messages,&self.relationships),
            ..*self
        }              
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn basics() {
        let mut n = Network::new();
        n.entities.push(SocialEntity::at_pos(1, Vec2::new(10.0,0.0)));        
        assert_eq!(n.get_entity(1).unwrap().id, 1);
        n.entities.push(SocialEntity::at_pos(2, Vec2::new(-10.0,0.0)));
        assert_eq!(n.get_entity(2).unwrap().id, 2);
        n.relationships.push(Relationship{ id: 1, from: 1, to: 2, length: DEFAULT_EDGE_LENGTH });
        let conn =
            match n.get_entity(1) {
                Ok(entity) => n.connected_entities(entity.id),
                Err(_) => vec![]
            };
        assert_eq!(conn.len(), 1);
        assert_eq!(conn[0].id, 2);        
        n=n.relax();
        // check we have moved
        assert!(n.entities[0].id==1);
        assert!(n.entities[0].pos.x!=10.0);

        assert_eq!(n.get_entities_nearby(0, Vec2::zero(), 20.0).len(),2);
        assert_eq!(n.get_entities_nearby(2, Vec2::zero(), 20.0).len(),1);
        
    }
}
