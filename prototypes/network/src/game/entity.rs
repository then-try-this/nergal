// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use vector2math::*;
use crate::game::message::{ Message };

pub const PLAYER_ID: i32 = 9999;

#[derive(Debug,Clone,Copy,PartialEq)]
pub enum EntityState {
    Idle,
    Speak,
    Spoken
}

pub trait Entity {
    fn new(id:i32) -> Self;
    fn update(&self) -> (Self,Vec<Message>) where Self: Sized;
    fn update_messages(&self,messages: &Vec<Message>, connected: Vec<i32>) -> Self;
//    fn render(&self,context:String);
}

