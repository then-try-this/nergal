// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use rand::Rng;
use vector2math::*;
use rand::prelude::SliceRandom;
use crate::game::vec2::{ Vec2 };
use crate::game::entity::{ Entity, EntityState, PLAYER_ID };
use crate::game::message::{ Message, MessageType };


#[derive(Debug,Clone)]
pub struct SocialEntity {
    pub id: i32,
    pub pos: Vec2,
    pub vel: Vec2,
    pub memory: Vec<String>,
    pub state: EntityState,
    pub timer: i32,
}

impl SocialEntity {
    pub fn at_pos(id: i32, pos: Vec2) -> Self {
        SocialEntity {
            id,
            pos,
            vel: Vec2::zero(),
            memory: vec![],
            state: EntityState::Idle,
            timer: 0
        }   
    }
}

impl Entity for SocialEntity {
    fn new(id: i32) -> Self {
        SocialEntity {
            id,
            pos: Vec2::zero(),
            vel: Vec2::zero(),
            memory: vec![],
            state: EntityState::Idle,
            timer: 0
        }   
    }
    fn update(&self) -> (Self,Vec<Message>) {
        let mut rng = rand::thread_rng();
        let timer = if self.timer>0 { self.timer-1 } else { self.timer };
        let memory = self.memory.clone();
        let messages: Vec<Message> = vec![];
        
        match self.state {
            EntityState::Idle => {
                if self.id!=PLAYER_ID && self.memory.len()>0 && rng.gen_range(0..400)==1 && memory.len()>0 {
                    return (SocialEntity {
                        state: EntityState::Speak,
                        memory: memory.clone(),
                        ..*self
                    }, vec![])
                } 
            },
            EntityState::Speak => {
                return (SocialEntity {
                    state: EntityState::Spoken,
                    timer: 100,
                    memory: memory.clone(),
                    ..*self
                }, vec![Message{
                    sender: self.id,
                    name: MessageType::Broadcast,
                    value: memory.choose(&mut rand::thread_rng()).unwrap().clone(),
                }]);
            },
            EntityState::Spoken => {
                return (SocialEntity {                    
                    state: if self.timer==0 { EntityState::Idle } else { self.state },                    
                    timer,
                    memory,
                    ..*self
                }, messages)
            },
        }
        return (SocialEntity {
            timer,
            memory,
            ..*self
        }, messages)
    }

    fn update_messages(&self, messages: &Vec<Message>, connected: Vec<i32>) -> Self {
        // search messages from ids connected to us
        let messages: Vec<&Message> = messages.iter().filter(|msg| {
            for connection in &connected {
                if connection == &msg.sender { return true }
            }
            false
        }).collect();

        SocialEntity {
            memory: messages.iter().fold(self.memory.clone(),|mut acc, msg| {
                if msg.name == MessageType::Broadcast {
                    if !self.memory.contains(&msg.value) {
                        acc.push(msg.value.clone());
                    }
                    return acc;
                }             
                acc
            }),                        
            ..*self
        }
    }

//    fn render(&self, context:String) {
//    }
}


#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn basics() {
        let mut s = SocialEntity::new(1);
        assert_eq!(s.id, 1);
        assert_eq!(s.state, EntityState::Idle);

        // check triggering of broadcasts
        s.state=EntityState::Speak;
        s.memory.push("A".into());
        let (new_s,messages) = s.update();
        assert_eq!(new_s.state, EntityState::Spoken);
        assert_eq!(messages[0].name, MessageType::Broadcast);
        assert_eq!(messages[0].sender, 1);
        assert_eq!(messages[0].value, "A");
        s = new_s;

        // check adding to memory
        s=s.update_messages(&vec![Message{
            sender: 0,
            name:MessageType::Broadcast,
            value:"X".to_string()
        }],vec![0]);

        assert_eq!(s.memory[1], "X".to_string());        
    }
}



