// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use vector2math::*;
use rand::Rng;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Vec2 {
    pub x: f64,
    pub y: f64,
}

#[allow(dead_code)]
impl Vec2 {
    pub fn new(x: f64, y: f64) -> Self {
        Vec2 { x, y }
    }
    pub fn zero() -> Self {
        Vec2 { x:0.0, y:0.0 }
    }
    pub fn norm(&self) -> Self {
        self.div(self.mag())
    }
//    pub fn dist(&self, other: Vec2) -> f64 {
//        self.sub(other).mag()
//    }
    // avoid a square root for comparisons etc
    pub fn dist_sq(&self, other: Vec2) -> f64 {
        let v = self.sub(other);
        v.x*v.x + v.y*v.y
    }
//    pub fn lerp(&self, other: Vec2, t: f64) -> Vec2 {
//        self.add(self.sub(other).mul(t))
//    }
    pub fn rnd() -> Self {
        let mut rng = rand::thread_rng();
        Vec2 {
            x: rng.gen_range(0.0..1.0),
            y: rng.gen_range(0.0..1.0)
        }
    }
}

impl Vector2 for Vec2 {
    type Scalar = f64;
    fn new(x: f64, y: f64) -> Self {
        Vec2 { x, y }
    }
    fn x(&self) -> f64 {
        self.x
    }
    fn y(&self) -> f64 {
        self.y
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn basics() {
        assert_eq!(Vec2::new(10.0,0.0).mag(),10.0);
        let v = Vec2::new(10.0,0.0);
        assert_eq!(v.div(v.mag()).mag(),1.0);
        assert_eq!(v.norm().mag(),1.0);
        let v2 = Vec2::zero();
        assert_eq!(v.dist(v2),10.0);
        assert_eq!(v.dist_sq(v2),100.0);
        assert_eq!(v.lerp(v2,0.5),Vec2::new(5.0,0.0));
        assert_eq!(v.lerp(v2,0.25),Vec2::new(7.5,0.0));
        
    }
}
