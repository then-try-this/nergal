import { start, greet, Game } from "nergal";

var game = new Game()

document.addEventListener("keypress", event => {
    game.handle(event.key);
}); 

const renderLoop = () => {
    game.render()
    requestAnimationFrame(renderLoop);
};

requestAnimationFrame(renderLoop);
