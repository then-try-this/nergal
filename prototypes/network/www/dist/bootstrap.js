/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 	};
/******/
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/
/******/
/******/ 	// script path function
/******/ 	function jsonpScriptSrc(chunkId) {
/******/ 		return __webpack_require__.p + "" + chunkId + ".bootstrap.js"
/******/ 	}
/******/
/******/ 	// object to store loaded and loading wasm modules
/******/ 	var installedWasmModules = {};
/******/
/******/ 	function promiseResolve() { return Promise.resolve(); }
/******/
/******/ 	var wasmImportObjects = {
/******/ 		"../pkg/nergal_bg.wasm": function() {
/******/ 			return {
/******/ 				"./nergal_bg.js": {
/******/ 					"__wbindgen_object_drop_ref": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_object_drop_ref"](p0i32);
/******/ 					},
/******/ 					"__wbindgen_string_new": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_string_new"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_getElementById_65b9547a428b5eb4": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_getElementById_65b9547a428b5eb4"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_instanceof_Window_3e5cd1f48c152d01": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_instanceof_Window_3e5cd1f48c152d01"](p0i32);
/******/ 					},
/******/ 					"__wbg_document_d609202d16c38224": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_document_d609202d16c38224"](p0i32);
/******/ 					},
/******/ 					"__wbg_instanceof_HtmlCanvasElement_fba0ac991170cc00": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_instanceof_HtmlCanvasElement_fba0ac991170cc00"](p0i32);
/******/ 					},
/******/ 					"__wbg_getContext_164dc98953ddbc68": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_getContext_164dc98953ddbc68"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_instanceof_CanvasRenderingContext2d_b43c8f92c4744b7b": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_instanceof_CanvasRenderingContext2d_b43c8f92c4744b7b"](p0i32);
/******/ 					},
/******/ 					"__wbg_setstrokeStyle_1b4551387606453b": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setstrokeStyle_1b4551387606453b"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_setfillStyle_1ebd7d8f502888fa": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setfillStyle_1ebd7d8f502888fa"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_setfont_a622e8f71fcc67bd": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_setfont_a622e8f71fcc67bd"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_beginPath_5c4c10cc904edc64": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_beginPath_5c4c10cc904edc64"](p0i32);
/******/ 					},
/******/ 					"__wbg_fill_854920f07573dffd": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_fill_854920f07573dffd"](p0i32);
/******/ 					},
/******/ 					"__wbg_stroke_d6cd6e7c98952ff8": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_stroke_d6cd6e7c98952ff8"](p0i32);
/******/ 					},
/******/ 					"__wbg_arc_1e8c0f1f2b86edf7": function(p0i32,p1f64,p2f64,p3f64,p4f64,p5f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_arc_1e8c0f1f2b86edf7"](p0i32,p1f64,p2f64,p3f64,p4f64,p5f64);
/******/ 					},
/******/ 					"__wbg_lineTo_ef21aebc726bbda0": function(p0i32,p1f64,p2f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_lineTo_ef21aebc726bbda0"](p0i32,p1f64,p2f64);
/******/ 					},
/******/ 					"__wbg_moveTo_23c53eed282c0730": function(p0i32,p1f64,p2f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_moveTo_23c53eed282c0730"](p0i32,p1f64,p2f64);
/******/ 					},
/******/ 					"__wbg_clearRect_d976768ba14de6f9": function(p0i32,p1f64,p2f64,p3f64,p4f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_clearRect_d976768ba14de6f9"](p0i32,p1f64,p2f64,p3f64,p4f64);
/******/ 					},
/******/ 					"__wbg_fillText_2dc739bc08581cf8": function(p0i32,p1i32,p2i32,p3f64,p4f64) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_fillText_2dc739bc08581cf8"](p0i32,p1i32,p2i32,p3f64,p4f64);
/******/ 					},
/******/ 					"__wbg_log_a4530b4fe289336f": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_log_a4530b4fe289336f"](p0i32);
/******/ 					},
/******/ 					"__wbg_crypto_58f13aa23ffcb166": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_crypto_58f13aa23ffcb166"](p0i32);
/******/ 					},
/******/ 					"__wbindgen_is_object": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_is_object"](p0i32);
/******/ 					},
/******/ 					"__wbg_process_5b786e71d465a513": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_process_5b786e71d465a513"](p0i32);
/******/ 					},
/******/ 					"__wbg_versions_c2ab80650590b6a2": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_versions_c2ab80650590b6a2"](p0i32);
/******/ 					},
/******/ 					"__wbg_node_523d7bd03ef69fba": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_node_523d7bd03ef69fba"](p0i32);
/******/ 					},
/******/ 					"__wbindgen_is_string": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_is_string"](p0i32);
/******/ 					},
/******/ 					"__wbg_msCrypto_abcb1295e768d1f2": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_msCrypto_abcb1295e768d1f2"](p0i32);
/******/ 					},
/******/ 					"__wbg_require_2784e593a4674877": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_require_2784e593a4674877"]();
/******/ 					},
/******/ 					"__wbindgen_is_function": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_is_function"](p0i32);
/******/ 					},
/******/ 					"__wbg_randomFillSync_a0d98aa11c81fe89": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_randomFillSync_a0d98aa11c81fe89"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_getRandomValues_504510b5564925af": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_getRandomValues_504510b5564925af"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_newnoargs_c62ea9419c21fbac": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_newnoargs_c62ea9419c21fbac"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbg_call_90c26b09837aba1c": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_call_90c26b09837aba1c"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbindgen_object_clone_ref": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_object_clone_ref"](p0i32);
/******/ 					},
/******/ 					"__wbg_self_f0e34d89f33b99fd": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_self_f0e34d89f33b99fd"]();
/******/ 					},
/******/ 					"__wbg_window_d3b084224f4774d7": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_window_d3b084224f4774d7"]();
/******/ 					},
/******/ 					"__wbg_globalThis_9caa27ff917c6860": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_globalThis_9caa27ff917c6860"]();
/******/ 					},
/******/ 					"__wbg_global_35dfdd59a4da3e74": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_global_35dfdd59a4da3e74"]();
/******/ 					},
/******/ 					"__wbindgen_is_undefined": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_is_undefined"](p0i32);
/******/ 					},
/******/ 					"__wbg_call_5da1969d7cd31ccd": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_call_5da1969d7cd31ccd"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_buffer_a448f833075b71ba": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_buffer_a448f833075b71ba"](p0i32);
/******/ 					},
/******/ 					"__wbg_newwithbyteoffsetandlength_d0482f893617af71": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_newwithbyteoffsetandlength_d0482f893617af71"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_new_8f67e318f15d7254": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_new_8f67e318f15d7254"](p0i32);
/******/ 					},
/******/ 					"__wbg_set_2357bf09366ee480": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_set_2357bf09366ee480"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbg_newwithlength_6c2df9e2f3028c43": function(p0i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_newwithlength_6c2df9e2f3028c43"](p0i32);
/******/ 					},
/******/ 					"__wbg_subarray_2e940e41c0f5a1d9": function(p0i32,p1i32,p2i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbg_subarray_2e940e41c0f5a1d9"](p0i32,p1i32,p2i32);
/******/ 					},
/******/ 					"__wbindgen_debug_string": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_debug_string"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbindgen_throw": function(p0i32,p1i32) {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_throw"](p0i32,p1i32);
/******/ 					},
/******/ 					"__wbindgen_memory": function() {
/******/ 						return installedModules["../pkg/nergal_bg.js"].exports["__wbindgen_memory"]();
/******/ 					}
/******/ 				}
/******/ 			};
/******/ 		},
/******/ 	};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var promises = [];
/******/
/******/
/******/ 		// JSONP chunk loading for javascript
/******/
/******/ 		var installedChunkData = installedChunks[chunkId];
/******/ 		if(installedChunkData !== 0) { // 0 means "already installed".
/******/
/******/ 			// a Promise means "currently loading".
/******/ 			if(installedChunkData) {
/******/ 				promises.push(installedChunkData[2]);
/******/ 			} else {
/******/ 				// setup Promise in chunk cache
/******/ 				var promise = new Promise(function(resolve, reject) {
/******/ 					installedChunkData = installedChunks[chunkId] = [resolve, reject];
/******/ 				});
/******/ 				promises.push(installedChunkData[2] = promise);
/******/
/******/ 				// start chunk loading
/******/ 				var script = document.createElement('script');
/******/ 				var onScriptComplete;
/******/
/******/ 				script.charset = 'utf-8';
/******/ 				script.timeout = 120;
/******/ 				if (__webpack_require__.nc) {
/******/ 					script.setAttribute("nonce", __webpack_require__.nc);
/******/ 				}
/******/ 				script.src = jsonpScriptSrc(chunkId);
/******/
/******/ 				// create error before stack unwound to get useful stacktrace later
/******/ 				var error = new Error();
/******/ 				onScriptComplete = function (event) {
/******/ 					// avoid mem leaks in IE.
/******/ 					script.onerror = script.onload = null;
/******/ 					clearTimeout(timeout);
/******/ 					var chunk = installedChunks[chunkId];
/******/ 					if(chunk !== 0) {
/******/ 						if(chunk) {
/******/ 							var errorType = event && (event.type === 'load' ? 'missing' : event.type);
/******/ 							var realSrc = event && event.target && event.target.src;
/******/ 							error.message = 'Loading chunk ' + chunkId + ' failed.\n(' + errorType + ': ' + realSrc + ')';
/******/ 							error.name = 'ChunkLoadError';
/******/ 							error.type = errorType;
/******/ 							error.request = realSrc;
/******/ 							chunk[1](error);
/******/ 						}
/******/ 						installedChunks[chunkId] = undefined;
/******/ 					}
/******/ 				};
/******/ 				var timeout = setTimeout(function(){
/******/ 					onScriptComplete({ type: 'timeout', target: script });
/******/ 				}, 120000);
/******/ 				script.onerror = script.onload = onScriptComplete;
/******/ 				document.head.appendChild(script);
/******/ 			}
/******/ 		}
/******/
/******/ 		// Fetch + compile chunk loading for webassembly
/******/
/******/ 		var wasmModules = {"0":["../pkg/nergal_bg.wasm"]}[chunkId] || [];
/******/
/******/ 		wasmModules.forEach(function(wasmModuleId) {
/******/ 			var installedWasmModuleData = installedWasmModules[wasmModuleId];
/******/
/******/ 			// a Promise means "currently loading" or "already loaded".
/******/ 			if(installedWasmModuleData)
/******/ 				promises.push(installedWasmModuleData);
/******/ 			else {
/******/ 				var importObject = wasmImportObjects[wasmModuleId]();
/******/ 				var req = fetch(__webpack_require__.p + "" + {"../pkg/nergal_bg.wasm":"1d4578c07e5c0850df6c"}[wasmModuleId] + ".module.wasm");
/******/ 				var promise;
/******/ 				if(importObject instanceof Promise && typeof WebAssembly.compileStreaming === 'function') {
/******/ 					promise = Promise.all([WebAssembly.compileStreaming(req), importObject]).then(function(items) {
/******/ 						return WebAssembly.instantiate(items[0], items[1]);
/******/ 					});
/******/ 				} else if(typeof WebAssembly.instantiateStreaming === 'function') {
/******/ 					promise = WebAssembly.instantiateStreaming(req, importObject);
/******/ 				} else {
/******/ 					var bytesPromise = req.then(function(x) { return x.arrayBuffer(); });
/******/ 					promise = bytesPromise.then(function(bytes) {
/******/ 						return WebAssembly.instantiate(bytes, importObject);
/******/ 					});
/******/ 				}
/******/ 				promises.push(installedWasmModules[wasmModuleId] = promise.then(function(res) {
/******/ 					return __webpack_require__.w[wasmModuleId] = (res.instance || res).exports;
/******/ 				}));
/******/ 			}
/******/ 		});
/******/ 		return Promise.all(promises);
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// on error function for async loading
/******/ 	__webpack_require__.oe = function(err) { console.error(err); throw err; };
/******/
/******/ 	// object with all WebAssembly.instance exports
/******/ 	__webpack_require__.w = {};
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./bootstrap.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./bootstrap.js":
/*!**********************!*\
  !*** ./bootstrap.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// A dependency graph that contains any wasm must all be imported\n// asynchronously. This `bootstrap.js` file does the single async import, so\n// that no one else needs to worry about it again.\n__webpack_require__.e(/*! import() */ 0).then(__webpack_require__.bind(null, /*! ./index.js */ \"./index.js\"))\n  .catch(e => console.error(\"Error importing `index.js`:\", e));\n\n\n//# sourceURL=webpack:///./bootstrap.js?");

/***/ })

/******/ });