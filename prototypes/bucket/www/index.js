import { start, greet, Game } from "nergal";

const fps = new class {
  constructor() {
    this.fps = document.getElementById("fps");
    this.frames = [];
    this.lastFrameTimeStamp = performance.now();
    this.stats = document.getElementById("stats");
  }

  render() {
    // Convert the delta time since the last frame render into a measure
    // of frames per second.
    const now = performance.now();
    const delta = now - this.lastFrameTimeStamp;
    this.lastFrameTimeStamp = now;
    const fps = 1 / delta * 1000;

    // Save only the latest 100 timings.
    this.frames.push(fps);
    if (this.frames.length > 100) {
      this.frames.shift();
    }

    // Find the max, min, and mean of our 100 latest timings.
    let min = Infinity;
    let max = -Infinity;
    let sum = 0;
    for (let i = 0; i < this.frames.length; i++) {
      sum += this.frames[i];
      min = Math.min(this.frames[i], min);
      max = Math.max(this.frames[i], max);
    }
    let mean = sum / this.frames.length;

    // Render the statistics.
    this.fps.textContent = `fps: ${Math.round(mean)}`.trim();

  }
};

const stats = new class {
    constructor(game) {
        this.stats = document.getElementById("stats");
        this.game = game
    }
    render() {
        this.stats.innerHTML = "";
        var items = game.stats();
        for (var i=0; i<items.length; i++) {
            var entry = document.createElement('li');
            entry.appendChild(document.createTextNode(items[i]));
            this.stats.appendChild(entry);
        };
    }
};



var game = new Game()

document.addEventListener("keypress", event => {
    game.handle(event.key);
}); 

const renderLoop = () => {
    stats.render();
    fps.render();
    game.render();
    requestAnimationFrame(renderLoop);
};

requestAnimationFrame(renderLoop);
