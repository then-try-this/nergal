// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use vector2math::*;
use crate::game::entity::{ Entity, EntityRefVec };
use crate::game::social_entity::{ SocialEntity };
use crate::game::vec2::{ Vec2 };

pub struct Bucket {
    pub min: Vec2,
    pub max: Vec2,
    pub entities: EntityRefVec
}

impl Bucket {
    pub fn new(min, max) -> Self {
        Bucket {
            min,
            max,
            entities: vec![]
        }
    }

    pub fn scan_and_remove(&mut self) -> EntityRefVec {
        let outside = self.entities.filter(|e| {
            !e.pos().inside(self.min, self.max)
        });

        self.entities = self.entities.filter(|e| {
            e.pos().inside(self.min, self.max)
        });

        return outside;        
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn basics() {
        let mut b=Bucket::new(Vec2::new(0.0,0.0),
                              Vec2::new(10.0,10.0),
                              vec![Box::new(SocialEntity::at_pos(5.0,5.0))]);

        let r = b.scan_and_remove();
        assert_eq!(r.len(),0);
    }
}
