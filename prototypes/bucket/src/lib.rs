// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::f64;
use wasm_bindgen::prelude::*;
use js_sys::Array;
mod game;
use crate::game::world;
//use crate::game::vec2::{ Vec2 };
use crate::game::id::{ EntityId, PLAYER_ID };
use crate::game::entity::{ EntityState };
use crate::game::player_entity::PlayerEntity;
use crate::game::social_entity::SocialEntity;

use web_sys::console;

#[wasm_bindgen]
pub struct Game {
    world: world::World,
}

#[wasm_bindgen]
impl Game {
    
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        console::log_1(&"Hello from nergal".into());

        let mut world = world::World::simple();

        world.bucket_grid.get_entity_mut_as::<SocialEntity>(3.into()).unwrap().memory.push("🍈".into());
        world.bucket_grid.get_entity_mut_as::<SocialEntity>(14.into()).unwrap().memory.push("🍎".into());
        world.bucket_grid.get_entity_mut_as::<SocialEntity>(16.into()).unwrap().memory.push("🥥".into());
        world.bucket_grid.get_entity_mut_as::<SocialEntity>(20.into()).unwrap().memory.push("🥝".into());
        world.bucket_grid.get_entity_mut_as::<SocialEntity>(29.into()).unwrap().memory.push("🫐".into());
        Game{ world }
    }

    #[wasm_bindgen]
    pub fn handle(&mut self, key: String) {
        if key=="a" {
            self.world.bucket_grid.get_entity_mut_as::<PlayerEntity>(PLAYER_ID).unwrap().pos.x+=-2.0;
        }
        if key=="d" {
            self.world.bucket_grid.get_entity_mut_as::<PlayerEntity>(PLAYER_ID).unwrap().pos.x+=2.0;
        }
        if key=="w" {
            self.world.bucket_grid.get_entity_mut_as::<PlayerEntity>(PLAYER_ID).unwrap().pos.y+=-2.0;
        }
        if key=="s" {
            self.world.bucket_grid.get_entity_mut_as::<PlayerEntity>(PLAYER_ID).unwrap().pos.y+=2.0;
        }
        if key==" " {
            self.world.bucket_grid.get_entity_mut_as::<PlayerEntity>(PLAYER_ID).unwrap().state=EntityState::Speak;
        }
        if key=="x" {
            
            
            //self.world.relationships = self.world.disconnect_all(self.world.relationships,PLAYER_ID);
        }

    }

    #[wasm_bindgen]
    pub fn stats(&self) -> wasm_bindgen::JsValue {
        return wasm_bindgen::JsValue::from([
            format!("grid size: {}", self.world.bucket_grid.grid.len()),
            format!("num entities: {}", self.world.bucket_grid.grid.iter().fold(0,|acc,b| acc+b.entities.len())),
            format!("crossings: {}", self.world.bucket_grid.num_crossings),
            format!("empty cells: {}", self.world.bucket_grid.grid.iter().fold(0,|acc,b| if b.entities.len()==0 { acc+1 } else { acc })),
            format!("messages: {}", self.world.num_messages),
        ].iter().map(|x| wasm_bindgen::JsValue::from_str(x)).collect::<Array>());
    }
    
    #[wasm_bindgen]
    pub fn render(&mut self) {

        let document = web_sys::window().unwrap().document().unwrap();
        let canvas = document.get_element_by_id("canvas").unwrap();
        let canvas: web_sys::HtmlCanvasElement = canvas
            .dyn_into::<web_sys::HtmlCanvasElement>()
            .map_err(|_| ())
            .unwrap();

        let context = canvas
            .get_context("2d")
            .unwrap()
            .unwrap()
            .dyn_into::<web_sys::CanvasRenderingContext2d>()
            .unwrap();


        self.world = self.world.update();

        self.world.bucket_grid.update_grid();
        
        context.clear_rect(0.0,0.0,1000.0,500.0);

        //self.world = self.world.relax();
        context.set_font("15px sans"); 

        self.world.render(&context);     
    }
}

#[wasm_bindgen(start)]
fn start() {
}

