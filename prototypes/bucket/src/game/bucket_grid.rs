// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use crate::game::bucket::Bucket;
use crate::game::id::{ EntityId };
use crate::game::message::{ Message };
use crate::game::world::{ World };
use crate::game::entity::{ Entity, EntityRefVec, EntityVec };
use crate::game::vec2::Vec2;
use vector2math::*;

// a spatial container for all the entities.
// designed to optimise entity updates (by only supplying local surrounding entities)
// as well as searching/lookup

pub struct BucketGrid {
    pub width: usize,
    pub height: usize,
    pub bucket_size: Vec2,
    pub grid: Vec<Bucket>,
    pub num_crossings: u32,
    pub entity_to_address: HashMap<EntityId,usize>
}

impl BucketGrid {
    pub fn new(width: usize, height: usize, bucket_size: Vec2) -> Self {
        let mut grid = vec![];
        for x in 0..height {
            for y in 0..width {
                grid.push(Bucket::new(
                    Vec2::new(
                        bucket_size.x * x as f64,
                        bucket_size.y * y as f64
                    ),
                    Vec2::new(
                        bucket_size.x * (x + 1) as f64,
                        bucket_size.y * (y + 1) as f64,
                    ),
                ));
            }
        }
        BucketGrid {
            width,
            height,
            bucket_size,
            grid,
            num_crossings: 0,
            entity_to_address: HashMap::new()                
        }
    }

    pub fn address_xy(&self, x: usize, y: usize) -> usize {
        x * self.height + y
    }

    pub fn address(&self, pos: Vec2) -> usize {
        let x = (pos.x / (self.bucket_size.x as f64)) as usize;
        let y = (pos.y / (self.bucket_size.y as f64)) as usize;
        self.address_xy(x, y)
    }

    pub fn insert(&mut self, e: Box<dyn Entity>) {
        let x = (e.pos().x / (self.bucket_size.x as f64)) as usize;
        let y = (e.pos().y / (self.bucket_size.y as f64)) as usize;
        let addr = self.address_xy(x, y);
        // add to/update the lookup hashmap
        self.entity_to_address.insert(e.id(),addr);
        self.grid[addr].entities.push(e);
    }

    pub fn get_surrounding(&self, pos: Vec2) -> Vec<usize> {
        let x = (pos.x / (self.bucket_size.x as f64)) as usize;
        let y = (pos.y / (self.bucket_size.y as f64)) as usize;

        if x>0 && y>0 && x<self.width-1 && y<self.height-1 {        
            // return 9 surrounding cell indices to the centre one
            [self.address_xy(x-1,y+1), self.address_xy(x,y+1), self.address_xy(x+1,y+1),
             self.address_xy(x-1,y), self.address_xy(x,y), self.address_xy(x+1,y),
             self.address_xy(x-1,y-1), self.address_xy(x,y-1), self.address_xy(x+1,y-1)].into()
        } else {
            // 
            vec![]
        }        
    }

    pub fn get_surrounding_entities(&self, pos: Vec2) -> EntityRefVec {
        self.get_surrounding(pos).iter().fold(vec![],|mut acc,b| {
            // need to convert to a mut refvec temporary explicitly:
            // a) so we can so we consume it in the append, which moves
            //    from one vec to the other
            // b) as we need a refvec anyway so we don't need to copy data
            // also - iter() does the job for us:
            let mut temp = self.grid[*b].entities.iter().collect();
            acc.append(&mut temp); acc
        })
    }

    pub fn get_entity(&self, id: EntityId) -> Result<&Box<dyn Entity>, &str> {
        // use the entitiy_to_address lookup to quickly find the bucket
        // containing the entitiy, then search within that
        match self.grid[*self.entity_to_address.get(&id)
                        .ok_or("entity not in lookup")?].get_entity(id) {
            Ok(entity) => {
                return Ok(entity);
            }
            Err(str) => { Err(str) }
        }
    }

    pub fn get_entity_mut(&mut self, id: EntityId) -> Result<&mut Box<dyn Entity>, &str> {        
        match self.grid[*self.entity_to_address.get(&id)
                        .ok_or("entity not in lookup")?].get_entity_mut(id) {            
            Ok(entity) => {
                return Ok(entity);
            }
            Err(str) => { Err(str) }
        }
    }

    pub fn get_entity_as<T: 'static>(&self, id: EntityId) -> Result<&T, &str> {
        let a = self.get_entity(id)?.as_any();
        let p = a.downcast_ref::<T>();
        match p {
            Some(pe) => Ok(pe),
            None => Err("not found"),
        }
    }

    pub fn get_entity_mut_as<T: 'static>(&mut self, id: EntityId) -> Result<&mut T, &str> {
        let a = self.get_entity_mut(id)?.as_any_mut();
        let p = a.downcast_mut::<T>();
        match p {
            Some(pe) => Ok(pe),
            None => Err("not found"),
        }
    }

    pub fn update(&self, world: &World) -> (Self, Vec<Message>) {
        let mut messages = vec![];
        // todo: update out of sight entities at a slower rate? or not at all?
        (BucketGrid {
            grid: self.grid.iter().map(|b| {
                // todo: don't need to calculate this every time...
                let centre = b.min.add(b.max).div(2.0);
                let (new_b,new_messages) = b.update(world, self.get_surrounding_entities(centre));
                messages.extend(new_messages);
                new_b
            }).collect(),
            entity_to_address: self.entity_to_address.clone(),
            ..*self
        }, messages)         
    }

    pub fn read_messages(&self, world: &World, messages: &Vec<Message>) -> Self {
        BucketGrid {
            grid: self.grid.iter().map(|b| {                               
                b.read_messages(world, messages)
            }).collect(),
            entity_to_address: self.entity_to_address.clone(),
            ..*self
        }        
    }    
    
    pub fn update_grid(&mut self) {
        let mut i = 0;
        self.num_crossings = 0;
        // iterating by index seems simpler than using iterators with borrow check
        while i < self.grid.len() {
            for e in self.grid[i].scan_and_remove() {
                self.insert(e);
                // for stats
                self.num_crossings+=1;
            }
            i += 1;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::social_entity::SocialEntity;

    #[test]
    fn bucket_grid() {
        let mut bg = BucketGrid::new(5, 5, Vec2::new(50.0, 50.0));
        bg.insert(Box::new(SocialEntity::at_pos(1.into(), Vec2::new(55.0, 55.0))));

        assert_eq!(bg.grid[bg.address(Vec2::new(55.0, 55.0))].entities.len(), 1);
        assert_eq!(bg.grid[bg.address(Vec2::new(15.0, 55.0))].entities.len(), 0);

        bg.insert(Box::new(SocialEntity::at_pos(2.into(), Vec2::new(55.0, 55.0))));
        assert_eq!(bg.grid[bg.address(Vec2::new(55.0, 55.0))].entities.len(), 2);

        bg.update_grid();
        assert_eq!(bg.num_crossings,0);
        bg.update_grid();
        assert_eq!(bg.num_crossings,0);
        
        // move the entity
        bg.get_entity_mut_as::<SocialEntity>(1.into()).unwrap().pos.x = 15.0;

        bg.update_grid();
        assert_eq!(bg.num_crossings,1);
        assert_eq!(bg.grid[bg.address(Vec2::new(55.0, 55.0))].entities.len(), 1);
        assert_eq!(bg.grid[bg.address(Vec2::new(15.0, 55.0))].entities.len(), 1);
        bg.update_grid();
        assert_eq!(bg.grid[bg.address(Vec2::new(55.0, 55.0))].entities.len(), 1);
        assert_eq!(bg.grid[bg.address(Vec2::new(15.0, 55.0))].entities.len(), 1);
        assert_eq!(bg.num_crossings,0);

        bg.insert(Box::new(SocialEntity::at_pos(3.into(), Vec2::new(105.0, 55.0))));
        bg.insert(Box::new(SocialEntity::at_pos(3.into(), Vec2::new(102.0, 55.0))));

        assert_eq!(bg.get_surrounding_entities(Vec2::new(55.0, 55.0)).len(),4); 
    }
}
