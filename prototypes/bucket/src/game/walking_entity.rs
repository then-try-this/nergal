use crate::game::vec2::{ Vec2 };
use crate::game::entity::*;


pub trait WalkingEntity {

    fn destination(&self) -> Vec2;
    fn set_destination(&self, s: Vec2);
    fn timer(&self) -> f64;
    fn set_timer(&self, t: f64);

    fn walk_to(&self, target: Box::<dyn Entity>) {
        self.set_destination(target.pos());
    }
}

