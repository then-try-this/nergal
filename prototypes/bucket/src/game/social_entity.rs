// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use rand::Rng;
use std::any::Any;
use vector2math::*;
use wasm_bindgen::prelude::*;
use rand::prelude::SliceRandom;
use crate::game::id::{ EntityId };
use crate::game::vec2::{ Vec2 };
use crate::game::entity::*;
use crate::game::message::{ Message, MessageType };

const WALK_SPEED:f64 = 2.0;
const FRIEND_DISTANCE:f64 = 10.0;
const STRANGER_DISTANCE:f64 = 100.0;
const WALK_LIKELIHOOD:i32 = 200;
const AVOID_DISTANCE:f64 = 30.0;
const SPOKEN_TICKS:i32 = 100;
const SPEAK_DISTANCE:f64 = 50.0;

#[derive(Debug,Clone)]
pub struct SocialEntity {
    pub id: EntityId,
    pub pos: Vec2,
    pub vel: Vec2,
    pub memory: Vec<String>,
    pub state: EntityState,
    pub timer: i32,
    pub target: Vec2
}

#[allow(dead_code)]
impl SocialEntity {
    fn new(id: EntityId) -> Self {
        SocialEntity {
            id,
            pos: Vec2::zero(),
            vel: Vec2::zero(),
            memory: vec![],
            state: EntityState::Idle,
            timer: 0,
            target: Vec2::zero()                
        }   
    }
    pub fn at_pos(id: EntityId, pos: Vec2) -> Self {
        SocialEntity {
            id,
            pos,
            vel: Vec2::zero(),
            memory: vec![],
            state: EntityState::Idle,
            timer: 0,
            target: Vec2::zero()                
        }   
    }

    fn walk(&self) -> (Box<dyn Entity>,Vec<Message>) {
        let v = self.target.sub(self.pos);
        let m = v.norm().mul(WALK_SPEED);
        return (Box::new(SocialEntity {                    
            state: if v.mag()<SPEAK_DISTANCE {
                if self.state==EntityState::Walk {
                    EntityState::Speak
                } else {
                    EntityState::Idle
                }
            } else {
                self.state
            },                    
            pos: self.pos.add(m),
            memory: self.memory.clone(),
            ..*self
        }), vec![])
    }
}


impl Entity for SocialEntity {
    fn id(&self) -> EntityId { self.id }
    fn pos(&self) -> Vec2 { self.pos }
    fn state(&self) -> EntityState { self.state }
    fn update(&self, entities: &EntityRefVec, connected: &Vec<EntityId>) -> (Box<dyn Entity>,Vec<Message>) {
        let mut rng = rand::thread_rng();

        let updated_entity = SocialEntity {
            timer: if self.timer>0 { self.timer-1 } else { self.timer },
            memory: self.memory.clone(),
            ..*self
        };

        
        // first priority is avoiding others
        if self.state!=EntityState::Avoid {
            let close = self.within_radius(entities,AVOID_DISTANCE);
            
            // any close by?
            if close.len()>0 {                
                return (Box::new(SocialEntity {
                    state: EntityState::Avoid,
                    target: self.pos().add(close.iter().fold(Vec2::zero(),|acc,e| {
                        acc.add(self.pos().sub(e.pos()).norm().mul(
                            if connected.contains(&e.id()) {
                                FRIEND_DISTANCE
                            } else {
                                STRANGER_DISTANCE
                            }))
                    })),
                    ..updated_entity
                }), vec![]);
            }
        }

         
        
        match self.state {
            EntityState::Idle => {
                if rng.gen_range(0..WALK_LIKELIHOOD)==1 {
                    match self.choose_connected(entities, connected) {
                        Ok(choice) => {
                            return (Box::new(SocialEntity {
                                state: EntityState::Walk,
                                target: choice.pos(),
                                ..updated_entity
                            }), vec![])
                        },
                        Err(str) => {}
                    }
                } 
            },
            EntityState::Speak => {
                // do we have something to say?
                if self.memory.len()>0 {
                    match self.nearest_connected(entities,connected) {
                        // are we connected to anyone?
                        Some(nearest) => {
                            // are they close enough?
                            if nearest.pos().dist(self.pos())<SPEAK_DISTANCE {
                                return (Box::new(SocialEntity {
                                    state: EntityState::Spoken,
                                    timer: SPOKEN_TICKS,
                                    ..updated_entity
                                }), vec![Message{
                                    sender: self.id,
                                    recipient: nearest.id(), 
                                    name: MessageType::Targeted,
                                    value: self.memory.choose(&mut rand::thread_rng()).unwrap().clone(),
                                }]);
                            }
                        }
                        None => {}
                    }
                }
                
                // go back to idling
                return (Box::new(SocialEntity {                    
                    state: EntityState::Idle,                    
                    ..updated_entity
                }), vec![])
            },
            EntityState::Spoken => {
                return (Box::new(SocialEntity {                    
                    state: if self.timer==0 {
                        EntityState::Idle
                    } else {
                        self.state
                    },                    
                    ..updated_entity
                }), vec![])
            },
            EntityState::StartWalk => {}
            EntityState::Walk => return self.walk(),
            EntityState::Avoid => return self.walk()
        }
        return (Box::new(updated_entity), vec![])
    }

    fn read_messages(&self, messages: &Vec<Message>, connected: &Vec<EntityId>) -> Box<dyn Entity> {
        Box::new(SocialEntity {
            memory: self.messages_from_connected(messages,connected).iter().fold(self.memory.clone(),|mut acc, msg| {
                match msg.name {
                    MessageType::Broadcast => {
                        if !self.memory.contains(&msg.value) {
                            acc.push(msg.value.clone());
                        }
                    }             
                    MessageType::Targeted => {
                        if msg.recipient == self.id {
                            if !self.memory.contains(&msg.value) {
                                acc.push(msg.value.clone());
                            }
                        }
                    }
                    _ => {}
                }                
                acc
            }),                        
            ..*self
        })
    }
    
    fn render(&self, context: &web_sys::CanvasRenderingContext2d) {
        context.begin_path();
        context
            .arc(self.pos.x,self.pos.y, 15.0, 0.0, std::f64::consts::PI * 2.00)
            .unwrap();
        context.set_fill_style(&"#fff".into());        
        context.fill();
        context.set_fill_style(&"#000".into());
        context.stroke();

        let mut pos=0.0;
        for message in &self.memory {
            context.fill_text(&message, self.pos.x-10.0, self.pos.y+pos).unwrap();
            pos+=10.0;
        }

        context.set_fill_style(&"#000".into());

        context.fill_text(
            match self.state() {
                EntityState::Idle => "",
                EntityState::Speak => "speak",
                EntityState::Spoken => "speak",
                EntityState::StartWalk => "start walk",
                EntityState::Walk => "walk",
                EntityState::Avoid => "avoid"
            }, self.pos.x-25.0, self.pos.y-25.0).unwrap();

    }

    fn as_any(&self) -> &dyn Any { self }
    fn as_any_mut(&mut self) -> &mut dyn Any { self }


}


#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn basics() {
        let mut s = Box::new(SocialEntity::new(1.into()));
        assert_eq!(s.id, 1.into());
        assert_eq!(s.state, EntityState::Idle);

        // check triggering of broadcasts
        s.state=EntityState::Speak;
        s.memory.push("A".into());
        let e:EntityVec = vec![Box::new(SocialEntity::at_pos(2.into(),Vec2::new(45.0,0.0)))];
        let er:EntityRefVec = e.iter().collect();
        let c = vec![EntityId::new(2)];
        let (new_s,messages) = s.update(&er,&c);
        assert_eq!(new_s.state(), EntityState::Spoken);
        assert_eq!(messages[0].name, MessageType::Targeted);
        assert_eq!(messages[0].sender, 1.into());
        assert_eq!(messages[0].recipient, 2.into());
        assert_eq!(messages[0].value, "A");

        // check adding to memory
        let binding=s.read_messages(&vec![Message{
            sender: 2.into(),
            recipient: 1.into(),
            name:MessageType::Targeted,
            value:"X".to_string()
        }],&c);
        let s2=binding.as_any().downcast_ref::<SocialEntity>().unwrap();

        assert_eq!(s2.memory[1], "X".to_string());        
    }
}



