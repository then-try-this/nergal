// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use vector2math::*;
use crate::game::entity::{ Entity, EntityVec, EntityRefVec };
use crate::game::social_entity::{ SocialEntity };
use crate::game::id::{ EntityId };
use crate::game::vec2::{ Vec2 };
use crate::game::world::{ World };
use crate::game::message::{ Message };

pub struct Bucket {
    pub min: Vec2,
    pub max: Vec2,
    pub entities: EntityVec
}

impl Bucket {
    pub fn new(min:Vec2, max:Vec2) -> Self {
        Bucket {
            min,
            max,
            entities: vec![]
        }
    }

    pub fn get_entity(&self, id: EntityId) -> Result<&Box<dyn Entity>,&str> {
        for entity in &self.entities {
            if entity.id() == id {
                return Ok(entity);
            }
        }
        return Err("not found");
    }

    pub fn get_entity_mut(&mut self, id: EntityId) -> Result<&mut Box<dyn Entity>, &str> {
        for entity in &mut self.entities {
            if entity.id() == id {
                return Ok(entity);
            }
        }
        return Err("not found");
    }

    // pub fn get_entities_nearby(&self, exclude: u32, pos: Vec2, dist: f64) -> Vec<&Box<dyn Entity>> {        
    //     self.entities.iter().fold(vec![],|mut acc,entity| {
    //         if entity.id()!=exclude && entity.pos().sub(pos).mag()<dist {
    //             acc.push(entity);               
    //         }
    //         acc
    //     })
    // }
    
    pub fn update(&self, world:&World, surrounding_entities: EntityRefVec) -> (Self, Vec<Message>) {
        let mut messages: Vec<Message> = vec![];        
        (Bucket {
            // first pass, update and collect messages
            entities: self.entities.iter().map(|entity| {
                // find (local) connections (todo: cache these)
                let connections = surrounding_entities.iter().fold(vec![],|mut acc: Vec<EntityId>,e| {
                    if world.connected_entity_ids(entity.id()).contains(&e.id()) {
                        acc.push(e.id()); 
                    } 
                    acc                                                     
                });
                let (entity,msg) = entity.update(&surrounding_entities,&connections);
                messages.extend(msg);
                entity
            }).collect::<EntityVec>(),
            ..*self
        }, messages)
    }

    pub fn read_messages(&self, world:&World, messages: &Vec<Message>) -> Self {        
        Bucket {
            entities: self.entities.iter().map(|entity| {
                // find (local) connections (todo: cache these)
                let connections = self.entities.iter().fold(vec![],|mut acc: Vec<EntityId>,e| {
                    if world.connected_entity_ids(entity.id()).contains(&e.id()) {
                        acc.push(e.id()); 
                    } 
                    acc                                                     
                });                                
                entity.read_messages(&messages,&connections)
            }).collect(),            
            ..*self
        }              
    }
    
    pub fn render(&self, context: &web_sys::CanvasRenderingContext2d) {
        context.begin_path();
        context.rect(self.min.x, self.min.y,
                     self.max.x-self.min.x,
                     self.max.y-self.min.y);
        context.stroke();
        
        for entity in &self.entities {
            entity.render(context);
        }        
    }
    
    
    pub fn scan_and_remove(&mut self) -> EntityVec {
        // this would be nice
        //self.entities.extract_if(|e| {
        //    !e.pos().inside(self.min, self.max)        
        //}).collect()

        if self.entities.len()==0 {
            return vec![];
        }

        // but, this is also fine...
        let mut ret = vec![];

        // well, remove in reverse order so indices remain correct 
        let mut i = self.entities.len()-1;
        loop {
            if !&self.entities[i].pos().inside(self.min, self.max) {
                ret.push(self.entities.remove(i));
            }
            if i>0 { i-=1; } else { break; }
        }
        return ret;              
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn basics() {
        let es:EntityVec = vec![Box::new(SocialEntity::at_pos(0.into(),Vec2::new(5.0,5.0)))];
        
        let mut b=Bucket{
            min: Vec2::new(0.0,0.0),
            max: Vec2::new(10.0,10.0),
            entities: es
        };

        {
            let r = b.scan_and_remove();
            assert_eq!(r.len(),0);
        }
        // add an entity out of the box and see if it gets removed...
        b.entities.push(Box::new(SocialEntity::at_pos(99.into(),Vec2::new(50.0,50.0))));
        {
            let r = b.scan_and_remove();
            assert_eq!(r.len(),1);
            assert_eq!(r[0].id(),99.into());            
            assert_eq!(b.entities.len(),1);
        }
        b.entities.push(Box::new(SocialEntity::at_pos(100.into(),Vec2::new(50.0,50.0))));
        b.entities.push(Box::new(SocialEntity::at_pos(101.into(),Vec2::new(5.0,5.0))));
        b.entities.push(Box::new(SocialEntity::at_pos(102.into(),Vec2::new(50.0,50.0))));
        {
            let r = b.scan_and_remove();
            assert_eq!(r.len(),2);
            assert_eq!(r[0].id(),102.into());            
            assert_eq!(r[1].id(),100.into());            
            assert_eq!(b.entities.len(),2);
            assert_eq!(b.entities[0].id(),0.into());            
            assert_eq!(b.entities[1].id(),101.into());            
            let r = b.scan_and_remove();
            assert_eq!(r.len(),0);            
        }

    }
}
