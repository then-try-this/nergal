// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use vector2math::*;
use std::any::Any;
use rand::Rng;
use rand::prelude::SliceRandom;
use crate::game::vec2::{ Vec2 };
use crate::game::id::{ EntityId };
use crate::game::message::{ Message };

pub const DEFAULT_SIZE: f64 = 20.0;

#[derive(Debug,Clone,Copy,PartialEq)]
pub enum EntityState {
    Idle,
    Speak,
    Spoken,
    StartWalk,
    Walk,
    Avoid,
}

pub type EntityVec = Vec<Box<dyn Entity>>;
pub type EntityRefVec<'a> = Vec<&'a Box<dyn Entity>>;

pub trait Entity {
    ////////////////////////////////////////////////////////////////////////
    // entity interface

    fn id(&self) -> EntityId;
    fn pos(&self) -> Vec2;
    fn state(&self) -> EntityState;

    // update the entity and return a bunch of outgoing messages
    fn update(&self, entities: &EntityRefVec, connected: &Vec<EntityId>) -> (Box<dyn Entity>, Vec<Message>);

    // read the messages passed in the same update
    // we get access to all the messages sent, so could contain systemwide ones etc
    // it's up to us to e.g. filter them to only listen to connected entities
    fn read_messages(&self,messages: &Vec<Message>, connected: &Vec<EntityId>) -> Box<dyn Entity>;

    // render as HTML5 canvas
    fn render(&self, context: &web_sys::CanvasRenderingContext2d) {}

    /////////////////////////////////////////////////////////////////////////
    // this allows us to cast to Any so we can then convert
    // from the 'base' trait to any entity type
    fn as_any(&self) -> &dyn Any;
    fn as_any_mut(&mut self) -> &mut dyn Any;

    /////////////////////////////////////////////////////////////////////////
    // helper functions
    
    fn connected_entities<'a>(&self, entities: &'a EntityVec, connected: &Vec<EntityId>) -> Vec<&'a Box<dyn Entity>> {
        entities.iter().filter(|e| connected.contains(&e.id())).collect()        
    }

    fn nearest<'a>(&self, entities: &'a EntityVec) -> Option<&'a Box<dyn Entity>> {
        if entities.len()>0 {
            Some(&entities.iter().fold((&entities[0],f64::MAX),|acc,e| {
                // check self id
                let d = self.pos().dist(e.pos());
                if d<acc.1 { (e,d) } else { acc }
            }).0)
        } else { None }
    }

    fn within_radius<'a>(&'a self, entities: &EntityRefVec<'a>, radius: f64) -> EntityRefVec {
        entities.into_iter().filter(|e| {
            e.id()!=self.id() && self.pos().dist(e.pos())<radius
        }).cloned().collect()        
    }

    fn choose_connected<'a>(&self, entities: &EntityRefVec<'a>, connected: &Vec<EntityId>) -> Result<&'a Box<dyn Entity>, &str> {
        Ok(*entities.iter().filter(|e| {
            connected.contains(&e.id())
        })
           .collect::<Vec<_>>()
           .choose(&mut rand::thread_rng())
           .ok_or("no connections availible")?)
    }

    // internal use for vectors of references, built by iterators
    fn nearest_refvec<'a>(&self, entities: EntityRefVec<'a>) -> Option<&'a Box<dyn Entity>> {
        if entities.len()>0 {
            Some(&entities.iter().fold((&entities[0],f64::MAX),|acc,e| {
                // check self id
                let d = self.pos().dist(e.pos());
                if d<acc.1 { (e,d) } else { acc }
            }).0)
        } else { None }
    }
        
    fn nearest_connected<'a>(&self, entities: &'a EntityRefVec, connected: &Vec<EntityId>) -> Option<&'a Box<dyn Entity>> {
        self.nearest_refvec::<'a>(entities.into_iter().filter(|e| {
            connected.contains(&e.id())
        }).cloned().collect())
    }

    fn nearest_unconnected<'a>(&self, entities: &'a EntityVec, connected: &Vec<EntityId>) -> Option<&'a Box<dyn Entity>> {
        self.nearest_refvec(entities.iter().filter(|e| {
            !connected.contains(&e.id())
        }).collect::<Vec<_>>())
    }

    fn messages_from_connected<'a>(&self, messages: &'a Vec<Message>, connected: &Vec<EntityId>) -> Vec<&'a Message> {
        messages.iter().filter(|msg| {
            for connection in connected {
                if connection == &msg.sender { return true }
            }
            false
        }).collect()
    }
    

}

