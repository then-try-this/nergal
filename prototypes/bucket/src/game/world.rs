// Nergal Copyright (C) 2023 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use vector2math::*;
use crate::game::id::{ EntityId, PLAYER_ID };
use crate::game::vec2::{ Vec2 };
use crate::game::entity::{ Entity, EntityVec };
use crate::game::social_entity::{ SocialEntity };
use crate::game::player_entity::{ PlayerEntity };
use crate::game::relationship::{ Relationship };
use crate::game::message::{ Message };
use crate::game::bucket_grid::{ BucketGrid };
//use web_sys::console;

//const MAX_SPEED: f64 = 2.0;
const PLAYER_OVERLAP: f64 = 45.0;

pub struct World {
    pub bucket_grid: BucketGrid,
    pub relationships: Vec<Relationship>,
    pub centre: Vec2,
    pub num_messages: u32
}

#[allow(dead_code)]
impl World {
    pub fn new() -> Self {
        World {
            bucket_grid: BucketGrid::new(8,8,Vec2::new(125.0,125.0)),
            relationships: vec![],
            centre: Vec2::zero(),
            num_messages: 0
        }
    }
    
    
    /// Simple constructor for testing purposes
    pub fn simple() -> Self {
        let centre =  Vec2::new(1500.0,1500.0);      
                
        let mut bg = BucketGrid::new(16,16,Vec2::new(250.0,250.0));        

        bg.insert(Box::new(PlayerEntity::new()));
        
        for i in 1..30 {
            bg.insert(Box::new(SocialEntity::at_pos(i.into(), Vec2::rnd().mul(500.0).add(centre))));            
        }
            
        World {
            bucket_grid: bg,
            relationships: vec![
                Relationship::new(1,1,2),
                Relationship::new(2,2,3),
                Relationship::new(3,3,4),
                Relationship::new(4,4,1),
                Relationship::new(5,5,1),

                Relationship::new(6,5,6),

                Relationship::new(7, 6, 7),
                Relationship::new(8, 7, 8),
                Relationship::new(9, 8, 9),
                Relationship::new(10, 9, 10),
                Relationship::new(11, 10, 6),

                Relationship::new(12, 10, 11),
                Relationship::new(13, 5, 12),
                Relationship::new(14, 3, 13),
                Relationship::new(15, 8, 14),

                Relationship::new(16, 15, 16),
                Relationship::new(17, 16, 17),
                Relationship::new(18, 17, 18),
                Relationship::new(19, 18, 15),

                Relationship::new(20, 20, 21),
                Relationship::new(21, 21, 22),
                Relationship::new(22, 22, 23),
                Relationship::new(23, 23, 24),
                Relationship::new(24, 24, 25),
                Relationship::new(25, 25, 26),
                Relationship::new(26, 26, 27),
                Relationship::new(27, 27, 20),               
            ],
            centre,
            num_messages: 0
        }
        
    }

    pub fn connected_entities(&self, entity_id: EntityId) -> Vec<&Box<dyn Entity>> {
        self.relationships.iter().fold(vec![], |mut acc:Vec<&Box<dyn Entity>>, edge| {           
            if edge.from==entity_id {
                match self.bucket_grid.get_entity(edge.to) {
                    Ok(centity) => { acc.push(centity); return acc; },
                    Err(_) => { return acc; }
                };
            }
            if edge.to==entity_id {
                match self.bucket_grid.get_entity(edge.from) {
                    Ok(centity) => { acc.push(centity); return acc; },
                    Err(_) => { return acc; }
                };
            }
            return acc;
        })
    }

    pub fn connected_entity_ids(&self, entity_id: EntityId) -> Vec<EntityId> {
        self.connected_entities(entity_id).iter().map(|e| e.id()).collect()
    }

/*    pub fn disconnect_all(&self, relationships: Vec<Relationship>, entity_id: i32) -> Vec<Relationship> {
        relationships.iter().filter(|rel| {
            rel.from != entity_id && rel.to != entity_id           
        }).collect()
    }*/
    
/*    fn relax_entity(&self, entity: &Box<dyn Entity>) -> Vec2 {
        let mut delta = entity.vel.mul(0.99);
        
        delta = delta.add(self.centre.sub(entity.pos()).mul(0.0005));

        delta = delta.add(self.entities.iter()
                          .fold(Vec2::zero(),|acc,other_entity| -> Vec2 {
                              if other_entity.id()!=entity.id() {
                                  // repel from each other  
                                  let r = entity.pos().sub(other_entity.pos());
                                  if r.mag()>0.0 {
                                      return acc.add(r.norm().mul(0.004))
                                  }
                              }
                              acc
                          }));
        
        delta = delta.add(self.connected_entities(entity.id()).iter()
                          .fold(Vec2::zero(),|acc,other_entity| -> Vec2 {
                              if other_entity.id()!=entity.id() {
                                  // try 
                                  let b = other_entity.pos().sub(entity.pos());
                                  let distance = b.mag()-DEFAULT_EDGE_LENGTH;
                                  if distance>0.0 {
                                      return acc.add(b.norm().mul((distance*distance)*0.001))
                                  }
                              }
                              acc
                          }));

        if delta.mag()>MAX_SPEED { delta = delta.norm().mul(MAX_SPEED); }
        
        SocialEntity {
            id: entity.id(),
            pos: entity.pos().add(delta),
            vel: delta,
            memory: entity.memory.clone(),
            state: entity.state,
            timer: entity.timer
        }
    }
*/
 /*   
    pub fn relax(&self) -> Self {
        return World {
            entities: self.entities.iter().map(|entity| {
                if entity.id()==PLAYER_ID {
                    entity.clone()
                } else {
                    self.relax_entity(entity)
                }
            }).collect::<Vec<SocialEntity>>(),
            relationships: self.relationships.clone(),
            centre: self.centre
        };                          
    }
     */
    /*
    /// add new connections to the player by sniffing the message list
    pub fn add_player_connections(&self,messages: &Vec<Message>, relationships: &Vec<Relationship>) -> Vec<Relationship> {
        let player_msgs: Vec<&Message> = messages.iter().filter(|msg| msg.sender==PLAYER_ID).collect();
        let mut new_relationships = vec![];
        
        // we don't actually need (currently) to look at any other messages
        // as just having one is all we need to trigger this (one day check type)
        if player_msgs.len()>0 {
            let nearby_entities = self.get_entities_nearby(
                PLAYER_ID,
                self.get_entity(PLAYER_ID).unwrap().pos(),
                PLAYER_OVERLAP);
            
            let connected_already:Vec<i32> = self.connected_entities(PLAYER_ID).iter()
                .map(|e| e.id()).collect();

            if nearby_entities.len()>0 {
                new_relationships = nearby_entities.iter()
                    // remove already existing connections
                    .filter(|e| !connected_already.contains(&e.id()))                
                    // create a relationship with every nearby entity
                    .map(|near| 
                        Relationship {
                            id: 999, // not actually used.. fixme
                            from: PLAYER_ID,
                            to: near.id(),
                            length: DEFAULT_EDGE_LENGTH
                        }
                    ).collect();                
            }

            let mut newrel = relationships.clone();
            newrel.extend(new_relationships);
            return newrel;
        }
        relationships.clone()
    }*/
    
    pub fn update(&mut self) -> Self {
        let player_pos = self.bucket_grid.get_entity(PLAYER_ID).expect("no player entity found").pos();
        World {
            // snoop on player speak and create new connections
            //relationships: self.add_player_connections(&messages,&self.relationships),
            bucket_grid: {
                let (grid, messages) = self.bucket_grid.update(self, player_pos);
                self.num_messages += messages.len() as u32;
                // todo: read and process messages for world here...
                grid.read_messages(self,&messages)
            },
            relationships: self.relationships.clone(),
            ..*self
        }              
    }

    pub fn render(&self, context: &web_sys::CanvasRenderingContext2d) {
        let player_pos = self.bucket_grid.get_entity(PLAYER_ID).expect("no player entity found").pos();
        // move the world around the player
        context.save();
        let centre = Vec2::new(500.0,250.0);
        context.translate(centre.x-player_pos.x,
                          centre.y-player_pos.y);        



        for rel in &self.relationships {

            let from = self.bucket_grid.get_entity(rel.from).expect("rel from not found");
            let to = self.bucket_grid.get_entity(rel.to).expect("rel to not found");
            
            context.begin_path();
            context.move_to(from.pos().x, from.pos().y);
            context.line_to(to.pos().x, to.pos().y);
            context.stroke();
        }

        // render view around the player only
        for addr in self.bucket_grid.get_surrounding(player_pos) {
            self.bucket_grid.grid[addr].render(context);
        }
        context.restore();
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::game::bucket::{Bucket};
    
    #[test]
    fn basics() {
        let mut n = World::new();
        n.bucket_grid.insert(Box::new(SocialEntity::at_pos(1.into(), Vec2::new(10.0,0.0))));
        n.bucket_grid.insert(Box::new(SocialEntity::at_pos(2.into(), Vec2::new(20.0,0.0))));                                           
                              
        assert_eq!(n.bucket_grid.get_entity(1.into()).unwrap().id(), 1.into());
        assert_eq!(n.bucket_grid.get_entity(2.into()).unwrap().id(), 2.into());
        n.relationships.push(Relationship::new(1,1,2));
        let conn =
            match n.bucket_grid.get_entity(1.into()) {
                Ok(entity) => n.connected_entities(entity.id()),
                Err(_) => vec![]
            };
        assert_eq!(conn.len(), 1);
        assert_eq!(conn[0].id(), 2.into());        
    }
}
