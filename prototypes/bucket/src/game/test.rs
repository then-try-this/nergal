/*use rand::Rng;
use vector2math::*;
use rand::prelude::SliceRandom;
use crate::game::vec2::{ Vec2 };


#[derive(Debug,Clone,PartialEq)]
pub enum MessageType {
    Broadcast
}

#[derive(Debug,Clone)]
pub struct Message {
    pub sender: i32,
    pub name: MessageType,
    pub value: String
}

use crate::game::relationship::{ Relationship };

/////////////////////////

pub const PLAYER_ID: i32 = 9999;

#[derive(Debug,Clone,Copy,PartialEq)]
pub enum EntityState {
    Idle,
    Speak,
    Spoken
}

pub trait Entity {
    fn update(&self) -> (Box<dyn Entity>,Vec<Message>);
    fn update_messages(&self,messages: &Vec<Message>, connected: Vec<i32>) -> Box<dyn Entity>;
}


////////////


pub struct Network {
    pub entities: Vec<Box<dyn Entity>>,
    pub relationships: Vec<Relationship>,
    pub centre: Vec2,
}


impl Network {
    pub fn new() -> Self {
        Network {
            entities: vec![],
            relationships: vec![],
            centre: Vec2::new(500.0,250.0)
        }
        
    }

    /// Randomised constructor for testing purposes
    pub fn rand() -> Self {
        let mut rng = rand::thread_rng();
        let centre = Vec2::new(500.0,250.0);
        let mut entities:Vec<Box<dyn Entity>> = vec![];


        entities.push(Box::new(SocialEntity::new(1)));

        for i in 0..20 {
            entities.push(Box::new(SocialEntity::at_pos(i,Vec2::new(rng.gen_range(-100.0..100.0),
                                                                    rng.gen_range(-100.0..100.0)).add(centre))));
        }

        let mut relationships = vec![];
        for i in 0..20 {
            relationships.push(Relationship{
                id: i,
                from: rng.gen_range(0..20),
                to: rng.gen_range(0..20),
                length: 99.0
            });
        }

        Network {
            entities: entities,
            relationships: relationships,
            centre
        }
        
    }

    pub fn update(&self) {
        for e in self.entities.iter() {
            e.update();
        }
    }
}

///////////////

pub struct SocialEntity {
    pub id: i32,
    pub pos: Vec2,
    pub vel: Vec2,
    pub memory: Vec<String>,
    pub state: EntityState,
    pub timer: i32,
}

impl SocialEntity {
    fn new(id: i32) -> Self {
        SocialEntity {
            id,
            pos: Vec2::zero(),
            vel: Vec2::zero(),
            memory: vec![],
            state: EntityState::Idle,
            timer: 0
        }   
    }
    pub fn at_pos(id: i32, pos: Vec2) -> Self {
        SocialEntity {
            id,
            pos,
            vel: Vec2::zero(),
            memory: vec![],
            state: EntityState::Idle,
            timer: 0
        }   
    }
}

impl Entity for SocialEntity {

    fn update(&self) -> (Box<dyn Entity>,Vec<Message>) {
        let mut rng = rand::thread_rng();
        let timer = if self.timer>0 { self.timer-1 } else { self.timer };
        let memory = self.memory.clone();
        let messages: Vec<Message> = vec![];
        
        match self.state {
            EntityState::Idle => {
                if self.id!=PLAYER_ID && self.memory.len()>0 && rng.gen_range(0..400)==1 && memory.len()>0 {
                    return (Box::new(SocialEntity {
                        state: EntityState::Speak,
                        memory: memory.clone(),
                        ..*self
                    }), vec![])
                } 
            },
            EntityState::Speak => {
                return (Box::new(SocialEntity {
                    state: EntityState::Spoken,
                    timer: 100,
                    memory: memory.clone(),
                    ..*self
                }), vec![Message{
                    sender: self.id,
                    name: MessageType::Broadcast,
                    value: memory.choose(&mut rand::thread_rng()).unwrap().clone(),
                }]);
            },
            EntityState::Spoken => {
                return (Box::new(SocialEntity {                    
                    state: if self.timer==0 { EntityState::Idle } else { self.state },                    
                    timer,
                    memory,
                    ..*self
                }), messages)
            },
        }
        return (Box::new(SocialEntity {
            timer,
            memory,
            ..*self
        }), messages)
    }

    fn update_messages(&self, messages: &Vec<Message>, connected: Vec<i32>) -> Box<dyn Entity> {
        // search messages from ids connected to us
        let messages: Vec<&Message> = messages.iter().filter(|msg| {
            for connection in &connected {
                if connection == &msg.sender { return true }
            }
            false
        }).collect();

        Box::new(SocialEntity {
            memory: messages.iter().fold(self.memory.clone(),|mut acc, msg| {
                if msg.name == MessageType::Broadcast {
                    if !self.memory.contains(&msg.value) {
                        acc.push(msg.value.clone());
                    }
                    return acc;
                }             
                acc
            }),                        
            ..*self
        })
    }
}

//////////////////////

struct PlayerEntity {
    width: u32,
    height: u32,
    options: Vec<String>,
}

impl Entity for PlayerEntity {
    fn update(&self) -> (Box<dyn Entity>,Vec<Message>) {
        println!("I'm a player entity");
        return (Box::new(PlayerEntity{ width:1, height:2, options: vec![]}),vec![]);
    }
    fn update_messages(&self,messages: &Vec<Message>, connected: Vec<i32>) -> Box<dyn Entity> {    
        println!("I'm a player entity message update");
        return Box::new(PlayerEntity{ width:1, height:2, options: vec![]});
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn basics() {

        let mut net = Network {
            entities: vec![],
        };
        
        net.entities.push(Box::new(SocialEntity::new(1)));

        net.entities.push(
            Box::new(PlayerEntity {
                width: 75,
                height: 10,
                options: vec![
                    String::from("Yes"),
                    String::from("Maybe"),
                    String::from("No"),
                ],
            }),
        );

        net.entities.push(Box::new(SocialEntity::new(2)));

        net.update();

        
        
    }
}
*/
