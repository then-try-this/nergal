# Nergal

A free and open source online game to understand how people's social
networks and decision making change how diseases spread. This will
gather data to contribute towards research, but should also be fun and
interesting to play in its own right.

![](https://gitlab.com/then-try-this/nergal/-/raw/development/artwork/background_bits.png)

Made with Matthew Silk and Nitara Wijayatilake at the Institute of
Ecology and Evolution at the University of Edinburgh. Funded by the
Royal Society.
     
Built with rust and running in browser via web assembly. See the
[code documentation](https://then-try-this.gitlab.io/nergal/nergal/) for
more info.
    
[More info here](https://thentrythis.org/projects/citizen-science-for-epidemiology/)

Features networks provided by the [Animal Social Network Repository](https://github.com/bansallab/asnr/)
    
## About

- Build: `wasm-pack build`
- Testing: `cargo test`
- Testing (wasm): `wasm-pack test --headless --firefox`
- Static packaging with webpack (inside www): `npm run build`
    
## Features

- Entity traits for composing behaviours
- A 'bucket' & 'bucket_grid' spatial hashing entity container for large worlds
- HTML5 canvas rendering system
- Global messaging between entities

## Notes

Building spritesheets:

    // montage *.png -mode concatenate -background none sheet.png

    // mogrify -resize 700x1200 -background none -gravity south -extent 700x1200 *.png

Batch converting wav to mp3:

    // for f in *.wav; do ffmpeg -i "$f" -c:a libmp3lame -q:a 2 "${f/%wav/mp3}"; done


    