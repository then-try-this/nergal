# Nergal Copyright (C) 2025 Then Try This
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.

import sqlite3
import collections

con = sqlite3.connect("20250306_db.sqlite3")
cur = con.cursor()

def get_players(cur): 
    res = cur.execute("select id from players")
    # flatten the results into an array
    return [v[0] for v in res.fetchall()]

def get_games_for_player(cur, player_id):
    # use the InitGame events to find games
    q = f"""select game_id from events
            where player_id={player_id}
            and ingame_msg like 'InitGame' || '%'
            and datetime(time) >= '2025-01-20 14:11:00'"""
    return [v[0] for v in cur.execute(q).fetchall()]

def get_events_for_player(cur, player_id, event_type):
    q = f"""select msg, time from events
            where player_id={player_id}
            and ingame_msg like '{event_type}' || '%'"""
    res = cur.execute(q)
    return res.fetchall()

def get_events_for_game(cur, game_id, event_type):
    q = f"""select msg, time from events
            where game_id={game_id}
            and ingame_msg like '{event_type}' || '%'"""
    res = cur.execute(q)
    return res.fetchall()

def get_events_for_game2(cur, game_id, event_type, second):
    q = f"""select msg, time from events
            where game_id={game_id}
            and ingame_msg like '{event_type}' || '%'
            and ingame_msg like '%' || '{second}' || '%'"""
    res = cur.execute(q)
    return res.fetchall()

def get_event_time_for_game(cur, game_id, event_type):
    q = f"""select time from events
            where game_id={game_id}
            and ingame_msg like '{event_type}' || '%'"""
    res = cur.execute(q)
    return res.fetchone()[0]

def get_healthchange_for_game(cur, game_id, reason):
    q = f"""select msg, time from events
            where game_id={game_id}
            and msg like '%' || '{reason}' || '%'"""
    res = cur.execute(q)
    return res.fetchall()

def get_game_value(cur, game_id, col):    
    q = f"select {col} from games where id={game_id}"
    r = cur.execute(q).fetchone()
    if r:
        return r[0]
    else:
        return 0

def short_network(v):
    if v!=0:
        if "dolphin" in v: return "dolphin"
        if "elephant" in v: return "elephant"
    return "badger"

def deep_player_info(cur):
    for player_id in get_players(cur):
        games = get_games_for_player(cur, player_id)
        if len(games)>0:
            print(f"Player: {player_id}")
            for game_id in games:
                network = short_network(get_game_value(cur, game_id, "network"))
                chats = len(get_events_for_game(cur, game_id, "TalkTo"))
                friends_made = len(get_events_for_game(cur, game_id, "AddRelationship"))
                friends_lost = len(get_events_for_game(cur, game_id, "RemoveRelationship"))
                nergal_chats = len(get_events_for_game2(cur, game_id, "TalkTo", "nergal"))
                lifetime = get_game_value(cur, game_id, "lifetime")
                time = get_event_time_for_game(cur, game_id, "InitGame")
                theme = get_game_value(cur, game_id, "theme")
                snacks = len(get_healthchange_for_game(cur, game_id, "Snack"))
                print(f"    {time}/{theme}/{network} game included ({nergal_chats}/{chats}) nergal chats and lasted {lifetime} seconds and had {snacks} snacks. {friends_made}/{friends_lost} friends made/lost.")
                
def game_stats(cur):
    total_lifetime = 0
    av_snacks = 0
    av_chats = 0
    av_chats_plants = 0
    av_chats_clouds = 0
    av_friends_made = 0
    av_games = 0
    game_count = 0
    player_count = 0
    win_count = 0
    players_with_snacks = 0
    players_who_chat = 0
    players_who_chat_plants = 0
    players_who_chat_clouds = 0
    players_who_chat_total = 0
    players_who_only_chat_non_nergals = 0
    for player_id in get_players(cur):
        games = get_games_for_player(cur, player_id)
        if len(games)>0:
            av_games += len(games)
            win = False
            snack = False
            chat = False
            chat_plants = False
            chat_clouds = False
            chat_total = False
            for game_id in games:
                av_friends_made += get_game_value(cur, game_id, "num_friends_made")
                lifetime = get_game_value(cur, game_id, "lifetime")
                if lifetime > 299: win = True
                total_lifetime += lifetime
                snacks = get_game_value(cur, game_id, "snacks_eaten")
                if snacks > 1: snack = True
                av_snacks += snacks
                chats = get_game_value(cur, game_id, "nergals_talked_to")
                if chats > 1: chat = True
                av_chats += chats
                chats = get_game_value(cur, game_id, "nergals_talked_to")
                if chats > 1: chat = True
                av_chats += chats
                t = get_game_value(cur, game_id, "plants_talked_to")
                if t > 1: chat_plants = True
                av_chats_plants += t
                t = get_game_value(cur, game_id, "clouds_talked_to")
                if t > 1: chat_clouds = True
                av_chats_clouds += t
                game_count += 1
            player_count += 1
            if win: win_count += 1
            if snack: players_with_snacks += 1
            if chat: players_who_chat += 1
            if chat_plants: players_who_chat_plants += 1
            if chat_clouds: players_who_chat_clouds += 1
            if chat or chat_plants or chat_clouds: players_who_chat_total += 1
            if not chat and (chat_plants or chat_clouds): players_who_only_chat_non_nergals += 1
    av_lifetime = total_lifetime / game_count
    av_snacks /= player_count
    av_chats /= player_count
    av_chats_plants /= player_count
    av_chats_clouds /= player_count
    av_games /= player_count
    av_friends_made /= player_count
    
    lifetime_hours = total_lifetime / (60*60)
    lifetime_minutes = total_lifetime % (60*60)

    win_prop = (win_count / player_count) * 100
    snack_prop = (players_with_snacks / player_count) * 100
    chat_prop = (players_who_chat / player_count) * 100
    chat_plants_prop = (players_who_chat_plants / player_count) * 100
    chat_clouds_prop = (players_who_chat_clouds / player_count) * 100
    chat_total_prop = (players_who_chat_total / player_count) * 100
    chat_non_nergals_prop = (players_who_only_chat_non_nergals / player_count) * 100
        
    for item in [f"total players = {player_count}",
                 f"total games = {game_count}",
                 f"total game time = {lifetime_hours:.2f} hours",
                 f"av games per player = {av_games:.2f}",
                 f"av snacks per player = {av_snacks:.2f}",
                 f"av friends made per player = {av_friends_made:.2f}",
                 f"av chats with nergals per player = {av_chats:.2f}",
                 f"av chats with plants per player = {av_chats_plants:.2f}",
                 f"av chats with clouds per player = {av_chats_clouds:.2f}",
                 f"av lifetime per game = {av_lifetime:.2f}",                 
                 f"proportion of players who chat = {chat_total_prop:.2f}%",
                 f"chat breakdown = nergals: {chat_prop:.2f}%, plants: {chat_plants_prop:.2f}%, clouds: {chat_clouds_prop:.2f}%",
                 f"chat non nergals only = {chat_non_nergals_prop:.2f}%",
                 f"proportion of players with snack = {snack_prop:.2f}%",
                 f"proportion of players who survived = {win_prop:.2f}%"]:
        print(item)

def player_info(cur):
    hist = {}
    for player_id in get_players(cur):
        info_events = get_events_for_player(cur, player_id, "PlayerInfo")
        for event in info_events:
            info_record = event[0].split()[1]
            if info_record not in hist:
                hist[info_record] = 1
            else:
                hist[info_record] += 1
    od = collections.OrderedDict(sorted(hist.items()))
    print(od)
    for k,v in od.items():        
        print(k+": "+str(v))

#game_stats(cur)
#deep_player_info(cur)
player_info(cur)
