// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use rocket::serde::Deserialize;

#[derive(Deserialize, Debug)]
#[serde(crate = "rocket::serde")]
pub struct PlayerRequest {
    pub name: Option<String>,
    pub last_played_date: Option<String>,
}

#[derive(Deserialize, Debug)]
#[serde(crate = "rocket::serde")]
pub struct EventRequest {
    pub player_id: i64,
    pub player_pos_x: f64,
    pub player_pos_y: f64,
    pub game_id: i64,
    pub sender: String,
    pub recipient: String,
    pub msg: String,
    pub ingame_sender_id: i64,
    pub ingame_recipient_id: i64,
    pub ingame_msg: String,
}

#[derive(Deserialize, Debug)]
#[serde(crate = "rocket::serde")]
pub struct GameRequest {
    pub id: i64,
    pub times_ill: f64,
    pub nergals_infected: f64,
    pub nergals_talked_to: f64,
    pub num_friends_made: f64,
    pub num_friends_at_end: f64,
    pub lifetime: f64,
    pub snacks_eaten: f64,
    pub plants_talked_to: f64,
    pub clouds_talked_to: f64,
    pub music_made: f64,
    pub theme: String,
    pub network: String
}

