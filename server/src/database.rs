// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use sqlx::{Pool, Sqlite, Row};

pub mod requests;
pub mod responses;

use rocket::serde::json::Json;
use requests::*;

// todo:
// responses per player
// played_before
// character type
// (game events)
// (game network before after)


use responses::*;

pub type DBResult<T, E = rocket::response::Debug<sqlx::Error>> = std::result::Result<T, E>;

//// players

pub async fn create_player(pool: &Pool<Sqlite>) -> DBResult<i64> {
    let mut connection = pool.acquire().await?;
    let id = sqlx::query_as!(Player, r#"INSERT INTO players(id) VALUES (null);"#)
        .execute(&mut connection)
        .await?
        .last_insert_rowid();    
    Ok(id)
}

pub async fn get_player(pool: &Pool<Sqlite>, id: i64) -> DBResult<Player> {
    let mut connection = pool.acquire().await?;
    let player = sqlx::query_as!(Player, r#"SELECT id from players WHERE id = ?;"#, id)
        .fetch_one(&mut connection)
        .await?;
    Ok(player)
}

pub async fn get_players(pool: &Pool<Sqlite>) -> DBResult<Vec<Player>> {
    let mut connection = pool.acquire().await.unwrap();
    let players = sqlx::query_as::<_, Player>(r#"select id from players;"#)
        .fetch_all(&mut connection)
        .await?;
        Ok(players)
}

//// games

// create game with empty scores
pub async fn create_game(pool: &Pool<Sqlite>, player_id: &i64) -> DBResult<i64> {
    let mut connection = pool.acquire().await?;
    let id = sqlx::query_as!(
        Player,
        r#"
        INSERT INTO games (player_id, times_ill, nergals_infected, nergals_talked_to, num_friends_made, num_friends_at_end, lifetime, snacks_eaten, plants_talked_to, clouds_talked_to, music_made, theme, network) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
        "#,
        player_id, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "")
        .execute(&mut connection)
        .await?
        .last_insert_rowid();
    Ok(id)
}

pub async fn update_game(pool: &Pool<Sqlite>, game: Json<GameRequest>) -> DBResult<i64> {
    let mut connection = pool.acquire().await?;
    let id = sqlx::query_as!(
        Player,
        r#"
        update games set (times_ill, nergals_infected, nergals_talked_to, num_friends_made, num_friends_at_end, lifetime, snacks_eaten, plants_talked_to, clouds_talked_to, music_made, theme, network) = (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) where id = ?;
        "#,
        game.times_ill,
        game.nergals_infected,
        game.nergals_talked_to,
        game.num_friends_made,
        game.num_friends_at_end,
        game.lifetime,
        game.snacks_eaten,
        game.plants_talked_to,
        game.clouds_talked_to,
        game.music_made,
        game.theme,
        game.network,
        game.id)
        .execute(&mut connection)
        .await?
        .last_insert_rowid();
    Ok(id)
}

pub async fn get_games(pool: &Pool<Sqlite>) -> DBResult<Vec<Game>> {
    let mut connection = pool.acquire().await.unwrap();
    let games = sqlx::query_as::<_, Game>(r#"select * from games;"#)
        .fetch_all(&mut connection)
        .await?;
    Ok(games)
}

pub async fn get_games_for_player(pool: &Pool<Sqlite>, player_id: i64) -> DBResult<Vec<Game>> {
    let mut connection = pool.acquire()
        .await
        .unwrap();

    let games = sqlx::query_as!(
        Game,
        r#"select * from games where player_id = ?;"#,
        player_id
    )
        .fetch_all(&mut connection)
        .await?;
    Ok(games)
}


pub async fn get_averages(pool: &Pool<Sqlite>) -> DBResult<Averages> {
    let mut connection = pool.acquire().await.unwrap();
    let games = sqlx::query_as::<_, Averages>(r#"select
        avg(times_ill) as times_ill,
        avg(nergals_infected) as nergals_infected,
        avg(nergals_talked_to) as nergals_talked_to,
        avg(num_friends_made) as num_friends_made,
        avg(num_friends_at_end) as num_friends_at_end,
        avg(lifetime) as lifetime,
        avg(snacks_eaten) as snacks_eaten,
        avg(plants_talked_to) as plants_talked_to,
        avg(clouds_talked_to) as clouds_talked_to,
        avg(music_made) as music_made
        from games where lifetime > 0;"#)
        .fetch_one(&mut connection)
        .await?;
    Ok(games)
}
                                                                                 
pub async fn get_averages_for_network(pool: &Pool<Sqlite>, network: &str) -> DBResult<Averages> {
    let mut connection = pool.acquire().await.unwrap();

    let results = sqlx::query(
        r#"select
        avg(times_ill) as times_ill,
        avg(nergals_infected) as nergals_infected,
        avg(nergals_talked_to) as nergals_talked_to,
        avg(num_friends_made) as num_friends_made,
        avg(num_friends_at_end) as num_friends_at_end,
        avg(lifetime) as lifetime,
        avg(snacks_eaten) as snacks_eaten,
        avg(plants_talked_to) as plants_talked_to,
        avg(clouds_talked_to) as clouds_talked_to,
        avg(music_made) as music_made
        from games where lifetime > 0 and network like '%' || ? || '%';"#
    )
        .bind(network)
        .fetch_one(&mut connection)
        .await?;    

    Ok(Averages {
        times_ill: results.get::<f64,usize>(0),
        nergals_infected: results.get::<f64,usize>(1),
        nergals_talked_to: results.get::<f64,usize>(2),
        num_friends_made: results.get::<f64,usize>(3),
        num_friends_at_end: results.get::<f64,usize>(4),
        lifetime: results.get::<f64,usize>(5),
        snacks_eaten: results.get::<f64,usize>(6),
        plants_talked_to: results.get::<f64,usize>(7),
        clouds_talked_to: results.get::<f64,usize>(8),
        music_made: results.get::<f64,usize>(9),
    })
}

//// events

pub async fn create_event(pool: &Pool<Sqlite>, event: &Json<EventRequest>) -> DBResult<i64> {
    let mut connection = pool
        .acquire()
        .await?;

    if event.msg == "PlayerInfo age_under_18" {
        sqlx::query(
            r#"delete from events where player_id = ?"#
        )
            .bind(event.player_id)
            .execute(&mut connection)
            .await?;            

        Ok(0)
    } else {    
        let id = sqlx::query_as!(
            Player,
            r#"
        INSERT INTO events (player_id, player_pos_x, player_pos_y, game_id, sender, recipient, msg, ingame_sender_id, ingame_recipient_id, ingame_msg, time) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, datetime());
        "#,
            event.player_id,
            event.player_pos_x,
            event.player_pos_y,
            event.game_id,
            event.sender,
            event.recipient,
            event.msg,
            event.ingame_sender_id,
            event.ingame_recipient_id,
            event.ingame_msg
        )
            .execute(&mut connection)
            .await?
            .last_insert_rowid();

        Ok(id)
    } 
}

pub async fn get_events(pool: &Pool<Sqlite>) -> DBResult<Vec<Event>> {
    let mut connection = pool.acquire().await.unwrap();
    let events = sqlx::query_as::<_, Event>(r#"select * from events;"#)
        .fetch_all(&mut connection)
        .await?;
        Ok(events)
}


pub async fn get_events_for_player(pool: &Pool<Sqlite>, player_id: i64) -> DBResult<Vec<Event>> {
    let mut connection = pool.acquire()
        .await
        .unwrap();

    let events = sqlx::query_as!(
        Event,
        r#"select * from events where player_id = ?;"#,
        player_id
    )
        .fetch_all(&mut connection)
        .await?;
    Ok(events)
}

pub async fn get_events_for_game(pool: &Pool<Sqlite>, game_id: i64) -> DBResult<Vec<Event>> {
    let mut connection = pool.acquire()
        .await
        .unwrap();

    let events = sqlx::query_as!(
        Event,
        r#"select * from events where game_id = ?;"#,
        game_id
    )
        .fetch_all(&mut connection)
        .await?;
    Ok(events)
}

