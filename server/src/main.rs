// Nergal Copyright (C) 2024 Then Try This
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#[macro_use]
extern crate rocket;
mod database;

use database::requests::*;
use database::responses::*;
use database::*;
use rocket::serde::json::Json;
use rocket::State;
use sqlx::{Pool, Sqlite, SqlitePool};
use dotenvy::dotenv;

use rocket::http::Method;
use rocket_cors::{AllowedOrigins, CorsOptions};
use rocket::fs::FileServer;


/////////////////////////////////////////////////////////////

#[post("/register_player")]
async fn register_player(pool: &State<Pool<Sqlite>>) -> DBResult<Json<i64>> {
    let id = create_player(pool).await?;
    Ok(Json(id))
}

#[get("/players")]
async fn players(pool: &State<Pool<Sqlite>>) -> DBResult<Json<Vec<Player>>> {
    let players = get_players(pool).await?;
    Ok(Json(players))
}

#[get("/players/<id>")]
async fn player(id: i64, pool: &State<Pool<Sqlite>>) -> DBResult<Json<Player>> {
    let player = get_player(pool, id).await?;
    Ok(Json(player))
}

/////////////////////////////////////////////////////////////

#[post("/register_game/<player_id>")]
async fn register_game(player_id: i64, pool: &State<Pool<Sqlite>>) -> DBResult<Json<i64>> {
    let id = create_game(pool, &player_id).await?;
    Ok(Json(id))
}

#[post("/update_game", format = "json", data = "<game>")]
async fn update_game(game: Json<GameRequest>, pool: &State<Pool<Sqlite>>) -> DBResult<Json<u32>> {
    database::update_game(pool, game).await?;
    Ok(Json(1))
}

#[get("/games")]
async fn games(pool: &State<Pool<Sqlite>>) -> DBResult<Json<Vec<Game>>> {
    let games = get_games(pool).await?;
    Ok(Json(games))
}

#[get("/games_for_player/<id>")]
async fn games_for_player(id: i64, pool: &State<Pool<Sqlite>>) -> DBResult<Json<Vec<Game>>> {
    let games = get_games_for_player(pool,id).await?;
    Ok(Json(games))
}

#[get("/averages")]
async fn averages(pool: &State<Pool<Sqlite>>) -> DBResult<Json<Averages>> {
    let games = get_averages(pool).await?;
    Ok(Json(games))
}

#[get("/network_averages/<network>")]
async fn network_averages(pool: &State<Pool<Sqlite>>, network: String) -> DBResult<Json<Averages>> {
    let games = get_averages_for_network(pool, &network).await?;
    Ok(Json(games))
}

////////////////////////////////////////////////////////////////

#[post("/create_event", format = "json", data = "<event>")]
async fn create_event(event: Json<EventRequest>, pool: &State<Pool<Sqlite>>) -> DBResult<Json<u32>> {
    database::create_event(pool, &event).await?;
    Ok(Json(1))
}

#[get("/events")]
async fn events(pool: &State<Pool<Sqlite>>) -> DBResult<Json<Vec<Event>>> {
    let events = get_events(pool).await?;
    Ok(Json(events))
}

#[get("/events_for_player/<id>")]
async fn events_for_player(id: i64, pool: &State<Pool<Sqlite>>) -> DBResult<Json<Vec<Event>>> {
    let events = get_events_for_player(pool, id).await?;
    Ok(Json(events))
}

#[get("/events_for_game/<id>")]
async fn events_for_game(id: i64, pool: &State<Pool<Sqlite>>) -> DBResult<Json<Vec<Event>>> {
    let events = get_events_for_game(pool, id).await?;
    Ok(Json(events))
}


#[rocket::main]
async fn main() -> Result<(), rocket::Error> {
    dotenv().ok(); 

    let pool = SqlitePool::connect(&std::env::var("DATABASE_URL").expect("DATABASE_URL must be set."))
        .await
        .expect("Couldn't connect to sqlite database");

    sqlx::migrate!()
        .run(&pool)
        .await
        .expect("Couldn't migrate the database tables");

    // todo: remove cors for release
/*    let cors = CorsOptions::default()
        .allowed_origins(AllowedOrigins::all())
        .allowed_methods(
            vec![Method::Get, Method::Post, Method::Patch]
                .into_iter()
                .map(From::from)
                .collect(),
        )
        .allow_credentials(true);
*/
    let _rocket = rocket::build()
//       .attach(cors.to_cors().unwrap())
        .mount("/", routes![
            register_player,
            register_game,
            update_game,
            create_event,
            averages,
            network_averages,
//            players,
//            games,
//            games_for_player,
//            events,
//            events_for_player,
//            events_for_game,
        ])
        .mount("/", FileServer::from("game"))
        .manage(pool)
        .launch()
        .await?;
    Ok(())
}
