// @generated automatically by Diesel CLI.

diesel::table! {
    _sqlx_migrations (version) {
        version -> Nullable<BigInt>,
        description -> Text,
        installed_on -> Timestamp,
        success -> Bool,
        checksum -> Binary,
        execution_time -> BigInt,
    }
}

diesel::table! {
    events (id) {
        id -> Integer,
        player_id -> Integer,
        player_pos_x -> Float,
        player_pos_y -> Float,
        game_id -> Integer,
        sender -> Text,
        recipient -> Text,
        msg -> Text,
        time -> Text,
        ingame_sender_id -> Integer,
        ingame_recipient_id -> Integer,
        ingame_msg -> Text,
    }
}

diesel::table! {
    games (id) {
        id -> Integer,
        player_id -> Integer,
        times_ill -> Float,
        nergals_infected -> Float,
        nergals_talked_to -> Float,
        num_friends_made -> Float,
        num_friends_at_end -> Float,
        lifetime -> Float,
        snacks_eaten -> Float,
        plants_talked_to -> Float,
        clouds_talked_to -> Float,
        music_made -> Float,
        theme -> Text,
        network -> Text,
    }
}

diesel::table! {
    players (id) {
        id -> Integer,
    }
}

diesel::allow_tables_to_appear_in_same_query!(
    _sqlx_migrations,
    events,
    games,
    players,
);
